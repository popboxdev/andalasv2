from database import ClientDatabase
__author__ = 'popbox@popbox.asia'


def get_mouth(msg):
    sql = 'SELECT * FROM Mouth WHERE express_id = :id'
    return ClientDatabase.get_result_set(sql, msg)[0]


def init_box(msg):
    sql = 'INSERT INTO Box (id, deleteFlag, name, orderNo, operator_id,validateType,syncFlag,currencyUnit,overdueType,freeDays,freeHours,receiptNo) VALUES (:id,0,:name,:orderNo,:operator_id,:validateType,:syncFlag,:currencyUnit,:overdueType,:freeDays,:freeHours,:receiptNo)'
    ClientDatabase.insert_or_update_database(sql, msg)


def update_box(msg):
    sql = 'UPDATE Box SET name=:name, orderNo=:orderNo,validateType=:validateType,currencyUnit=:currencyUnit,overdueType=:overdueType,freeDays=:freeDays,freeHours=:freeHours WHERE id=:id'
    ClientDatabase.insert_or_update_database(sql, msg)

def init_cabinet(cabinet):
    sql = 'INSERT INTO Cabinet(id, deleteFlag, number)VALUES (:id,:deleteFlag,:number)'
    ClientDatabase.insert_or_update_database(sql, cabinet)


def init_mouth(mouth):
    sql = 'INSERT INTO Mouth(id, deleteFlag, number,usePrice ,overduePrice, status, box_id, cabinet_id, express_id, mouthType_id,numberInCabinet,syncFlag,openOrder)VALUES (:id,:deleteFlag,:number,:usePrice,:overduePrice,:status,:box_id,:cabinet_id,:express_id,:mouthType_id,:numberInCabinet,:syncFlag,:openOrder)'
    ClientDatabase.insert_or_update_database(sql, mouth)


def init_mouth_type(mouth_type):
    sql = 'SELECT * FROM MouthType WHERE id=:id'
    result_set = ClientDatabase.get_result_set(sql, mouth_type)
    if len(result_set) != 0:
        return
    sql = 'INSERT INTO MouthType (id, defaultUsePrice,defaultOverduePrice, name,deleteFlag)VALUES (:id,:defaultUsePrice,:defaultOverduePrice,:name,:deleteFlag)'
    ClientDatabase.insert_or_update_database(sql, mouth_type)


def free_mouth(mouth):
    sql = "UPDATE mouth SET express_id = NULL , status = 'ENABLE' WHERE id = :id"
    ClientDatabase.insert_or_update_database(sql, mouth)


def get_mouth_type(mouth_type_param):
    sql = 'SELECT * FROM MouthType WHERE name = :name AND deleteFlag = 0'
    return ClientDatabase.get_result_set(sql, mouth_type_param)


def get_free_mouth_by_type(mouth_param):
    sql = 'SELECT * FROM Mouth WHERE mouthType_id=:mouthType_id AND deleteFlag = :deleteFlag AND status = :status'
    return ClientDatabase.get_result_set(sql, mouth_param)


def get_box_by_order_no(order_no):
    sql = 'SELECT * FROM Box WHERE orderNo = :orderNo AND deleteFlag = :deleteFlag'
    return ClientDatabase.get_result_set(sql, order_no)


def get_box_by_box_id(box_id):
    sql = 'SELECT * FROM Box WHERE id = :id AND deleteFlag = :deleteFlag'
    return ClientDatabase.get_result_set(sql, box_id)


def use_mouth(mouth_param__):
    sql = 'UPDATE mouth SET express_id=:express_id ,Status=:status WHERE id = :id'
    ClientDatabase.insert_or_update_database(sql, mouth_param__)


def get_mouth_by_id(mouth_param):
    sql = 'SELECT Mouth.*, MouthType.name FROM Mouth INNER JOIN MouthType on Mouth.mouthType_id = MouthType.id WHERE Mouth.id = :id'
    return ClientDatabase.get_result_set(sql, mouth_param)


def get_cabinet_by_id(param):
    sql = 'SELECT * FROM Cabinet WHERE id=:id'
    return ClientDatabase.get_result_set(sql, param)


def get_free_mouth_count_by_mouth_type_name(param):
    sql = 'SELECT count(1) AS count FROM Mouth  INNER JOIN MouthType ON Mouth.mouthType_id = MouthType.id AND Mouth.status = :status AND MouthType.name = :name'
    return ClientDatabase.get_result_set(sql, param)


def update_free_time(param):
    sql = 'UPDATE Box SET freeHours=:freeHours,freeDays=:freeDays,overdueType=:overdueType WHERE id=:id'
    ClientDatabase.insert_or_update_database(sql, param)


def update_mouth_status(param):
    sql = 'UPDATE mouth SET status=:status WHERE id=:id '
    ClientDatabase.insert_or_update_database(sql, param)


def get_mouth_list(param):
    sql = 'SELECT mouth.id, mouth.deleteFlag, Mouth.number, Mouth.syncFlag, Mouth.status, MouthType.name FROM Mouth INNER JOIN MouthType ON Mouth.mouthType_id = MouthType.id AND Mouth.deleteFlag = :deleteFlag ORDER BY Mouth.number LIMIT :startLine,25 '
    return ClientDatabase.get_result_set(sql, param)


def get_all_mouth(param):
    sql = 'SELECT * FROM Mouth WHERE deleteFlag=:deleteFlag ORDER BY Mouth.number'
    return ClientDatabase.get_result_set(sql, param)


def get_empty_mouth(param):
    sql = 'SELECT * FROM Mouth WHERE deleteFlag=:deleteFlag and status=:status ORDER BY Mouth.number'
    return ClientDatabase.get_result_set(sql, param)


def get_all_mouth_count(param):
    sql = 'SELECT count(1) AS count FROM Mouth WHERE deleteFlag = :deleteFlag'
    return ClientDatabase.get_result_set(sql, param)


def manage_set_mouth(param):
    sql = 'UPDATE mouth SET status=:status, syncFlag=:syncFlag WHERE id=:id '
    return ClientDatabase.insert_or_update_database(sql, param)


def mark_sync_success(param):
    sql = 'UPDATE mouth SET syncFlag = 1 WHERE id = :id'
    ClientDatabase.insert_or_update_database(sql, param)


def mark_box_sync_success(param):
    sql = 'UPDATE Box SET syncFlag = 1'
    ClientDatabase.insert_or_update_database(sql, param)


def get_all_mouth_type(param):
    sql = 'SELECT * FROM MouthType WHERE deleteFlag = :deleteFlag'
    return ClientDatabase.get_result_set(sql, param)


def get_count_by_status_and_mouth_type_id(param):
    sql = 'SELECT count(1) AS mouth_count FROM Mouth WHERE status=:status AND mouthType_id=:mouthType_id AND deleteFlag=:deleteFlag'
    return ClientDatabase.get_result_set(sql, param)


def get_not_sync_mouth_list(param):
    sql = 'select * from Mouth WHERE deleteFlag=0 and syncFlag=:syncFlag'
    return ClientDatabase.get_result_set(sql, param)


def get_free_mouth_by_id(param):
    sql = 'SELECT * FROM Mouth WHERE deleteFlag=0 AND id=:id AND status =:status'
    return ClientDatabase.get_result_set(sql, param)

def get_detail_mouth_by_id(param):
    sql = 'SELECT a.number, b.name FROM Mouth a, MouthType b WHERE a.mouthType_id=b.id AND a.id=:id'
    return ClientDatabase.get_result_set(sql, param)

def get_rules(param):
    sql = 'SELECT regularContent, groupName FROM ReturnRules WHERE deleteFlag=:deleteFlag'
    return ClientDatabase.get_result_set(sql, param)

def get_mouth_type_name(mouth_type_param):
    sql = 'SELECT * FROM MouthType WHERE id = :id AND deleteFlag = 0'
    return ClientDatabase.get_result_set(sql, mouth_type_param)[0]

def update_price_for_poptitip(price_param):
    sql = 'UPDATE MouthType SET defaultOverduePrice=:defaultOverduePrice WHERE id=:id'
    return ClientDatabase.insert_or_update_database(sql, price_param)

def get_list_size(param):
    sql = 'SELECT * FROM MouthType WHERE deleteFlag=:deleteFlag'
    return ClientDatabase.get_result_set(sql, param)

def get_box_info():
    sql = 'SELECT * FROM Box'
    return ClientDatabase.get_result_void(sql)

def get_box_counter(param):
    sql = 'SELECT * FROM Box'
    return ClientDatabase.get_result_telegram(sql, param)

def check_table_info(param):
    sql = 'SELECT name FROM sqlite_master WHERE type =:table AND name=:table_name'
    return ClientDatabase.check_table(sql, param)

def update_box_to_used(param):
    sql = 'UPDATE Mouth SET status=:status, express_id=:express_id, syncFlag=:syncFlag WHERE id=:id'
    return ClientDatabase.insert_or_update_database(sql, param)

def update_box_to_available(msg):
    sql = 'UPDATE Mouth SET status=:status, express_id = NULL, syncFlag=:syncFlag WHERE id=:id'
    return ClientDatabase.insert_or_update_database(sql, msg)

def get_data_by_express_id_mouth_id(param):
    sql = 'SELECT * FROM Mouth WHERE id=:mouth_id AND express_id=:express_id'
    return ClientDatabase.get_result_set(sql, param)

def check_mouth_for_open_again(param):
    sql = 'SELECT * FROM Mouth WHERE id=:mouth_id AND status=:status'
    return ClientDatabase.get_result_set(sql, param)

def check_otp(param):
    sql = 'SELECT * FROM UserOtp WHERE phone_number=:phone_number AND deleteFlag = 0'    
    return ClientDatabase.get_result_set(sql, param)

def insert_otp(param):
    sql = 'INSERT INTO UserOtp (phone_number, valid_date, used_times) VALUES (:phone_number, :valid_date, 1)'
    return ClientDatabase.insert_or_update_database(sql, param)

def update_otp(param):
    sql = 'UPDATE UserOtp SET valid_date=:valid_date, used_times = used_times + 1 WHERE phone_number=:phone_number'
    return ClientDatabase.insert_or_update_database(sql, param)
