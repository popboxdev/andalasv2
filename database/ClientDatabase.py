import logging
import sys
import threading
import Configurator
__author__ = 'gaoyang'
import sqlite3
lock = threading.Lock()
_LOG_ = logging.getLogger()

if 'pr0x' not in Configurator.get_value('ClientInfo', 'serveraddress'):
    localDB = 'pakpobox.db'
else:
    try:
        localDB = Configurator.get_value('ClientInfo', 'dbName')
    except ValueError:
        localDB = 'popboxclient.db'


def dict_factory(cursor, row):
    d = {}
    for idx, col in enumerate(cursor.description):
        d[col[0]] = row[idx]
    return dict(((k, v) for k, v in d.items() if v is not None))


def get_conn():
    conn = sqlite3.connect(sys.path[0] + '/database/' + localDB)
    conn.row_factory = dict_factory
    return conn

def get_conn_(param):
    path = ('D:/'+param+'/database/counter.db')
    conn = sqlite3.connect(path)
    conn.row_factory = dict_factory
    return conn 

def get_result_set(sql, parameter):
    try:
        lock.acquire()
        _LOG_.info("SQL PARAMETER : "+ str(sql) + " PARAMETER : " + str(parameter))
        conn__ = get_conn()
        cursor = conn__.cursor().execute(sql, parameter)
        result = cursor.fetchall()
        # _LOG_.info(result)
    except Exception as e:
        lock.release()
        lock.acquire()
        _LOG_.info("SQL PARAMETER : "+ str(sql) + " PARAMETER : " + str(parameter))
        conn__ = get_conn()
        cursor = conn__.cursor().execute(sql, parameter)
        result = cursor.fetchall()
    finally:
        lock.release()
    
    _LOG_.info("RESULT PARAMETER CCC" + str(result))
    return result

def get_result_void(sql):
    try:
        lock.acquire()
        conn__ = get_conn()
        cursor = conn__.cursor().execute(sql)
        result = cursor.fetchall()
    finally:
        lock.release()
    return result

def get_result_telegram(sql, param):
    try:
        lock.acquire()
        conn__ = get_conn_(param)
        cursor = conn__.cursor().execute(sql)
        result = cursor.fetchall()
    finally:
        lock.release()
    return result

def delete_record(sql):
    try:
        lock.acquire()
        _LOG_.info(sql)
        conn__ = sqlite3.connect(sys.path[0] + '/database/' + localDB)
        conn__.cursor().execute(sql)
        conn__.commit()
    finally:
        lock.release()


def insert_or_update_database(sql, parameter):
    try:
        lock.acquire()
        _LOG_.info((sql, str(parameter)))
        conn__ = get_conn()
        conn__.execute(sql, parameter)
        conn__.commit()
        return True
    except Exception as e:
        lock.release()
        lock.acquire()
        _LOG_.info((sql, str(parameter)))
        conn__ = get_conn()
        conn__.execute(sql, parameter)
        conn__.commit()
        return True
    finally:
        lock.release()

def init_database():
    try:
        lock.acquire()
        conn__ = get_conn()
        with open(sys.path[0] + '/database/ClientDatabase.sql') as f:
            conn__.cursor().executescript(f.read())
    finally:
        lock.release()

def check_tb():
    conn__ = get_conn()
    cursor = conn__.cursor()
    cursor.execute("SELECT name FROM sqlite_master WHERE type='table';")
    return cursor.fetchall()

def check_table(sql, parameter):
    try:
        lock.acquire()
        _LOG_.info((sql, str(parameter)))
        conn__ = get_conn()
        cursor = conn__.cursor().execute(sql, parameter)
        result = cursor.fetchall()
        return result
    finally:
        lock.release()

def create_table(parameter):
    try:
        lock.acquire()
        conn__ = get_conn()
        conn__.execute(parameter)
    finally:
        lock.release()



if __name__ == '__main__':
    __conn = sqlite3.connect(localDB)
    with open('ClientDatabase.sql') as f:
        __conn.cursor().executescript(f.read())