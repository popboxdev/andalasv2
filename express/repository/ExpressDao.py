from database import ClientDatabase
import time
__author__ = 'wahyudi@popbox.asia'


def init_express(express):
    sql = 'INSERT INTO Express (id, expressNumber, expressType, overdueTime, status, storeTime, syncFlag, takeUserPhoneNumber, storeUserPhoneNumber, validateCode, version, box_id, logisticsCompany_id, mouth_id, operator_id, storeUser_id,groupName,customerStoreNumber, chargeType, continuedHeavy, continuedPrice, endAddress, startAddress, firstHeavy, firstPrice, designationSize, electronicCommerce_id, takeUser_id)VALUES (:id,:expressNumber,:expressType,:overdueTime,:status,:storeTime,:syncFlag,:takeUserPhoneNumber,:storeUserPhoneNumber,:validateCode,:version,:box_id,:logisticsCompany_id,:mouth_id,:operator_id,:storeUser_id,:groupName,:customerStoreNumber,:chargeType,:continuedHeavy,:continuedPrice,:endAddress,:startAddress,:firstHeavy,:firstPrice,:designationSize,:electronicCommerce_id,:takeUser_id)'
    ClientDatabase.insert_or_update_database(sql, express)


def get_express_by_id_and_validate(param):
    sql = 'SELECT * FROM Express WHERE id = :id AND ValidateCode = :validateCode AND Status=:status'
    return ClientDatabase.get_result_set(sql, param)

def get_express_by_id(param):
    sql = 'SELECT * FROM Express WHERE id=:id'
    return ClientDatabase.get_result_set(sql, param)


def take_express(param):
    sql = 'UPDATE express SET TakeTime=:takeTime,SyncFlag = :syncFlag,Version = :version,Status = :status,staffTakenUser_id=:staffTakenUser_id, recipientName=:recipientName WHERE id=:id'
    ClientDatabase.insert_or_update_database(sql, param)

def save_data_operator_taken(param):
    sql = 'UPDATE express SET dataOperator = :dataOperator WHERE id = :id'
    ClientDatabase.insert_or_update_database(sql, param)  

def getdata_express_operator_taken(param):
    sql = 'SELECT * FROM Express WHERE id=:id'
    return ClientDatabase.get_result_set(sql, param)[0]

def get_express_by_validate(param):
    sql = 'SELECT * FROM Express WHERE validateCode=:validateCode AND Status=:status'
    return ClientDatabase.get_result_set(sql, param)

def get_express_by_validate_all(param):
    # sql = 'SELECT * FROM Express WHERE validateCode=:validateCode'
    sql = 'SELECT Express.id AS id_express, * FROM Mouth LEFT JOIN Express ON Mouth.id = Express.mouth_id LEFT JOIN MouthType ON Mouth.mouthType_id = MouthType.id WHERE Express.validateCode=:validateCode'
    return ClientDatabase.get_result_set(sql, param)


def save_express(express_param):
    sql = 'INSERT INTO Express (id, ExpressNumber, expressType, overdueTime, status, StoreTime, SyncFlag, TakeUserPhoneNumber, ValidateCode, Version, box_id, logisticsCompany_id, mouth_id, operator_id, storeUser_id,groupName,courier_phone,other_company_name, transactionId, drop_by_courier_login,paymentParam) VALUES (:id,:expressNumber,:expressType,:overdueTime,:status,:storeTime,:syncFlag,:takeUserPhoneNumber,:validateCode,:version,:box_id,:logisticsCompany_id,:mouth_id,:operator_id,:storeUser_id,:groupName,:courier_phone,:other_company_name,:transactionRecord,:drop_by_courier_login, :payment_param)'
    return ClientDatabase.insert_or_update_database(sql, express_param)

def save_data_temp(express_param):
    sql = 'INSERT INTO Express (id, ExpressNumber, expressType, overdueTime, status, StoreTime, SyncFlag, TakeUserPhoneNumber, ValidateCode, Version, box_id, logisticsCompany_id, mouth_id, operator_id, storeUser_id,groupName) VALUES (:id,:expressNumber,:expressType,:overdueTime,:status,:storeTime,:syncFlag,:takeUserPhoneNumber,:validateCode,:version,:box_id,:logisticsCompany_id,:mouth_id,:operator_id,:storeUser_id,:groupName)'
    return ClientDatabase.insert_or_update_database(sql, express_param)


def mark_sync_success(express_param):
    sql = 'UPDATE express SET SyncFlag = 1 WHERE id =:id'
    ClientDatabase.insert_or_update_database(sql, express_param)


def update_express(express_param):
    sql = 'UPDATE express SET overdueTime=:overdueTime,status=:status,StoreTime=:storeTime,SyncFlag=:syncFlag,ValidateCode=:validateCode,box_id=:box_id,logisticsCompany_id=:logisticsCompany_id,mouth_id=:mouth_id,operator_id=:operator_id,storeUser_id=:storeUser_id WHERE id=:id'
    ClientDatabase.insert_or_update_database(sql, express_param)


def import_express(express_param__):
    sql = 'INSERT INTO Express (id,ExpressNumber,expressType,TakeUserPhoneNumber,Version,SyncFlag)VALUES (:id,:expressNumber,:expressType,:takeUserPhoneNumber,:version,:syncFlag)'
    ClientDatabase.insert_or_update_database(sql, express_param__)


def update_recode(record):
    sql = 'UPDATE TransactionRecord SET Amount=:amount WHERE express_id = :express_id'
    ClientDatabase.insert_or_update_database(sql, record)


def get_record(record_param):
    sql = 'SELECT * FROM TransactionRecord WHERE express_id = :express_id'
    return ClientDatabase.get_result_set(sql, record_param)


def get_record_appexress(record_param):
    sql = 'SELECT * FROM TransactionRecord WHERE paymentType = :validateCode '
    return ClientDatabase.get_result_set(sql, record_param)

def check_parcel(param):
    sql = 'SELECT * FROM Express WHERE id = :id'
    return ClientDatabase.get_result_set(sql, param)


def save_record(record_param):
    sql = 'INSERT INTO TransactionRecord (id, paymentType, transactionType, amount, express_id, createTime, mouth_id)VALUES (:id,:paymentType,:transactionType,:amount,:express_id,:createTime,:mouth_id)'
    ClientDatabase.insert_or_update_database(sql, record_param)


def get_overdue_express_by_logistics_id(param):
    sql = 'SELECT * FROM Mouth LEFT JOIN Express ON Mouth.express_id = Express.id WHERE Express.logisticsCompany_id =:logisticsCompany_id AND Express.status =:status AND Express.overdueTime < :overdueTime AND Express.expressType =:expressType AND Express.groupName !=:groupName_1 AND Express.groupName !=:groupName_2 ORDER BY Express.overdueTime DESC'
    # sql = 'SELECT * FROM Express WHERE logisticsCompany_id = :logisticsCompany_id AND status =:status AND overdueTime < :overdueTime AND expressType =:expressType AND groupName !=:groupName_1 AND groupName !=:groupName_2 ORDER BY overdueTime DESC'
    return ClientDatabase.get_result_set(sql, param)


def get_overdue_express_by_manager(param):
    sql = 'SELECT * FROM Mouth LEFT JOIN Express ON Mouth.express_id = Express.id WHERE Express.status =:status AND Express.overdueTime < :overdueTime AND Express.expressType =:expressType AND Express.groupName !=:groupName_1 AND Express.groupName !=:groupName_2 ORDER BY Express.overdueTime DESC'
    #sql = 'SELECT * FROM Express WHERE status =:status AND overdueTime < :overdueTime AND expressType =:expressType AND groupName !=:groupName_1 AND groupName !=:groupName_2 ORDER BY overdueTime DESC'
    return ClientDatabase.get_result_set(sql, param)


def get_all_overdue_express_by_logistics_id(param):
    sql = 'SELECT * FROM Express WHERE logisticsCompany_id = :logisticsCompany_id AND status =:status AND expressType =:expressType AND overdueTime < :overdueTime ORDER BY overdueTime DESC'
    return ClientDatabase.get_result_set(sql, param)


def get_overdue_express_by_centralization(param):
    sql = 'SELECT * FROM Mouth LEFT JOIN Express ON Mouth.express_id = Express.id WHERE Express.logisticsCompany_id !=:logisticsCompany_id AND Express.status =:status AND Express.overdueTime < :overdueTime AND Express.expressType =:expressType AND Express.groupName !=:groupName_1 AND Express.groupName !=:groupName_2 ORDER BY Express.overdueTime DESC'
    # sql = 'SELECT * FROM Express WHERE logisticsCompany_id !=:logisticsCompany_id AND status =:status AND overdueTime < :overdueTime AND expressType =:expressType AND groupName !=:groupName_1 AND groupName !=:groupName_2 ORDER BY overdueTime DESC '
    return ClientDatabase.get_result_set(sql, param)


def get_all_overdue_express_by_manager(param):
    sql = 'SELECT * FROM Express WHERE status =:status AND overdueTime < :overdueTime AND expressType =:expressType ORDER BY overdueTime DESC'
    return ClientDatabase.get_result_set(sql, param)


def get_in_store_overdue_express_by_over_time_and_operator_id_and_start_line(param):
    sql = 'SELECT * FROM Express WHERE operator_id = :operator_id AND status =:status AND overdueTime < :overdueTime ORDER BY overdueTime DESC LIMIT :startLine,5 '
    return ClientDatabase.get_result_set(sql, param)


def get_overdue_express_count_by_logistics_id(param):
    sql = 'SELECT COUNT(1) AS count FROM Mouth LEFT JOIN Express ON Mouth.express_id = Express.id WHERE Express.logisticsCompany_id =:logisticsCompany_id AND Express.status =:status AND Express.overdueTime < :overdueTime AND Express.expressType =:expressType AND Express.groupName !=:groupName_1 AND Express.groupName !=:groupName_2 ORDER BY Express.overdueTime DESC'
    # sql = 'SELECT count(1) AS count FROM Express WHERE logisticsCompany_id = :logisticsCompany_id AND status =:status AND overdueTime < :overdueTime AND expressType =:expressType AND groupName !=:groupName_1 AND groupName !=:groupName_2'
    return ClientDatabase.get_result_set(sql, param)


def get_overdue_express_count_by_manager(param):
    sql = 'SELECT COUNT(1) AS count FROM Mouth LEFT JOIN Express ON Mouth.express_id = Express.id WHERE Express.status =:status AND Express.overdueTime < :overdueTime AND Express.expressType =:expressType AND Express.groupName !=:groupName_1 AND Express.groupName !=:groupName_2 ORDER BY Express.overdueTime DESC'
    #sql = 'SELECT count(1) AS count FROM Express WHERE status =:status AND overdueTime <:overdueTime AND expressType =:expressType AND groupName !=:groupName_1 AND groupName !=:groupName_2'
    return ClientDatabase.get_result_set(sql, param)


def get_overdue_express_count_by_centralization(param):
    sql = 'SELECT COUNT(1) AS count FROM Mouth LEFT JOIN Express ON Mouth.express_id = Express.id WHERE Express.logisticsCompany_id !=:logisticsCompany_id AND Express.status =:status AND Express.overdueTime < :overdueTime AND Express.expressType =:expressType AND Express.groupName !=:groupName_1 AND Express.groupName !=:groupName_2 ORDER BY Express.overdueTime DESC'
    # sql = 'SELECT count(1) AS count FROM Express WHERE logisticsCompany_id !=:logisticsCompany_id AND status =:status AND overdueTime <:overdueTime AND expressType =:expressType AND groupName !=:groupName_1 AND groupName !=:groupName_2'
    return ClientDatabase.get_result_set(sql, param)


def get_express_by_id(param):
    sql = 'SELECT * FROM Express WHERE id=:id'
    return ClientDatabase.get_result_set(sql, param)


def get_not_sync_express_list(param):
    sql = 'SELECT * FROM Express WHERE syncFlag=:syncFlag'
    return ClientDatabase.get_result_set(sql, param)


def save_customer_express(customer_store_express):
    sql = "INSERT INTO Express(id, customerStoreNumber, expressType, status, storeTime, syncFlag, version, box_id, logisticsCompany_id, mouth_id, operator_id, storeUser_id, recipientName, weight, recipientUserPhoneNumber, chargeType)VALUES (:id,:customerStoreNumber,:expressType,'IN_STORE',:storeTime,0,0,:box_id,:logisticsCompany_id,:mouth_id,:operator_id,:storeUser_id,:recipientName,:weight,:recipientUserPhoneNumber, :chargeType)"
    return ClientDatabase.insert_or_update_database(sql, customer_store_express)


def save_customer_reject_express(customer_store_express):
    sql = "INSERT INTO Express(id, customerStoreNumber, expressType, status, storeTime, syncFlag, version, electronicCommerce_id, box_id, logisticsCompany_id, mouth_id, operator_id, storeUser_id, recipientName, weight, storeUserPhoneNumber, chargeType)VALUES (:id,:customerStoreNumber,:expressType,'IN_STORE',:storeTime,0,0,:electronicCommerce_id,:box_id,:logisticsCompany_id,:mouth_id,:operator_id,:storeUser_id,:recipientName,:weight,:storeUserPhoneNumber, :chargeType)"
    return ClientDatabase.insert_or_update_database(sql, customer_store_express)


def get_send_express_count_by_logistics_id(param):
    sql = 'SELECT count(1) AS count FROM Express WHERE logisticsCompany_id = :logisticsCompany_id AND Express.status =:status AND expressType =:expressType '
    return ClientDatabase.get_result_set(sql, param)


def get_send_express_count_by_operator(param):
    sql = 'SELECT count(1) AS count FROM Express WHERE Express.status =:status AND expressType =:expressType '
    return ClientDatabase.get_result_set(sql, param)


def get_send_express_by_logistics_id(param):
    sql = 'SELECT * FROM Express WHERE logisticsCompany_id = :logisticsCompany_id AND status =:status AND expressType =:expressType ORDER BY storeTime LIMIT :startLine,5 '
    return ClientDatabase.get_result_set(sql, param)


def get_send_express_by_operator(param):
    sql = 'SELECT * FROM Express WHERE status =:status AND expressType =:expressType ORDER BY storeTime LIMIT :startLine,5 '
    return ClientDatabase.get_result_set(sql, param)


def get_all_send_express_by_logistics_id(param):
    sql = 'SELECT * FROM Express WHERE logisticsCompany_id = :logisticsCompany_id AND status =:status AND expressType =:expressType ORDER BY storeTime '
    return ClientDatabase.get_result_set(sql, param)


def get_all_send_express_by_manager(param):
    sql = 'SELECT * FROM Express WHERE status =:status AND expressType =:expressType ORDER BY storeTime '
    return ClientDatabase.get_result_set(sql, param)


def get_mouth(express__):
    sql = 'SELECT * FROM TransactionRecord WHERE express_id = :id'
    return ClientDatabase.get_result_set(sql, express__)


def save_express_for_customer_to_staff(express_param):
    sql = 'INSERT INTO Express (id, expressType, overdueTime, status, StoreTime, SyncFlag, Version, box_id, mouth_id, operator_id, storeUser_id,groupName) VALUES (:id,:expressType,:overdueTime,:status,:storeTime,:syncFlag,:version,:box_id,:mouth_id,:operator_id,:storeUser_id,:groupName)'
    ClientDatabase.insert_or_update_database(sql, express_param)


def save_send_express(express_param):
    sql = 'INSERT INTO Express (id, customerStoreNumber, expressType, overdueTime, status, storeTime, syncFlag, version, box_id, mouth_id, storeUserPhoneNumber, operator_id, groupName,Weight) VALUES (:id,:customerStoreNumber,:expressType,:overdueTime,:status,:storeTime,:syncFlag,:version,:box_id,:mouth_id,:storeUserPhoneNumber,:operator_id,:groupName,:Weight)'
    ClientDatabase.insert_or_update_database(sql, express_param)


def get_express_by_express_type(param):
    sql = 'SELECT * FROM Express WHERE expressType=:expressType AND status=:status'
    return ClientDatabase.get_result_set(sql, param)


def staff_get_send_express_by_logistics_id(param):
    sql = 'SELECT * FROM Express WHERE status =:status AND expressType =:expressType ORDER BY storeTime LIMIT :startLine,5 '
    return ClientDatabase.get_result_set(sql, param)


def reset_express(param):
    sql = 'UPDATE express SET overdueTime =:overdueTime, storeTime =:storeTime, status =:status, syncFlag =:syncFlag, takeUserPhoneNumber =:takeUserPhoneNumber, validateCode =:validateCode, version =:version, takeTime=:takeTime WHERE id =:id'
    return ClientDatabase.insert_or_update_database(sql, param)


def delete_express(limit=1, duration='DAY'):
    duration_types = {'DAY': limit * 86400,
                      'WEEK': limit * 604800,
                      'MONTH': limit * 2629743}
    limit = (int(time.time()) - duration_types.get(duration, 86400)) * 1000
    sql = 'DELETE FROM express WHERE status <> "IN_STORE" AND storeTime < ' + str(limit)
    return ClientDatabase.delete_record(sql)

def update_apexpress_by_pin(express_param):
    sql = 'UPDATE express SET overdueTime=:overdueTime,syncFlag=:syncFlag,  transactionId =:transactionId WHERE validateCode=:validateCode AND status=:status'
    ClientDatabase.insert_or_update_database(sql, express_param)

def local_extend_express(express_param):
    sql = 'UPDATE express SET overdueTime=:overdueTime,syncFlag=:syncFlag WHERE expressNumber=:expressNumber AND status="IN_STORE" '
    ClientDatabase.insert_or_update_database(sql, express_param)

#UPDATE ANDALAS V2.0

def get_popsend_list_by_manager(param):
    sql = 'SELECT * FROM Express WHERE status =:status AND expressType =:expressType AND storeUser_id =:storeUser_id ORDER BY storeTime DESC '
    return ClientDatabase.get_result_set(sql, param)

def get_popsend_list_by_logistic_id(param):
    sql = 'SELECT * FROM Express WHERE status =:status AND logisticsCompany_id =:logisticsCompany_id AND expressType =:expressType AND storeUser_id=:storeUser_id ORDER BY storeTime DESC'
    return ClientDatabase.get_result_set(sql, param)

def get_reject_list_by_manager(param):
    sql = 'SELECT * FROM Express WHERE status =:status AND expressType =:expressType ORDER BY storeTime DESC '
    return ClientDatabase.get_result_set(sql, param)

def get_reject_list_by_logistic_id(param):
    sql = 'SELECT * FROM Express WHERE status =:status AND logisticsCompany_id =:logisticsCompany_id AND expressType =:expressType ORDER BY storeTime DESC '
    return ClientDatabase.get_result_set(sql, param)

def local_user_get(param):
    sql = 'SELECT *, count(1) AS count FROM User WHERE userName=:username AND userPassword=:userpassword'
    return ClientDatabase.get_result_set(sql, param)

def get_overdue_express_by_operator(param):
    sql = 'SELECT *, Express.id AS id_express FROM Mouth LEFT JOIN Express ON Mouth.express_id = Express.id WHERE Express.status =:status AND Express.overdueTime < :overdueTime AND Express.expressType =:expressType ORDER BY Express.storeTime DESC'
    #sql = 'SELECT * FROM Express WHERE status =:status AND overdueTime < :overdueTime AND expressType =:expressType ORDER BY overdueTime DESC'
    return ClientDatabase.get_result_set(sql, param)
 
def get_all_express_by_operator(param): # INI
    sql = 'SELECT *, Express.id AS id_express FROM Mouth LEFT JOIN Express ON Mouth.express_id = Express.id WHERE Express.status =:status ORDER BY Express.storeTime DESC'
    #sql = 'SELECT id AS id_express, mouth_id, validateCode, storeTime, overdueTime, expressType, expressNumber, customerStoreNumber, takeUserPhoneNumber, storeUserPhoneNumber FROM Express WHERE status=:status ORDER BY storeTime DESC'
    return ClientDatabase.get_result_set(sql, param)

def get_survey_record(param):
    sql = 'SELECT * FROM Survey WHERE id_express =:id_express AND expressNumber =:expressNumber'
    return ClientDatabase.get_result_set(sql, param)

def import_survey_store_record(param):
    sql = 'INSERT INTO Survey (id_express, expressNumber, survey_store, storetime_survey, syncFlag, deleteFlag)VALUES (:id_express,:expressNumber,:survey_store,:storetime_survey,:syncFlag,:deleteFlag)'
    ClientDatabase.insert_or_update_database(sql, param)


def import_survey_take_record(param):
    sql = 'INSERT INTO Survey (id_express, expressNumber, survey_take, taketime_survey, syncFlag, deleteFlag)VALUES (:id_express,:expressNumber,:survey_take,:taketime_survey,:syncFlag,:deleteFlag)'
    ClientDatabase.insert_or_update_database(sql, param)


def update_survey_record(param):
    sql = 'UPDATE Survey SET survey_take=:survey_take,taketime_survey=:taketime_survey, syncFlag=:syncFlag WHERE id_express=:id_express AND expressNumber=:expressNumber'
    ClientDatabase.insert_or_update_database(sql, param)


def get_not_sync_survey_list(param):
    #sql = 'SELECT * FROM Survey WHERE syncFlag=:syncFlag AND survey_take IS NOT NULL'
    sql = 'SELECT * FROM Survey WHERE syncFlag=:syncFlag'
    return ClientDatabase.get_result_set(sql, param)


def mark_sync_survey_success(param):
    sql = 'UPDATE Survey SET syncFlag = 1 WHERE id_express=:id_express AND expressNumber=:expressNumber'
    ClientDatabase.insert_or_update_database(sql,param)


def update_mouth_id_by_express_id(param):
    sql = 'UPDATE express SET mouth_id =:mouth_id, syncFlag=:syncFlag WHERE id = :id'
    return ClientDatabase.insert_or_update_database(sql, param)


def update_data_extend(express_param):
    sql = 'UPDATE express SET dataExtend=:dataExtend WHERE validateCode=:validateCode'
    ClientDatabase.insert_or_update_database(sql, express_param)


def get_return_express_count_by_manager(param):
    sql = 'SELECT count(1) AS count FROM Express WHERE status =:status AND expressType =:expressType'
    return ClientDatabase.get_result_set(sql, param)


def get_popsend_express_count_by_manager(param):
    sql = 'SELECT count(1) AS count FROM Express WHERE status =:status AND storeUser_id =:storeUser_id ORDER BY storeTime DESC '
    return ClientDatabase.get_result_set(sql, param)


def get_popsend_express_count_by_logistics_id(param):
    sql = 'SELECT count(1) AS count FROM Express WHERE status =:status AND logisticsCompany_id =:logisticsCompany_id AND storeUser_id=:storeUser_id ORDER BY storeTime DESC'
    return ClientDatabase.get_result_set(sql, param)


def get_popsafe_express_count_by_manager(param):
    sql = 'SELECT count(1) AS count FROM Express WHERE status =:status AND overdueTime <:overdueTime AND (groupName =:groupName_1 OR groupName =:groupName_2)'
    return ClientDatabase.get_result_set(sql, param)


def get_popsafe_express_count_by_centralization(param):
    sql = 'SELECT count(1) AS count FROM Express WHERE logisticsCompany_id !=:logisticsCompany_id AND status =:status AND overdueTime <:overdueTime AND (groupName =:groupName_1 OR groupName =:groupName_2)'
    return ClientDatabase.get_result_set(sql, param)


def get_popsafe_express_count_by_logistics_id(param):
    sql = 'SELECT count(1) AS count FROM Express WHERE logisticsCompany_id = :logisticsCompany_id AND status =:status AND overdueTime < :overdueTime AND (groupName =:groupName_1 OR groupName =:groupName_2)'
    return ClientDatabase.get_result_set(sql, param)


def get_popsafe_express_by_manager(param):
    sql = 'SELECT * FROM Express WHERE status =:status AND overdueTime < :overdueTime AND (groupName =:groupName_1 OR groupName =:groupName_2) ORDER BY overdueTime DESC'
    return ClientDatabase.get_result_set(sql, param)


def get_popsafe_express_by_logistics_id(param):
    sql = 'SELECT * FROM Express WHERE logisticsCompany_id =:logisticsCompany_id AND status =:status AND overdueTime < :overdueTime AND (groupName =:groupName_1 OR groupName =:groupName_2) ORDER BY overdueTime DESC'
    return ClientDatabase.get_result_set(sql, param)


def get_popsafe_express_by_centralization(param):
    sql = 'SELECT * FROM Express WHERE logisticsCompany_id !=:logisticsCompany_id AND status =:status AND overdueTime < :overdueTime AND (groupName =:groupName_1 OR groupName =:groupName_2) ORDER BY overdueTime DESC '
    return ClientDatabase.get_result_set(sql, param)

def check_column_exist(param, table):
    sql = 'PRAGMA table_info('+ table +')'
    return ClientDatabase.get_result_set(sql, param)


def adding_column_reason(msg):
    sql = 'ALTER TABLE Express ADD reason VARCHAR(255)'
    return ClientDatabase.get_result_set(sql, msg)

def cancel_order(param):
    sql = "UPDATE express SET syncFlag=:syncFlag, status=:status, reason=:reason WHERE id=:id"
    ClientDatabase.insert_or_update_database(sql, param)

def adding_column(msg, column, default, table):
    sql = 'ALTER TABLE '+ table +' ADD ' + column + ' VARCHAR(255) NOT NULL DEFAULT('+ str(default) +')'
    return ClientDatabase.get_result_set(sql, msg)

def update_set_flag_pod(param):
    sql = "UPDATE express SET is_needed_pod=:is_needed_pod WHERE id=:id"
    ClientDatabase.insert_or_update_database(sql, param)

def save_signature(record_param):
    sql = 'INSERT INTO Epod (express_id, barcode, groupname, pincode, cust_name, create_at, signature, syncFlag, deleteFlag)VALUES (:express_id,:barcode,:groupname,:pincode,:cust_name,:create_at,:signature,:syncFlag,:deleteFlag)'
    return ClientDatabase.insert_or_update_database(sql, record_param)

def get_scan_not_sync_signature_list(param):
    sql = 'SELECT * FROM Epod WHERE deleteFlag=0 AND syncFlag=:syncFlag'
    return ClientDatabase.get_result_set(sql, param)

def mark_sync_signature_success(param):
    sql = 'UPDATE Epod SET syncFlag = 1 WHERE express_id=:express_id AND barcode=:barcode'
    ClientDatabase.insert_or_update_database(sql, param)

def save_contactless(record_param):
    sql = 'INSERT INTO Contactless (express_id, express_number, locker_name, box_id, is_taken, is_drop, storeTime, takeTime, is_pushed, is_success, qr_content, syncFlag, locker_size, locker_number)VALUES (:express_id, :express_number, :locker_name, :box_id, :is_taken, :is_drop, :storeTime, :takeTime, :is_pushed, :is_success, :qr_content, :syncFlag, :locker_size, :locker_number)'
    return ClientDatabase.insert_or_update_database(sql, record_param)

def get_not_sync_contactless(param):
    sql = 'SELECT * FROM Contactless WHERE syncFlag=:syncFlag'
    return ClientDatabase.get_result_set(sql, param)

def update_sync_contactless(param):
    sql = 'UPDATE Contactless SET syncFlag = 1, push_date=:push_date WHERE qr_content=:qr_content'
    ClientDatabase.insert_or_update_database(sql, param)