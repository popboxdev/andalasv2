import QtQuick 2.4
import QtQuick.Controls 1.2
import Qt.labs.folderlistmodel 1.0
import QtMultimedia 5.0

Rectangle{
    id:parent_root
    color: "black"
    property var img_path: "/advertisement/video/"
    property url img_path_: ".." + img_path
    property var qml_pic
    property string pic_source: ""
    property int num_pic
    property string mode // ["staticVideo", "mediaPlayer", "liveView"]
    property variant media_files: []
    property int tvc_timeout: 60
    property int timer_scanner: 20
    property var language: 'ID'
    property bool scanner_status: true
    property var contactless_status: "disable"

    Stack.onStatusChanged:{
        if(Stack.status==Stack.Activating){
            check_contactless();
            scanner_restart.stop();
            console.log('ads mode : ' +  mode)
            console.log('tvc_timer_adv : ' +  tvc_timeout)
            if(mode=="mediaPlayer" && media_files.length == 0){
                slot_handler.start_get_file_dir(img_path);
            }
            tvc_stopping.counter = tvc_timeout;
            show_tvc_stopping.start();
        }
        if(Stack.status==Stack.Deactivating){
            slot_handler.stop_courier_scan_barcode();
            player.stop()
            while (media_files.length > 0) {
                media_files.pop();
            }
        }
    }

    Component.onCompleted: {
        root.start_get_file_dir_result.connect(get_result)
        root.start_get_detection_result.connect(get_detection)
        root.barcode_result.connect(handle_text)
    }

    Component.onDestruction: {
        root.start_get_file_dir_result.disconnect(get_result)
        root.start_get_detection_result.disconnect(get_detection)
        root.barcode_result.disconnect(handle_text)
    }

    function get_detection(result){
        console.log("get_detection : ", JSON.stringify(result))
        if (result=="FAILED" || result=="ERROR"){
            return
        } else {
            var handle = JSON.parse(result)
            var detections = handle.result_face + handle.result_motion
            if (detections > 0){
                player.stop()
                while (media_files.length > 0) {
                    media_files.pop();
                }
                my_stack_view.pop()
                root.reference = "people_count"
            }
        }
    }


    function get_result(result){
        if (result == "ERROR" || result == ""){
            console.log("No Media Files!")
        } else {
            var files = JSON.parse(result)
            media_files = files.output
            console.log("Media Files (" + media_files.length + ") : " + media_files)
            if (media_files.length > 0){
                media_mode.setIndex(root.media_idx);
            } else{
                console.log("Cannot Play Media!")
            }
        }
    }

    function handle_text(text){
        check_pattern(text);
    }

    function check_pattern(barcode_text){
        var input = barcode_text;
        var result = input.split("#");
        var page = result[0];
        var code = result[1];
        if(page=="AMBIL"){
            if(scanner_status==true){
                slot_handler.stop_courier_scan_barcode();
                my_stack_view.push(take_parcel, {language: language, pin_code: code, contactless: true});
                scanner_status = false;
            }
        }else{
            console.log("KIRIM");
            slot_handler.start_courier_scan_barcode();
        }
    }

    Rectangle{
        id: timer_tvc
        width: 10
        height: 10
        x:0
        y:0
        visible: false
        QtObject{
            id:tvc_stopping
            property int counter
            Component.onCompleted:{
                tvc_stopping.counter = tvc_timeout
            }
        }
        Timer{
            id:show_tvc_stopping
            interval:1000
            repeat:true
            running:false
            triggeredOnStart:true
            onTriggered:{
                tvc_stopping.counter -= 1
                if(tvc_stopping.counter == 0){
                    player.stop()
                    while (media_files.length > 0) {
                        media_files.pop();
                    }
                    slot_handler.start_post_activity("ReleaseTVC")
                    my_stack_view.pop()
                }
            }
        }
    }

    Timer {
        id: scanner_restart
        interval: 1000
        repeat: true
        running: true
        onTriggered:{   
            timer_scanner -=1;
            // console.log("timer_scanner = ", timer_scanner);
            if(timer_scanner == 10){
                slot_handler.stop_courier_scan_barcode();
            }
            if(timer_scanner == 0){
                timer_scanner = 20;
                slot_handler.start_courier_scan_barcode();
            }
        }
    }


    // Play Multiple Videos
    Rectangle {
        id: media_mode
        visible: (mode=="mediaPlayer") ? true : false
        width: 1024
        height: 768
        color: "black"

        function setIndex(i){
            root.media_idx = i;
            root.media_idx %= media_files.length;
            player.source = img_path_ + media_files[root.media_idx];
            slot_handler.start_post_tvclog(media_files[root.media_idx])
            console.log('Playing Media [', root.media_idx, '-', media_files[root.media_idx], ']')
            player.play()
        }

        function next(){
            setIndex(root.media_idx + 1);
        }

        function previous(){
            setIndex(root.media_idx - 1);
        }

        Connections {
            target: player
            onStopped: {
                if (player.status == MediaPlayer.EndOfMedia) {
                    if (root.media_idx==media_files.length-1){ //Looping start from beginning
                        media_mode.setIndex(0);
                    } else{
                        media_mode.next();
                    }
                }
            }
        }

        MediaPlayer {
            id: player
        }

        VideoOutput {
            anchors.fill: parent
            source: player
        }

        MouseArea{
            anchors.fill: parent
            onClicked: {
                player_stop();
                my_stack_view.pop(null)
            }
            onDoubleClicked: {
                player_stop();
                my_stack_view.pop(null);
            }
        }

    }

    function player_stop() {
        player.stop()
        while (media_files.length > 0) {
            media_files.pop();
        }
        slot_handler.start_post_activity("ReleaseTVC")
    }

    function check_contactless() {
        if(contactless_status=="enable"){
            slot_handler.start_courier_scan_barcode();
            scanner_status = true;
            scanner_restart.restart();
        }else{
            scanner_status = false;
            scanner_restart.stop();
        }
        
    }
}

