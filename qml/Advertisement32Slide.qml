import QtQuick 2.4
import QtQuick.Controls 1.2

Rectangle{
    id:parent_root
    color: "transparent"
    property var img_path: "../advertisement/source/"
    property variant qml_pic: []
    property variant pic_source: []
    property int num_pic: 0

    Stack.onStatusChanged:{
        if(Stack.status==Stack.Activating){
            slot_handler.start_get_file_banner(img_path);
        }
        if(Stack.status==Stack.Deactivating){
            slider_timer.stop()
        }
    }

    Component.onCompleted: {
        root.start_get_file_banner_result.connect(init_ad_images);
    }

    Component.onDestruction: {
        root.start_get_file_banner_result.disconnect(init_ad_images);
        slider_timer.stop();
    }

    function init_ad_images(result){
        console.log(result);
        if(result=='') return;
        var i = JSON.parse(result);
        qml_pic = i.output;
        loader.sourceComponent = loader.Null
        pic_source = img_path + qml_pic[0]
        loader.sourceComponent = component
        slider_timer.start()
    }


    Timer{
        id:slider_timer
        interval:10000
        repeat:true
        running:false
        triggeredOnStart:false
        onTriggered:{
            if(num_pic < qml_pic.length){
                num_pic += 1
                if(num_pic == qml_pic.length){
                    slider_timer.restart()
                    loader.sourceComponent = loader.Null
                    pic_source = img_path + qml_pic[0]
                    loader.sourceComponent = component
                    num_pic = 0
                }else{
                    slider_timer.restart()
                    loader.sourceComponent = loader.Null
                    pic_source = img_path + qml_pic[num_pic]
                    loader.sourceComponent = component
                }
            }
        }
    }

    Component {
        id: component
        Rectangle {
            AnimatedImage {
                id:ad_pic
                x: 0
                y: 0
                width: parent_root.width
                height: parent_root.height
                source: pic_source
                fillMode: Image.PreserveAspectFit
            }
        }
    }

    Loader { id: loader }

}

