import QtQuick 2.4
import QtQuick.Controls 1.2

Background{
    id:box_size
    show_img : ""
    img_vis:true
    property int count: 0
    property var press: '0'
    property var asset_path: "asset/global/input_phone/"
    property var path_background: 'asset/send_page/background/'
    property var red1 : "#ff524f"
    property var red1rev: "#AA"
    property var temp:""
    property var plus:""
    property var popsafe_status:""
    property var title:""
    property var title_body:""
    property var phone_number: ''
    property var parcel_number: ''
    property var param_return: ''
    property var box_choose: ''
    property var opendoor_status: ''
    property var size_XS: 0
    property var size_S: 0
    property var size_M: 0
    property var size_L: 0
    property var size_XL: 0
    property var popsafe_param: undefined
    property var locker_number: 0
    property var door_type: ''
    property bool isPopDeposit: false
    property int value_timer : 90
    property var txt_time: ""
    property var merchant_size: ""
    property bool is_change_locker: false
    property bool change_locker: false
    property var express_id: ""
    property bool drop_by_kirim: false

    property var id_company: ""
    property var status_courier: ""
    property var select_courier: ""
    property var phone_courier: ""
    property var drop_by_other_company: 0
    property var drop_by_courier: 0
    
    Stack.onStatusChanged:{
        if(Stack.status == Stack.Activating){
            console.log("isPopDeposit: " + isPopDeposit)
            timer.secon = 90
            timer_note.restart()
            slot_handler.start_get_free_mouth_mun()
            show_box_size.start()
            dimm_display.visible= true
            press = '0';
            count = 0;
            temp = red1.slice(-6)
            plus = red1rev + temp
            
        }
        if(Stack.status==Stack.Deactivating){
            timer_note.stop()
        }
    }
    FontType{
        id:fontStyle
        //Type Font ### bold , extrabold , light , book, medium, black ###
    }

    Header{
        id:header
        title_text:title
        text_timer : txt_time
    }

    Item{
        id: timer
        property int secon  : 90
        Timer{
            id:timer_note
            interval:1000
            repeat:true
            running:true
            triggeredOnStart:true
            onTriggered:{
                timer.secon -= 1
                txt_time = timer.secon
                if(timer.secon < 1){
                    timer_note.stop()
                    my_stack_view.push(time_out)
                }
            }
        }
    }

    NumberAnimation {
        id:hide_box_size
        targets: notif_box_size
        properties: "y"
        from:83
        to:768
        duration: 500
        easing.type: Easing.InOutBack
    }

    NumberAnimation {
        id:show_box_size
        targets: notif_box_size
        properties: "y"
        from:768
        to:83
        duration: 500
        easing.type: Easing.InOutBack
    }
    

    DimmDisplay{
        id:dimm_display
        visible:false
    }

    NotifSwipe{
        id:notif_box_size
        title_text:qsTr("Pilih Ukuran Loker")
        titlebody_x:53
        body_y:36
        r_height: 685
        // source_img:"asset/box/background_boxsize.png"
        img_y:40
        img_height: 645

        Image{
            x: 42
            y: 118
            width:45
            height:45
            source: "asset/point_icon/lamp.png"
        }

        Text{
            x:99
            y:128
            text: qsTr("Pastikan barang kamu sesuai dengan ukuran loker.")
            font.pixelSize:18
            color:"#ffa90e"
            font.family: fontStyle.book
        }

        Image{
            id: box_xs
            x:41
            y:190
            width:177
            height:385
            source: "asset/box/XS_disable.png"
            enabled:false

            Rectangle {
                id:status_boxXS
                x: 130
                y: 7
                width:41
                height:41
                color: "#a29f9f"
                radius: width*0.5
                visible:  true
                Text {
                    id:sumsizeXS
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.horizontalCenter: parent.horizontalCenter
                    color: "#ffffff"
                    text: "0"
                    font.pixelSize:25
                    font.family: fontStyle.book
                }
            }

            MouseArea{
                anchors.fill: parent
                onClicked: {
                    box_choose = "MINI"
                    box_selected(box_choose);
                }
            }
        }

        Image{
            id: box_s
            x:232
            y:190
            width:177
            height:385
            source: "asset/box/S_disable.png"
            enabled:false
            Rectangle {
                id:status_boxS
                x: 130
                y: 7
                width:41
                height:41
                color: "#a29f9f"
                radius: width*0.5
                visible: true
                Text {
                    id:sumsizeS
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.horizontalCenter: parent.horizontalCenter
                    color: "#ffffff"
                    text: "0"
                    font.pixelSize:25
                    font.family: fontStyle.book
                }
            }

            MouseArea{
                anchors.fill: parent
                onClicked: {
                    box_choose = "S"
                    box_selected(box_choose);
                }
            }
        }

        Image{
            id: box_m
            x:420
            y:190
            width:177
            height:385
            source: "asset/box/M_disable.png"
            enabled:false

            Rectangle {
                id:status_boxM
                x: 130
                y: 7
                width:41
                height:41
                color: "#a29f9f"
                radius: width*0.5
                visible: true
                Text {
                    id:sumsizeM
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.horizontalCenter: parent.horizontalCenter
                    color: "#ffffff"
                    text: "0"
                    font.pixelSize:25
                    font.family: fontStyle.book
                }
            }

            MouseArea{
                anchors.fill: parent
                onClicked: {
                    box_choose = "M"
                    box_selected(box_choose);
                }
            }
        }

        Image{
            id: box_l
            x:611
            y:190
            width:177
            height:385
            source: "asset/box/L_disable.png"
            enabled:false

            Rectangle {
                id:status_boxL
                x: 130
                y: 7
                width:41
                height:41
                color: "#a29f9f"
                radius: width*0.5
                visible: true
                Text {
                    id:sumsizeL
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.horizontalCenter: parent.horizontalCenter
                    color: "#ffffff"
                    text: "0"
                    font.pixelSize:25
                    font.family: fontStyle.book
                }
            }

            MouseArea{
                anchors.fill: parent
                onClicked: {
                    box_choose = "L"
                    box_selected(box_choose);
                }
            }
        }

        Image{
            id: box_xl
            x:809
            y:190
            width:177
            height:385
            source: "asset/box/XL_disable.png"
            enabled:false

            Rectangle {
                id:status_boxXL
                x: 130
                y: 7
                width:41
                height:41
                color: "#a29f9f"
                radius: width*0.5
                visible: true
                Text {
                    id:sumsizeXL
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.horizontalCenter: parent.horizontalCenter
                    color: "#ffffff"
                    text: "0"
                    font.pixelSize:25
                    font.family: fontStyle.book
                }
            }

            MouseArea{
                anchors.fill: parent
                onClicked: {
                    box_choose = "XL"
                    box_selected(box_choose);
                }
            }
        }



        // NotifButtonLong{
        //     id: xs
        //     x:32
        //     y:155
        //     buttonText:"XS"
        //     color_font: "#f1f1f1"
        //     lefttext: 30
        //     font_size:60
        //     r_width : 439
        //     r_height: 85
        //     r_radius:2
        //     buttonColor: "#c4c4c4"
        //     insertImage:false
        //     enabled:false

        //     Rectangle{
        //         x: 339
        //         y: 0
        //         width: 100
        //         height: 85
        //         color:"black"
        //         opacity: 0.15
        //     }
        //     Rectangle {
        //         id:status_boxXS
        //         x: 375
        //         y: 41
        //         width:30
        //         height:30
        //         color: "white"
        //         border.color: "white"
        //         border.width: 1
        //         radius: width*0.5
        //         visible:false
        //         Text {
        //             id:sumsizeXS
        //             x:8
        //             y:8
        //             color: "red"
        //             text: "10"
        //             font.pixelSize:12
        //         }
        //     }
        //     Text{
        //         x:115
        //         y:31
        //         text: qsTr("18x19x48 cm")
        //         font.pixelSize:22
        //         color:"#f1f1f1"
        //         font.family: fontStyle.medium
        //     }
        //     Text{
        //         id:boxxs
        //         x:352
        //         y:37
        //         text: qsTr("Tidak Tersedia")
        //         font.pixelSize:12
        //         color:"#ffffff"
        //         font.family: fontStyle.medium
        //     }
        //     MouseArea{
        //         anchors.fill: parent
        //         onClicked: {
        //             box_choose = "MINI"
        //             box_selected(box_choose);
        //         }
        //         onEntered:{
        //             xs.buttonColor = plus
        //         }
        //         onExited:{
        //             xs.buttonColor = red1
        //         }
        //     }
        // }

        // NotifButtonLong{
        //     id: s
        //     x:32
        //     y:250
        //     buttonText:"S"
        //     color_font: "#f1f1f1"
        //     lefttext: 30
        //     font_size:60
        //     r_width : 439
        //     r_height: 85
        //     r_radius:2
        //     buttonColor: "#c4c4c4"
        //     insertImage:false
        //     enabled:false

        //     Rectangle{
        //         x: 339
        //         y: 0
        //         width: 100
        //         height: 85
        //         color:"black"
        //         opacity: 0.15
        //     }
        //     Rectangle {
        //         id:status_boxS
        //         x: 375
        //         y: 41
        //         width:30
        //         height:30
        //         color: "white"
        //         border.color: "white"
        //         border.width: 1
        //         radius: width*0.5
        //         visible:false
        //         Text {
        //             id:sumsizeS
        //             x:8
        //             y:8
        //             color: "red"
        //             text: "10"
        //             font.pixelSize:12
        //         }
        //     }
        //     Text{
        //         x:115
        //         y:31
        //         text: qsTr("10x34x48 cm")
        //         font.pixelSize:22
        //         color:"#f1f1f1"
        //         font.family: fontStyle.medium
        //     }
        //     Text{
        //         id:boxs
        //         x:352
        //         y:37
        //         text: qsTr("Tidak Tersedia")
        //         font.pixelSize:12
        //         color:"#ffffff"
        //         font.family: fontStyle.medium
        //     }

        //     MouseArea{
        //         anchors.fill: parent
        //         onClicked: {
        //             box_choose = "S"
        //             box_selected(box_choose);
        //         }
        //         onEntered:{
        //             s.buttonColor = plus
        //         }
        //         onExited:{
        //             s.buttonColor = red1
        //         }
        //     }
        // }

        // NotifButtonLong{
        //     id: m
        //     x:32
        //     y:345
        //     buttonText:"M"
        //     color_font: "#f1f1f1"
        //     lefttext: 30
        //     font_size:60
        //     r_width : 439
        //     r_height: 85
        //     r_radius:2
        //     buttonColor: "#c4c4c4"
        //     insertImage:false
        //     enabled:false

        //     Rectangle{
        //         x: 339
        //         y: 0
        //         width: 100
        //         height: 85
        //         color:"black"
        //         opacity: 0.15
        //     }
        //     Rectangle {
        //         id:status_boxM
        //         x: 375
        //         y: 41
        //         width:30
        //         height:30
        //         color: "white"
        //         border.color: "white"
        //         border.width: 1
        //         radius: width*0.5
        //         visible:false
        //         Text {
        //             id:sumsizeM
        //             x:8
        //             y:8
        //             color: "red"
        //             text: "10"
        //             font.pixelSize:12
        //         }
        //     }
        //     Text{
        //         x:115
        //         y:31
        //         text: qsTr("18x34x48 cm")
        //         font.pixelSize:22
        //         color:"#f1f1f1"
        //         font.family: fontStyle.medium
        //     }
        //     Text{
        //         id:boxm
        //         x:352
        //         y:37
        //         text: qsTr("Tidak Tersedia")
        //         font.pixelSize:12
        //         color:"#ffffff"
        //         font.family: fontStyle.medium
        //     }
        //     MouseArea{
        //         anchors.fill: parent
        //         onClicked: {
        //             box_choose = "M"
        //             box_selected(box_choose);
        //         }
        //         onEntered:{
        //             m.buttonColor = plus
        //         }
        //         onExited:{
        //             m.buttonColor = red1
        //         }
        //     }
        // }
        // NotifButtonLong{
        //     id: l
        //     x:32
        //     y:440
        //     buttonText:"L"
        //     color_font: "#f1f1f1"
        //     lefttext: 30
        //     font_size:60
        //     r_width : 439
        //     r_height: 85
        //     r_radius:2
        //     buttonColor: "#c4c4c4"
        //     insertImage:false
        //     enabled:false

        //     Rectangle{
        //         x: 339
        //         y: 0
        //         width: 100
        //         height: 85
        //         color:"black"
        //         opacity: 0.15
        //     }
        //     Rectangle {
        //         id:status_boxL
        //         x: 375
        //         y: 41
        //         width:30
        //         height:30
        //         color: "white"
        //         border.color: "white"
        //         border.width: 1
        //         radius: width*0.5
        //         visible:false
        //         Text {
        //             id:sumsizeL
        //             x:8
        //             y:8
        //             color: "red"
        //             text: "10"
        //             font.pixelSize:12
        //         }
        //     }
        //     Text{
        //         x:115
        //         y:31
        //         text: qsTr("33x34x48 cm")
        //         font.pixelSize:22
        //         color:"#f1f1f1"
        //         font.family: fontStyle.medium
        //     }
        //     Text{
        //         id:boxl
        //         x:352
        //         y:37
        //         text: qsTr("Tidak Tersedia")
        //         font.pixelSize:12
        //         color:"#ffffff"
        //         font.family: fontStyle.medium
        //     }
        //     MouseArea{
        //         anchors.fill: parent
        //         onClicked: {
        //             box_choose = "L"
        //             box_selected(box_choose);
        //         }
        //         onEntered:{
        //             l.buttonColor = plus
        //         }
        //         onExited:{
        //             l.buttonColor = red1
        //         }
        //     }
        // }

        // NotifButtonLong{
        //     id: xl
        //     x:32
        //     y:535
        //     buttonText:"XL"
        //     color_font: "#f1f1f1"
        //     lefttext: 30
        //     font_size:60
        //     r_width : 439
        //     r_height: 85
        //     r_radius:2
        //     buttonColor: "#c4c4c4"
        //     insertImage:false
        //     enabled:false

        //     Rectangle{
        //         x: 339
        //         y: 0
        //         width: 100
        //         height: 85
        //         color:"black"
        //         opacity: 0.15
        //     }
        //     Rectangle {
        //         id:status_boxXL
        //         x: 375
        //         y: 41
        //         width:30
        //         height:30
        //         color: "white"
        //         border.color: "white"
        //         border.width: 1
        //         radius: width*0.5
        //         visible:false
        //         Text {
        //             id: sumsizeXL
        //             x:8
        //             y:8
        //             color: "red"
        //             text: "10"
        //             font.pixelSize:12
        //         }
        //     }
        //     Text{
        //         x:115
        //         y:31
        //         text: qsTr("101x24x48 cm")
        //         font.pixelSize:22
        //         color:"#f1f1f1"
        //         font.family: fontStyle.medium
        //     }
        //     Text{
        //         id:boxxl
        //         x:352
        //         y:37
        //         text: qsTr("Tidak Tersedia")
        //         font.pixelSize:12
        //         color:"#ffffff"
        //         font.family: fontStyle.medium
        //     }
        //     MouseArea{
        //         anchors.fill: parent
        //         onClicked: {
        //             box_choose = "XL"
        //             box_selected(box_choose);
        //         }
        //     }
        // }

        // Rectangle{
        //     x:915
        //     y:45
        //     width: 80
        //     height: 80
        //     color:"transparent"
        //     Image {
        //         x:20
        //         y:20
        //         width:40
        //         height:40
        //         visible:img_vis
        //         source:"asset/global/button/close.png"
        //     }
        //     MouseArea {
        //         anchors.fill: parent
        //         onClicked: {
        //             hide_animation("size");
        //             my_stack_view.pop()
        //         }
        //     }
        // }
    }

    Component.onCompleted: {
        root.free_mouth_result.connect(show_free_mouth_num);
        root.choose_mouth_result.connect(open_door_locker)
        root.mouth_number_result.connect(door_locker_choosen)
        root.choose_mouth_for_change_size_result.connect(door_change_size)
        root.set_value_change_size_result.connect(change_size_locker)
        // root.store_customer_express_result_result.connect(response_store_reject)
        // root.store_express_result.connect(store_lastmile_result)
        
    }

    Component.onDestruction: {
        root.free_mouth_result.disconnect(show_free_mouth_num);
        root.choose_mouth_result.disconnect(open_door_locker)
        root.mouth_number_result.disconnect(door_locker_choosen)
        root.choose_mouth_for_change_size_result.disconnect(door_change_size)
        root.set_value_change_size_result.disconnect(change_size_locker)

        // root.store_customer_express_result_result.disconnect(response_store_reject)
        // root.store_express_result.disconnect(store_lastmile_result)
    }

    function hide_animation(param){
        if(param=="size")hide_box_size.start();
        dimm_display.visible=false
        press=0;
    }
    function show_free_mouth_num(locker_size){
        var obj = JSON.parse(locker_size)
        for(var i in obj){
            if(i == "XL"){
                size_XL = obj[i]
                if(obj[i]>0){
                    // boxxl.text="Tersedia"
                    // status_boxXL.visible=true
                    box_xl.enabled = true
                    box_xl.source = "asset/box/XL.png"
                    sumsizeXL.text= obj[i]
                    status_boxXL.color = "#ffa90e"
                    // xl.buttonColor = "#ff524f"
                    // boxxl.x =366
                    // boxxl.y =17
                    // xl.enabled=true
                }
            }
            if(i == "L"){
                size_L = obj[i]
                if(obj[i]>0){
                    // boxl.text="Tersedia"
                    // status_boxL.visible=true
                    box_l.enabled = true
                    box_l.source = "asset/box/L.png"
                    sumsizeL.text= obj[i]
                    status_boxL.color = "#ffa90e"
                    // l.buttonColor = "#ff524f"
                    // boxl.x =366
                    // boxl.y =17
                    // l.enabled=true
                }
            }
            if(i == "M"){
                size_M = obj[i]
                if(obj[i]>0){
                    // boxm.text="Tersedia"
                    // status_boxM.visible=true
                    box_m.enabled = true
                    box_m.source = "asset/box/M.png"
                    sumsizeM.text= obj[i]
                    status_boxM.color = "#ffa90e"
                    // m.buttonColor = "#ff524f"
                    // boxm.x =366
                    // boxm.y =17
                    // m.enabled=true
                }
            }
            if(i == "S"){
                size_S = obj[i]
                if(obj[i]>0){
                    // boxs.text="Tersedia"
                    // status_boxS.visible=true
                    box_s.enabled = true
                    box_s.source = "asset/box/S.png"
                    sumsizeS.text= obj[i]
                    status_boxS.color = "#ffa90e"
                    // s.buttonColor = "#ff524f"
                    // boxs.x =366
                    // boxs.y =17
                    // s.enabled=true
                }
            }
            if(i == "MINI"){
                size_XS = obj[i]
                if(obj[i]>0){
                    // boxxs.text="Tersedia"
                    // status_boxXS.visible=true
                    box_xs.enabled = true
                    box_xs.source = "asset/box/XS.png"
                    sumsizeXS.text= obj[i]
                    status_boxXS.color = "#ffa90e"
                    // sumsizeXS.text = "10"
                    // xs.buttonColor = "#ff524f"
                    // boxxs.x =366
                    // boxxs.y =17
                    // xs.enabled=true
                }
            }
        }
    } 

    function choose_door(door_size,type) {
        console.log(" box_size: " + door_size)
                    
        if (door_size != "") {
            if (type == "lastmile") {
                slot_handler.start_choose_mouth_size(door_size,"staff_store_express")
            } else if (type == "change_size") {
                slot_handler.start_choose_mouth_for_change_size(door_size, "change_size")
            } else {
                slot_handler.start_choose_mouth_size(door_size, "customer_store_express") 
            }
        }
    }

    function door_locker_choosen(data) {
        
        console.log("doooorrr: " + data)
        
        var result = JSON.parse(data)
        if (result.isSuccess == "true") {
            locker_number = result.locker_number
            door_type = result.locker_number_size
            console.log(" locker_number_choosen: " + locker_number)
        }
    }

    function open_door_locker(result) { // OpenDoor Harus Setelah dapat balikan dari slot.handler
        
        console.log("hasil_open_door_locker: " + result)
        
        if (result == 'Success') {
            slot_handler.get_express_mouth_number()
            
            if (popsafe_status == "return") {
                // slot_handler.start_store_customer_reject_for_electronic_commerce()
                my_stack_view.push(open_door,{opendoor_status:popsafe_status, nomorLoker: locker_number, header_title: "RETURN", door_type: door_type, popsafe_status: popsafe_status, changebox_state: true, change_locker: is_change_locker,access_door: true, button_type:3})
            } else if (popsafe_status == "lastmile" || popsafe_status == "courier_store") {
                // slot_handler.start_store_express();
                
                if (popsafe_status == "courier_store") {
                    my_stack_view.push(open_door, {nomorLoker: locker_number, door_type: door_type, opendoor_status: "courier", popsafe_status: popsafe_status, drop_by_courier: drop_by_courier, changebox_state: true, change_locker: is_change_locker, drop_by_courier: drop_by_courier, id_company: id_company, phone_courier: phone_courier, status_courier: status_courier, select_courier: select_courier, drop_by_other_company: drop_by_other_company, access_door: true, button_type:3})
                } else {
                    console.log('opeeen dooorrrr')
                    my_stack_view.push(open_door, {nomorLoker: locker_number, door_type: door_type, popsafe_status: popsafe_status, changebox_state: true, drop_by_courier: drop_by_courier, change_locker: is_change_locker, drop_by_kirim: drop_by_kirim,drop_by_other_company: drop_by_other_company, id_company: id_company, phone_courier: phone_courier, status_courier: status_courier, select_courier: select_courier, access_door: true, button_type:4})
                }
            } else if (popsafe_status == 'ondemand') {
                // slot_handler.start_store_customer_express() //Bermasalah Balikan sama dengan return
                my_stack_view.push(open_door, {nomorLoker: locker_number, door_type: door_type, popsafe_status: popsafe_status, changebox_state: true, change_locker: is_change_locker,access_door: true,button_type:3})
            } else if(popsafe_status == 'popsend'){
                // slot_handler.start_store_customer_express();
                my_stack_view.push(open_door,{nomorLoker: locker_number, header_title: "POPSEND", door_type: door_type, popsafe_status: popsafe_status, changebox_state: true, change_locker: is_change_locker,access_door: true,button_type:3})
            } else if (popsafe_status=="app") {
                my_stack_view.push(open_door, {nomorLoker: locker_number, door_type: door_type, popsafe_status: popsafe_status, isPopDeposit:isPopDeposit, access_door: true,button_type:3})
                
            }

        }
        if (result == 'NotMouth') {
            console.log("Status : " + result)
        }
    }

    function change_size_locker(response) {
        
        console.log("response change locker: " + response)
        var result = JSON.parse(response)
        if (result.isSuccess == "true") {
            if (result.change_locker != "") {
                is_change_locker = true
                change_locker = true
                express_id = result.data.express_id
                opendoor_status = result.data.opendoor_status
                console.log("express_id_door_change_size_set: " + express_id)
            }
        }
        
    }

    
    function door_change_size(result) {
        console.log('door_change_size_result' + result)
        if (result == 'Success') {
            slot_handler.get_express_mouth_number()
            
            console.log("express_id_door_change_size: " + express_id)
            
            my_stack_view.push(open_door, {nomorLoker: locker_number, door_type: door_type, change_locker: true, express_id: express_id, opendoor_status: opendoor_status, access_door: true, button_type:3})
        }
    }

    function box_selected(box_choose) {
        console.log(popsafe_status)
        if (is_change_locker == true) {
            choose_door(box_choose, "change_size")            
        } else {
            if (popsafe_status=="inlocker") {
                my_stack_view.push(select_payment, {show_img: show_img, phone_number: phone_number, box_size: box_choose, door_type: door_type})
            }else if(popsafe_status =="app"){ 
                choose_door(box_choose, popsafe_status)
            }else if(popsafe_status == "return") {
                choose_door(box_choose, popsafe_status)
            }else if(popsafe_status == "lastmile") {
                choose_door(box_choose, popsafe_status)
            }else if(popsafe_status == "ondemand") {
                choose_door(box_choose, popsafe_status)
            }else if (popsafe_status == "popsend") {
                choose_door(box_choose, popsafe_status)            
            }else if (popsafe_status == "courier_store") {
                choose_door(box_choose, popsafe_status)            
            }
        }
        hide_animation("size");
    }
    
}