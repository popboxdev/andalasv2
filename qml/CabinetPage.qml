import QtQuick 2.4
import QtQuick.Controls 1.3
import QtQuick.Controls.Styles 1.3

Background{
    id: base
    width: 1024
    height: 768
    property var colortext_status: "#000000"
    property var userAccess
    property var show_box_number:""
    property var show_size:""
    property var asset_path: "asset/courier/cabinet/"
    property var show_image_status:asset_path + "fulllocker.png"
    property var show_image_size:asset_path + "c.png"
    property int timer_value: 1800
    property var press : "0"
    property var press_time:1
    property var box_id
    property var box_size
    property var box_status
    property var box_number
    property var json_express: new Array()
    property var id_express
    property var id_express_temp:""
    property var show_box_id_express:""
    property var show_id_express:""
    property var show_box_number_temp
    property var show_size_temp
    property var show_image_status_temp
    property var show_image_size_temp
    property var show_box_status
    property var box_id_list
    property var set_box_window_show_id:""
    property var set_box_window_show_last_store_time:""
    property var set_box_window_show_boxsize:""
    property var set_box_window_show_boxnum:""
    property var set_box_window_show_box_status:""
    property var set_box_window_show_box_id: ""
    property var setbox_status
    property var cabinet_data
    property var cabinet_num: 25
    property var page_num
    property var set_status_time:1
    property int size:10
    property var cabinet_total
    property var overdue_data: ""
    property var express_data: ""
    property var overdue_num
    property var overdue_id
    property var cabinet_page
    property bool isOverdue : false
    property bool enable_status : false
    property bool grid_act : false

    property var courierUsername: undefined
    property var courierName: undefined

    property var txt_numpress: ""
    property var txt_numbox: ""
    property var txt_sizebox: ""
    property var txt_duration: ""
    property var txt_storetime: ""
    property var txt_overduetime: ""
    property var txt_handphone: ""

    // ON FUNCTION
    
    property bool locker_enable: true
    property var locale: Qt.locale()
    property date currentDate: new Date()
    property var express_num: ""
    property var store_time: ""
    property var overdue_time: ""
    property var duration_time: ""
    property var id_mouth: ""
    property var no_telp: ""
    property var status_buka: ""
    property bool buka_semua_vis: false
    property bool buka_kosong_vis: false
    
    
    // ON GRIDDEL
    property var express_num_grid: ""
    property var store_time_grid: ""
    property var overdue_time_grid: ""
    property var duration_time_grid: ""
    property var no_telp_grid: ""
    property var color_detail: ""
    property bool isOverdue_grid: false


    Stack.onStatusChanged:{
        if(Stack.status==Stack.Activating){
            startup();
            user_access();
            console.log("USER Access ManageCabinet: ", userAccess)
        }
        if(Stack.status==Stack.Deactivating){
            my_timer.stop()
            timer_startup.start();
            slot_handler.stop_user_bypass()
        }
    }
    

    Rectangle{
        QtObject{
            id:abc
            property int counter
            Component.onCompleted:{
                abc.counter = timer_value
            }
        }

        Timer{
            id:my_timer
            interval:1000
            repeat:true
            running:true
            triggeredOnStart:true
            onTriggered:{
                abc.counter -= 1
                if(abc.counter < 0){
                    my_timer.stop()
                    my_stack_view.push(time_out)
                    // my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
                }
            }
        }
    }

    Timer {
        id: timer_startup
        interval: 1000
        repeat: false
        running: true
        onTriggered:{
            call_slot();
            dimm_display.visible = false
            waiting.visible = false
            press = '0';
        }
    }

    FontType{
        id:fontStyle
        //Type Font ### bold , extrabold , light , book, medium, black ###
    }

    Header{
        id:header
        title_text:qsTr("CABINET")
        // img_vis:false
        timer_view:false
    }

    Item  {
        id: page
        x:50
        y:252
        width:906
        height:460

        GridView{
            id:gridview
            anchors.fill: parent
            delegate: griddel
            model:gridModel
            cellWidth: 190
            cellHeight: 80
            flow:Grid.TopToBottom
            interactive: false
            contentY: listview.contentY
        }

        ListView {
            id: listview
            anchors.rightMargin: 0
            anchors.bottomMargin: 0
            anchors.leftMargin: 0
            anchors.topMargin: 0
            anchors.fill: parent
            model: listModel
            delegate: delegate
            spacing:0
            interactive:false
        }

        Component {
            id: griddel

            Rectangle {
                color: "transparent"

                Text{
                    id:text_show_box_status
                    font.family:fontStyle.book
                    text:show_box_status
                    visible:false
                    anchors.horizontalCenterOffset: 810
                    color: colortext_status
                    font.pixelSize:17
                    anchors.centerIn: parent;
                }

                Text{
                    id:text_show_box_size
                    font.family:fontStyle.book
                    text:show_box_size
                    visible:false
                    anchors.horizontalCenterOffset: 810
                    color: colortext_status
                    font.pixelSize:17
                    anchors.centerIn: parent;
                }

                Image {
                    id: img_numlocker
                    x: 0
                    y: 0
                    width: 150
                    height: 50
                    source: show_image_status
                    enabled: enable_status

                    Text {
                        id: txt_numlocker
                        x: 0
                        y: 0
                        width: 103
                        height: 50
                        color: "#ffffff"
                        font.family: fontStyle.medium
                        font.pixelSize: 22
                        verticalAlignment: Text.AlignVCenter
                        horizontalAlignment: Text.AlignHCenter
                        text:show_box_number
                    }

                    MouseArea{
                        anchors.fill: parent
                        onClicked: {
                            
                            console.log("BOX STATUS : ", show_box_status)
                            
                            if(show_box_status=="USED" && show_id_express!=""){
                                show_used.start();
                                if(isOverdue_grid==true) color_detail="#feb005" 
                                else color_detail="#0fc3e1"
                                
                                // console.log("OVERDUE GRID: " + isOverdue_grid)
                                
                            }else if(show_box_status=="ENABLE"){
                                show_enable.start();
                                locker_enable = true;
                            }else if(show_box_status=="LOCKED"){
                                show_enable.start();
                                locker_enable = false;
                            }else{
                                console.log("NOT SYNC LOCKER");
                                show_sync.start();
                            }
                            dimm_display.visible =true;
                            abc.counter = timer_value
                            my_timer.restart()
                            txt_numbox=show_box_number
                            txt_overduetime = overdue_time_grid
                            txt_storetime = store_time_grid
                            txt_duration = duration_time_grid
                            txt_handphone = no_telp_grid
                            txt_sizebox=text_show_box_size.text
                            txt_numpress= express_num_grid
                            setbox_status = show_box_status
                            box_id_list = show_box_id
                            show_box_id_express = show_id_express
                        }
                    }
                }

                Image {
                    id: image2
                    x: 100
                    y: 0
                    width: 50
                    height: 50
                    source: show_image_size

                    Text {
                        id: text2
                        x: 0
                        y: 0
                        width: 50
                        height: 50
                        text:show_size
                        verticalAlignment: Text.AlignVCenter
                        horizontalAlignment: Text.AlignHCenter
                        font.pixelSize: 22
                        font.family: fontStyle.book
                    }
                }
            }
        }

        ListModel {
            id: gridModel
        }

        Component{
            id:delegate
            Rectangle{
                width: 150
                height: 70
                color: "transparent"
            }
        }

        ListModel{
            id:listModel
        }
    }

    NotifButton{
        id:btn_open_all
        x:53
        y:676
        visible:buka_semua_vis
        r_width:160
        r_height:50
        r_radius:2
        buttonText:qsTr("Buka Semua")
        buttonColor: "#ff524f"
        modeReverse: false
        MouseArea{
            anchors.fill: parent
            onClicked: {
                status_buka = "ALL";
                show_buka_semua.start();
                dimm_display.visible=true;
                console.log("PENCET Buka Semua: ")
            }
            onEntered:{
                btn_open_all.modeReverse = true
            }
            onExited:{
                btn_open_all.modeReverse = false
            }
        }
    }

    NotifButton{
        id:btn_open_empty
        x:230
        y:676
        visible:buka_kosong_vis
        r_width:160
        r_height:50
        r_radius:2
        buttonText:qsTr("Buka Kosong")
        buttonColor: "#ff524f"
        modeReverse: true
        MouseArea{
            anchors.fill: parent
            onClicked: {
                status_buka = "EMPTY";
                show_buka_semua.start();
                dimm_display.visible=true;
                console.log("PENCET Buka Semua Kosong: ")
            }
            onEntered:{
                btn_open_empty.modeReverse = false
            }
            onExited:{
                btn_open_empty.modeReverse = true
            }
        }
    }

    Image {
        x: 510
        y: 682
        width: 455
        height: 20
        source: asset_path +  "legend.png"
        Text {
            x: 25
            y: 0
            text: qsTr("Failed")
            font.family:fontStyle.medium
            font.pointSize: 12
            color: colortext_status
        }

        Text {
            x: 125
            y: 0
            text: qsTr("Disable")
            font.family:fontStyle.medium
            font.pointSize: 12
            color: colortext_status
        }

        Text {
            x: 215
            y: 0
            text: qsTr("Kosong")
            font.family:fontStyle.medium
            font.pointSize: 12
            color: colortext_status
        }

        Text {
            x: 310
            y: 0
            text: qsTr("Penuh")
            font.family:fontStyle.medium
            font.pointSize: 12
            color: colortext_status
        }

        Text {
            x: 395
            y: 0
            text: qsTr("Terlambat")
            font.family:fontStyle.medium
            font.pointSize: 12
            color: colortext_status
        }
    }

    Text {
        x: 50
        y: 152
        width: 300
        height: 50
        text: qsTr("Total Cabinet") + " : " + cabinet_total
        font.family:fontStyle.book
        color: colortext_status
        textFormat: Text.PlainText
        font.pointSize: 30
        horizontalAlignment: Text.AlignLeft
        verticalAlignment: Text.AlignBottom
    }

    Image {
        x: 50
        y: 210
        width: 911
        source: asset_path + "stripe.png"
    }

    Image {
        x: 50
        y: 664
        width: 911
        source: asset_path + "stripe.png"
    }

    Rectangle{
        id:up_button
        y:155
        x:833
        width:46
        height:46
        color:"transparent"

        Image{
            width:46
            height:46
            source: asset_path + "back.png"
        }

        MouseArea {
            anchors.fill: parent
            onClicked: {
                abc.counter = timer_value
                my_timer.restart()
                gridModel.clear()
                listModel.clear()
                cabinet_data = ""
                down_button.visible = true
                down_button.enabled = true
                press_time = press_time-1
                slot_handler.start_load_mouth_list(press_time)
                if(press_time == 1){
                    up_button.visible = false
                    up_button.enabled = false
                }
            }
        }
    }

    Rectangle{
        id:down_button
        y:155
        x:908
        width:46
        height:46
        color:"transparent"
        anchors.right: parent.right
        anchors.rightMargin: 80

        Image{
            width:46
            height:46
            source: asset_path + "next.png"
        }

        MouseArea {
            anchors.fill: parent
            onClicked: {
                abc.counter = timer_value
                my_timer.restart()
                gridModel.clear()
                listModel.clear()
                cabinet_data = ""
                up_button.visible = true
                up_button.enabled = true
                press_time = press_time+1
                slot_handler.start_load_mouth_list(press_time)
                console.log("Page NUM : ", page_num)
                if(press_time == cabinet_page){
                    down_button.visible = false
                    down_button.enabled = false
                }
            }
        }
    }

    Rectangle{
        id:buka_semua
        visible:false
        width: 1080
        height: 768
        color: "#AA4A4A4A"
    }

    Component.onCompleted:{
        root.all_express_list_result.connect(all_express_list)
        root.overdue_express_list_result.connect(overdue_express_list)
        root.overdue_express_count_result.connect(overdue_count)

        root.manager_mouth_count_result.connect(mouth_count)
        root.mouth_list_result.connect(show_mouth_list)
        // root.overdue_list_result.connect(show_overdue_list)
        root.load_mouth_list_result.connect(load_list_result)
        root.manager_set_mouth_result.connect(log_text)
        root.manager_open_mouth_by_id_result.connect(log_text)
        root.manager_open_all_mouth_result.connect(log_text)
    }

    Component.onDestruction:{
        root.all_express_list_result.disconnect(all_express_list)
        root.overdue_express_list_result.disconnect(overdue_express_list)
        root.overdue_express_count_result.disconnect(overdue_count)

        root.manager_mouth_count_result.disconnect(mouth_count)
        root.mouth_list_result.disconnect(show_mouth_list)
        // root.overdue_list_result.disconnect(show_overdue_list)
        root.load_mouth_list_result.disconnect(load_list_result)
        root.manager_set_mouth_result.disconnect(log_text)
        root.manager_open_mouth_by_id_result.disconnect(log_text)
        root.manager_open_all_mouth_result.disconnect(log_text)
    }

    function all_express_list(text){
        express_data = text
        // var obj = JSON.parse(text)
        // var str_json = JSON.stringify(text)
        // console.log("EXPRESS ALL : " + text)
    }

    function overdue_express_list(text){
        overdue_data = text
        // combine_param(overdue_data)
        var obj = JSON.parse(overdue_data)
        for(var i in obj){
            overdue_id = obj[i].id
            // console.log("OVERDUE LIST : ", i + "OVERDUE ID : ", overdue_id)
        }
        // show_overdue_express(text)
        if(grid_act==false){
            gridModel.clear()
            listModel.clear()
            grid_act=true
        }
    }

    function overdue_count(text){
        overdue_num = text
        // console.log("OVERDUE NUMBER : ", text)
        // count_page(text)
        // if(text<=5){
        //     down_button.enabled = false
        //     down_button.visible = false
        // }
        // else{
        //     down_button.enabled = true
        //     down_button.visible = true
        // }
    }

    function load_list_result(text){
        console.log("load_list_result : ", text)
        // waiting.close()
        page.enabled = true
        // exit_btn.enabled = true
        // pick_up_Button1.enabled = true
        up_button.enabled = true
        down_button.enabled = true
    }

    function show_mouth_list(text){
        // console.log("mouth_list_result : ", text)
        cabinet_data = text
        show_cabinet_data(cabinet_data)
    }

    function count_page(cabinet_num){
        var xcabinet = cabinet_num/25
        var mathfloor = Math.floor(cabinet_num/25)
        // console.log("if xcabinet : ", page_num)
        // console.log("if mathfloor : ", page_num)

        if(cabinet_num/25 > Math.floor(cabinet_num/25)){
            page_num = Math.floor(cabinet_num/25)+1
            // console.log("if page_num : ", page_num)
        }
        // else
        //     page_num = cabinet_num/25
        //     console.log("else page_num : ", page_num)
    }

    function mouth_count(text){
        // console.log("manager_mouth_count_result : ", text)
        cabinet_total = text
        cabinet_page = Math.ceil(text/25)
        
        console.log(" SISA : " + cabinet_page)
        
        cabinet_num = text
        count_page(cabinet_num)
        if(text<=25){
            down_button.enabled = false
            down_button.visible = false
        }
        else{
            down_button.enabled = true
            down_button.visible = true
        }
    }

    // function show_overdue_data(overdue_data){
    //     var obj = JSON.parse(overdue_data)
    // }

    function show_cabinet_data(cabinet_data){
        var obj = JSON.parse(cabinet_data)
        
        // console.log(" CABINET DATA : ", cabinet_data)
        
        for(var i in obj){
            id_express_temp = ""
            box_id = obj[i].id
            id_express = ""
            box_status = obj[i].status
            
            // console.log("BOX ID: ", obj[i])
            
            if (express_data!=""){
                var obj_exp = JSON.parse(express_data)
                // console.log(" CABINET DATA : ", express_data)
                for(var z in obj_exp){
                    id_mouth = obj_exp[z].mouth_id;
                    if(box_id==id_mouth){
                        id_express = obj_exp[z].id_express;
                        var date_overdue;
                        var start_date = new Date().getTime();
                        var duration_date = start_date - obj_exp[z].storeTime
                        var seconds = (duration_date / 1000);
                        duration_time = secondsToHms(seconds)
                        
                        // var convert_store = new Date(obj_exp[z].storeTime);
                        var date_store = Qt.formatDateTime(new Date(obj_exp[z].storeTime), "dd/MMM/yyyy hh:mm");
                        if(obj_exp[z].expressType=="CUSTOMER_REJECT"){
                            express_num = obj_exp[z].customerStoreNumber;
                            no_telp = obj_exp[z].storeUserPhoneNumber;
                            date_overdue = "-"
                        }else{
                            express_num = obj_exp[z].expressNumber;
                            no_telp = obj_exp[z].takeUserPhoneNumber;
                            // var convert_overdue = new Date(obj_exp[z].overdueTime);
                            date_overdue = Qt.formatDateTime(new Date(obj_exp[z].overdueTime), "dd/MMM/yyyy hh:mm");
                        }
                        store_time = date_store;
                        overdue_time = date_overdue;
                    }
                }
            }
            if(overdue_data!=""){
                var obj2 = JSON.parse(overdue_data)
                for(var x in obj2){
                    overdue_id = obj2[x].id
                    // console.log("OVERDUE LIST : ", overdue_id)
                    // console.log("OVERDUE LIST : ", id_express)
                    if (id_express==overdue_id){
                        isOverdue = true
                        // console.log("OVERDUE LIST : ", overdue_id)
                    }
                    // else{
                    //     isOverdue = false
                    // }
                }
            }
            // console.log("JUMLAH OVERDUE : ", isOverdue)
            // console.log("BOX STATUS ASLI : ", box_status)
            if(box_status == "ENABLE"){
                if((userAccess==1) || (userAccess==2) || (userAccess==3) || (userAccess==4) || (userAccess==5)){
                    enable_status = true
                    show_image_status_temp=asset_path + "freelocker.png"
                }else{
                    enable_status = false
                    show_image_status_temp = asset_path + "freelocker_inactive.png"
                }
            }else if(box_status == "USED" && id_express!=""){
                // console.log("EXPRESS LIST : ", id_express)
                // console.log("BOX STATUS SETELAH FILTER : ", box_status)
                if(userAccess==5 || userAccess==2){
                    enable_status = false
                    show_image_status_temp = asset_path + "freelocker_inactive.png"
                }else{
                    if(isOverdue == true){
                        show_image_status_temp=asset_path + "overduelocker.png"
                        isOverdue_grid = isOverdue
                        enable_status = true
                    }else{
                        if(userAccess==4){
                            enable_status = false
                            show_image_status_temp = asset_path + "freelocker_inactive.png"
                        }else{
                            enable_status = true
                            show_image_status_temp=asset_path + "fulllocker.png"
                        }
                        isOverdue_grid = false
                    }
                }
                id_express_temp = id_express
                
                // console.log("ID EXPRESSSSS : ", id_express_temp)
                
            } else if(box_status== "LOCKED"){
                show_image_status_temp=asset_path + "badlocker.png"
            } else{
                show_image_status_temp=asset_path + "notsynclocker.png"
            }
            // console.log("STATUS ENABLE ", enable_status)
            box_number = obj[i].number
            show_box_number_temp=box_number
            box_size = obj[i].name
            if(box_size=="MINI"){
                // show_image_size_temp=asset_path + "a.png"
                show_size_temp="XS"
            }
            else if(box_size=="S"){
                // show_image_size_temp=asset_path + "b.png"
                show_size_temp="S"
            }
            else if(box_size=="M"){
                // show_image_size_temp=asset_path + "c.png"
                show_size_temp="M"
            }
            else if(box_size=="L"){
                // show_image_size_temp=asset_path + "d.png"
                show_size_temp="L"
            }
            else{
                // show_image_size_temp=asset_path + "e.png"
                show_size_temp="XL"
            }
            
            // console.log("EXPRESS : ", id_express)
            
            show_image_size_temp=asset_path + "b.png"
            gridModel.append({"show_image_status":show_image_status_temp , "show_image_size": show_image_size_temp , "show_box_number": show_box_number_temp , "show_size": show_size_temp , "show_box_id":box_id , "show_box_size":box_size , "show_box_status":box_status, "show_id_express":id_express_temp, "enable_status":enable_status,"express_num_grid": express_num, "store_time_grid":store_time, "overdue_time_grid":overdue_time , "duration_time_grid":duration_time, "no_telp_grid":no_telp, "isOverdue_grid": isOverdue_grid});
            listModel.append({});
            isOverdue = false
        }
    }

    function log_text(text){
        console.log("log_text : ", text)
        box_id_list = ""
        if(text == 'Success'){
            console.log("LOKER BUKA : ", text)
            show_success.start();
        }
        else{
            console.log("LOKER BUKA : GAGAL")
            show_failed.start();
        }
    }

    DimmDisplay{
        id:dimm_display
        visible:false
    }

    AnimatedImage{
        id: waiting
        visible:false
        width: 200
        height: 200
        anchors.verticalCenter: parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter
        source: 'asset/global/animation/loading.gif'
    }

    NotifSwipe{
        id:notif_sync
        r_height: 684

        Rectangle {
            x:50
            y:60
            width: 190
            height: 95
            color: "#606060"
            Text{
                font.family: fontStyle.medium
                font.pixelSize:39
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: parent.verticalCenter
                color: "#ffffff"
                text: txt_numbox
            }
        }

        Rectangle {
            x:240
            y:60
            width: 80
            height: 95
            color: "#d8d8d8"
            Text{
                font.family: fontStyle.medium
                font.pixelSize:39
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: parent.verticalCenter
                text: txt_sizebox
            }
        }

        Text{
            x:50
            y:210
            font.pixelSize: 24
            font.family: fontStyle.book
            text: qsTr("STATUS") + "\t: " + qsTr("GAGAL SYNC")
        }

        Text{
            x:50
            y:260
            font.pixelSize: 24
            font.family: fontStyle.book
            text: qsTr("CATATAN") + "\t: " + qsTr("HUBUNGI TEAM IT SEBELUM SYNC LOKER")
        }

        NotifButton{
            x:50
            y:350
            r_radius:2
            modeReverse:true
            buttonText:qsTr("BUKA LOKER")
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    show_buka.start();
                    hide_sync.start();
                }
            }
        }
        NotifButton{
            x:320
            y:350
            r_radius:2
            buttonText:qsTr("SYNC LOKER")
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    console.log("box_id_list : " + box_id_list)
                    hide_sync.start();
                    setbox_status = "ENABLE"
                    slot_handler.start_manager_set_mouth(box_id_list,setbox_status)
                }

            }
        }
        Image{
            x:939
            y:60
            height:33
            width:33
            source:"asset/global/button/close.png"
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    hide_sync.start();
                    dimm_display.visible=false;
                }

            }
        }
    }

    NotifSwipe{
        id:notif_enable
        r_height: 684

        Rectangle {
            x:50
            y:60
            width: 190
            height: 95
            color: (locker_enable==true) ? "#03db11" : "#ff524f"
            Text{
                font.family: fontStyle.medium
                font.pixelSize:39
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: parent.verticalCenter
                color: "#ffffff"
                text: txt_numbox
            }
        }

        Rectangle {
            x:240
            y:60
            width: 80
            height: 95
            color: "#d8d8d8"
            Text{
                font.family: fontStyle.medium
                font.pixelSize:39
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: parent.verticalCenter
                text: txt_sizebox
            }
        }

        Text{
            x:50
            y:210
            font.pixelSize: 24
            font.family: fontStyle.book
            text: qsTr("STATUS") + "\t\t: "
        }

        Image{
            x:208
            y:195
            height:52
            width:151
            source:(locker_enable==true) ? asset_path + "lock_enable.png" : asset_path + "lock_disable.png"
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    
                    console.log(" setbox_status mm: " + setbox_status)
                    
                    if(locker_enable==true){
                        locker_enable=false;
                        setbox_status = "LOCKED"
                    } else {
                        locker_enable=true;
                        setbox_status = "ENABLE"
                    }
                }

            }
        }

        NotifButton{
            x:50
            y:350
            r_radius:2
            modeReverse:true
            buttonText:qsTr("BUKA LOKER")
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    show_buka.start();
                    hide_enable.start();
                }
            }
        }
        NotifButton{
            x:320
            y:350
            r_radius:2
            buttonText:qsTr("SIMPAN")
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    console.log("box_id_list : " + box_id_list)
                    hide_enable.start();
                    slot_handler.start_manager_set_mouth(box_id_list,setbox_status)
                }

            }
        }
        Image{
            x:939
            y:60
            height:33
            width:33
            source:"asset/global/button/close.png"
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    hide_enable.start();
                    dimm_display.visible=false;
                }

            }
        }
    }

    NotifSwipe{
        id:notif_used
        r_height: 684
        
        Rectangle {
            x:50
            y:60
            width: 190
            height: 95
            color: color_detail
            Text{
                font.family: fontStyle.medium
                font.pixelSize:39
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: parent.verticalCenter
                color: "#ffffff"
                text: txt_numbox
            }
        }

        Rectangle {
            x:240
            y:60
            width: 80
            height: 95
            color: "#d8d8d8"
            Text{
                font.family: fontStyle.medium
                font.pixelSize:39
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: parent.verticalCenter
                text: txt_sizebox
            }
        }

        Text{
            x:50
            y:189
            font.pixelSize: 24
            font.family: fontStyle.book
            text: qsTr("Nomor Resi                       ") + "\t:   " + txt_numpress
        }

        Text{
            x:50
            y:250
            font.pixelSize: 24
            font.family: fontStyle.book
            text: qsTr("Nomor Handphone         ") + "\t:   " + txt_handphone
        }

        Text{
            x:50
            y:310
            font.pixelSize: 24
            font.family: fontStyle.book
            text: qsTr("Durasi Simpan ") + "\t\t:   " + txt_duration
        }

        Text{
            x:50
            y:369
            font.pixelSize: 24
            font.family: fontStyle.book
            text: qsTr("Waktu Simpan  ") + "\t\t:   " + txt_storetime
        }

        Text{
            x:50
            y:429
            font.pixelSize: 24
            font.family: fontStyle.book
            text: qsTr("Waktu Terlambat") + "\t\t:   " + txt_overduetime
        }

        NotifButton{
            x:50
            y:514
            r_radius:0
            modeReverse:true
            buttonText:qsTr("BUKA LOKER")
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    show_buka.start();
                    hide_used.start();
                }
            }
        }
        NotifButton{
            x:320
            y:514
            r_radius:0
            buttonText:qsTr("AMBIL BARANG")
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    show_ambil.start();
                    hide_used.start();
                }

            }
        }
        Image{
            x:939
            y:60
            height:33
            width:33
            source:"asset/global/button/close.png"
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    hide_used.start();
                    dimm_display.visible=false;
                    // quit_gui = false;
                }

            }
        }
    }

    NotifSwipe{
        id:notif_ambil
        source_img: asset_path + "bg_ambil.png"
        img_y:50
        titlebody_x:50
        title_y:60
        body_y:180
        title_text: qsTr("Proses Ini akan Menyelesaikan <br>Order Kamu")
        body_text: qsTr("Pastikan barang tidak tertinggal didalam loker")

        NotifButton{
            id:ok_yes
            x:310
            y:350
            r_radius:0
            modeReverse:true
            buttonText:qsTr("LANJUT")
            MouseArea{
                anchors.fill: parent
                onClicked: {
                     if (show_box_id_express!=""){
                        json_express = [show_box_id_express];
                        console.log("DATA JSON : " + JSON.stringify(json_express));
                        
                        var op_taken = JSON.stringify({
                            "express_id": json_express[0],
                            "op_username": courierUsername,
                            "op_name": courierName
                        })
                        console.log("AMBIL BARANG: " + op_taken);
                        slot_handler.operator_taken(op_taken);
                    }else{
                        console.log("DATA JSON : KOSONG") //+ JSON.stringify(show_box_id_express))
                    }

                    hide_ambil.start();
                    show_success.start();

                }
                onEntered:{
                    ok_yes.modeReverse = false
                }
                onExited:{
                    ok_yes.modeReverse = true
                }
            }
        }

        NotifButton{
            id:ok_no
            x:50
            y:350
            r_radius:0
            buttonText:qsTr("BATAL")
            modeReverse:false
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    hide_ambil.start();
                    dimm_display.visible=false;
                }
                onEntered:{
                    ok_no.modeReverse = true
                }
                onExited:{
                    ok_no.modeReverse = false
                }
            }
        }


        Image{
            x:939
            y:60
            height:33
            width:33
            source:"asset/global/button/close.png"
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    hide_ambil.start();
                    dimm_display.visible=false;
                }

            }
        }
    }

    NotifSwipe{
        id:notif_buka_semua
        source_img: asset_path + "bg_buka_semua.png"
        img_y:50
        titlebody_x:50
        title_y:60
        body_y:180
        title_text: (status_buka=="EMPTY") ? qsTr("Apakah Kamu Ingin Membuka <br>Semua Loker yang Kosong ?") : qsTr("Apakah Kamu Ingin Membuka <br>Semua Loker ?")
        body_text: (status_buka=="EMPTY") ? qsTr("Pilihan ini untuk membuka semua loker yang kosong <br>Data login akan tercatat dalam sistem") : qsTr("Pilihan ini untuk membuka semua loker <br>Data login akan tercatat dalam sistem")

        NotifButton{
            id:ok_buka_semua
            x:310
            y:350
            r_radius:0
            modeReverse:true
            buttonText:qsTr("LANJUT")
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    hide_buka_semua.start();
                    console.log("BOX ID LIST : "+ courierName + " : " + courierUsername)
                    var epochTime = Math.round((new Date()).getTime())
                    var log_title = "OPERATOR_EMPTY_TAKEN"
                    var op_taken = JSON.stringify({
                        "status_buka": status_buka,
                        "op_username": courierUsername,
                        "op_name": courierName,
                        "time_stamp": epochTime
                    })
                    slot_handler.operator_logging(log_title, op_taken)
                    if(status_buka=="EMPTY"){
                        console.log("Buka loker kosong")
                        slot_handler.start_manager_open_empty_mouth();
                    }else{
                        console.log("Buka loker semua")
                        slot_handler.start_manager_open_all_mouth();
                    }
                }
                onEntered:{
                    ok_buka_semua.modeReverse = false
                }
                onExited:{
                    ok_buka_semua.modeReverse = true
                }
            }
        }

        NotifButton{
            id:no_buka_semua
            x:50
            y:350
            r_radius:0
            buttonText:qsTr("BATAL")
            modeReverse:false
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    hide_buka_semua.start();
                    dimm_display.visible=false;
                }
                onEntered:{
                    no_buka_semua.modeReverse = true
                }
                onExited:{
                    no_buka_semua.modeReverse = false
                }
            }
        }


        Image{
            x:939
            y:60
            height:33
            width:33
            source:"asset/global/button/close.png"
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    hide_buka_semua.start();
                    dimm_display.visible=false;
                }

            }
        }
    }

    NotifSwipe{
        id:notif_buka
        source_img: asset_path + "bg_buka.png"
        img_y:50
        titlebody_x:50
        title_y:60
        body_y:180
        title_text: qsTr("Cek Isi Loker Atau Tambahkan <br>Barang di Loker")
        body_text: qsTr("Pilihan ini hanya untuk mengecek isi loker atau <br>menambah barang di dalam loker")

        NotifButton{
            id:ok_buka
            x:310
            y:350
            r_radius:0
            modeReverse:true
            buttonText:qsTr("LANJUT")
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    hide_buka.start();
                    // console.log("BOX ID LIST : "+ courierName + " : " + courierUsername)
                    var epochTime = Math.round((new Date()).getTime())
                    var log_title = "OPERATOR_EMPTY_TAKEN"
                    var op_taken = JSON.stringify({
                        "box_id": box_id_list,
                        "box_number": txt_numbox,
                        "box_id_express": show_id_express,
                        "op_username": courierUsername,
                        "op_name": courierName,
                        "time_stamp": epochTime
                    })
                    slot_handler.operator_logging(log_title, op_taken)
                    slot_handler.start_manager_open_mouth(box_id_list)
                }
                onEntered:{
                    ok_buka.modeReverse = false
                }
                onExited:{
                    ok_buka.modeReverse = true
                }
            }
        }

        NotifButton{
            id:no_buka
            x:50
            y:350
            r_radius:0
            buttonText:qsTr("BATAL")
            modeReverse:false
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    hide_buka.start();
                    dimm_display.visible=false;
                }
                onEntered:{
                    no_buka.modeReverse = true
                }
                onExited:{
                    no_buka.modeReverse = false
                }
            }
        }


        Image{
            x:939
            y:60
            height:33
            width:33
            source:"asset/global/button/close.png"
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    hide_buka.start();
                    dimm_display.visible=false;
                }

            }
        }
    }

    NotifSwipe{
        id:notif_failed
        source_img: asset_path + "bg_failed.png"
        img_y:50
        titlebody_x:50
        title_y:60
        body_y:137
        title_text: qsTr("Proses Gagal")
        body_text: qsTr("Proses Gagal , Silakan coba kembali.")

        NotifButton{
            id:ok_failed
            x:50
            y:350
            r_radius:0
            buttonText:qsTr("OKE")
            modeReverse:false
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    hide_failed.start();
                    dimm_display.visible=false;
                }
                onEntered:{
                    ok_failed.modeReverse = true
                }
                onExited:{
                    ok_failed.modeReverse = false
                }
            }
        }
    }

    NotifSwipe{
        id:notif_success
        source_img: asset_path + "bg_success.png"
        img_y:50
        titlebody_x:50
        title_y:60
        body_y:137
        title_text: qsTr("Proses Berhasil")
        body_text: qsTr("Selamat Proses Berhasil")

        NotifButton{
            id:ok_success
            x:50
            y:350
            r_radius:0
            buttonText:qsTr("OKE")
            modeReverse:false
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    hide_success.start();
                    dimm_display.visible=false;
                    refresh();
                }
                onEntered:{
                    ok_success.modeReverse = true
                }
                onExited:{
                    ok_success.modeReverse = false
                }
            }
        }
    }

    NumberAnimation {
        id:hide_success
        targets: notif_success
        properties: "y"
        from:248
        to:768
        duration: 500
        easing.type: Easing.InOutBack
    }
    NumberAnimation {
        id:show_success
        targets: notif_success
        properties: "y"
        from:768
        to:248
        duration: 500
        easing.type: Easing.InOutBack
    }

    NumberAnimation {
        id:hide_failed
        targets: notif_failed
        properties: "y"
        from:248
        to:768
        duration: 500
        easing.type: Easing.InOutBack
    }
    NumberAnimation {
        id:show_failed
        targets: notif_failed
        properties: "y"
        from:768
        to:248
        duration: 500
        easing.type: Easing.InOutBack
    }

    NumberAnimation {
        id:hide_buka
        targets: notif_buka
        properties: "y"
        from:248
        to:768
        duration: 500
        easing.type: Easing.InOutBack
    }
    NumberAnimation {
        id:show_buka
        targets: notif_buka
        properties: "y"
        from:768
        to:248
        duration: 500
        easing.type: Easing.InOutBack
    }

    NumberAnimation {
        id:hide_ambil
        targets: notif_ambil
        properties: "y"
        from:248
        to:768
        duration: 500
        easing.type: Easing.InOutBack
    }
    NumberAnimation {
        id:show_ambil
        targets: notif_ambil
        properties: "y"
        from:768
        to:248
        duration: 500
        easing.type: Easing.InOutBack
    }

    NumberAnimation {
        id:hide_used
        targets:notif_used
        properties: "y"
        from:80
        to:768
        duration: 500
        easing.type: Easing.InOutBack
    }
    NumberAnimation {
        id:show_used
        targets:notif_used
        properties: "y"
        from:768
        to:80
        duration: 500
        easing.type: Easing.InOutBack
    }

    NumberAnimation {
        id:hide_sync
        targets:notif_sync
        properties: "y"
        from:248
        to:768
        duration: 500
        easing.type: Easing.InOutBack
    }
    NumberAnimation {
        id:show_sync
        targets:notif_sync
        properties: "y"
        from:768
        to:248
        duration: 500
        easing.type: Easing.InOutBack
    }

    NumberAnimation {
        id:hide_enable
        targets:notif_enable
        properties: "y"
        from:248
        to:768
        duration: 500
        easing.type: Easing.InOutBack
    }
    NumberAnimation {
        id:show_enable
        targets:notif_enable
        properties: "y"
        from:768
        to:248
        duration: 500
        easing.type: Easing.InOutBack
    }

    NumberAnimation {
        id:hide_buka_semua
        targets: notif_buka_semua
        properties: "y"
        from:248
        to:768
        duration: 500
        easing.type: Easing.InOutBack
    }
    NumberAnimation {
        id:show_buka_semua
        targets: notif_buka_semua
        properties: "y"
        from:768
        to:248
        duration: 500
        easing.type: Easing.InOutBack
    }
    
    function user_access() {
        if(userAccess==1){
            buka_semua_vis = true;
            buka_kosong_vis = true;
        }else if(userAccess==2){
            buka_semua_vis = false;
            buka_kosong_vis = true;
            btn_open_empty.x = 53;
        }else{
            buka_semua_vis = false;
            buka_kosong_vis = false;
        }
    }

    function secondsToHms(seconds) {
        seconds = Number(seconds);
        var d = Math.floor(seconds / (3600*24));
        var h = Math.floor(seconds % (3600*24) / 3600);
        var m = Math.floor(seconds % 3600 / 60);
        var s = Math.floor(seconds % 60);
        var xJam = ""
        var xMenit = ""
        var xDetik = ""
        if (d <= 0 && h > 0)xJam = qsTr(" Jam")
        else if (d <= 0 && h <= 0 && m > 0) xMenit = qsTr(" Menit")
        else if (d <= 0 && h <= 0 && m <= 0 && s > 0) xDetik = qsTr(" Detik")
        else{
            xJam = ""
            xMenit = ""
            xDetik = ""
        }
        var dDisplay = d > 0 ? d + " Hari  " : "";
        var hDisplay = h > 0 ? h + " " + xJam +" : " : "";
        // var mDisplay = m > 0 ? m + " " + xMenit : "";
        var mDisplay = m > 0 ? m + " " + xMenit +" : " : "";
        var sDisplay = s > 0 ? s + " " + xDetik +"" : "";

        return  dDisplay +  hDisplay + mDisplay + sDisplay
    }

    function refresh(){
        press_time = 1
        cabinet_data = "";
        gridModel.clear();
        listModel.clear();
        if(cabinet_num>=25){
            startup();
        }else{
            call_slot();
        }
    }

    function startup() {
        page.enabled = false
        up_button.enabled = false
        down_button.enabled = false
        dimm_display.visible = true
        waiting.visible = true
        call_slot();
        down_button.enabled = false
        down_button.visible = false
        up_button.visible = false
        up_button.enabled = false
        press = "0"
        abc.counter = timer_value
        my_timer.restart()
        timer_startup.start();
    }

    function call_slot(params) {
        slot_handler.start_load_express_all()
        slot_handler.start_load_mouth_list(1)
        slot_handler.start_load_manager_mouth_count()
        slot_handler.start_operator_load_overdue_express_list(1)
        slot_handler.start_load_courier_overdue_express_count()
    }
}
