import QtQuick 2.4
import QtQuick.Controls 1.2

Background{
    id: success_return
    property var asset_path: 'asset/faq/'
    property var font_reg: "SourceSansPro-Regular"
    // property var font_bold: "source-sans-pro.semibold.ttf"
    property var font_light: "SourceSansPro-Light"
    show_img: ""
    img_vis: true
    property var txt_time: ""
    property var language: 'ID'

    Stack.onStatusChanged:{
        if(Stack.status == Stack.Activating){
            timer.secon = 90
            timer_note.restart()
        }
        if(Stack.status==Stack.Deactivating){
            timer_note.stop()
        }
    }

    FontType{
        id:fontStyle
        //Type Font ### bold , extrabold , light , book, medium, black ###
    }

    Header{
        id:header
        title_text:qsTr("Hubungi Kami")
        funct: "home"
        text_timer : txt_time
    }

    Item{
        id: timer
        property int secon  : 90
        Timer{
            id:timer_note
            interval:1000
            repeat:true
            running:true
            triggeredOnStart:true
            onTriggered:{
                timer.secon -= 1
                txt_time = timer.secon
                if(timer.secon < 1){
                    timer_note.stop()
                    my_stack_view.push(time_out)
                }
            }
        }
    }

    Image{
        x:50
        y:125
        height:35
        width:118
        source:asset_path + "popbox.png"
        visible: false
    }

    Text {
        x:58
        y:165
        text: qsTr("Punya Pertanyaan?")
        font.pixelSize:35
        color:"#3a3a3a"
        font.family: fontStyle.medium
    }
    Text {
        x:58
        y:220
        text: qsTr("Tim kami akan senang membantu kamu")
        font.pixelSize:18
        color:"#4a4a4a"
        font.family: fontStyle.light
    }

    Rectangle{
        x:566
        y:149
        Text{
            x:0
            y:0
            font.pixelSize:20
            color:"#392828"
            font.family: fontStyle.medium
            text: qsTr("Jam Operasional")
        }
        // Text{
        //     x:0
        //     y:40
        //     font.pixelSize:17
        //     lineHeight : 1.4
        //     color:"#414042"
        //     font.family: fontStyle.light
        //     text: qsTr("Senin - Jumat") + qsTr("\t\t\t: ") + qsTr("08:00 - 21:00") + "<br>" + qsTr("Sabtu") + "\t\t\t\t: " + qsTr("08:00 - 19:00") + "<br>" + qsTr("Public Holiday/Hari Libur Nasional") + "\t:" + qsTr("Libur")
        // }
        Text{
            x:0
            y:70
            font.pixelSize:17
            lineHeight : 1.4
            color:"#392828"
            font.family: fontStyle.medium
            text: qsTr("Senin - Jumat") + "<br>" + qsTr("Sabtu") + qsTr(" &amp; ") + qsTr("Minggu")
        }

        Text{
            x:220
            y:70
            font.pixelSize:17
            lineHeight : 1.4
            color:"#392828"
            font.family: fontStyle.medium
            text: " :  " + qsTr("08:00 - 21:00") + "<br> :  " + qsTr("08:00 - 19:00")
        }
    }
    
    Rectangle{
        x: 566
        y: 305
        width: 405
        height: 436
        color: "white"
        border.color: "#c4c4c4"
        border.width: 2

        // Image{
        //     x: 311
        //     y: 38
        //     width: 68
        //     height: 20
        //     source: asset_path + "popbox.png"
        // }

        Image{
            x: 27
            y: 25
            width: 25
            height: 25
            source: asset_path + "phone.png"
        }
        Text{
            x: 75
            y: 26
            text: qsTr("Hotline : 021-22538719")
            color: "#4a4a4a"
            font.family: fontStyle.light
            font.pixelSize:15
        }
        // Text{
        //     x: 80
        //     y: 35
        //     text: qsTr("08118698566")
        //     color: "#4a4a4a"
        //     font.family: fontStyle.medium
        //     font.pixelSize:17
        // }
        // Text{
        //     x: 80
        //     y: 52.5
        //     text: qsTr("081294520927")
        //     color: "#4a4a4a"
        //     font.family: fontStyle.medium
        //     font.pixelSize:17
        // }
        // Text{
        //     x: 80
        //     y: 70
        //     text: qsTr("087800135269")
        //     color: "#4a4a4a"
        //     font.family: fontStyle.medium
        //     font.pixelSize:17
        // }
        Image{
            x: 27
            y: 66
            width: 25
            height: 25
            source: asset_path + "mail.png"
        }
        Text{
            x: 75
            y: 67
            text: qsTr("Email : info@popbox.asia")
            color: "#4a4a4a"
            font.family: fontStyle.light
            font.pixelSize:15
        }

        Image{
            x: 27
            y: 110
            width: 25
            height: 25
            source: asset_path + "livechat.png"
        }
        Text{
            x: 75
            y: 111
            text: qsTr("Live Chat : PopBoxApp dan Website PopBox")
            color: "#4a4a4a"
            font.family: fontStyle.light
            font.pixelSize:15
        }

        Image{
            id: wa_logo
            x: 27
            y: 150
            width: 25
            height: 25
            source: asset_path + "whatsapp.png"
            visible: (language=="MY") ? true : false
        }
        Text{
            id: wa_body
            x: 75
            y: 151
            text: qsTr("WhatsApp: 0111-0606 011")
            color: "#4a4a4a"
            font.family: fontStyle.light
            font.pixelSize:15
            visible: (language=="MY") ? true : false
        }

        Image{
            id: fb_logo
            x: 27
            y: 193
            width: 25
            height: 25
            source: asset_path + "facebook.png"
            visible: (language=="MY") ? true : false
        }
        Text{
            id: fb_body
            x: 75
            y: 194
            text: qsTr("Facebook Messenger: @popboxmy")
            color: "#4a4a4a"
            font.family: fontStyle.light
            font.pixelSize:15
            visible: (language=="MY") ? true : false
        }

        Image{
            id: ig_logo
            x: 27
            y: 236
            width: 25
            height: 25
            source: asset_path + "instagram.png"
            visible: (language=="MY") ? true : false
        }
        Text{
            id: ig_body
            x: 75
            y: 237
            text: qsTr("Instagram Messenger: @popboxmy")
            color: "#4a4a4a"
            font.family: fontStyle.light
            font.pixelSize:15
            visible: (language=="MY") ? true : false
        }
    }

    Image{
        id: img_asset
        x: 61
        y: 282
        width: 394
        height: 397
        source: asset_path+ "contact_us_asset.png"
    }

    Image{
        id: img_scan_me
        x: 773
        y: 540
        width: 193
        height: 196
        source: asset_path + "contact_us_qr.png"
        visible: (language=="MY") ? false : true
    }

    Image{
        id: img_scan_me_my
        x: 800
        y: 580
        width: 160
        height: 151
        source: asset_path + "contact_us_qr_my.png"
        visible: (language=="MY") ? true : false
    }
    
}
