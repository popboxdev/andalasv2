import QtQuick 2.4
import QtQuick.Controls 1.2

Background{
    property var topPanelColor: "#ff524f"
    property var asset_path: "asset/courier/button/"
    property var press: "0"
    property var c_access: "full"
    property var fromExpress: "No"
    property var pref_login_user: "undefined"
    property var courier_name: "undefined"
    property var courier_username: "undefined"
    property var courier_com: "undefined"
    property var courier_slice: "undefined"
    property var txt_color: "#000000"
    property var txt_button_color: "#ffffff"
    property var txt_inactive: "#656565"
    property var buttonY: 320
    property var buttonY2: 520
    property var buttonXstore: 50 //1
    property var buttonXtake: 290 //2
    property var buttonXoverdue: 530 //3
    property var buttonXreject: 770 //4
    property var op_taken: ""
    property var quit_gui: false
    property var btn_trans: "#40707070"
    property var level_user

    Stack.onStatusChanged:{
        if(Stack.status == Stack.Activating){
            console.log('courier_credential : ' + c_access + ', with prefix_user_account : ' + pref_login_user)
            slot_handler.get_user_info()
            slot_handler.start_load_courier_overdue_express_count()
            press = "0"
            timer.secon = 90
            timer_note.restart()
        }

        if(Stack.status==Stack.Deactivating){
            timer_note.stop()
        }
    }

    Component.onCompleted: {
        root.user_info_result.connect(detail_user)
        root.overdue_express_count_result.connect(overdue_count)
        root.init_client_result.connect(handle_result)
        root.get_version_result.connect(show_version)
        root.box_start_migrate_result.connect(box_migrate)
    }

    Component.onDestruction: {
        root.user_info_result.disconnect(detail_user)
        root.overdue_express_count_result.disconnect(overdue_count)
        root.init_client_result.disconnect(handle_result)
        root.get_version_result.disconnect(show_version)
        root.box_start_migrate_result.disconnect(box_migrate)
    }

    FontType{
        id:fontStyle
        //Type Font ### bold , extrabold , light , book, medium, black ###
    }

    Header{
        id:header
        title_text:qsTr("HALAMAN ADMIN")
        img_vis:false
        timer_view: false
    }

    Item{
        id: timer
        property int secon  : 90
        Timer{
            id:timer_note
            interval:1000
            repeat:true
            running:true
            triggeredOnStart:true
            onTriggered:{
                timer.secon -= 1
                if(timer.secon < 1){
                    timer_note.stop()
                    my_stack_view.push(time_out)
                }
            }
        }
    }

    NotifButton{
        id:btn_logout
        x:807
        y:25
        r_width:160
        r_height:50
        r_radius:2
        buttonText:qsTr("Keluar")
        buttonColor: "#ff524f"
        modeReverse: true
        MouseArea{
            anchors.fill: parent
            onClicked: {
                show_logout.start()
                dimm_display.visible=true
            }
            onEntered:{
                btn_logout.modeReverse = false
            }
            onExited:{
                btn_logout.modeReverse = true
            }
        }
    }

    function button_inactive(text){
        if(text=="overdue"){
            courier_take_overdue_button.enabled = false
            courier_take_overdue_button.show_text_color = txt_inactive
            courier_take_overdue_button.color = btn_trans

        }
        else if(text=="opendoor"){
            access_door_button.enabled = false
            access_door_button.show_text_color = txt_inactive
            access_door_button.color = btn_trans
        }
        else if(text=="quitgui"){
            quit_gui_button.enabled = false
            quit_gui_button.show_text_color = txt_inactive
            quit_gui_button.color = btn_trans
        }
        else if(text=="sisfo"){
            sisfo_button.enabled = false
            sisfo_button.show_text_color = txt_inactive
            sisfo_button.color = btn_trans
        }
    }

    function overdue_count(text){
        // overdue_num.text = text
        console.log("OVERDUE COUNT : " , text)
    }

    function detail_user(text){
        // console.log("DETAIL USER : ", text)
        var result = JSON.parse(text)
        console.log("RESULT SERVICE PAGE : " + JSON.stringify(result))
        courier_name = result.name
        courier_username = result.namelog
        courier_slice = result.namelog
        level_user = result.role_level

        // console.log("USER Name TIPE : ", courier_name)
        courier_com = result.company_name
        courier_slice = courier_slice.slice(-3)
        var log_title = "OPERATOR_LOGIN"
        op_taken = JSON.stringify(result)
        slot_handler.operator_logging(log_title, op_taken)
        if(level_user==1){ // SUPERADMIN
            console.log("LOGIN : SUPERADMIN")
        }else if(level_user==2){ //ENGINEER
            button_inactive("quitgui")
            button_inactive("overdue")
        }else if(level_user==3){ //SUPERVISOR
            button_inactive("overdue")
            button_inactive("quitgui")
            button_inactive("sisfo")
        }else if(level_user==4) { // ADMIN / OPS_LEADER
            button_inactive("overdue")
            button_inactive("quitgui")
            button_inactive("sisfo")
        }else if(level_user==5) { //OPERASIONAL
            button_inactive("quitgui")
            button_inactive("sisfo")
            button_inactive("overdue")
        }else{
            button_inactive("quitgui")
            button_inactive("opendoor")
            button_inactive("overdue")
            button_inactive("sisfo")
        }
    }


    Text {
        id: greeting_text
        x: 50
        y: 150
        width: 200
        height: 40
        font.family: fontStyle.book
        color:txt_color
        text: 'Hi, ' + courier_name + ' | ' + courier_com
        verticalAlignment: Text.AlignVCenter
        textFormat: Text.PlainText
        font.pointSize:22
        font.capitalization: Font.Capitalize
    }

    Text {
        x: 50
        y: 230
        color:txt_color
        text:qsTr("Pilih Opsi Layanan")
        font.pointSize:35
        font.family: fontStyle.medium
    }

    CourierServiceUpButton{
        id:courier_take_overdue_button
        x:buttonXstore
        y:buttonY
        width: 200
        height: 200
        radius: 20
        color:topPanelColor
        show_text_color: txt_button_color
        show_text:qsTr("Ambil Paket")
        show_image:asset_path + "btn_overdue.png"

        MouseArea {
            anchors.fill: parent
            onClicked: {
                if(press != "0"){
                    return
                }
                press = "1"
                my_stack_view.push(courier_take,{courierUsername:courier_username, courierName:courier_name})
            }
            onEntered:{
                courier_take_overdue_button.color = btn_trans
                courier_take_overdue_button.show_text_color = "white"
            }
            onExited:{
                courier_take_overdue_button.color = topPanelColor
                courier_take_overdue_button.show_text_color = txt_button_color
            }
        }
    }

    CourierServiceUpButton{
        id:access_door_button
        x:buttonXtake
        y:buttonY
        width: 200
        height: 200
        radius: 20
        color:topPanelColor
        show_text_color:txt_button_color
        show_text:qsTr("Akses Pintu")
        show_image:asset_path + "btn_opendoor.png"

        MouseArea {
            anchors.fill: parent
            onClicked: {
                if(press != "0"){
                    return
                }
                press = "1"
                my_stack_view.push(cabinet_page, {userAccess: level_user, courierUsername: courier_username, courierName: courier_name, topPanelColor: topPanelColor})
            }
            onEntered:{
                access_door_button.color = btn_trans
                access_door_button.show_text_color = "white"
            }
            onExited:{
                access_door_button.color = topPanelColor
                access_door_button.show_text_color = txt_button_color
            }
        }
    }

    CourierServiceUpButton{
        id:quit_gui_button
        x:buttonXreject
        y:buttonY
        width: 200
        height: 200
        radius: 20
        color:topPanelColor
        show_text_color: txt_button_color
        show_text:qsTr("Restart GUI")
        show_image:asset_path + "btn_quitgui.png"
        show_source:"img/button/1.png"

        MouseArea {
            anchors.fill: parent
            onClicked: {
                quit_gui = true
                show_logout.start()
                dimm_display.visible = true
            }
            onEntered:{
                quit_gui_button.color = btn_trans
                quit_gui_button.show_text_color = "white"
            }
            onExited:{
                quit_gui_button.color = topPanelColor
                quit_gui_button.show_text_color = txt_button_color
            }
        }
    }

    CourierServiceUpButton{
        id:sisfo_button
        x:buttonXoverdue
        y:buttonY
        width: 200
        height: 200
        radius: 20
        color:topPanelColor
        show_text_color: txt_button_color
        show_text:qsTr("Sys-Info")
        show_image:asset_path + "btn_sisfo.png"

        MouseArea {
            anchors.fill: parent
            onClicked: {
                console.log("sisfo_button")
                dimm_display.visible = true;
                show_sisfo.start();
                slot_handler.start_get_version()
            }
            onEntered:{
                sisfo_button.color = btn_trans
                sisfo_button.show_text_color = "white"
            }
            onExited:{
                sisfo_button.color = topPanelColor
                sisfo_button.show_text_color = txt_button_color
            }
        }
    }

    DimmDisplay{
        id:dimm_display
        visible:false
    }

    AnimatedImage{
        id: waiting
        visible:false
        width: 200
        height: 200
        anchors.verticalCenter: parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter
        source: 'asset/global/animation/loading.gif'
    }

    NotifSwipe{
        id:notif_success_init
        source_img: asset_path + "bg_success_init.png"
        img_y:50
        titlebody_x:50
        title_y:60
        title_text:qsTr("Proses Init Database<br>Berhasil")

        NotifButton{
            id:ok_success
            x:50
            y:351
            r_radius:0
            buttonText:qsTr("OKE")
            modeReverse:false
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    hide_success_init.start();
                    dimm_display.visible=false;
                }
                onEntered:{
                    ok_success.modeReverse = true
                }
                onExited:{
                    ok_success.modeReverse = false
                }
            }
        }

        Image{
            x:939
            y:60
            height:33
            width:33
            source:"asset/global/button/close.png"
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    hide_success_init.start();
                    dimm_display.visible=false;
                }

            }
        }
    }

    NotifSwipe{
        id:notif_sisfo
        source_img: asset_path + "bg_init.png"
        title_text:""
        img_y:50
        titlebody_x:50
        title_y:60
        body_y:180

        CourierServiceUpButton{
            id:init_button
            x:250
            y:170
            width: 200
            height: 200
            radius: 20
            color:topPanelColor
            show_text_color: txt_button_color
            show_text:qsTr("Init DB")
            show_image:asset_path + "btn_init.png"

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    console.log("init_button")
                    if(press != "0"){
                        return
                    }
                    press = "1"
                    slot_handler.start_init_client()
                    // main_page.enabled = false
                    hide_sisfo.start();
                    waiting.visible = true
                }
                onEntered:{
                    init_button.color = btn_trans
                    init_button.show_text_color = "white"
                }
                onExited:{
                    init_button.color = topPanelColor
                    init_button.show_text_color = txt_button_color
                }
            }
        }

        CourierServiceUpButton{
            id:migrate_button
            x:550
            y:170
            width: 200
            height: 200
            radius: 20
            color:topPanelColor
            show_text_color: txt_button_color
            show_text:qsTr("Migrate DB")
            show_image:asset_path + "btn_migrate.png"

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    console.log("migrate_button")
                    if(press != "0"){
                        return
                    }
                    press = "1"
                    slot_handler.box_start_migrate()
                    // main_page.enabled = false
                    waiting.visible =true
                }
                onEntered:{
                    migrate_button.color = btn_trans
                    migrate_button.show_text_color = "white"
                }
                onExited:{
                    migrate_button.color = topPanelColor
                    migrate_button.show_text_color = txt_button_color
                }
            }
        }
        Image{
            x:939
            y:60
            height:33
            width:33
            source:"asset/global/button/close.png"
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    hide_sisfo.start();
                    dimm_display.visible=false;
                }

            }
        }
        
    }

    NotifSwipe{
        id:notif_logout
        source_img: asset_path + "logout.png"
        img_y:50
        titlebody_x:50
        title_y:60
        body_y:180
        title_text:(quit_gui==true) ? qsTr("Keluar dari Gui / Aplikasi ?") : qsTr("Keluar dari Menu ?")

        NotifButton{
            id:ok_yes
            x:310
            y:170
            r_radius:0
            modeReverse:true
            buttonText:qsTr("YA")
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    if(quit_gui==true){
                        slot_handler.start_explorer()
                        Qt.quit()
                        var log_title = "OPERATOR_QUIT_GUI"
                        console.log("QUIT_GUI : ", op_taken)
                    }else{
                        timer_note.stop()
                        my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
                        var log_title = "OPERATOR_LOGOUT"
                        slot_handler.set_logout_user()
                        console.log("LOGOUT : ", op_taken)
                    }
                    slot_handler.operator_logging(log_title, op_taken)
                }
                onEntered:{
                    ok_yes.modeReverse = false
                }
                onExited:{
                    ok_yes.modeReverse = true
                }
            }
        }

        NotifButton{
            id:ok_no
            x:50
            y:170
            r_radius:0
            buttonText:qsTr("TIDAK")
            modeReverse:false
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    hide_logout.start();
                    dimm_display.visible=false;
                    quit_gui = false;
                }
                onEntered:{
                    ok_no.modeReverse = true
                }
                onExited:{
                    ok_no.modeReverse = false
                }
            }
        }


        Image{
            x:939
            y:60
            height:33
            width:33
            source:"asset/global/button/close.png"
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    hide_logout.start();
                    dimm_display.visible=false;
                    quit_gui = false;
                }

            }
        }
    }

    NumberAnimation {
        id:hide_success_init
        targets: notif_success_init
        properties: "y"
        from:248
        to:768
        duration: 500
        easing.type: Easing.InOutBack
    }
    NumberAnimation {
        id:show_success_init
        targets: notif_success_init
        properties: "y"
        from:768
        to:248
        duration: 500
        easing.type: Easing.InOutBack
    }

    NumberAnimation {
        id:hide_sisfo
        targets: notif_sisfo
        properties: "y"
        from:248
        to:768
        duration: 500
        easing.type: Easing.InOutBack
    }
    NumberAnimation {
        id:show_sisfo
        targets: notif_sisfo
        properties: "y"
        from:768
        to:248
        duration: 500
        easing.type: Easing.InOutBack
    }

    NumberAnimation {
        id:hide_logout
        targets: notif_logout
        properties: "y"
        from:248
        to:768
        duration: 500
        easing.type: Easing.InOutBack
    }
    NumberAnimation {
        id:show_logout
        targets: notif_logout
        properties: "y"
        from:768
        to:248
        duration: 500
        easing.type: Easing.InOutBack
    }

    function box_migrate(result){
        press = "0"
        waiting.visible = false
        // main_page.enabled = false
        // img_init_ok.source = "img/otherImages/x-mark.png"
        // if (result == ''){
        //     text_init_ok.text = qsTr("Please Init DB First..!")
        // } else if (result == 'ERROR'){
        //     text_init_ok.text = qsTr("Please Ensure Init DB is correct!")
        // } else if (result == 'FAILED') {
        //     text_init_ok.text = qsTr("Something went wrong..!")
        // } else if (result == 'NO NEED') {
        //     text_init_ok.text = qsTr("Already Migrated Before..!")
        // } else if (result == 'SUCCESS') {
        //     img_init_ok.source = "img/otherImages/checklist.png"
        //     text_init_ok.text = qsTr("Data Migration is Success")
        //     canQuit = true
        // }
        // init_ok.open()
        console.log("box_migrate : " + result)
    }

    function show_version(text){
        notif_sisfo.title_text = text
    }

    function handle_result(text){
        press = "0"
        waiting.visible = false
        // dimm_display.visible = false
        if(text == "Success"){
            console.log("init : SUKSES")
            show_success_init.start();
        }
    }
}
