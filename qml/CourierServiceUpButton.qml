import QtQuick 2.4
import QtQuick.Controls 1.2

Rectangle{
    width:200
    height:200
    property var show_text:""
    property var show_text_color:"#BF2E26"
    property var show_image:""
    property var show_source:""
    FontType{
        id:fontStyle
        //Type Font ### bold , extrabold , light , book, medium, black ###
    }
    Image{
        x:0
        y:0
        width:200
        height:200
        source:show_image
    }

    Rectangle{
        y:150
        width:200
        height:30
        color:"transparent"


        Text{
            text:show_text
            font.family: fontStyle.medium
            color:show_text_color
            font.pixelSize:28
            anchors.centerIn: parent;
        }
    }


}
