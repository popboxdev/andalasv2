import QtQuick 2.4
import QtQuick.Controls 1.2

Background{
    id: courier_store
    property var press: '0'
    property var asset_global: 'asset/global/'
    property var asset_path: 'asset/send_page/'
    property var path_background: 'asset/send_page/background/'
    // property var font_type: "SourceSansPro-Regular"
    property var target_swipe: "notif_swipe"
    property var param_choice: ""
    property var status: ""
    // show_img: path_background + "bg.png"
    show_img:""
    img_vis:true
    property int character:26
    property var ch:1  
    property var param_parcel: ""
    property var no_parcel: ""
    property var courier_name: ''
    property var phone_cust: '087766776655'
    property var select_courier:''
    property var count:0
    property var txt_time: ""
    property var merchant_info: undefined
    property var drop_by_courier: 1
    property var status_qr: "COURIER"
    
    Stack.onStatusChanged:{
        if(Stack.status == Stack.Activating){
            press = '0';
            // header.timer_start=true
            timer.secon = 90;
            timer_note.restart();
            show_key.start();
            slot_handler.start_courier_scan_barcode()
            parcel_number.show_text = "";
        }
        if(Stack.status==Stack.Deactivating){
            timer_note.stop();
            slot_handler.stop_courier_scan_barcode()
            // header.timer_start=false
        }
    }

    FontType{
        id:fontStyle
        //Type Font ### bold , extrabold , light , book, medium, black ###
    }

    Header{
        id:header
        title_text:qsTr("SIMPAN PAKET")
        text_timer : txt_time
    }

    Item{
        id: timer
        property int secon  : 90
        Timer{
            id:timer_note
            interval:1000
            repeat:true
            running:true
            triggeredOnStart:true
            onTriggered:{
                timer.secon -= 1
                txt_time = timer.secon
                if(timer.secon < 1){
                    timer_note.stop()
                    my_stack_view.push(time_out)
                }
            }
        }
    }
    
    Text {
        id: krm
        x:80
        y:145
        text: qsTr("Ketik / Scan No. Resi")
        font.pixelSize:40
        color:"#414042"
        font.family: fontStyle.medium
    }

    Image{
        id: img_help
        width:26
        height:26
        // x:435
        x:50
        y:148
        source:path_background+"help.png"
        MouseArea {
            anchors.fill: parent
            onClicked: {
                if(press==0){
                    show_resi.start();
                    dimm_display.visible=true
                    press=1
                    slot_handler.stop_courier_scan_barcode();
                }
            }
        }
    }

    Image{
        id: img_bg
        width:359
        height:293
        x:50
        y:463
        source:path_background+"bglogo.png"
    }

    Text {
        id:a
        y:325
        x:51
        text: qsTr("Lebih gampang scan no. resi kamu, <b><u>lihat caranya di sini</u></b>")
        font.pixelSize:22
        color:"#3a3a3a"
        font.family: fontStyle.book
        MouseArea {
            anchors.fill: parent
            onClicked: {
                if(press==0){
                    show_scan.start();
                    dimm_display.visible=true
                    press=1
                    slot_handler.stop_courier_scan_barcode();
                }
            }
        }
    }
    Text {
        id:ab
        y:520
        x:487
        text: qsTr("Kamu Tidak Punya / Lupa No. Resi ?")
        font.pixelSize:30
        color:"#414042"
        font.family: fontStyle.medium
    }

    NotifButton{
        id:btn_lanjut
        x:690
        y:230
        buttonText:qsTr("LANJUT")
        buttonColor: "#8c8c8c"
        r_radius:0
        modeReverse:false
        MouseArea{
            anchors.fill: parent
            onClicked: {
                if(parcel_number.show_text != ""){
                    dimm_display.visible = true
                    waiting.visible = true
                    no_parcel = parcel_number.show_text
                    // my_stack_view.push(detail_courier_store,{no_parcel:no_parcel, courier_name:courier_name, phone_cust:phone_cust, select_courier:select_courier})
                    // my_stack_view.push(input_phone, {no_parcel:no_parcel, courier_name:courier_name, select_courier:select_courier, param_return: param_parcel, status: "logistic", title: "SIMPAN PAKET"}) 
                    // slot_handler.checking_awb(parcel_number.show_text)
                    // slot_handler.start_get_imported_express(no_parcel)
                    check_pattern(no_parcel);
                }
            }
            onEntered:{
                btn_lanjut.modeReverse = true
            }
            onExited:{
                btn_lanjut.modeReverse = false
            }
        }
    }

    Rectangle{
        x:50
        y:230
        width: 620
        height: 80
        color: "transparent"
        InputText{
            id:parcel_number
            x:2
            y:2
            borderColor : "#8c8c8c"
            // password: false
            // show_image:"img/apservice/courier/user_name.png"
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    if(press==0){
                        // show_key.start();
                        ch=1
                        parcel_number.bool = true
                        press=1
                        // courier_login_psw.bool = false
                    }
                }
            }
        }
    }

    Image{
        id: del_img
        x:613
        y:252
        width:36
        height:36
        visible:false
        source:asset_global + "button/delete.png"
        MouseArea {
            anchors.fill: parent
            onClicked: {
                parcel_number.show_text = "";
                count=0;
                touch_keyboard.input_text("")
            }
        }
    }


    KeyBoardAlphanumeric{
        id:touch_keyboard
        property var validate_c
        closeButton: false
        Component.onCompleted: {
            touch_keyboard.letter_button_clicked.connect(input_text)
            // touch_keyboard.function_button_clicked.connect(on_function_button_clicked)
        }

        function input_text(str){
            if(ch==1){
                if (str == "" && parcel_number.show_text.length > 0){
                    parcel_number.show_text.length--
                    parcel_number.show_text=parcel_number.show_text.substring(0,parcel_number.show_text.length-1)
                    count--;                 
                }
                if(str == "OKE" || str == "OK"){
                    str = "";
                    if(parcel_number.show_text != ""){
                        dimm_display.visible = true
                        waiting.visible = true
                        no_parcel = parcel_number.show_text
                        // no_parcel = "+033000011766820~2002111929~SUB10000CGK10510~REG19~~1~1~D.R.O.T~+6281236586460~DEDE~13230~2~~0~15000~0~~~0~"
                        // slot_handler.start_get_imported_express(no_parcel)
                        check_pattern(no_parcel);
                        // my_stack_view.push(detail_courier_store,{no_parcel:no_parcel, courier_name:courier_name, phone_cust:phone_cust, select_courier:select_courier})
                    }
                }
                if (str != "" && parcel_number.show_text.length< character){
                    parcel_number.show_text.length++
                    count++;
                }
                if (parcel_number.show_text.length>=character){
                    str=""
                    parcel_number.show_text.length=character
                }else{
                    parcel_number.show_text += str
                }
                if(count>0){
                    btn_lanjut.buttonColor = "#ff524f"
                    parcel_number.borderColor = "#ff7e7e"
                    del_img.visible=true
                } else{
                    btn_lanjut.buttonColor = "#c4c4c4"
                    parcel_number.borderColor = "#8c8c8c"
                    del_img.visible=false
                }

            }
        }
    }

    NumberAnimation {
        id:hide_key
        targets: touch_keyboard
        properties: "y"
        from:368
        to:768
        duration: 500
        easing.type: Easing.InOutBack
    }
    NumberAnimation {
        id:show_key
        targets: touch_keyboard
        properties: "y"
        from:768
        to:368
        duration: 500
        easing.type: Easing.InOutBack
    }

    NumberAnimation {
        id:hide_scan
        targets: notif_scan
        properties: "y"
        from:266
        to:768
        duration: 500
        easing.type: Easing.InOutBack
    }
    NumberAnimation {
        id:show_scan
        targets: notif_scan
        properties: "y"
        from:768
        to:266
        duration: 500
        easing.type: Easing.InOutBack
    }

    NumberAnimation {
        id:hide_resi
        targets: notif_resi
        properties: "y"
        from:266
        to:768
        duration: 500
        easing.type: Easing.InOutBack
    }

    NumberAnimation {
        id:show_resi
        targets: notif_resi
        properties: "y"
        from:768
        to:266
        duration: 500
        easing.type: Easing.InOutBack
    }

    NumberAnimation {
        id:show_forbidden
        targets: notif_forbidden_lzd
        properties: "y"
        from:768
        to:266
        duration: 500
        easing.type: Easing.InOutBack
    }

    NumberAnimation {
        id:hide_forbidden
        targets: notif_forbidden_lzd
        properties: "y"
        from:266
        to:768
        duration: 500
        easing.type: Easing.InOutBack
    }

    NumberAnimation {
        id:show_forbidden_space
        targets: notif_forbidden_space
        properties: "y"
        from:768
        to:266
        duration: 500
        easing.type: Easing.InOutBack
    }

    NumberAnimation {
        id:hide_forbidden_space
        targets: notif_forbidden_space
        properties: "y"
        from:266
        to:768
        duration: 500
        easing.type: Easing.InOutBack
    }

    NumberAnimation {
        id:hide_wrongawb
        targets: notif_wrongawb
        properties: "y"
        from:248
        to:768
        duration: 500
        easing.type: Easing.InOutBack
    }

    NumberAnimation {
        id:show_wrongawb
        targets: notif_wrongawb
        properties: "y"
        from:768
        to:248
        duration: 500
        easing.type: Easing.InOutBack
    }

    DimmDisplay{
        id:dimm_display
        visible:false
    }

    AnimatedImage{
        id: waiting
        visible:false
        width: 200
        height: 200
        anchors.verticalCenter: parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter
        source: 'asset/global/animation/loading.gif'
    }


    NotifSwipe{
        id:notif_wrongawb
        source_img: asset_global + "handle/resi_salah.png"
        img_y:50
        titlebody_x:50
        title_y:60
        body_y:130
        title_text:qsTr("Format No. Resi Salah")
        body_text:qsTr("Silakan periksa kembali no. resi yang <br>dimasukkan. Pastikan kamu scan ke barcode <br>yang benar.")

        NotifButton{
            id:coba_wrongawb
            x:50
            y:300
            r_radius:0
            r_width:360
            buttonText:qsTr("MASUKKAN ULANG")
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    hide_wrongawb.start();
                    dimm_display.visible = false
                    slot_handler.start_courier_scan_barcode();
                }
                onEntered:{
                    coba_wrongawb.modeReverse = true
                }
                onExited:{
                    coba_wrongawb.modeReverse = false
                }
            }
        }

        Image{
            x:939
            y:60
            height:33
            width:33
            source:"asset/global/button/close.png"
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    hide_wrongawb.start();
                    dimm_display.visible = false
                    slot_handler.start_courier_scan_barcode();
                }
            }
        }
    }


    NotifSwipe{
        id:notif_scan
        img_y:50
        source_img: path_background + "notif_scan.png"
        title_text:qsTr("Lebih Gampang dengan Scan <br>No. Resi")
        body_text:qsTr("Cukup arahkan barcode no. resi pada barcode <br>scanner yang berada di bawah layar bagian <br>tengah. Jaga jarak antara barcode dengan <br>barcode scanner agar dapat dibaca.")
        NotifButton{
            id:btn_ok
            x:50
            y:337
            r_radius:2
            buttonText:qsTr("MENGERTI")
            modeReverse:false
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    hide_animation("scan");
                }
                onEntered:{
                    btn_ok.modeReverse = true
                }
                onExited:{
                    btn_ok.modeReverse = false
                }
            }
        }

        Image{
            x:939
            y:60
            height:33
            width:33
            source:"asset/global/button/close.png"
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    hide_animation("scan");
                }

            }
        }
    }

    NotifSwipe{
        id:notif_resi
        img_y:50
        source_img: path_background + "no_resi.png"
        // titlebody_x:311
        title_y:60
        body_y:128
        title_text:qsTr("Apa itu No. Resi ?")
        body_text:qsTr("No. Resi adalah nomor pengiriman yang <br>diberikan oleh logistik sebagai tanda bukti <br>pengiriman. <br> <br>Istilah untuk beberapa layanan: <br>Pengembalian Barang: return number/ nomor <br>Pengembalian <br>PopSend/PopSafe: order number (dapat dicek <br>di aplikasi PopBox)")

        Image{
            x:939
            y:60
            height:33
            width:33
            source:"asset/global/button/close.png"
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    hide_animation("resi");
                }

            }
        }
    }

    NotifSwipe{
        id:notif_forbidden_lzd
        img_y:50
        source_img: path_background + "blocking_parcel.jpg"
        title_y:60
        body_y:128
        title_text:qsTr("Nomor Paket Tidak Dapat Diterima")
        body_text:qsTr("Pastikan paket Anda adalah paket Collection <br>Point dan pastikan nomor tracking sudah <br>benar(nomor diawali dengan 'LXRP-', 'LXRT-', <br>'ID', 'LXAP-', 'LXAT-', 'LXXB-', dan 'LXEE-').")
        NotifButton{
            id:btn_ok_forbidden
            x:50
            y:337
            r_radius:2
            buttonText:qsTr("MASUKKAN ULANG")
            modeReverse:false
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    hide_forbidden.start();
                    dimm_display.visible=false
                    press=0;
                    slot_handler.start_courier_scan_barcode();
                }
                onEntered:{
                    btn_ok_forbidden.modeReverse = true
                }
                onExited:{
                    btn_ok_forbidden.modeReverse = false
                }
            }
        }

        Image{
            x:939
            y:60
            height:33
            width:33
            source:"asset/global/button/close.png"
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    hide_forbidden.start();
                    dimm_display.visible=false
                    press=0;
                    slot_handler.start_courier_scan_barcode();
                }

            }
        }
    }

    NotifSwipe{
        id:notif_forbidden_space
        img_y:50
        source_img: path_background + "blocking_parcel.jpg"
        title_text:qsTr("Nomor Paket Tidak Dapat Diterima")
        body_text:qsTr("Pastikan paket Anda adalah paket Collection Point <br>dan pastikan nomor tracking sudah benar <br>dan tidak mengandung spasi atau sepecial karakter.")
        NotifButton{
            id:btn_ok_forbidden_space
            x:50
            y:337
            r_radius:2
            buttonText:qsTr("MASUKKAN ULANG")
            modeReverse:false
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    hide_forbidden_space.start();
                    dimm_display.visible=false
                    press=0;
                    slot_handler.start_courier_scan_barcode();
                }
                onEntered:{
                    btn_ok_forbidden_space.modeReverse = true
                }
                onExited:{
                    btn_ok_forbidden_space.modeReverse = false
                }
            }
        }

        Image{
            x:939
            y:60
            height:33
            width:33
            source:"asset/global/button/close.png"
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    hide_forbidden_space.start();
                    dimm_display.visible=false
                    press=0;
                    slot_handler.start_courier_scan_barcode();
                }

            }
        }
    }

    Component.onCompleted: {
        root.barcode_result.connect(handle_text)
        root.imported_express_result.connect(result_check_imported)
    }

    Component.onDestruction: {
        root.barcode_result.disconnect(handle_text)
        root.imported_express_result.disconnect(result_check_imported)
    }

    function handle_text(text){
        slot_handler.stop_courier_scan_barcode();
        parcel_number.show_text = text
        no_parcel=text
        parcel_number.show_text.length = text.length
        // slot_handler.start_get_imported_express(text) 
        check_pattern(text);
    }

    function hide_animation(param){
        // if(param=="keyboard") hide_key.start();
        if(param=="scan")hide_scan.start();
        if(param=="resi")hide_resi.start();
        dimm_display.visible=false
        slot_handler.start_courier_scan_barcode();
        press=0;
    }

    
    function result_check_imported(result) {
        
        var response = JSON.parse(result)
        
        console.log("response: " + result)
        if (response.isSuccess == "true") {

            if (response.merchant_info != '') {
                merchant_info = JSON.stringify(response.merchant_info);
            }

            // parcel_number.show_text = "";
            count=0;
            touch_keyboard.input_text("")
            dimm_display.visible = false
            waiting.visible = false
            if(response.data == "no_imported"){
                slot_handler.set_express_number(no_parcel)
                my_stack_view.push(input_phone, {no_parcel:no_parcel, courier_name:courier_name, select_courier:select_courier, param_return: param_parcel, status: "logistic",title_body: " Customer", title: qsTr("SIMPAN PAKET"), drop_by_courier: drop_by_courier, reinput_number: true}) 

            }else{
                slot_handler.set_express_number(no_parcel)
                if (response.phone_number != '') {
                    
                    console.log("merchant_info: " + merchant_info)
                    
                    my_stack_view.push(detail_courier_store,{no_parcel:no_parcel, courier_name:courier_name, phone_cust:response.phone_number, select_courier:select_courier, merchant_info: merchant_info, drop_by_courier: drop_by_courier})
                } else {
                    my_stack_view.push(input_phone, {no_parcel:no_parcel, courier_name:courier_name, select_courier:select_courier, param_return: param_parcel, status: "logistic", title: qsTr("SIMPAN PAKET"), merchant_info: merchant_info, drop_by_courier: drop_by_courier}) 
                }
            }
            
        } else if (response.isSuccess == "false") {
            if (response.data == "forbidden_pattern") {
                waiting.visible = false
                show_forbidden.start();
                dimm_display.visible=true
                press=1;
            } else if (response.data == "forbidden_space"){
                waiting.visible = false
                show_wrongawb.start();
                dimm_display.visible=true
                press=1;
            } else if (response.data == "forbidden_pattern_poslaju"){
                waiting.visible = false
                notif_forbidden_lzd.body_text = qsTr("Pattern not like (EXXXMY or PLXXX or PSXXX or PLFFXX) <br>is not allowed in our system. Please ensure <br>the tracking number is correct.")
                show_forbidden.start();
                dimm_display.visible=true
                press=1;
            }
        }
    }


    function check_pattern(text){
        var pattern = /^[a-zA-Z0-9-_]{1,24}$/gm;
        var parcel_text = text;
        console.log("Parcel oke : "+ parcel_text);
        if(parcel_text.match(pattern)){
            console.log("BENAR : "+ parcel_text);
            slot_handler.start_get_imported_express(parcel_text);
            waiting.visible = true;
        }else{
            console.log("SALAH : "+ parcel_text);
            show_wrongawb.start();
            dimm_display.visible=true
            parcel_number.show_text = "";
            count=0;
            touch_keyboard.input_text("")
        }
    }

}
