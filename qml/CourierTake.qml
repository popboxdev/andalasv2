import QtQuick 2.4
import QtQuick.Controls 1.2
import QtQuick.Controls.Styles 1.2

Background{
    id: courier_take
    show_img : ""
    img_vis:true
    property int timer_value: 60
    property var press:"0"
    property var express_id:""
    property var express_number:""
    property var phone_number:""
    property var phone_text:""
    property var phone:""
    property var overdue_time:""
    property var store_time:""
    property int number
    property var remark
    property var save_time
    property var over_time
    property var time
    property var num
    property var check_list: new Array()
    property var page_num
    property var press_time:1
    property var overdue_data
    property var overdue_num
    property var return_num
    property var popsafe_num
    property var popsend_num
    property var st_overdue
    property var takeOverdue
    property var durationOverdue
    property var duration
    property bool enableOverdue:false
    property var colortext_title: "#000000"
    property var colortext_row: "#000000"
    property var title:""
    property var choose_ambil:"popsend"
    //UPDATE ANDALAS V2
    property var and_resi:""
    property var and_duration:""
    property var and_phone: ""
    property var and_boxnum: ""
    property var and_storetime: ""
    property var and_taketime: "-"
    property var nomorLoker: ""
    property var door_no: ""
    property bool send_reverse: false
    property bool late_reverse: false
    property bool return_reverse: false
    property bool popsafe_reverse: false
    property bool reverse: false
    property var pin:0
    property var txt_time: ""

    property var courierUsername: 'undefined'
    property var courierName: 'undefined'

    Stack.onStatusChanged:{
        if(Stack.status==Stack.Activating){
            timer.secon = 300
            timer_note.restart()
            //slot_handler.start_courier_load_overdue_express_list()
            slot_handler.start_courier_load_popsend_list()
            slot_handler.start_load_courier_overdue_express_count()
            slot_handler.start_load_courier_return_express_count()
            slot_handler.start_load_courier_popsafe_express_count()
            slot_handler.start_load_courier_popsend_express_count()
            // slot_handler.start_load_courier_overdue_express_count()
            slot_handler.start_time_overdue()
            send_reverse=true;
            console.log(courierUsername+""+courierName)
            //up_button.visible = false
            //up_button.enabled = false
            //down_button.enabled = false
            //down_button.visible = false
            press = "0"
            //abc.counter = timer_value
            //my_timer.restart()
            
            
        }
        if(Stack.status==Stack.Deactivating){
            timer_note.stop()
            //my_timer.stop()
        }
    }

    FontType{
        id:fontStyle
        //Type Font ### bold , extrabold , light , book, medium, black ###
    }

    Header{
        id:header
        title_text:qsTr("AMBIL PAKET")
        text_timer : txt_time
    }

    Item{
        id: timer
        property int secon  : 90
        Timer{
            id:timer_note
            interval:1000
            repeat:true
            running:true
            triggeredOnStart:true
            onTriggered:{
                timer.secon -= 1
                txt_time = timer.secon
                if(timer.secon < 1){
                    timer_note.stop()
                    my_stack_view.push(time_out)
                }
            }
        }
    }

    Rectangle{
        id: button_popsend
        x: 50
        y: 125
        width: 200
        height: 70
        color: "white"
        border.color: (send_reverse==false) ? "#414042" : "#ff524f"
        radius: 2
        Text{
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            font.family: fontStyle.book
            font.pixelSize:22
            color:(send_reverse==false) ? "#414042" : "#ff524f"
            text:qsTr("PopSend") + " (" +popsend_num+ ")"
        }
        MouseArea{
            anchors.fill: parent
            onClicked:{
                check_list = new Array();
                send_reverse=true;
                popsafe_reverse=false;
                late_reverse=false;
                return_reverse=false;
                choose_ambil = "popsend"
                model.clear()
                slot_handler.start_load_courier_popsend_express_count()
                slot_handler.start_courier_load_popsend_list()
            }
        }
    }
    Rectangle{
        id: button_overdue
        x: 265
        y: 125
        width: 200
        height: 70
        color: "white"
        border.color: (late_reverse==false) ? "#414042" : "#ff524f"
        radius: 2
        Text{
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            font.family: fontStyle.book
            font.pixelSize:22
            color:(late_reverse==false) ? "#414042" : "#ff524f"
            text:qsTr("Terlambat") + " (" +overdue_num+ ")"
        }
        MouseArea{
            anchors.fill: parent
            onClicked:{
                check_list = new Array();
                late_reverse=true;
                popsafe_reverse=false;
                send_reverse=false;
                return_reverse=false;
                choose_ambil = "overdue"
                model.clear()
                slot_handler.start_load_courier_overdue_express_count()
                slot_handler.start_courier_load_overdue_express_list()
            }
        }
        
    }
    Rectangle{
        id: button_return
        x: 480
        y: 125
        width: 200
        height: 70
        color: "white"
        border.color: (return_reverse==false) ? "#414042" : "#ff524f"
        radius: 2
        Text{
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            font.family:fontStyle.book
            font.pixelSize:22
            color:(return_reverse==false) ? "#414042" : "#ff524f"
            text:qsTr("Kembalikan") + " (" +return_num+ ")"
        }
        MouseArea{
            anchors.fill: parent
            onClicked:{
                check_list = new Array();
                return_reverse=true;
                popsafe_reverse=false;
                send_reverse=false;
                late_reverse=false;
                model.clear()
                choose_ambil = "reject"
                slot_handler.start_courier_load_reject_express_list()
                slot_handler.start_load_courier_return_express_count()
            }
        }
    }

    Rectangle{
        id: button_popsafe
        x: 700
        y: 125
        width: 200
        height: 70
        color: "white"
        border.color: (popsafe_reverse==false) ? "#414042" : "#ff524f"
        radius: 2
        Text{
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            font.family:fontStyle.book
            font.pixelSize:22
            color:(popsafe_reverse==false) ? "#414042" : "#ff524f"
            text:qsTr("PopSafe") + " (" +popsafe_num+ ")"
        }
        MouseArea{
            anchors.fill: parent
            onClicked:{
                check_list = new Array();
                popsafe_reverse=true;
                return_reverse=false;
                send_reverse=false;
                late_reverse=false;
                model.clear()
                choose_ambil = "popsafe"
                slot_handler.start_load_courier_popsafe_express_count()
                slot_handler.start_courier_load_popsafe_express_list()
                // slot_handler.start_load_courier_return_express_count()
            }
        }
    }

    Rectangle{

        // Image {
        //     id: image1
        //     x: 60
        //     y: 161
        //     width: 880
        //     height: 45
        //     source: "img/courier19/one_1.png"
        // }
        Rectangle{
            id: rec_01
            x: 50
            y: 215
            width: 934
            height: 77
            color: "#e0e0e0"//topPanelColor
        }

        Row {
            id: row1
            x: 50
            y: 215
            width: 934
            height: 77
            Rectangle{
                Text{
                    font.family:fontStyle.medium
                    text:qsTr("Resi")
                    anchors.verticalCenterOffset: 35
                    anchors.horizontalCenterOffset: 120
                    color:"#414042"
                    font.pixelSize:22
                    anchors.centerIn: parent;
                }
            }

            Rectangle{
                Text{
                    font.family:fontStyle.book
                    text:qsTr("Phone")
                    anchors.verticalCenterOffset: 35
                    anchors.horizontalCenterOffset:  240
                    color:colortext_title
                    font.pixelSize:17
                    anchors.centerIn: parent;
                    visible: false
                }
            }

            Rectangle{
                Text{
                    font.family:fontStyle.book
                    text:qsTr("store-time")
                    anchors.verticalCenterOffset: 35
                    anchors.horizontalCenterOffset:400
                    color:"#414042"
                    font.pixelSize:22
                    anchors.centerIn: parent;
                    visible: false
                }
            }

            Rectangle{
                Text{
                    font.family:fontStyle.medium
                    text:qsTr("No Loker")
                    anchors.verticalCenterOffset: 35
                    anchors.horizontalCenterOffset: 460
                    color:"#414042"
                    font.pixelSize:22
                    anchors.centerIn: parent;
                }
            }

            Rectangle{
                Text{
                    font.family:fontStyle.book
                    text:qsTr("overdue-time")
                    anchors.verticalCenterOffset: 35
                    anchors.horizontalCenterOffset: 650
                    color:colortext_title
                    font.pixelSize:17
                    anchors.centerIn: parent;
                    visible: false
                }
            }

            Rectangle{
                Text{
                    font.family:fontStyle.medium
                    text:qsTr("Durasi")
                    anchors.verticalCenterOffset: 35
                    anchors.horizontalCenterOffset: 300
                    color:"#414042"
                    font.pixelSize:22
                    anchors.centerIn: parent;
                }
            }
        }
    }

    Item  {
        id: page
        x:50
        y:300 //batas bawah nya
        width: 970
        height:250

        ScrollView{
            width: 930
            height: 340
            ListView{
                id:listView
                anchors.fill:parent
                model:model
                delegate:listDel
                snapMode:ListView.SnapOneItem
            }
        }

        ListModel {
            id: model
        }

        ListView {
            id: view
            anchors.fill: parent
            model: model
            delegate: delegate
            spacing:0
            interactive:false
        }

        Component {
            id: listDel

            Rectangle {
                height: 60

                Rectangle{
                    width:930
                    height: 60
                    color: "white"
                    border.color: "#e0e0e0"
                }
                Image {
                    id: image1
                    y:60
                    width: 930
                    height: 1
                    source: "asset/courier/list/stripe.png"
                    visible: false
                }

                RadioButton {
                    id: checkbox1
                    x:40
                    y:13
                    visible: st_overdue
                    style: RadioButtonStyle{
                        indicator: Rectangle{
                            implicitWidth: 30
                            implicitHeight: 30
                            radius: 25
                            border.color: "#ff524f"
                            border.width: 2
                            Rectangle {
                                anchors.fill: parent
                                visible: control.checked
                                color: "#ff524f"//"#555"
                                radius: 25
                                anchors.margins: 7
                            }
                        }
                    }
                    onCheckedChanged: {
                        if (checked) {
                            check_list.push(show_express_id)
                            pin = pin+1
                        }
                        else {
                            for(var i in check_list){
                                if(show_express_id == check_list[i]){
                                    check_list.splice(i,1)
                                }
                            }
                            pin = pin-1
                        }
                        if(pin>0){
                            take.enabled= true
                            reverse = true
                        }
                        else{
                            take.enabled = false
                            reverse = false
                        }
                        //abc.counter = timer_value
                        //my_timer.restart()
                    }
                }

                Text{
                    width:150
                    font.family:fontStyle.book
                    text:id_text
                    anchors.horizontalCenterOffset: 180
                    color:colortext_row
                    font.pixelSize:17
                    anchors.centerIn: parent;
                    wrapMode: Text.WrapAnywhere
                }

                Text{
                    font.family:fontStyle.book
                    text:save_time
                    anchors.horizontalCenterOffset: 400
                    color:colortext_row
                    font.pixelSize:17
                    anchors.centerIn: parent;
                    visible: false
                }

                Text{
                    font.family:fontStyle.book
                    text:phone_text
                    anchors.horizontalCenterOffset: 230
                    color:colortext_row
                    font.pixelSize:17
                    anchors.centerIn: parent;
                    visible: false
                }

                Text{
                    font.family:fontStyle.book
                    text:number
                    anchors.horizontalCenterOffset: 460
                    color:colortext_row
                    font.pixelSize:17
                    anchors.centerIn: parent;
                }

                Text{
                    font.family:fontStyle.book
                    text:over_time
                    anchors.horizontalCenterOffset: 640
                    color:colortext_row
                    font.pixelSize:17
                    anchors.centerIn: parent;
                    visible: false
                }

                Text{
                    font.family:fontStyle.book
                    text:duration + qsTr(" Hari")
                    // visible:false
                    anchors.horizontalCenterOffset: 300
                    color:colortext_row
                    font.pixelSize:17
                    anchors.centerIn: parent;
                }
                Rectangle{
                    width: 124
                    height: 40
                    x: 770
                    y:10
                    color: "white"
                    border.color: "#ff524f"
                    radius: 3
                    Text{
                        anchors.horizontalCenter: parent.horizontalCenter
                        anchors.verticalCenter: parent.verticalCenter
                        text:qsTr("Detail")
                        font.pixelSize: 22
                        color: "#ff524f"
                        font.family: fontStyle.book
                    }
                    MouseArea{
                        anchors.fill: parent
                        onClicked:{
                            and_boxnum = number
                            and_duration = duration
                            and_phone = phone_text
                            and_resi = id_text
                            and_storetime = save_time
                            and_taketime = over_time
                            main_rec_dimm.visible= true
                            dimm_display.visible = true
                        }
                    }
                }  
            }   
        }
    }
    Rectangle{
        id: take
        x: 754
        y: 657
        width: 230
        height: 80
        radius: 2
        color:(reverse==false) ? "#c4c4c4" : "#ff524f"
        Text{
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            font.family:fontStyle.book
            font.pixelSize:24
            color:"white"
            text:qsTr("AMBIL")
        }

        MouseArea{
            anchors.fill: parent
            onClicked:{                
                if (check_list.length) {
                    console.log("EXPRESS_NUM : "+(JSON.stringify(check_list)))
                    take_express(choose_ambil)
                    dimm_display.visible = true;
                    waiting.visible = true;
                } else {    
                    console.log("express list is null")
                }
            }
        }
    }

    // function count_page(overdue_num){
    //     if(overdue_num/10 > Math.floor(overdue_num/10)){
    //         page_num = Math.floor(overdue_num/10)+1
    //     }
    //     else
    //         page_num = overdue_num/10
    // }

    function show_overdue_express(overdue_data){
        if(overdue_data==""){
            return
        }
        var nowTime = Math.round((new Date()).getTime()/1000)
        // console.log("DAY TAKE EXPRESS: ", takeOverdue)
        var epochTime = Math.round((new Date()).getTime()/1000)-(60*60*takeOverdue);
        var obj = JSON.parse(overdue_data)
        overdue_time="-"
        // console.log("OVERDUE DATA : " + JSON.stringify(overdue_data))
        for(var i in obj){
            if (obj[i]['overdueTime'] != null){
                var time_overdue = obj[i]['overdueTime'] /1000
                durationOverdue = Math.round((nowTime - time_overdue) / 86400)
            }else{
                var time_overdue = obj[i]['storeTime'] /1000
                durationOverdue = Math.round((nowTime - time_overdue) / 86400)
            }
            enableOverdue = true
            // if(epochTime<time_overdue){
            //     enableOverdue = false
            // }
            //console.log("OVERDUE tgl : " + time_overdue)
            //console.log("OVERDUE Enable : " + enableOverdue)
            num = 0;
            for(var key in obj[i]){
                // num = 0;
                if(key == "takeUserPhoneNumber"){
                    phone = obj[i][key]
                } else if(key == "recipientUserPhoneNumber" ){
                    phone = obj[i][key]
                } else if(key == "storeUserPhoneNumber" ){
                    phone = obj[i][key]
                }

                if(key == "id"){
                    express_id = obj[i][key]
                }

                if(key == "expressNumber"){
                    express_number = obj[i][key]
                    //console.log("OVERDUE durationOverdue : " + durationOverdue + " id_express : " + express_number)
                }
                else if(key == "customerStoreNumber"){
                    express_number = obj[i][key]
                }
                if(key == "overdueTime"){
                    time = obj[i][key]
                    var date = new Date(time);
                    var time_Y = date.getFullYear() + '-';
                    var time_M = (date.getMonth()+1 < 10 ? '0'+(date.getMonth()+1) : date.getMonth()+1) + '-';
                    var time_D = date.getDate() + ' ';
                    var time_h = date.getHours() + ':';
                    var time_m = date.getMinutes() + ':';
                    var time_s = date.getSeconds();
                    overdue_time = time_Y+time_M+time_D+time_h+time_m+time_s
                }
                if(key == "storeTime"){
                    time = obj[i][key]
                    var date = new Date(time);
                    var time_Y = date.getFullYear() + '-';
                    var time_M = (date.getMonth()+1 < 10 ? '0'+(date.getMonth()+1) : date.getMonth()+1) + '-';
                    var time_D = date.getDate() + ' ';
                    var time_h = date.getHours() + ':';
                    var time_m = date.getMinutes() + ':';
                    var time_s = date.getSeconds();
                    store_time = time_Y+time_M+time_D+time_h+time_m+time_s
                }
                if(key == "number"){
                    num = obj[i][key]
                }
                if (num==0){
                    if(key == "mouth"){
                        for(var key_1 in obj[i][key])
                            if(key_1 == "number"){
                                num = obj[i][key][key_1]
                                // console.log("NUMBER BOX : " + num)
                            }
                    }
                }
                console.log("Number LIST = ", num);

                // if(key == "mouth"){
                //     for(var key_1 in obj[i][key])
                //         if(key_1 == "number"){
                //             num = obj[i][key][key_1]
                //             // console.log("NUMBER BOX : " + num)
                //         }
                // }
            }
            model.append({"id_text":express_number,"phone_text": phone,"save_time": store_time,"number": num,"over_time": overdue_time,"show_express_id":express_id,"st_overdue":enableOverdue, "duration":durationOverdue});
        }
    }

    Component.onCompleted: {
        root.overdue_express_list_result.connect(overdue_express_list)
        root.overdue_express_count_result.connect(overdue_count)
        root.return_express_count_result.connect(return_count)
        root.popsafe_express_count_result.connect(popsafe_count)
        root.popsend_express_count_result.connect(popsend_count)
        root.courier_take_overdue_express_result.connect(take_result)
        root.start_time_overdue_result.connect(take_overdue_result)
        root.take_send_express_result.connect(result_take_send_parcel)
        root.mouth_number_result.connect(get_locker_number)
    }

    Component.onDestruction: {
        root.overdue_express_list_result.disconnect(overdue_express_list)
        root.overdue_express_count_result.disconnect(overdue_count)
        root.return_express_count_result.disconnect(return_count)
        root.popsafe_express_count_result.disconnect(popsafe_count)
        root.popsend_express_count_result.disconnect(popsend_count)
        root.courier_take_overdue_express_result.disconnect(take_result)
        root.start_time_overdue_result.disconnect(take_overdue_result)
        root.take_send_express_result.disconnect(result_take_send_parcel)
        root.mouth_number_result.disconnect(get_locker_number)
    }

    function take_result(text){
        
        // console.log("take_result test: " + text)
        
        var val = JSON.parse(text)
        page.enabled = true

        if(val.isSuccess == 'true'){
            console.log("isSuccess take result: ")
            dimm_display.visible = false;
            waiting.visible = false;
            model.clear()
            check_list = new Array()
            slot_handler.start_courier_load_overdue_express_list()
            // my_stack_view.push(open_door,{nomorLoker: val.door, logistic: true, batal_state: false})
            if ((val.door).length > 1) {
                my_stack_view.push(open_door, {nomorLoker: val.door, logistic: true, batal_state: false, access_door: true, button_type:2})
                console.log("TRUE take result: ")
            } else {
                console.log("ELSE take result: ")
                var locker = val.door[0]
                console.log("locker_number: " + JSON.stringify(locker))
                var locker_num = locker.number
                var locker_size = locker.size
                express_id = locker.id_express
                my_stack_view.push(open_door, {nomorLoker:locker_num, door_type:locker_size, logistic: true, batal_state: false, express_id: express_id, access_door: true, button_type:2})
                console.log("nomor Lokcer : " + locker_num + " SIze Locker : " + locker_size)
                // my_stack_view.push(open_door, {nomorLoker: door_no, door_type: door_type, header_title: "AMBIL"})
            }
        }
    }

    function overdue_express_list(text){
        console.log("OVERDUE DATA : " + JSON.stringify(text))
        overdue_data = text
        show_overdue_express(text)
    }

    function overdue_count(text){
        overdue_num = text
        // count_page(text)
        // if(text<=10){
        //     //down_button.enabled = false
        //     //down_button.visible = false
        // }
        // else{
        //     //down_button.enabled = true
        //     //down_button.visible = true
        // }
    }

    function return_count(text){
        return_num = text
        console.log("RETURN NUMBER : ", text)
    }

    function popsafe_count(text){
        popsafe_num = text
        console.log("POPSAFE NUMBER : ", text)
    }

    function popsend_count(text){
        popsend_num = text
        console.log("POPSEND NUMBER : ", text)
    }

    function take_overdue_result(text){
        takeOverdue = text
        // takeOverdue = takeOverdue*5
        console.log("DAY TAKE RESULT : ", takeOverdue)
    }

    
    function take_express(choosen_take) {
        
        console.log(" choosen_take: " + choosen_take)
        
        if (choosen_take == "popsend") {
            slot_handler.start_courier_take_send_express(JSON.stringify(check_list))
        } else if (choosen_take == "overdue") {
            console.log("overdue loading...")
            slot_handler.start_courier_take_overdue_express(JSON.stringify(check_list))
        }else if(choosen_take=="reject"){
            slot_handler.start_courier_take_reject_express(JSON.stringify(check_list))
        }else if(choosen_take=="popsafe"){
            slot_handler.start_courier_take_overdue_express(JSON.stringify(check_list))
        }
        check_list = []
    }

    function get_locker_number(argument) {
        console.log("locker_number: " + argument)
        var result = JSON.parse(argument)
        if (result.isSuccess == "true") {
            console.log("locker_number_number: " + result.locker_number)
            door_no = result.locker_number
        }
    }
    
    function result_take_send_parcel(result) {
        
        console.log("result_reject: " + result)
        
        var response = JSON.parse(result)
        if (response.isSuccess == "true") {
            console.log("door: " + (response.door).length + "express_id :" + response.door[0].id_express)
            express_id = response.door[0].id_express
            if ((response.door).length > 1) {
                my_stack_view.push(open_door, {nomorLoker: response.door, logistic: true, batal_state: false, express_id: express_id, access_door: true})
            } else {
                var locker = response.door[0]
                console.log("locker_number: " + locker.number )
                my_stack_view.push(open_door, {nomorLoker: locker.number, door_type:locker.size, logistic: true, batal_state: false, express_id: express_id, access_door: true, button_type:2})
            }
            dimm_display.visible = false;
            waiting.visible = false;
        }
    }

    DimmDisplay{
        id:dimm_display
        visible:false
    }

    AnimatedImage{
        id: waiting
        visible:false
        width: 200
        height: 200
        anchors.verticalCenter: parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter
        source: 'asset/global/animation/loading.gif'
    }

    Rectangle{
        id:main_rec_dimm
        visible:false
        width: 1080
        height: 768
        color: "transparent"

        Rectangle{
            x:50//28
            y: 336//316
            width: 934//967
            height: 100//162
            color: "white"//"#f3f3f3"

            Rectangle{
                anchors.left: parent.left
                anchors.leftMargin: 0
                width: 934//967
                height: 45//77
                color: "#f3f3f3"
            }

            Text{
                anchors.top: parent.top
                anchors.topMargin: 14
                anchors.left: parent.left
                anchors.leftMargin: 19
                anchors.verticalCenter: parent.verticalCenter
                text:qsTr("Resi")
                font.family: fontStyle.medium
                color: "#4f4b4b"
                font.pixelSize: 16
            }
            Text{
                anchors.top: parent.top
                anchors.topMargin: 14
                anchors.left: parent.left
                anchors.leftMargin: 243
                anchors.verticalCenter: parent.verticalCenter
                text:qsTr("Durasi")
                font.family: fontStyle.medium
                color: "#4f4b4b"
                font.pixelSize: 16
            }
            Text{
                anchors.top: parent.top
                anchors.topMargin: 14
                anchors.left: parent.left
                anchors.leftMargin: 331
                anchors.verticalCenter: parent.verticalCenter
                text:qsTr("No Loker")
                font.family: fontStyle.medium
                color: "#4f4b4b"
                font.pixelSize: 16
            }
            Text{
                anchors.top: parent.top
                anchors.topMargin: 14
                anchors.left: parent.left
                anchors.leftMargin: 438
                anchors.verticalCenter: parent.verticalCenter
                text:qsTr("No Handphone")
                font.family: fontStyle.medium
                color: "#4f4b4b"
                font.pixelSize: 16
            }
            Text{
                anchors.top: parent.top
                anchors.topMargin: 14
                anchors.left: parent.left
                anchors.leftMargin: 589
                anchors.verticalCenter: parent.verticalCenter
                text:qsTr("Waktu Simpan")
                font.family: fontStyle.medium
                color: "#4f4b4b"
                font.pixelSize: 16
            }
            Text{
                anchors.top: parent.top
                anchors.topMargin: 14
                anchors.left: parent.left
                anchors.leftMargin: 772
                anchors.verticalCenter: parent.verticalCenter
                text:qsTr("Waktu Terlambat")
                font.family: fontStyle.medium
                color: "#4f4b4b"
                font.pixelSize: 16
            }
            Row {
                id: row2
                x: 0
                y: 62
                width: 934
                height: 77
                Rectangle{
                    Text{
                        font.family:fontStyle.medium
                        text: and_resi
                        //x: 80
                        //y: 62
                        anchors.left: parent.left
                        anchors.leftMargin: 19
                        //anchors.verticalCenterOffset: 45
                        //anchors.horizontalCenterOffset: 45
                        color:"#414042"
                        font.pixelSize:16
                        //.centerIn: parent;
                    }
                }

                Rectangle{
                    Text{
                        font.family:fontStyle.book
                        text:and_duration + qsTr(" Hari")
                        anchors.left: parent.left
                        anchors.leftMargin: 243
                        // anchors.verticalCenterOffset: 45
                        // anchors.horizontalCenterOffset:  135
                        color:"#414042"
                        font.pixelSize:16
                        // anchors.centerIn: parent;
                    }
                }

                Rectangle{
                    Text{
                        font.family:fontStyle.book
                        text:and_boxnum
                        anchors.left: parent.left
                        anchors.leftMargin: 331
                        // anchors.verticalCenterOffset: 45
                        // anchors.horizontalCenterOffset:200
                        color:"#414042"
                        font.pixelSize:16
                        //anchors.centerIn: parent;
                    }
                }

                Rectangle{
                    Text{
                        font.family:fontStyle.medium
                        text:and_phone
                        anchors.left: parent.left
                        anchors.leftMargin: 438
                        // anchors.verticalCenterOffset: 45
                        // anchors.horizontalCenterOffset: 360
                        color:"#414042"
                        font.pixelSize:16
                        //anchors.centerIn: parent;
                    }
                }

                Rectangle{
                    Text{
                        font.family:fontStyle.book
                        text:and_storetime
                        anchors.left: parent.left
                        anchors.leftMargin: 589
                        // anchors.verticalCenterOffset: 45
                        // anchors.horizontalCenterOffset: 560
                        color:"#414042"
                        font.pixelSize:16
                        //anchors.centerIn: parent;
                    }
                }

                Rectangle{
                    Text{
                        font.family:fontStyle.book
                        text: and_taketime
                        anchors.left: parent.left
                        anchors.leftMargin: 772
                        // anchors.verticalCenterOffset: 45
                        // anchors.horizontalCenterOffset: 780
                        color:"#414042"
                        font.pixelSize:16
                        //anchors.centerIn: parent;
                    }
                }
            }
        }
        Image{
            x:956
            y:273
            width:30
            height:30
            source: "asset/courier/list/close.png"
            MouseArea{
                anchors.fill: parent
                onClicked:{
                    main_rec_dimm.visible=false
                    dimm_display.visible = false
                }
            }
        }
    }
}
