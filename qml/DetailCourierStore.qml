import QtQuick 2.4
import QtQuick.Controls 1.2

Background{
    id: detail_parcel
    property var press: '0'
    property var asset_path: 'asset/send_page/'
    property var ext_name: 'asset/global/logo/usr_'
    property var select_courier:''
    property var parcel_number: ''
    property var param_return: ''
    property var courier_name: ''
    property var phone_cust: ''
    property var no_parcel : ''
    show_img: ""
    img_vis: true
    property var txt_time: ""
    property var status_courier : "others"
    property var merchant_info : undefined
    property var drop_by_courier: 0



    Stack.onStatusChanged:{
        if(Stack.status == Stack.Activating){
            timer.secon = 90
            press = '0';
            timer_note.restart()
            courier_logo();
        }
        if(Stack.status==Stack.Deactivating){
            timer_note.stop();
        }
    }

    FontType{
        id:fontStyle
        //Type Font ### bold , extrabold , light , book, medium, black ###
    }

    Header{
        id:header
        title_text:qsTr("SIMPAN PAKET")
        text_timer : txt_time
    }

    Item{
        id: timer
        property int secon  : 90
        Timer{
            id:timer_note
            interval:1000
            repeat:true
            running:true
            triggeredOnStart:true
            onTriggered:{
                timer.secon -= 1
                txt_time = timer.secon
                if(timer.secon < 1){
                    timer_note.stop()
                    // my_stack_view.pop(null)
                    my_stack_view.push(time_out)
                }
            }
        }
    }

    Text {
        id: title_text
        x:50
        y:147
        text: qsTr("Detail Pengiriman Barang")
        font.pixelSize:40
        color:"#414042"
        font.family: fontStyle.medium
    }

    Text {
        id:noresi
        y:242
        x:50
        text: qsTr("No. Resi                ") + "\t\t: " + no_parcel
        font.pixelSize:24
        color:"#3a3a3a"
        font.family: fontStyle.book
        
    }

    Text {
        id:phonecourier
        y:307
        x:52
        text: qsTr("User Name Kurir") + "\t\t: " + courier_name
        font.pixelSize:24
        color:"#3a3a3a"
        font.family: fontStyle.book
    }

    Text {
        id:phonecust
        y:371
        x:52
        text: qsTr("No. Handphone Penerima") +  "\t: " + phone_cust
        font.pixelSize:24
        color:"#3a3a3a"
        font.family: fontStyle.book
    }

    Text {
        id:courier
        y:441
        x:52
        text: qsTr("Kurir     ") + "\t\t\t: " + select_courier
        font.pixelSize:24
        font.capitalization: Font.AllUppercase
        color:"#3a3a3a"
        font.family: fontStyle.book
    }

    Image{
        x: 52
        y: 502
        width: 45
        height:45
        source: "asset/point_icon/lamp.png"
    }

    Image{
        x: 685
        y: 238
        width: 274
        height:176
        // source: ext_name + select_courier +".png"
        source: (status_courier=="others") ? ext_name + "others.png" : ext_name + status_courier +".png"
    }
    Text{
        x:107
        y:512
        text: qsTr("Pastikan nomor yang  dimasukkan aktif, notifikasi akan dikirimkan ke nomor tersebut.")
        font.pixelSize:22
        color:"#ea9803"
        font.family: fontStyle.medium
    }

    NotifButton{
        id:btn_batal
        x:50
        y:597
        buttonText:qsTr("BATAL")
        r_radius:2
        modeReverse:true
        MouseArea{
            anchors.fill: parent
            onClicked: {
                show_batal.start();
                dimm_display.visible=true
            }
            onEntered:{
                btn_batal.modeReverse = false
            }
            onExited:{
                btn_batal.modeReverse = true
            }
        }
    }

    NotifButton{
        id:btn_lanjut
        x:302
        y:597
        r_radius:2
        buttonText:qsTr("LANJUT")
        modeReverse:false
        MouseArea{
            anchors.fill: parent
            onClicked: {
                slot_handler.set_phone_number(phone_cust)
                if (merchant_info != undefined) {
                    merchant_info = JSON.parse(merchant_info)
                    slot_handler.set_client_overdue_popex_info(merchant_info.overdue_time, merchant_info.overdue_type, merchant_info.name)
                }
                my_stack_view.push(box_size, {popsafe_status:"courier_store", drop_by_courier: drop_by_courier})
            }
            onEntered:{
                btn_lanjut.modeReverse = true
            }
            onExited:{
                btn_lanjut.modeReverse = false
            }
        }
    }

    DimmDisplay{
        id:dimm_display
        visible:false
    }

    NotifSwipe{
        id:notif_batal
        source_img: asset_path + "return/batalbg.png"
        img_y:50
        titlebody_x:50
        title_y:60
        body_y:180
        title_text:qsTr("Apakah Kamu Ingin Membatalkan <br>Proses ?")
        body_text:qsTr("Jika kamu membatalkan proses, maka akan diarahkan <br>ke halaman awal.")
        NotifButton{
            id:ok_yes
            x:50
            y:272
            r_radius:0
            modeReverse:true
            buttonText:qsTr("YA")
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 2) return true }))
                }
                onEntered:{
                    ok_yes.modeReverse = false
                }
                onExited:{
                    ok_yes.modeReverse = true
                }

            }
        }
        NotifButton{
            id:ok_no
            x:300
            y:272
            r_radius:0
            buttonText:qsTr("TIDAK")
            modeReverse:false
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    hide_batal.start();
                    dimm_display.visible=false;
                }
                onEntered:{
                    ok_no.modeReverse = true
                }
                onExited:{
                    ok_no.modeReverse = false
                }

            }
        }


        Image{
            x:939
            y:60
            height:33
            width:33
            source:"asset/global/button/close.png"
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    hide_batal.start();
                    dimm_display.visible=false;
                }

            }
        }
    }

    NumberAnimation {
        id:hide_batal
        targets: notif_batal
        properties: "y"
        from:248
        to:768
        duration: 500
        easing.type: Easing.InOutBack
    }
    NumberAnimation {
        id:show_batal
        targets: notif_batal
        properties: "y"
        from:768
        to:248
        duration: 500
        easing.type: Easing.InOutBack
    }

    function courier_logo() {
        var courier_company = select_courier.toLowerCase();
        if(courier_company.search("popbox") > -1){
            status_courier = "popexpress";
        }else if(courier_company.search("anteraja") > -1){
            status_courier = "anteraja";
        }else if(courier_company.search("blibli") > -1){
            status_courier = "blibli";
        }else if(courier_company.search("gosend") > -1){
            status_courier = "gosend";
        }else if(courier_company.search("grab") > -1){
            status_courier = "grab";
        }else if(courier_company.search("jne") > -1){
            status_courier = "jne";
        }else if(courier_company.search("jnt") > -1){
            status_courier = "jnt";
        }else if(courier_company.search("jx") > -1){
            status_courier = "jx";
        }else if(courier_company.search("lex") > -1){
            status_courier = "lex";
        }else if(courier_company.search("ninja") > -1){
            status_courier = "ninja";
        }else if(courier_company.search("pos") > -1){
            status_courier = "pos";
        }else if(courier_company.search("rcl") > -1){
            status_courier = "rcl";
        }else if(courier_company.search("sicepat") > -1){
            status_courier = "sicepat";
        }else if(courier_company.search("tiki") > -1){
            status_courier = "tiki";
        }else if(courier_company.search("wahana") > -1){
            status_courier = "wahana";
        }else{
            status_courier = "others"
        }
    }
}
