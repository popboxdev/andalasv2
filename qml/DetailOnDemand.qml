import QtQuick 2.4
import QtQuick.Controls 1.2

Background{
    id: detail_parcel
    property var press: '0'
    property var asset_path: 'asset/send_page/'
    property var parcel_number: ''
    property var nameowner: "Mike Tyson"
    property var orderno : "PDS654321"
    property var lockername: "Loker Ariobimo Sentral"
    property var lockertarget: "Locker Apartemen Kalibata City"
    property var delimiter: " ]==)> "
    property var ecommerce: "TAYAKA"
    property var data_ondemand: ''
    property var txt_time: ""
    
    // property var taketime: "Selasa, 7 Mei 2019 10:09:00"
    //show_img: asset_path + "background/bg.png"
    show_img: ""
    img_vis: true

    Stack.onStatusChanged:{
        if(Stack.status == Stack.Activating){
            timer.secon = 90
            timer_note.restart()
            press = '0';
            timer_note.stop()
            if (data_ondemand != '') {
                parse_data_ondemand(data_ondemand)
            }
        }
        if(Stack.status==Stack.Deactivating){
            timer_note.stop()
        }
    }

    FontType{
        id:fontStyle
        //Type Font ### bold , extrabold , light , book, medium, black ###
    }

    Header{
        id:header
        title_text:"ONDEMAND"
        text_timer : txt_time
    }

    Item{
        id: timer
        property int secon  : 90
        Timer{
            id:timer_note
            interval:1000
            repeat:true
            running:true
            triggeredOnStart:true
            onTriggered:{
                timer.secon -= 1
                txt_time = timer.secon
                if(timer.secon < 1){
                    timer_note.stop()
                    my_stack_view.push(time_out)
                }
            }
        }
    }

    Text {
        id: nama
        x:50
        y:125
        text: qsTr("Hai, ") + nameowner
        font.pixelSize:40
        color:"#414042"
        font.family: fontStyle.medium
    }

    Text {
        id:noparcel
        y:176
        x:52
        text: qsTr("No. Order Kamu ") + orderno
        font.pixelSize:18
        color:"#3a3a3a"
        font.family: fontStyle.book
        
    }

    Image{
        id:img_merchant
        x:695
        y:228
        width:274
        height:220
        source : ""
    }

    Rectangle{
        x: 45
        y: 229
        width : 631
        height: 219
        color : "#ffffff"
        border.color: "#c4c4c4"
        border.width: 3
        smooth : false
        radius:5

        Rectangle{
            x:110
            y: 95
            width: 510
            height: 2
            color : "#c4c4c4"
        }

        Image{
            x:16
            y:22
            width:27
            height: 27
            source : "asset/point_icon/dotred.png"
        }
        Text{
            y:21
            x:58
            text: qsTr("Dari")
            font.pixelSize:22
            color:"#3a3a3a"
            font.family: fontStyle.light
        }
        Image{
            x:64
            y:57
            width:15
            height: 19
            source : "asset/point_icon/locflag.png"
        }
        Text{
            x:93
            y:54
            font.pixelSize:22
            color:"#3a3a3a"
            font.family: fontStyle.book
            text: lockername
        }

        Image{
            width: 27
            height: 27
            x: 18
            y: 113
            source : "asset/point_icon/dotyellow.png"
        }
        Text{
            y:111
            x:58
            text: qsTr("Tujuan")
            font.pixelSize:22
            color:"#3a3a3a"
            font.family: fontStyle.light
        
        }
        Image{
            x:60
            y:164
            width:15
            height: 19
            source : "asset/point_icon/locflag.png"
        }
        Text{
            x:93
            y:159
            font.pixelSize:22
            color:"#3a3a3a"
            font.family: fontStyle.book
            text:qsTr(lockertarget)
        }
    }

    Image{
        x: 45
        y: 485
        width: 45
        height:45
        source: "asset/point_icon/lamp.png"
    }
    Text{
        x:94
        y:495
        width:488
        height: 20
        text: qsTr("Pastikan paket kamu sudah dibungkus dan dicantumkan no. order ")
        font.pixelSize:22
        font.letterSpacing:0.5
        color:"#ea9803"
        font.family: fontStyle.medium
    }

    NotifButton{
        id:btn_lanjut
        x:50
        y:599
        buttonText:qsTr("LANJUT")
        r_radius:2
        modeReverse:false
        MouseArea {
            anchors.fill: parent
            onClicked: {
                my_stack_view.push(box_size, {popsafe_status:"ondemand"})
                // dimm_display.visible=true;
            }
            onEntered:{
                btn_lanjut.modeReverse = true
            }
            onExited:{
                btn_lanjut.modeReverse = false
            }
        }
    }

    Text{
        x:53
        y:694
        text: qsTr("Dengan mengklik tombol lanjut kamu telah setuju dengan <b>Syarat dan Ketentuan.<b>")
        font.pixelSize:18
        color:"#4a4a4a"
        font.family: fontStyle.book
    }

    DimmDisplay{
        id:dimm_display
        visible:false
    }

    function parse_data_ondemand(data) {
        console.log(" detail_ondemand: " + data)
        var result = JSON.parse(data)
        if (result.isSuccess == "true") {
            var addresses = result.endAddress
            var show_locker = ''
            var show_address = ''
            var split_address = ''

            //Parse From-To
            if (addresses.indexOf(delimiter) > -1){
                split_address = addresses.split(delimiter.toString())
                show_locker = split_address[0]
                show_address = split_address[1]
            }else{
                show_address = addresses
                show_locker = ''
            }

            nameowner = result.recipientName
            orderno = result.customerStoreNumber
            lockername = result.box_name
            // lockertarget = show_address
            ecommerce = result.groupName
        //     orderno = result.package_id
        //     lockername = result.box_name
        //     ecommerce = result.groupName
        //     slot_handler.start_customer_reject_for_electronic_commerce(orderno)            
            if (ecommerce == 'TAPTOPIK') {
                img_merchant.source = asset_path + "ondemand/taptopik.png"
                lockertarget = "Warehouse Taptopik"
            } 
            if (ecommerce == 'OMAISU') {
                lockertarget = "Warehouse Omaisu"
                img_merchant.source = asset_path + "ondemand/omaisu.png"
            } 
            if (ecommerce == 'TAYAKA') {
                img_merchant.source = asset_path + "ondemand/tayaka.png"
                lockertarget = "Warehouse Tayaka"
            } 
            if (ecommerce == 'VCLEAN') {
                img_merchant.source = asset_path + "ondemand/vclean.png"
                lockertarget = "Warehouse Vclean"
            } 
        }
    }

}
