import QtQuick 2.4
import QtQuick.Controls 1.2

Background{
    id: detail_parcel
    property var press: '0'
    property var asset_path: 'asset/send_page/'
    property var parcel_number: ''
    property var nameowner: "Mike Tyson"
    property var orderno : "PDS654321"
    property var lockername: "Loker Ariobimo Sentral"
    property var nametarget: "Kris Jhon"
    property var notarget: "08774383892938"
    property var lockertarget: "Locker Apartemen Kalibata City"
    property var delimiter: " ]==)> "
    property var id_company: ''
    property var data_popsend: ''
    property var txt_time: ""

    show_img: ""
    img_vis: true

    Stack.onStatusChanged:{
        if(Stack.status == Stack.Activating){
            timer.secon = 90
            timer_note.restart()
            press = '0';
            if (data_popsend != '') {
                
                console.log("parcel_number: " + parcel_number)
                
                show_data_popsend(data_popsend)
            }
        }
        if(Stack.status==Stack.Deactivating){
            timer_note.stop()
        }
    }

    FontType{
        id:fontStyle
        //Type Font ### bold , extrabold , light , book, medium, black ###
    }

    Header{
        id:header
        title_text:"PopSend"
        text_timer : txt_time
    }

    Item{
        id: timer
        property int secon  : 90
        Timer{
            id:timer_note
            interval:1000
            repeat:true
            running:true
            triggeredOnStart:true
            onTriggered:{
                timer.secon -= 1
                txt_time = timer.secon
                if(timer.secon < 1){
                    timer_note.stop()
                    my_stack_view.push(time_out)
                }
            }
        }
    }

    Text {
        id: nama
        x:50
        y:125
        text: qsTr("Hai, ") + nameowner
        font.pixelSize:40
        color:"#414042"
        font.family: fontStyle.medium
    }

    Text {
        id:noparcel
        y:176
        x:52
        text: qsTr("No. Order Kamu ") + orderno
        font.pixelSize:18
        color:"#3a3a3a"
        font.family: fontStyle.book
        
    }

    Image{
        x:701
        y:250
        width:288
        height:252
        source : asset_path + "popsend/icon.png"
    }

    Rectangle{
        x: 45
        y: 229
        width : 631
        height: 320
        color : "#ffffff"
        radius: 3
        border.color: "#c4c4c4"
        border.width: 1
        smooth : false

        Image{
            x:16
            y:22
            width:27
            height: 27
            source : "asset/point_icon/dotred.png"
        }
        Text{
            y:21
            x:58
            text: qsTr("Dari")
            font.pixelSize:22
            color:"#3a3a3a"
            font.family: fontStyle.light
        }
        Image{
            x:64
            y:57
            width:15
            height: 19
            source : "asset/point_icon/locflag.png"
        }
        Text{
            x:93
            y:54
            font.pixelSize:22
            color:"#3a3a3a"
            font.family: fontStyle.book
            text: lockername
        }

        Image{
            width: 27
            height: 27
            x: 18
            y: 113
            source : "asset/point_icon/dotyellow.png"
        }
        Text{
            y:111
            x:58
            text: qsTr("Tujuan")
            font.pixelSize:22
            color:"#3a3a3a"
            font.family: fontStyle.light
        
        }
        Image{
            x:60
            y:164
            width:19
            height: 19
            source : "asset/point_icon/icon_user.png"
        }
        Text{
            x:93
            y:159
            font.pixelSize:22
            color:"#3a3a3a"
            font.family: fontStyle.book
            text:qsTr(nametarget +" | "+ notarget)
        }
        Image{
            x:64
            y:220
            width:15
            height: 19
            source : "asset/point_icon/locflag.png"
        }
        Text{
            x:93
            y:215
            width: 500
            height: 80 //Harus 80
            font.pixelSize:22
            color:"#3a3a3a"
            font.family: fontStyle.book
            // elide: Text.ElideRight
            text:qsTr(lockertarget)
            elide: Text.ElideRight
        }
    }

    Image{
        x: 45
        y: 550
        width: 45
        height:45
        source: "asset/point_icon/lamp.png"
    }
    Text{
        x:94
        y:557
        width:488
        height: 20
        text: qsTr("Pastikan paket kamu sudah dibungkus dan dicantumkan no. order")
        font.pixelSize:22
        font.letterSpacing:0.5
        color:"#ea9803"
        font.family: fontStyle.medium
    }

    NotifButton{
        id:btn_lanjut
        x:50
        y:599
        buttonText:qsTr("LANJUT")
        r_radius:2
        modeReverse:false
        MouseArea {
            anchors.fill: parent
            onClicked: {
                my_stack_view.push(box_size, {popsafe_status:"popsend", isPopDeposit: false, parcel_number: parcel_number})
            }
            onEntered:{
                btn_lanjut.modeReverse = true
            }
            onExited:{
                btn_lanjut.modeReverse = false
            }
        }
    }

    Text{
        x:53
        y:694
        text: qsTr("Dengan mengklik tombol lanjut kamu telah setuju dengan <b>Syarat dan Ketentuan.</b>")
        font.pixelSize:18
        color:"#4a4a4a"
        font.family: fontStyle.book
    }

    DimmDisplay{
        id:dimm_display
        visible:false
    }

    function show_data_popsend(argument) {
        console.log(" detail_popsend: " + argument)
        var result = JSON.parse(argument)
        if (result.isSuccess == "true") {
            var addresses = result.endAddress
            var show_locker = ''
            var show_address = ''
            var split_address = ''

            //Parse From-To
            if (addresses.indexOf(delimiter) > -1){
                split_address = addresses.split(delimiter.toString())
                show_locker = split_address[0]
                show_address = split_address[1]
            }else{
                show_address = addresses
                show_locker = locker_name
            }
        
            lockername = result.box_name
            lockertarget = show_address
            // notarget = result.takeUserPhoneNumber //OLD
            notarget = result.recipientUserPhoneNumber
            orderno = result.customerStoreNumber
            nameowner = result.sender
            nametarget = result.recipientName
        }
        
    }

}
