import QtQuick 2.4
import QtQuick.Controls 1.2

Background{
    id: detail_parcel
    property var press: '0'
    property var asset_path: 'asset/send_page/'
    property var parcel_number: ''
    property int character:26
    property var ch:1 
    property var idpacket: '(kosong)'
    property var phone_number: ''
    property var lokasilocker: '(kosong)'
    property var data_return: ''
    property var ecommerce: ''
    property var txt_time: ""
    
    
    // property var taketime: "Selasa, 7 Mei 2019 10:09:00"
    //show_img: asset_path + "background/bg.png"
    show_img: ""
    img_vis: true

    Stack.onStatusChanged:{
        if(Stack.status == Stack.Activating){
            timer.secon = 90
            timer_note.restart()
            press = '0';
            if (data_return != '') {
                parse_data_return(data_return)
            }
        }
        if(Stack.status==Stack.Deactivating){
            timer_note.stop()
        }
    }

    FontType{
        id:fontStyle
        //Type Font ### bold , extrabold , light , book, medium, black ###
    }

    Header{
        id:header
        title_text:qsTr("Pengembalian Barang")
        text_timer : txt_time
    }

    Item{
        id: timer
        property int secon  : 90
        Timer{
            id:timer_note
            interval:1000
            repeat:true
            running:true
            triggeredOnStart:true
            onTriggered:{
                timer.secon -= 1
                txt_time = timer.secon
                if(timer.secon < 1){
                    timer_note.stop()
                    my_stack_view.push(time_out)
                }
            }
        }
    }

    Text {
        x:50
        y:145
        text: qsTr("Detail Pengembalian Barang")
        font.pixelSize:40
        color:"#414042"
        font.family: fontStyle.medium
    }

    Image{
        id:img_merchant
        x:685
        y:145
        height:176
        width:274
        source: ""
    }

    Text {
        id:id_packet
        y:245
        x:50
        text: qsTr("ID Paket") + "\t\t: " + idpacket
        font.pixelSize:24
        color:"#3a3a3a"
        font.family: fontStyle.book
        
    }
    Text {
        id:no_hp
        y:311
        x:50
        text: qsTr("No Handphone") + "\t: " + phone_number
        font.pixelSize:24
        color:"#3a3a3a"
        font.family: fontStyle.book
    }
    Text {
        id:change_no_hp
        y:313
        x:500
        text: qsTr("<u>Ubah No Handphone</u>")
        font.pixelSize:18
        color:"#ff524f"
        font.family: fontStyle.book
        MouseArea{
            anchors.fill: parent
            onClicked:{
                my_stack_view.pop()
            }
        }
    }

    Text {
        id:lokasi
        y:377
        x:50
        text: qsTr("Lokasi Loker      ") + "\t: " + lokasilocker
        font.pixelSize:24
        color:"#3a3a3a"
        font.family: fontStyle.book
    }

    Text {
        y:567
        x:50
        text: qsTr("Pastikan nomor yang  dimasukkan aktif, notifikasi akan dikirimkan ke nomor tersebut.")
        font.pixelSize:22
        color:"#3a3a3a"
        font.family: fontStyle.book
    }

    NotifButton{
        id:btn_batal
        x:57
        y:468
        buttonText:qsTr("BATAL")
        r_radius:0
        modeReverse:true
        MouseArea{
            anchors.fill: parent
            onClicked: {
                show_batal.start();
                dimm_display.visible=true
            }
            onEntered:{
                btn_batal.modeReverse = false
            }
            onExited:{
                btn_batal.modeReverse = true
            }
        }
    }

    NotifButton{
        id: btn_lanjut
        x:302
        y:468
        r_radius:0
        buttonText:qsTr("LANJUT")
        modeReverse:false
        MouseArea{
            anchors.fill: parent
            onClicked: {
                my_stack_view.push(box_size, {popsafe_status:"return"})
            }
            onEntered:{
                btn_lanjut.modeReverse = true
            }
            onExited:{
                btn_lanjut.modeReverse = false
            }
        }
    }

    DimmDisplay{
        id:dimm_display
        visible:false
    }

    NotifSwipe{
        id:notif_batal
        source_img: asset_path + "return/batalbg.png"
        img_y:50
        titlebody_x:50
        title_y:60
        body_y:180
        title_text:qsTr("Apakah Kamu Ingin Membatalkan <br>Proses ?")
        body_text:qsTr("Jika kamu membatalkan proses, maka akan diarahkan <br>ke halaman awal.")
        NotifButton{
            id:ok_yes
            x:50
            y:272
            r_radius:0
            modeReverse:true
            buttonText:qsTr("YA")
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
                }
                onEntered:{
                    ok_yes.modeReverse = false
                }
                onExited:{
                    ok_yes.modeReverse = true
                }
            }
        }
        NotifButton{
            id:ok_no
            x:300
            y:272
            r_radius:0
            buttonText:qsTr("TIDAK")
            modeReverse:false
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    hide_batal.start();
                    dimm_display.visible=false;
                }
                onEntered:{
                    ok_no.modeReverse = true
                }
                onExited:{
                    ok_no.modeReverse = false
                }
            }
        }


        Image{
            x:939
            y:60
            height:33
            width:33
            source:"asset/global/button/close.png"
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    hide_batal.start();
                    dimm_display.visible=false;
                }

            }
        }
    }

    NumberAnimation {
        id:hide_batal
        targets: notif_batal
        properties: "y"
        from:248
        to:768
        duration: 500
        easing.type: Easing.InOutBack
    }
    NumberAnimation {
        id:show_batal
        targets: notif_batal
        properties: "y"
        from:768
        to:248
        duration: 500
        easing.type: Easing.InOutBack
    }

    
    function parse_data_return(data) {
        var result = JSON.parse(data)
        if (result.isSuccess == "true") {
            idpacket = result.package_id
            lokasilocker = result.box_name
            ecommerce = (result.groupName).toUpperCase()
            slot_handler.start_customer_reject_for_electronic_commerce(idpacket)

            if (ecommerce == 'BLIBLI') {
                img_merchant.source = asset_path + "return/blibli.png"

            } else if (ecommerce == 'ZALORA') {
                img_merchant.source = asset_path + "return/zalora.png"

            } else if (ecommerce == 'ORIFLAME') {
                img_merchant.source = asset_path + "return/oriflame.png"

            } else if (ecommerce == 'MATAHARIMALL') {
                img_merchant.source = asset_path + "return/matahari.png"

            } else if (ecommerce == 'BUKALAPAK') {
                img_merchant.source = asset_path + "return/bukalapak.png"
            
            } else if (ecommerce == 'FARMAKU') {
                img_merchant.source = asset_path + "return/farmaku.png"
            
            } else if (ecommerce == 'HOMTEL') {
                img_merchant.source = asset_path + "return/homtel.png"
            
            } else if (ecommerce == 'BERRYBENKA') {
                img_merchant.source = asset_path + "return/berrybenka.png"
            
            } else if (ecommerce == 'JAVAMIFI') {
                img_merchant.source = asset_path + "return/javamifi.png"
            
            } else if (ecommerce == 'LAZADA') {
                img_merchant.source = asset_path + "return/lazada.png"
            
            } else if (ecommerce == 'RAYSPEED') {
                img_merchant.source = asset_path + "return/rayspeed.png"
            
            } else if (ecommerce == 'GIG') {
                img_merchant.source = asset_path + "return/gig.png"
            
            } else {
                img_merchant.source = asset_path + "return/popbox.png"
            
            } 
        }
    }
}
