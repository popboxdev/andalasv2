import QtQuick 2.4
import QtQuick.Controls 1.2

Background{
    id: detail_parcel
    property var press: '0'
    property var asset_path: 'asset/send_page/'
    property var parcel_number: ''
    property int character:26
    property var ch:1 
    property var nameowner: "Developer"
    property var orderno : "PDS654321"
    property var 
    lockername: "Loker PopBox"
    property var taketime: ""
    //show_img: asset_path + "background/bg.png"
    property var step_choose:''
    property var delimiter: " ]==)> "    
    property var data_popsafe: ''
    property var lockername_local: ''
    property var lockername_popsafe: ''
    property var chargeType: ''
    
    property var locker_number: 0
    
    property bool isPopDeposit: false
    property string designationSize: "none"
    
    property var popsafe_param: undefined
    property var txt_time: ""
    property var popsafe_status:""
    property bool drop_by_kirim: true
    
    show_img: ""
    img_vis: true

    Stack.onStatusChanged:{
        if(Stack.status == Stack.Activating){
            timer.secon = 90
            timer_note.restart()
            // slot_handler.start_get_file_dir(path);
            // slot_handler.start_get_locker_name();
            // timer_clock.start();
            // timer_startup.start();
            // hide_key.start();s
            press = '0';
            slot_handler.start_get_locker_name()
            slot_handler.start_video_capture('store_popsafe_app_'+parcel_number)
            if (data_popsafe != '') {
                parse_data_popsafe(data_popsafe)
            }
        }
        if(Stack.status==Stack.Deactivating){
            timer_note.stop()
            // timer_clock.stop();
            // slider_timer.stop();
            // timer_startup.stop();
        }
    }

    FontType{
        id:fontStyle
        //Type Font ### bold , extrabold , light , book, medium, black ###
    }

    Header{
        id:header
        title_text:qsTr("TITIP")
        text_timer : txt_time
    }

    Item{
        id: timer
        property int secon  : 90
        Timer{
            id:timer_note
            interval:1000
            repeat:true
            running:true
            triggeredOnStart:true
            onTriggered:{
                timer.secon -= 1
                txt_time = timer.secon
                if(timer.secon < 1){
                    timer_note.stop()
                    my_stack_view.push(time_out)
                }
            }
        }
    }

    Text {
        id: nama
        x:50
        y:125
        text: qsTr("Hai, ") + nameowner
        font.pixelSize:40
        color:"#414042"
        font.family: fontStyle.medium
    }

    Text {
        id:noparcel
        y:176
        x:52
        text: qsTr("No. Order Kamu ") + orderno
        font.pixelSize:18
        color:"#3a3a3a"
        font.family: fontStyle.book
        
    }

    
    Rectangle{
        x: 45
        y: 229
        width : 944
        height: 205
        color : "#ffffff"
        border.color: "#c4c4c4"
        border.width: 5
        smooth : true
        radius: 5

        Rectangle{
            x:110
            y: 95
            width: 785
            height: 2
            color : "#c4c4c4"
        }
        Text{
            y:21
            x:58
            text: qsTr("Locker Titip")
            font.pixelSize:22
            color:"#3a3a3a"
            font.family: fontStyle.light
        
        }
        Text{
            y:114
            x:69
            text: qsTr("Batas Pengambilan Barang* ")
            font.pixelSize:22
            color:"#3a3a3a"
            font.family: fontStyle.light
        
        }
        Image{
            x:16
            y:22
            width:27
            height: 27
            source : "asset/point_icon/dotred.png"
        }
        Image{
            width: 27
            height: 27
            x: 18
            y: 113
            source : "asset/point_icon/dotyellow.png"
        }
        Image{
            x:64
            y:57
            width:15
            height: 19
            source : "asset/point_icon/locflag.png"
        }
        Text{
            x:93
            y:54
            font.pixelSize:22
            color:"#3a3a3a"
            font.family: fontStyle.book
            text: lockername
            id: id_locker_name
        }
        Image{
            x:64
            y:156
            width:25
            height: 23.4
            source : "asset/point_icon/timer.png"
        }
        Text{
            x:105
            y:152
            font.pixelSize:22
            color:"#3a3a3a"
            font.family: fontStyle.book
            text: taketime
        }
    }
    Image{
        x: 45
        y: 485
        width: 45
        height:45
        source: "asset/point_icon/lamp.png"
    }
    Text{
        x:100
        y:485
        width:488
        height: 20
        text: qsTr("Transaksi ini berlaku untuk satu (1) kali penyimpanan barang, kode buka yang akan <br>dikirimkan hanya dapat digunakan satu (1) kali.")
        font.pixelSize:16
        font.bold: true
        font.letterSpacing:0.5
        color:"#4a4a4a"
        font.family: fontStyle.medium
    }
    

    Rectangle{
        id : nextButton
        x:50
        y:598.9
        width: 230
        height: 80
        color: "#ff524f"
        Text {
            id: text_button
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            font.pixelSize:24
            color:"#ffffff"
            font.family: fontStyle.book
            text: qsTr("LANJUT")
        }
        MouseArea {
            anchors.fill: parent
            onClicked: {
                
                console.log(" parcel_number: " + orderno)
                
                my_stack_view.push(box_size, {popsafe_status:"app", isPopDeposit: isPopDeposit, parcel_number: orderno, drop_by_kirim: true})
                dimm_display.visible=false;
            }
        }
    }
    Text{
        x:53
        y:694
        text: qsTr("Dengan mengklik tombol lanjut kamu telah setuju dengan <b>Syarat dan Ketentuan.</b>")
        font.pixelSize:18
        color:"#4a4a4a"
        font.family: fontStyle.book
    }

    DimmDisplay{
        id:dimm_display
        visible:false
    }

    function parse_data_popsafe(data) {
        var result = JSON.parse(data)
        if (result.isSuccess == "true") {
            var addresses = result.endAddress
            var show_locker = ''
            var show_address = ''
            var split_address = ''

            //Parse From-To
            if (addresses.indexOf(delimiter) > -1){
                split_address = addresses.split(delimiter.toString())
                show_locker = split_address[0]
                show_address = split_address[1]
            }else{
                show_address = addresses
                show_locker = lockername_local
            }
        
            lockername = result.box_name
            taketime = result.expired_time
            orderno = result.customerStoreNumber
            if (result.parcelType == 'POPSAFE') {
                nameowner = result.recipientName
            } else {
                nameowner = result.sender

            }
            lockername_popsafe = show_locker
            chargeType = result.chargeType

            //Parse if classified as PopDeposit Parcel under "PDS" prefix
            if(orderno.substring(0,3)=="PLL" || orderno.substring(0,3)=="PLA" || orderno.substring(0,3)=="PLI"){ //Bermasalah
                isPopDeposit = false
            }else{
                isPopDeposit = true
            }

            if (chargeType == "NOT_CHARGE") {
                if ("designationSize" in result) {
                    if (result.designationSize=="") {
                        designationSize = "none"
                    }else if (result.designationSize=="DOC") {
                        designationSize = "S"
                    }else{
                        designationSize = result.designationSize
                    }
                }else{
                    designationSize = "none"
                }
            }
        }
    }
    
}


