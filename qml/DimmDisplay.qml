import QtQuick 2.4
import QtQuick.Controls 1.2

Rectangle {
    id:dimm_display
    width:parent.width
    height:parent.height
    color: "black"
    opacity: 0.5
    MouseArea {
        anchors.fill: parent
    }
}

