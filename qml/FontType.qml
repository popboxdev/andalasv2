import QtQuick 2.4
import QtQuick.Controls 1.2

Item {
    id: fontStyle
    property var black : black_f.name
    property var bold : bold_f.name
    property var extrabold : extrabold_f.name
    property var book : book_f.name
    property var light : light_f.name
    property var medium : medium_f.name
    property var asset_font: 'asset/font/'

    FontLoader {
        id: black_f
        source: asset_font + "AirbnbCerealBlack.ttf"
    }
    FontLoader {
        id: bold_f
        source: asset_font + "AirbnbCerealBold.ttf"
    }
    FontLoader {
        id: extrabold_f
        source: asset_font + "AirbnbCerealExtraBold.ttf"
    }
    FontLoader {
        id: book_f
        source: asset_font + "AirbnbCerealBook.ttf"
    }
    FontLoader {
        id: light_f
        source: asset_font + "AirbnbCerealLight.ttf"
    }
    FontLoader {
        id: medium_f
        source: asset_font + "AirbnbCerealMedium.ttf"
    }

}