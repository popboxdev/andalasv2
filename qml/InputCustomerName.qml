import QtQuick 2.4
import QtQuick.Controls 1.2

Background{
    id: courier_login_page
    property var press: '0'
    property var asset_global: 'asset/global/'
    property var asset_path: 'asset/take_page/'
    property int character:26
    property var ch:1 
    property var count:0
    property var identity:""
    property var level_user
    property var level_name
    property var txt_time: ""
    property var pin_code: ""
    property var parcel_number: ""
    property var door_type: ""
    property int door_no: 0
    property var nomorLoker: ""
    property var survey_expressNumber : ""
    property var survey_expressId : ""
    property var express_id : ""
    property var language: 'ID'
    property var recipient_name: ''
    
    

    Stack.onStatusChanged:{
        if(Stack.status == Stack.Activating){
            console.log("Parcel Number " + parcel_number + "Pin : "+ pin_code);
            
            timer.secon = 90
            timer_note.restart()
            press = '0';
            show_key.start();
            input_username.bool = true
            console.log(identity)
            // clear_all();
        }
        if(Stack.status==Stack.Deactivating){
            timer_note.stop()
        }
    }

    FontType{
        id:fontStyle
        //Type Font ### bold , extrabold , light , book, medium, black ###
    }

    Header{
        id:header
        title_text:identity
        text_timer : txt_time
        img_vis: false
    }

    Item{
        id: timer
        property int secon  : 90
        Timer{
            id:timer_note
            interval:1000
            repeat:true
            running:true
            triggeredOnStart:true
            onTriggered:{
                timer.secon -= 1
                txt_time = timer.secon
                if(timer.secon < 1){
                    timer_note.stop()
                    my_stack_view.push(time_out)
                }
            }
        }
    }
    
    Text {
        id: username_txt
        x:50
        y:145
        text: qsTr("Nama Kamu")
        font.pixelSize:40
        color:"#414042"
        font.family: fontStyle.medium
    }

    Text {
        id: wrong_user
        visible:false
        x:51
        y:324
        text: qsTr("Username dan password salah! silakan masukkan kembali")
        font.pixelSize:22
        color:"#ff524f"
        font.family: fontStyle.book
    }

    InputText{
        id:input_username
        x:50
        y:230
        text_width:750
        r_width: 680
        borderColor : "#8c8c8c"
        MouseArea {
            anchors.fill: parent
            onClicked: {
                ch=1
                press=1
                input_username.bool = true
                
                console.log(" length: " + ch)
                
                if (input_username.show_text.length) {
                    btn_lanjut.buttonColor = "#ff524f"
                    btn_lanjut.enabled = true
                }
            }
        }
        Image{
            id: del_username
            x:620
            y:22
            width:36
            height:36
            visible:false
            source:asset_global + "button/delete.png"
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    input_username.show_text = "";
                    count=0;
                    ch=1
                    touch_keyboard.input_text("")
                    del_username.visible=false
                }
            }
        }
    }

    NotifButton{
        id:btn_lanjut
        x:750
        y:230
        enabled: false
        buttonText:qsTr("LANJUT")
        buttonColor: "#8c8c8c"
        r_radius:0
        modeReverse:false
        MouseArea{
            anchors.fill: parent
            onClicked: {
                console.log("clicked: " + input_username.show_text)
                recipient_name = input_username.show_text
                // slot_handler.set_recipient_name(recipient_name)
                if(language=="MY"){
                    show_selectidcard.start()
                }else{
                    // slot_handler.start_get_express_mouth_number_by_express_id(express_id)
                    slot_handler.set_recipient_name(recipient_name)
                    slot_handler.customer_take_express(pin_code);
                }
                dimm_display.visible = true
                // waiting.visible = true // dirubah
            }
            onEntered:{
                btn_lanjut.modeReverse = true
            }
            onExited:{
                btn_lanjut.modeReverse = false
            }
        }
    }

    KeyBoardAlphanumeric{
        id:touch_keyboard
        property var validate_c
        closeButton: false
        Component.onCompleted: {
            touch_keyboard.letter_button_clicked.connect(input_text)
            // touch_keyboard.function_button_clicked.connect(on_function_button_clicked)
        }

        function input_text(str){
            if(str == "OKE" || str == "OK"){
                str = "";
                if(press != "1"){
                    return
                }
                press = "0";
                dimm_display.visible = true
                waiting.visible = true
            }
            if(ch==1){
                if (str == "" && input_username.show_text.length > 0){
                    input_username.show_text.length--
                    input_username.show_text=input_username.show_text.substring(0,input_username.show_text.length-1)
                    count--;                 
                }
                if (str != "" && input_username.show_text.length< character){
                    input_username.show_text.length++
                    count++;
                }
                if (input_username.show_text.length>=character){
                    str=""
                    input_username.show_text.length=character
                }else{
                    input_username.show_text += str
                }
                if(count>0){
                    if (input_username.show_text.length > 1){
                        btn_lanjut.buttonColor = "#ff524f"
                        btn_lanjut.enabled = true
                    }
                    input_username.borderColor = "#ff7e7e"
                    del_username.visible=true
                } else{
                    btn_lanjut.buttonColor = "#c4c4c4"
                    btn_lanjut.enabled = false
                    input_username.borderColor = "#8c8c8c"
                    del_username.visible=false
                }
            }
        }
    }

    Component.onCompleted: {
        root.customer_take_express_result.connect(process_take_parcel)
        root.mouth_number_result.connect(get_locker_number)
    }

    Component.onDestruction: {
        root.customer_take_express_result.disconnect(process_take_parcel)
        root.mouth_number_result.disconnect(get_locker_number)
    }
        

    AnimatedImage{
        id: waiting
        visible:false
        width: 200
        height: 200
        anchors.verticalCenter: parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter
        source: 'asset/global/animation/loading.gif'
    }

    NumberAnimation {
        id:hide_key
        targets: touch_keyboard
        properties: "y"
        from:368
        to:768
        duration: 500
        easing.type: Easing.InOutBack
    }
    NumberAnimation {
        id:show_key
        targets: touch_keyboard
        properties: "y"
        from:768
        to:368
        duration: 500
        easing.type: Easing.InOutBack
    }

    NumberAnimation {
        id:hide_selectidcard
        targets: select_idcard
        properties: "y"
        from:248
        to:768
        duration: 500
        easing.type: Easing.InOutBack
    }
    
    NumberAnimation {
        id:show_selectidcard
        targets: select_idcard
        properties: "y"
        from:768
        to:248
        duration: 500
        easing.type: Easing.InOutBack
    }

    NumberAnimation {
        id:hide_doorfail
        targets: door_fail
        properties: "y"
        from:248
        to:768
        duration: 500
        easing.type: Easing.InOutBack
    }
    
    NumberAnimation {
        id:show_doorfail
        targets: door_fail
        properties: "y"
        from:768
        to:248
        duration: 500
        easing.type: Easing.InOutBack
    }

    DimmDisplay{
        id:dimm_display
        visible:false
    }

    NotifSwipe{
        id:select_idcard
        img_y:50
        source_img: asset_path + "signature.png"
        title_text:qsTr("Choose Identification Method")
        body_y: 120
        body_text:qsTr("Please select one of these methods to continue <br>the process")
        NotifButton{
            id:btn_idcard
            x:50
            y:337
            r_radius:2
            buttonText:qsTr("IC CARD")
            modeReverse:false
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    hide_selectidcard.start();
                    dimm_display.visible=false;
                    my_stack_view.push(input_idcard, {customer_name: input_username.show_text, pin_code:pin_code, parcel_number:parcel_number, identity:identity, survey_expressNumber: survey_expressNumber, survey_expressId: survey_expressId, express_id: express_id, recipient_name: recipient_name})
                    // clear_all();

                }
                onEntered:{
                    btn_idcard.modeReverse = true
                }
                onExited:{
                    btn_idcard.modeReverse = false
                }
            }
        }

        NotifButton{
            id:btn_idpassport
            x:300
            y:337
            r_radius:2
            buttonText:qsTr("PASSPORT")
            modeReverse:false
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    hide_selectidcard.start();
                    dimm_display.visible=false;
                    my_stack_view.push(input_passport, {customer_name: input_username.show_text, pin_code:pin_code, parcel_number:parcel_number, identity:identity, survey_expressNumber: survey_expressNumber, survey_expressId: survey_expressId, express_id: express_id, recipient_name: recipient_name})
                    // clear_all();
                }
                onEntered:{
                    btn_idpassport.modeReverse = true
                }
                onExited:{
                    btn_idpassport.modeReverse = false
                }
            }
        }

        Image{
            x:939
            y:60
            height:33
            width:33
            source:"asset/global/button/close.png"
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    hide_selectidcard.start();
                    dimm_display.visible=false
                }
            }
        }
    }

    NotifSwipe{
        id:door_fail
        img_y:50
        source_img: "asset/send_page/background/unknown_popsafeCopy.PNG"
        title_text:qsTr("Oops pintu gagal dibuka")
        body_text:qsTr("Mohon ulangi kembali proses")
        NotifButton{
            id:btn_door_fail
            x:50
            y:337
            r_radius:2
            buttonText:qsTr("BATAL")
            modeReverse:false
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    hide_doorfail.start();
                    dimm_display.visible=false
                    press=0;
                    my_stack_view.pop(null)
                }
                onEntered:{
                    btn_door_fail.modeReverse = true
                }
                onExited:{
                    btn_door_fail.modeReverse = false
                }
            }
        }

        Image{
            x:939
            y:60
            height:33
            width:33
            source:"asset/global/button/close.png"
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    hide_doorfail.start();
                    dimm_display.visible=false
                    my_stack_view.pop()
                }
            }
        }
    }

    function handle_result(result){
       
    }

    function get_locker_number(argument) {
        console.log("locker_number: " + argument)
        var result = JSON.parse(argument)
        if (result.isSuccess == "true") {
            console.log("locker_number_number: " + result.locker_number)
            door_no = result.locker_number
            door_type = result.locker_number_size
            my_stack_view.push(open_door, {nomorLoker: door_no, door_type: door_type, header_title: qsTr("AMBIL"), survey_expressNumber: survey_expressNumber, survey_expressId: survey_expressId, express_id: express_id, access_door: true, button_type:1})
        }
    }

    function process_take_parcel(response) {
        console.log("customer_take_express_customer_name: " + JSON.stringify(response))
        var result = JSON.parse(response)
        if (result.isSuccess == "true") {
            slot_handler.start_video_capture('take_express_'+pin_code)
            slot_handler.start_get_express_mouth_number_by_express_id(express_id)
        } 
        else if (result.isSuccess == "false" && result.doorFlag == 'false'){
            waiting.visible = false
            show_doorfail.start()
        }
        else {
            dimm_display.visible=true
            show_popsafe.start()
        }
    }
    
    function clear_all() {
        press = 0;
        count = 0;
        input_username.bool = true
        input_username.show_text = "";
        touch_keyboard.input_text("")
        btn_lanjut.buttonColor = "#c4c4c4"
        btn_lanjut.enabled = false
        input_username.borderColor = "#8c8c8c"
        del_username.visible=false
        ch=1;
    }

}