import QtQuick 2.4
import QtQuick.Controls 1.2

Background{
    id: input_door_page
    show_img : ""
    img_vis:true
    property var asset_path: "asset/global/input_phone/"
    property var asset_global: 'asset/global/'
    property var path_background: 'asset/send_page/background/'
    property var font_type: "SourceSansPro-Regular"
    property var font_type2: "SourceSansPro-SemiBold"
    property var title_body:""
    property var input_door:""
    property int count:0

    Stack.onStatusChanged:{
        if(Stack.status == Stack.Activating){
            // header.timer_start=true
        }
        if(Stack.status==Stack.Deactivating){
            // header.timer_start=false
        }
    }
    Header{
        id:header
        title_text:"INPUT DOOR"
    }

    Rectangle{
        id:phonebox
        x:50
        y:230
        width: 620
        height: 80
        color: "transparent"
        //border.color: '#c4c4c4'
        border.width: 3
        radius : 5
        //borderColor : "#c4c4c4"
        

        TextEdit{
            id:phone
            text:input_door
            anchors.topMargin: 22
            anchors.leftMargin: 20
            anchors.fill: parent
            horizontalAlignment: Text.AlignLeft
            font.family:"SourceSansPro-SemiBold"
            color:"#414042"
            font.pixelSize:32
            font.bold: true
        }
    }

    Rectangle{
        id : nextButton
        x:690
        y:230
        width: 230
        height: 80
        color: "#c4c4c4"
        Text {
            id: text_button
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            font.pixelSize:24
            color:"#ffffff"
            font.family: font_type
            text: qsTr("LANJUT")            
        }
        MouseArea {
            anchors.fill: parent
            onClicked: {
                if(input_door != ""){

                    console.log(input_door)
                    input_door = input_door*1
                    // my_stack_view.push(open_door, {nomorLoker: input_door, door_type: "XS", popsafe_status: "lastmile", logistic: false, })
                    my_stack_view.push(open_door, {nomorLoker: input_door, door_type: "XS", button_type:4})

                }
            }
        }
    }


    KeyBoardNumber{
        id:keyboard_number

        Component.onCompleted: {
            keyboard_number.letter_button_clicked.connect(show_validate_code);
        }

        NumButton{
            id: reset_button
            x: 298
            y: 245
            width: 125
            height: 65
            show_img: true
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    if (count >= 1 || count <= 15){
                        if (count >=14)
                            count = count-1
                        count--;
                    }
                    if (count <=0){ 
                        count=0;
                    }
                    // input_door = input_door.substring(0,input_door.length-1);
                }
            }
        }

        function show_validate_code(str){
            if (str != "" && count < 15){
                count++;
            }
            if (count >= 15){
                str="";
            }
            input_door += str;
            // abc.counter = timer_value;
            // my_timer.restart()
        }
    }

}
