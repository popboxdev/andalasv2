import QtQuick 2.4
import QtQuick.Controls 1.2

Background{
    id: input_email_page
    property var press: '0'
    property var asset_global: 'asset/global/'
    property var asset_path: 'asset/subscribe/'
    property int character:26
    property var ch:1 
    property var count:0
    property var txt_time: ""
    property var language: 'ID'
    property var cust_phone: ''
    property var cust_name: ''
    property variant check_domain: [".com", ".net", ".co.id", ".asia", ".ac.id", ".id", ".com.my", ".my" ,".sg", ".ph", ".info", ".tv", ".sch.id", ".go.id"]
    

    Stack.onStatusChanged:{
        if(Stack.status == Stack.Activating){
            slot_handler.start_get_language();
            timer.secon = 90
            timer_note.restart()
            press = '0';
            show_key.start();
            input_email_customer.bool = true
        }
        if(Stack.status==Stack.Deactivating){
            timer_note.stop()
        }
    }

    Component.onCompleted: {
        root.start_get_language_result.connect(locker_language);
        root.start_post_subscribe_data_result.connect(post_data_result)
    }

    Component.onDestruction: {
        root.start_get_language_result.disconnect(locker_language);
        root.start_post_subscribe_data_result.disconnect(post_data_result)
    }

    FontType{
        id:fontStyle
        //Type Font ### bold , extrabold , light , book, medium, black ###
    }

    Header{
        id:header
        title_text:qsTr("SUBSCRIBE")
        text_timer : txt_time
    }

    Item{
        id: timer
        property int secon  : 90
        Timer{
            id:timer_note
            interval:1000
            repeat:true
            running:true
            triggeredOnStart:true
            onTriggered:{
                timer.secon -= 1
                txt_time = timer.secon
                if(timer.secon < 1){
                    timer_note.stop()
                    my_stack_view.push(time_out)
                }
            }
        }
    }

    Text {
        id:note
        y:317
        x:50
        text: qsTr("Silakan masukkan alamat email yang benar, misalnya name@mail.com")
        font.pixelSize:20
        color:"#3a3a3a"
        font.family: fontStyle.book
    }
    
    Text {
        id: username_txt
        x:50
        y:145
        text: qsTr("Email")
        font.pixelSize:40
        color:"#414042"
        font.family: fontStyle.medium
    }

    InputText{
        id:input_email_customer
        x:50
        y:232
        text_width:750
        r_width:621
        borderColor : "#8c8c8c"
        MouseArea {
            anchors.fill: parent
            onClicked: {
                ch=1
                press=1
                input_email_customer.bool = true
            }
        }
        Image{
            id: del_username
            x:563
            y:22
            width:36
            height:36
            visible:false
            source:asset_global + "button/delete.png"
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    input_email_customer.show_text = "";
                    count=0;
                    ch=1
                    touch_keyboard.input_text("")
                    del_username.visible=false
                }
            }
        }
    }

    NotifButton{
        id:btn_lanjut
        x:691
        y:230
        enabled: false
        buttonText:qsTr("LANJUT")
        buttonColor: "#8c8c8c"
        r_radius:0
        modeReverse:false
        MouseArea{
            anchors.fill: parent
            onClicked: {
                // if(press != "1"){
                //     return
                // }
                // press = "0";
                check_email(input_email_customer.show_text);
            }
            onEntered:{
                btn_lanjut.modeReverse = true
            }
            onExited:{
                btn_lanjut.modeReverse = false
            }
        }
    }

    KeyBoardAlphanumeric{
        id:touch_keyboard
        property var validate_c
        closeButton: false
        button_ok: "@"
        Component.onCompleted: {
            touch_keyboard.letter_button_clicked.connect(input_text)
            // touch_keyboard.function_button_clicked.connect(on_function_button_clicked)
        }

        function input_text(str){
            if(ch==1){
                if (str == "" && input_email_customer.show_text.length > 0){
                    input_email_customer.show_text.length--
                    input_email_customer.show_text=input_email_customer.show_text.substring(0,input_email_customer.show_text.length-1)
                    count--;                 
                }
                if (str != "" && input_email_customer.show_text.length< character){
                    input_email_customer.show_text.length++
                    count++;
                }
                if (input_email_customer.show_text.length>=character){
                    str=""
                    input_email_customer.show_text.length=character
                }else{
                    input_email_customer.show_text += str
                }
                if(count>0){
                    btn_lanjut.buttonColor = "#ff524f"
                    btn_lanjut.enabled = true
                    input_email_customer.borderColor = "#ff7e7e"
                    del_username.visible=true
                } else{
                    btn_lanjut.buttonColor = "#c4c4c4"
                    btn_lanjut.enabled = false
                    input_email_customer.borderColor = "#8c8c8c"
                    del_username.visible=false
                }
            }
        }
    }

    DimmDisplay{
        id:dimm_display
        visible:false
    }

    AnimatedImage{
        id: waiting
        visible:false
        width: 200
        height: 200
        anchors.verticalCenter: parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter
        source: 'asset/global/animation/loading.gif'
    }

    NotifSwipe{
        id:notif_duplicateemail
        img_y:50
        body_y:138
        source_img: asset_path + "email_duplicate.png"
        title_text:qsTr("Email Sudah Digunakan")
        body_text:qsTr("Email yang kamu masukkan sudah terdaftar <br>untuk berlangganan newsletter, silahkan <br>masukkan email lain.")
        NotifButton{
            id:batal
            x:50
            y:260
            buttonText:qsTr("BATAL")
            modeReverse:true
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true}))
                }
                onEntered:{
                    batal.modeReverse = false;
                }
                onExited:{
                    batal.modeReverse = true;
                }
            }
        }

        NotifButton{
            id:masukkan_ulang
            x:301
            y:260
            buttonText:qsTr("MASUKKAN ULANG")
            modeReverse:false
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    hide_duplicate_email.start();
                    dimm_display.visible = false;
                }
                onEntered:{
                    masukkan_ulang.modeReverse = true;
                }
                onExited:{
                    masukkan_ulang.modeReverse = false;
                }
            }
        }
    }

    NotifSwipe{
        id:notif_wrongemail
        img_y:50
        body_y:138
        source_img: asset_path + "email_failed.png"
        title_text:qsTr("Alamat Email Salah")
        body_text:qsTr("Silakan masukkan alamat email yang benar, <br>misalnya nama@mail.com")
        NotifButton{
            id:coba_lagi
            x:50
            y:260
            buttonText:qsTr("COBA LAGI")
            modeReverse:false
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    hide_wrong_email.start();
                    dimm_display.visible = false;
                }
                onEntered:{
                    coba_lagi.modeReverse = true;
                }
                onExited:{
                    coba_lagi.modeReverse = false;
                }
            }
        }
    }

    NotifSwipe{
        id:notif_success_post
        img_y:50
        body_y:138
        source_img: asset_path + "post_success.png"
        title_text:qsTr("Terima Kasih")
        body_text:qsTr("Telah berlangganan newsletter kami, Silahkan cek <br>kotak masuk di email kamu.")
        NotifButton{
            id:ok_back
            x:50
            y:260
            buttonText:qsTr("OKE")
            modeReverse:false
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true}))
                }
                onEntered:{
                    ok_back.modeReverse = true;
                }
                onExited:{
                    ok_back.modeReverse = false;
                }
            }
        }
    }

    NumberAnimation {
        id:hide_key
        targets: touch_keyboard
        properties: "y"
        from:368
        to:768
        duration: 500
        easing.type: Easing.InOutBack
    }
    NumberAnimation {
        id:show_key
        targets: touch_keyboard
        properties: "y"
        from:768
        to:368
        duration: 500
        easing.type: Easing.InOutBack
    }

    NumberAnimation {
        id:hide_success_post
        targets: notif_success_post
        properties: "y"
        from:287
        to:768
        duration: 500
        easing.type: Easing.InOutBack
    }

    NumberAnimation {
        id:show_success_post
        targets: notif_success_post
        properties: "y"
        from:768
        to:287
        duration: 500
        easing.type: Easing.InOutBack
    }

    NumberAnimation {
        id:hide_wrong_email
        targets: notif_wrongemail
        properties: "y"
        from:287
        to:768
        duration: 500
        easing.type: Easing.InOutBack
    }
    NumberAnimation {
        id:show_wrong_email
        targets: notif_wrongemail
        properties: "y"
        from:768
        to:287
        duration: 500
        easing.type: Easing.InOutBack
    }

    NumberAnimation {
        id:hide_duplicate_email
        targets: notif_duplicateemail
        properties: "y"
        from:287
        to:768
        duration: 500
        easing.type: Easing.InOutBack
    }
    NumberAnimation {
        id:show_duplicate_email
        targets: notif_duplicateemail
        properties: "y"
        from:768
        to:287
        duration: 500
        easing.type: Easing.InOutBack
    }

    function validate_email(str) {
        var pattern = /^[-a-z0-9~!$%^&*_=+}{\'?]+(\.[-a-z0-9~!$%^&*_=+}{\'?]+)*@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+)*\.(net|org|biz|com|edu|gov|info|net|asia|[a-z][a-z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?$/i
        return pattern.test(str)
     }

     function check_email(email){
        if(email.length > 6 && email.indexOf("@") > -1 && validate_email(email)==true){
            console.log("EMAIL BENAR",email);
            start_post_cust_data();
            waiting.visible = true;
            dimm_display.visible = true;
        }else{
            console.log("EMAIL SALAH",email);
            show_wrong_email.start();
            dimm_display.visible = true;
        }
    }

    function locker_language(result) {
        console.log("Bahasa : ", result);
        language = result;
    }

    function post_data_result(message){
        console.log("Post Subscribe data is ", message)
        waiting.visible = false;
        // dimm_display.visible = false;
        if(message == "ERROR" || message == "FAILED"){
            console.log("ERROR post_data_result");
            show_wrong_email.start();
        }else{
            var post_result = message
            var post_success;
            if(post_result.indexOf("Exist") > -1){
                post_success = "Duplicate"
                show_duplicate_email.start();
                console.log("Post Sudah Terdaftar", post_success);
            }else{
                post_success = "True"
                console.log("POST Berhasil", post_success);
                show_success_post.start();
            }
        }
    }

    function start_post_cust_data(){
        cust_name = cust_name.toUpperCase()
        var cust_email = input_email_customer.show_text.toLowerCase()
        var cust_data = cust_email + "||" + cust_phone
        // console.log("posting_data_customer : " + cust_name + "||" + cust_data)
        slot_handler.start_post_subscribe_data(cust_name, cust_data)
    }

}