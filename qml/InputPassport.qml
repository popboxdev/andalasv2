import QtQuick 2.4
import QtQuick.Controls 1.2

Background{
    id: input_passport_page
    property var press: '0'
    property var asset_global: 'asset/global/'
    property var asset_path: 'asset/logistic/'
    property int char_max:12
    property int char_min:6
    property var ch:1 
    property var count:0
    property var identity:""
    property var level_user
    property var level_name
    property var txt_time: ""
    property var pin_code: ""
    property var parcel_number: ""
    property var door_type: ""
    property int door_no: 0
    property var nomorLoker: ""
    property var survey_expressNumber : ""
    property var survey_expressId : ""
    property var express_id : ""
    property var passport_number: ''
    property var customer_name: ""
    property var is_passport: 1
    property var recipient_name: ''
    property var identification_number: ''

    Stack.onStatusChanged:{
        if(Stack.status == Stack.Activating){
            timer.secon = 90
            timer_note.restart()
            press = '0';
            show_key.start();
            // input_passport.bool = true
            recbox.border.color ="#c4c4c4";
            btn_lanjut.buttonColor="#c4c4c4"
            count = 0;
            console.log(identity)
            console.log("Parcel Number " + parcel_number + "Pin : "+ pin_code + "custname : "+ customer_name);
        }
        if(Stack.status==Stack.Deactivating){
            timer_note.stop()
        }
    }

    FontType{
        id:fontStyle
        //Type Font ### bold , extrabold , light , book, medium, black ###
    }

    Header{
        id:header
        title_text:identity
        text_timer : txt_time
    }

    Item{
        id: timer
        property int secon  : 90
        Timer{
            id:timer_note
            interval:1000
            repeat:true
            running:true
            triggeredOnStart:true
            onTriggered:{
                timer.secon -= 1
                txt_time = timer.secon
                if(timer.secon < 1){
                    timer_note.stop()
                    my_stack_view.push(time_out)
                }
            }
        }
    }
    
    Text {
        id: username_txt
        x:50
        y:145
        text: qsTr("Input Your Passport")
        font.pixelSize:40
        color:"#414042"
        font.family: fontStyle.medium
    }

    Rectangle{
        id:recbox
        x:50
        y:230
        width: 620
        height: 80
        color: "transparent"
        border.width: 3
        radius : 5
        

        TextEdit{
            id:box_number
            text:passport_number
            anchors.topMargin: 22
            anchors.leftMargin: 20
            anchors.fill: parent
            horizontalAlignment: Text.AlignLeft
            font.family: fontStyle.medium
            color:"#414042"
            font.pixelSize:32
        }
    }

    Image{
        id: del_img
        x:613
        y:252
        width:36
        height:36
        visible:false
        source:asset_global + "button/delete.png"
        MouseArea {
            anchors.fill: parent
            onClicked: {
                passport_number = "";
                count=0;
                recbox.border.color ="#c4c4c4";
                btn_lanjut.buttonColor="#c4c4c4";
                btn_lanjut.enabled=false;
                del_img.visible=false
            }
        }
    }

    NotifButton{
        id:btn_lanjut
        x:691
        y:230
        enabled: false
        buttonText:qsTr("NEXT")
        buttonColor: "#8c8c8c"
        r_radius:0
        modeReverse:false
        MouseArea{
            anchors.fill: parent
            onClicked: {
                // if (press!='0') return
                //     press = '1'
                console.log("clicked: " + passport_number)
                // slot_handler.set_recipient_name(input_passport.show_text)                
                // slot_handler.customer_take_express(pin_code);
                dimm_display.visible = true
                show_verify.start();
                // waiting.visible = true
            }
            onEntered:{
                btn_lanjut.modeReverse = true
            }
            onExited:{
                btn_lanjut.modeReverse = false
            }
        }
    }

    KeyBoardAlphanumeric{
        id:touch_keyboard
        property var validate_c
        closeButton: false
        Component.onCompleted: {
            touch_keyboard.letter_button_clicked.connect(input_text)
        }

        function input_text(str){
            if(str=="OKE" || str =="OK"){
                console.log("clicked: " + passport_number)
                dimm_display.visible = true
                show_verify.start();
            }
            if(str==""){   
                if (count > 0){
                    count--;
                }
                if (count<=0){ 
                    count=0;
                    del_img.visible=false
                }
                if(count<char_min){
                    recbox.border.color ="#c4c4c4";
                    btn_lanjut.buttonColor="#c4c4c4"
                    btn_lanjut.enabled=false
                }
                passport_number = passport_number.substring(0,passport_number.length-1);
                console.log("counter : ",count);
            }else{
                if (count < (char_max+1)){
                    count++;
                }
                if (count > char_max){
                    str="";
                    count--;
                }
                if(str != "" ){
                    recbox.border.color = "#ff524f"
                    del_img.visible=true
                }
                if((count>=char_min) || (count>=char_max)){
                    btn_lanjut.buttonColor = "#ff524f"
                    btn_lanjut.enabled = true
                }
                press = '0';
                passport_number += str;

            }
        }
    }

    AnimatedImage{
        id: waiting
        visible:false
        width: 200
        height: 200
        anchors.verticalCenter: parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter
        source: 'asset/global/animation/loading.gif'
    }

    NumberAnimation {
        id:hide_key
        targets: touch_keyboard
        properties: "y"
        from:368
        to:768
        duration: 500
        easing.type: Easing.InOutBack
    }
    NumberAnimation {
        id:show_key
        targets: touch_keyboard
        properties: "y"
        from:768
        to:368
        duration: 500
        easing.type: Easing.InOutBack
    }

    NumberAnimation {
        id:hide_verify
        targets: notif_verify
        properties: "y"
        from:248
        to:768
        duration: 500
        easing.type: Easing.InOutBack
    }
    NumberAnimation {
        id:show_verify
        targets: notif_verify
        properties: "y"
        from:768
        to:248
        duration: 500
        easing.type: Easing.InOutBack
    }

    DimmDisplay{
        id:dimm_display
        visible:false
    }

    NotifSwipe{
        id:notif_verify
        img_y:56
        source_img: ""
        title_text:qsTr("Please verify your NRIC/passport number<br> ") + passport_number
        // body_text:qsTr("Pastikan no. handphone kamu aktif dan dapat <br>terima WhatsApp. Kode verifikasi akan dikirimkan <br>ke nomor kamu.")

        NotifButton{
            id:ok_change
            x:57
            y:292
            r_radius:2
            buttonText:qsTr("CHANGE NUMBER")
            modeReverse:true
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    // hide_animation("popsend");
                    press = '0';
                    hide_verify.start()
                    dimm_display.visible=false
                }
                onEntered:{
                    ok_change.modeReverse = false
                }
                onExited:{
                    ok_change.modeReverse = true
                }
            }
        }
        NotifButton{
            id:btn_lanjutkan
            x:317
            y:292
            r_radius:2
            buttonText:qsTr("CONTINUE")
            modeReverse:false
            MouseArea{
                anchors.fill: parent
                onClicked: {          
                    console.log("Lanjutkan : ",passport_number );
                    identification_number = passport_number;
                    // is_passport = 1
                    // slot_handler.set_identification_number(is_passport, passport_number)
                    my_stack_view.push(input_signature,{customer_name: customer_name, pin_code:pin_code, parcel_number:parcel_number, identity:identity, survey_expressNumber: survey_expressNumber, survey_expressId: survey_expressId, express_id: express_id, recipient_name: recipient_name, is_passport: is_passport, identification_number: identification_number})
                    hide_verify.start();
                    dimm_display.visible=false;
                    del_img.visible=false;
                }
                onEntered:{
                    btn_lanjutkan.modeReverse = true
                }
                onExited:{
                    btn_lanjutkan.modeReverse = false
                }
            }
        }
    }
    
    function clear_all() {
        press = 0;
        count = 0;
        del_img.visible=false;
    }

}