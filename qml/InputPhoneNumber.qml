import QtQuick 2.4
import QtQuick.Controls 1.2

Background{
    id: input_phone
    show_img : ""
    img_vis:true
    property int count: 0
    property var press: '0'
    property var asset_path: "asset/global/input_phone/"
    property var asset_global: 'asset/global/'
    property var status:""
    property var popsafe_status: "inlocker"
    property var title:""
    property var title_body:""
    property var note_body: qsTr("Pastikan nomor yang dimasukkan aktif, notifikasi akan dikirimkan ke nomor tersebut")
    property var select_courier:''
    property var phone_number: ''
    property var no_parcel: ''
    property var param_return: ''
    property var phone_courier: ''
    property var box_choose: ''
    property var id_company: ''
    property var parcel_number: ''
    property var courier_name: ''
    property var txt_time: ""
    property var customer_phone: ""
    property var lastmile_type: ""
    property var status_courier : ""
    property var drop_by_courier : 0
    property var drop_by_other_company: 0
    property var language: 'ID'
    property variant check_number:[]
    property var phone_substring;
    property var status_back: "default"
    property bool reinput_number: false
    property bool next_reinput_number: false
    property var customer_phone_input: ""
    // property var customer_phone_reinput: ""

    
    
    property variant check_number_id: ['0814','0815', '0816', '0855', '0856', '0857', '0858',
        '0811', '0812', '0813', '0821', '0822', '0823', '0851', '0852', '0853',
        '0817', '0818', '0819', '0859', '0877', '0878', '0879',
        '0831', '0832', '0838', '9991', '9992', '9993', '9994', '9995', '9988',
        '0881', '0882', '0883', '0884', '0885', '0886', '0887', '0888', '0889',
        '0895', '0896', '0897', '0898', '0899']
    property variant check_number_my: ['010','011','012','013','014','015','016','017','018','019']
    //show_img: asset_path + "background/bg.png"

    Stack.onStatusChanged:{
        if(Stack.status == Stack.Activating){
            timer.secon = 90
            timer_note.restart()
            slot_handler.start_get_language();
            // slot_handler.start_get_locker_name();
            // timer_clock.start();
            // timer_startup.start();
            // hide_key.start();
            phonebox.border.color ="#c4c4c4";
            nextButton.buttonColor="#c4c4c4"
            count = phone_number.length;
            console.log('drop_by_courier =>' + drop_by_courier)

            if (count>0)
            {
                nextButton.buttonColor = "#ff524f"
                phonebox.border.color = "#ff524f"
                del_img.visible=true
            }
            press = '0';
        }
        if(Stack.status==Stack.Deactivating){
            timer_note.stop()
        }
    }

    FontType{
        id:fontStyle
        //Type Font ### bold , extrabold , light , book, medium, black ###
    }

    Header{
        id:header
        title_text:qsTr(title)
        text_timer : txt_time
        funct: status_back
    }
    Item{
        id: timer
        property int secon  : 90
        Timer{
            id:timer_note
            interval:1000
            repeat:true
            running:true
            triggeredOnStart:true
            onTriggered:{
                timer.secon -= 1
                txt_time = timer.secon
                if(timer.secon < 1){
                    timer_note.stop()
                    my_stack_view.push(time_out)
                }
            }
        }
    }

    Text {
        id: ttp
        x:50
        y:145
        text: qsTr("No. Handphone ") + qsTr(title_body)
        font.pixelSize:40
        color:"#414042"
        font.family: fontStyle.book
    }

    Text {
        id:note
        y:325
        x:51
        text: qsTr(note_body)
        font.pixelSize:22
        color:"#3a3a3a"
        font.family: fontStyle.book
    }
    Rectangle{
        id:phonebox
        x:50
        y:230
        width: 620
        height: 80
        color: "transparent"
        //border.color: '#c4c4c4'
        border.width: 3
        radius : 5
        //borderColor : "#c4c4c4"
        

        TextEdit{
            id:phone
            text:phone_number
            anchors.topMargin: 22
            anchors.leftMargin: 20
            anchors.fill: parent
            horizontalAlignment: Text.AlignLeft
            font.family: fontStyle.medium
            color:"#414042"
            font.pixelSize:32
            // font.bold: true
        }
    }
    Image{
        id: del_img
        x:613
        y:252
        width:36
        height:36
        visible:false
        source:asset_global + "button/delete.png"
        MouseArea {
            anchors.fill: parent
            onClicked: {
                reset_box();
            }
        }
    }

    NotifButton{
        id:nextButton
        x:690
        y:230
        buttonText:qsTr("LANJUT")
        buttonColor: "#8c8c8c"
        r_radius:0
        modeReverse:false
        MouseArea{
            anchors.fill: parent
            onClicked: {
                if (press!='0') return
                press = '1'
                lanjut_button();
            }
            onEntered:{
                nextButton.modeReverse = true
            }
            onExited:{
                nextButton.modeReverse = false
            }
        }
    }

    // Rectangle{
    //     id : nextButton
    //     x:690
    //     y:230
    //     width: 230
    //     height: 80
    //     //color: "#c4c4c4"
    //     Text {
    //         id: text_button
    //         anchors.horizontalCenter: parent.horizontalCenter
    //         anchors.verticalCenter: parent.verticalCenter
    //         font.pixelSize:24
    //         color:"#ffffff"
    //         font.family: fontStyle.medium
    //         text: qsTr("LANJUT")            
    //     }
    //     MouseArea {
    //         anchors.fill: parent
    //         onClicked: {
    //             if (press!='0') return
    //                 press = '1'
    //                 if(phone_number != ""){
    //                     console.log('Phone Number : ', phone_number);
    //                     if(check_number.indexOf(phone_number.substring(0,4)) > -1 && phone_number.length > 9){
    //                         if(status=="lastmile"){
    //                             if(phone_courier==phone_number)show_verif02.start();
    //                             else show_verif01.start();
    //                         }
    //                         else show_verif01.start();
    //                         dimm_display.visible=true;
    //                         count = 0;
    //                     }else{
    //                         show_wrong_number.start()
    //                         dimm_display.visible= true
                            
    //                     }
    //                 }else{
    //                     show_wrong_number.start()
    //                     dimm_display.visible= true
    //                 }
    //         }
    //     }
    // }


    KeyBoardNumber{
        id:keyboard_number

        Component.onCompleted: {
            keyboard_number.letter_button_clicked.connect(show_validate_code);
            // number_keyboard.function_button_clicked.connect(function_button_action);
        }

        NumButton{
            id: reset_button
            x: 298
            y: 245
            width: 125
            height: 65
            show_img: true
            source_img: "asset/keyboard/delete.png"
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    if (count >= 1 || count <= 15){
                        if (count >=14)
                            count = count-1
                        count--;
                    }
                    if (count <=0){ 
                        count=0;
                    }
                    if(count==0)
                    {
                        phonebox.border.color ="#c4c4c4";
                        nextButton.buttonColor="#c4c4c4"
                        del_img.visible=false
                    }
                    phone_number = phone_number.substring(0,phone_number.length-1);
                }
            }
        }

        NumButton{
            id: find_button
            x: 574
            y: 245
            width: 125
            height: 65
            show_text: qsTr('OKE')
            num_fontsize : 30
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    if (press!='0') return
                    press = '1'
                    lanjut_button();
                }
            }
        }

        function show_validate_code(str){
            if (str != "" && count < 15){
                count++;
            }
            if (count >= 15){
                str="";
            }
            if(str != "" ){
                nextButton.buttonColor = "#ff524f"
                phonebox.border.color = "#ff524f"
                del_img.visible=true
            }
            press = '0';
            phone_number += str;
        }
    }

    NumberAnimation {
        id:hide_wrong_number
        targets: notif_wrongnumber
        properties: "y"
        from:287
        to:768
        duration: 500
        easing.type: Easing.InOutBack
    }
    NumberAnimation {
        id:show_wrong_number
        targets: notif_wrongnumber
        properties: "y"
        from:768
        to:287
        duration: 500
        easing.type: Easing.InOutBack
    }

    NumberAnimation {
        id:hide_reinput_failed
        targets: notif_reinput_failed
        properties: "y"
        from:287
        to:768
        duration: 500
        easing.type: Easing.InOutBack
    }
    NumberAnimation {
        id:show_reinput_failed
        targets: notif_reinput_failed
        properties: "y"
        from:768
        to:287
        duration: 500
        easing.type: Easing.InOutBack
    }

    NumberAnimation {
        id:hide_verif01
        targets: notif_verif01
        properties: "y"
        from:248
        to:768
        duration: 500
        easing.type: Easing.InOutBack
    }
    NumberAnimation {
        id:show_verif01
        targets: notif_verif01
        properties: "y"
        from:768
        to:248
        duration: 500
        easing.type: Easing.InOutBack
    }

    NumberAnimation {
        id:hide_verif02
        targets: notif_verif02
        properties: "y"
        from:248
        to:768
        duration: 500
        easing.type: Easing.InOutBack
    }
    NumberAnimation {
        id:show_verif02
        targets: notif_verif02
        properties: "y"
        from:768
        to:248
        duration: 500
        easing.type: Easing.InOutBack
    }  

    DimmDisplay{
        id:dimm_display
        visible:false
    }

    AnimatedImage{
        id: waiting
        visible:false
        width: 200
        height: 200
        anchors.verticalCenter: parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter
        source: 'asset/global/animation/loading.gif'
    }

    NotifSwipe{
        id:notif_verif02
        img_y:56
        source_img: asset_path + "check02.png"
        title_text:qsTr("No. Handphone Kurir dan Pelanggan <br>Sama")
        body_text:qsTr("No. Handphone Kurir") +  "\t \t: " + phone_courier + " <br>" + qsTr("No. Handphone Pelanggan") + "\t: "+ phone_number + " <br>" + qsTr("Nomor handphone kurir dan nomor yang kamu masukkan <br>tidak boleh sama, silakan perbaiki sebelum melanjutkan <br>proses.")
        NotifButton{
            id:ok_btl
            x:50
            y:350
            r_radius:2
            buttonText:qsTr("BATAL")
            modeReverse:true
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    press = '0';
                    hide_verif02.start()
                    dimm_display.visible=false
                }
                onEntered:{
                    ok_btl.modeReverse = false
                }
                onExited:{
                    ok_btl.modeReverse = true
                }
            }
        }
        NotifButton{
            id:ok_change
            x:295
            y:350
            r_radius:2
            buttonText:qsTr("UBAH NO. KURIR")
            modeReverse:true
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    press = '0';
                    my_stack_view.push(input_phone, {select_courier:select_courier, status: "courier", title: qsTr("KURIR"), title_body: qsTr("Kamu (Kurir)"), reinput_number: false}) 
                    ttp.text =  qsTr("No. Handphone ") + qsTr(title_body);
                    hide_verif02.start();
                    dimm_display.visible=false
                }
                onEntered:{
                    ok_change.modeReverse = false
                }
                onExited:{
                    ok_change.modeReverse = true
                }
            }
        }
        NotifButton{
            id:ok_change2
            x:540
            y:350
            r_radius:2
            r_width:308
            buttonText:qsTr("UBAH NO. PELANGGAN")
            modeReverse:false
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    press = '0';
                    hide_verif02.start()
                    ttp.text =  qsTr("No. Handphone ") + qsTr(title_body);
                    customer_phone_input = "";
                    reinput_number=true
                    dimm_display.visible=false
                }
                onEntered:{
                    ok_change2.modeReverse = true
                }
                onExited:{
                    ok_change2.modeReverse = false
                }
            }
        }

        Image{
            x:939
            y:60
            height:33
            width:33
            source:"asset/global/button/close.png"
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    press = '0';
                    hide_verif02.start()
                    dimm_display.visible=false
                }
            }
        }
    }

    NotifSwipe{
        id:notif_reinput_failed
        img_y:50
        source_img: asset_path + "check01.png"
        title_text:qsTr("No.Handphone Yang Kamu <br>Masukkan Salah")
        body_text:qsTr("Periksa kembali, Pastikan kamu memasukkan <br>No. Customer secara benar")

        NotifButton{
            id:batal_reinput
            x:50
            y:337
            buttonText:qsTr("BATAL")
            modeReverse:true
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    if(status=="lastmile") {
                        my_stack_view.pop(null);
                    }else if(status=="logistic") {
                        my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 2) return true }));
                    }
                }
                onEntered:{
                    batal_reinput.modeReverse = false
                }
                onExited:{
                    batal_reinput.modeReverse = true
                }
            }
        }

        NotifButton{
            id:ok_reinput
            x:310
            y:337
            buttonText:qsTr("ULANGI")
            modeReverse:false
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    reset_box();
                    ttp.text =  qsTr("No. Handphone ") + qsTr(title_body);
                    customer_phone_input = "";
                    reinput_number = true;
                    hide_reinput_failed.start();
                    dimm_display.visible=false;
                }
                onEntered:{
                    ok_reinput.modeReverse = true
                }
                onExited:{
                    ok_reinput.modeReverse = false
                }
            }
        }

    }

    NotifSwipe{
        id:notif_wrongnumber
        img_y:50
        source_img: asset_path + "wrong_number.png"
        title_text:qsTr("Periksa kembali No. Handphone <br>kamu ")
        body_text:qsTr("Mohon periksa kembali, format no. handphone yang <br>kamu masukkan salah.")
        NotifButton{
            id:ok_back
            x:50
            y:337
            buttonText:qsTr("KEMBALI")
            modeReverse:false
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    hide_animation("salah");
                }
                onEntered:{
                    ok_back.modeReverse = true
                }
                onExited:{
                    ok_back.modeReverse = false
                }
            }
        }
    }

    NotifSwipe{
        id:notif_verif01
        img_y:56
        source_img: asset_path + "check01.png"
        title_text:qsTr("Periksa kembali No. Handphone <br>kamu ") + phone_number
        body_text:qsTr("Pastikan no. handphone kamu aktif dan dapat <br>terima WhatsApp. Kode verifikasi akan dikirimkan <br>ke nomor kamu.")

        NotifButton{
            id:ok_change3
            x:57
            y:292
            r_radius:2
            buttonText:qsTr("GANTI NOMOR")
            modeReverse:true
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    // hide_animation("popsend");
                    press = '0';
                    hide_verif01.start()
                    dimm_display.visible=false
                }
                onEntered:{
                    ok_change3.modeReverse = false
                }
                onExited:{
                    ok_change3.modeReverse = true
                }
            }
        }
        NotifButton{
            id:btn_lanjut
            x:317
            y:292
            r_radius:2
            buttonText:qsTr("LANJUTKAN")
            modeReverse:false
            MouseArea{
                anchors.fill: parent
                onClicked: {          
                    hide_verif01.start();
                    dimm_display.visible=false;
                    del_img.visible=false;
                    
                    if(status=="titip") {
                        // hide_verif01.start()
                        my_stack_view.push(box_size,{popsafe_status:popsafe_status,phone_number: phone_number, isPopDeposit: true})
                    } else if(status=="return") {
                        slot_handler.set_phone_number(phone_number)
 
                        my_stack_view.push(detail_return, {phone_number: phone_number, data_return: param_return, id_company: id_company})
                    } else if(status=="courier") {
                        dimm_display.visible = true
                        waiting.visible      = true
                        slot_handler.start_checking_otp_validality(phone_number)
                        
                    } else if(status=="lastmile") {
                        slot_handler.set_phone_number(phone_number)
                        my_stack_view.push(detail_courier_popsend, {no_parcel:no_parcel, phone_cust:phone_number, phone_courier:phone_courier, select_courier:select_courier, id_company: id_company, popsafe_status: status, status_courier: status_courier, drop_by_courier: drop_by_courier, drop_by_other_company: drop_by_other_company})
                        // slot_handler.send_request_otp(phone_number, "request","")
                    } 
                    else if(status=="logistic") {
                        slot_handler.set_phone_number(phone_number)
                        my_stack_view.push(detail_courier_store,{no_parcel:no_parcel, courier_name:courier_name, phone_cust:phone_number, select_courier:select_courier, drop_by_courier: drop_by_courier})
                    } 
                }
                onEntered:{
                    btn_lanjut.modeReverse = true
                }
                onExited:{
                    btn_lanjut.modeReverse = false
                }
            }
        }
    }

    Component.onCompleted: {
        // root.otp_result.connect(get_otp);
        root.otp_validality_result.connect(otp_validality_result);
        root.start_get_language_result.connect(locker_language);
    }

    Component.onDestruction: {
        // root.otp_result.disconnect(get_otp);
        root.otp_validality_result.disconnect(otp_validality_result);
        root.start_get_language_result.disconnect(locker_language);
    }

    function hide_animation(param){
        if(param=="salah")hide_wrong_number.start();
        dimm_display.visible=false
        press=0;
    }

    function otp_validality_result(param) {
        console.log("otp: " + param)
        var result = JSON.parse(param)
        if ( result.isSuccess == "true" && result.session_valid == "true") {
            var courier_phone = phone_number 
            phone_number = ""
            my_stack_view.push(input_phone, {no_parcel:no_parcel, select_courier:select_courier, phone_courier: courier_phone, status: "lastmile", title: qsTr("KURIR"), title_body: qsTr("Penerima"), note_body: qsTr("Pastikan nomor Customer yang kamu masukan sudah benar"), id_company: id_company, customer_phone: customer_phone, status_courier: status_courier, drop_by_courier: drop_by_courier, drop_by_other_company: drop_by_other_company, reinput_number: true})
            ttp.text =  qsTr("No. Handphone ") + qsTr(title_body);
        } else {
            slot_handler.send_request_otp(phone_number, "request","")
            my_stack_view.push(input_pin, {no_parcel:no_parcel, phone_number: phone_number, select_courier:select_courier, id_company: id_company, lastmile_type: lastmile_type, customer_phone: customer_phone, status_courier: status_courier, drop_by_courier: drop_by_courier, drop_by_other_company: drop_by_other_company})
        }

        dimm_display.visible = false
        waiting.visible      = false
    }

    function locker_language(result) {
        console.log("Bahasa : ", result);
        
        language = result;
    }

    function lanjut_button() {
        if(phone_number != ""){
            if(reinput_number==true){
                ttp.text = qsTr("Masukkan Ulang No. Handphone ") + qsTr(title_body);
                customer_phone_input = phone_number;
                reset_box();
                reinput_number=false;
                next_reinput_number=true;
            }else{
                if (language=="MY") {
                    check_number = check_number_my;
                    phone_substring = phone_number.substring(0,3);
                }else{
                    check_number = check_number_id;
                    phone_substring = phone_number.substring(0,4);
                }
                if(check_number.indexOf(phone_substring) > -1 && phone_number.length > 9){
                    if(next_reinput_number==true){
                        if(customer_phone_input!=phone_number) {
                            show_reinput_failed.start();
                            dimm_display.visible=true;
                        }else if(phone_courier==phone_number){
                            show_verif02.start();
                            dimm_display.visible=true;
                        }else {
                            push_reinput();
                            dimm_display.visible=false;
                        }
                        next_reinput_number = false;
                    }else if(status=="lastmile"){
                        if(phone_courier==phone_number)show_verif02.start();
                        else show_verif01.start();
                        dimm_display.visible=true;
                    }
                    else {
                        show_verif01.start();
                        dimm_display.visible=true;
                    }
                    count = 0;
                }else{
                    show_wrong_number.start()
                    dimm_display.visible= true
                    
                }
            }
        }else{
            show_wrong_number.start()
            dimm_display.visible= true
        }
    }

    function reset_box(){
        phone_number = "";
        count=0;
        keyboard_number.show_validate_code("")
        phonebox.border.color ="#c4c4c4";
        nextButton.buttonColor="#c4c4c4"
        del_img.visible=false
    }

    function push_reinput() {
        if(status=="lastmile") {
            slot_handler.set_phone_number(phone_number)
            my_stack_view.push(detail_courier_popsend, {no_parcel:no_parcel, phone_cust:phone_number, phone_courier:phone_courier, select_courier:select_courier, id_company: id_company, popsafe_status: status, status_courier: status_courier, drop_by_courier: drop_by_courier, drop_by_other_company: drop_by_other_company})
            // slot_handler.send_request_otp(phone_number, "request","")
        }else if(status=="logistic") {
            slot_handler.set_phone_number(phone_number)
            my_stack_view.push(detail_courier_store,{no_parcel:no_parcel, courier_name:courier_name, phone_cust:phone_number, select_courier:select_courier, drop_by_courier: drop_by_courier})
        } else{
            show_reinput_failed.start();
        }
    }
}
