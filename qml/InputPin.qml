import QtQuick 2.4
import QtQuick.Controls 1.2

Background{
    id: input_pin
    property var press: '0'
    property var pin_code: ''
    property var asset_path: 'asset/send_page/popsend/'
    property var title: qsTr("KURIR")
    property var select_courier:""
    property var phone_number: ""
    property var customer_phone: ""
    property var lastmile_type: ""
    property var no_parcel: ""
    property int kirim_ulang: 2
    property var font_reg: "SourceSansPro-Regular"
    property var font_bold: "SourceSansPro-SemiBold"
    property int count: 0
    property int header_timer: 0
    property int status_code: 0
    property var id_company: ""
    property var txt_time: ""
    property var phone_cust: ""
    property var status_courier : ""
    property var drop_by_courier : 0
    property var drop_by_other_company : 0

    Stack.onStatusChanged:{
        if(Stack.status == Stack.Activating){
            press = '0';
            timer.secon = 90
            timer_note.restart()
            console.log('drop_by_courier =>' + drop_by_courier)
            // slot_handler.send_request_otp(phone_number, "request","")

            // extend = undefined;
            // slot_handler.start_customer_scan_qr_code();
        }
        if(Stack.status==Stack.Deactivating){
            timer_note.stop()
        }
    }

    FontType{
        id:fontStyle
        //Type Font ### bold , extrabold , light , book, medium, black ###
    }

    Header{
        id:header
        title_text:title
        text_timer : txt_time
    }

    Item{
        id: timer
        property int secon: 90
        property int time_sms: 30
        
        Timer{
            id:timer_note
            interval:1000
            repeat:true
            running:true
            triggeredOnStart:true
            onTriggered:{
                timer.secon -= 1
                if(timer.time_sms>0){
                    timer.time_sms -= 1
                }else{
                    timer.time_sms = 0
                }
                txt_time = timer.secon
                if(timer.secon < 1){
                    timer_note.stop()
                    my_stack_view.push(time_out)
                }
            }
        }
    }
    
    Text {
        id: krm
        x:50
        y:145
        text: qsTr("Kode Verifikasi")
        font.pixelSize:40
        color:"#414042"
        font.family: fontStyle.medium
    }

    function reset_value(){
        first_box.show_text = "";
        second_box.show_text = "";
        third_box.show_text = "";
        fourth_box.show_text = "";
        fifth_box.show_text = "";
        sixth_box.show_text = "";
        pin_code = '';
        count = 0;
    }

    Row{
        x:50
        y: 230
        spacing: 15
        PinBox{
            id:first_box
        }
        PinBox{
            id:second_box
        }
        PinBox{
            id:third_box
        }
        PinBox{
            id:fourth_box
        }
        PinBox{
            id:fifth_box
        }
        PinBox{
            id:sixth_box
        }
    }

    NotifButton{
        id:btn_lanjut
        x:619
        y:230
        buttonText:qsTr("VERIFIKASI")
        buttonColor: "#8c8c8c"
        r_radius:0
        modeReverse:false
        MouseArea{
            anchors.fill: parent
            onClicked: {
                if (press!='0') return;
                press = '1';
                pin_code = '';
                pin_code += first_box.show_text;
                pin_code += second_box.show_text;
                pin_code += third_box.show_text;
                pin_code += fourth_box.show_text;
                pin_code += fifth_box.show_text;
                pin_code += sixth_box.show_text;
                slot_handler.send_request_otp(phone_number, "validation", pin_code)
            }
            onEntered:{
                btn_lanjut.modeReverse = true
            }
            onExited:{
                btn_lanjut.modeReverse = false
            }
        }
    }

    Text {
        id: txt_pin
        x:50
        y:329
        text: qsTr("Masukkan kode verifikasi yang dikirimkan lewat <br>WhatsApp ke nomor ") + phone_number
        font.pixelSize:22
        color:"#3a3a3a"
        font.family: fontStyle.medium
    }

    Text {
        id: txt_kirim_ulang
        x:675
        y:329
        // text: qsTr("<u>Kirim Ulang</u>" + "("+header.timer_counter+")" ) 
        text:(timer.time_sms==0) ? qsTr("<u>Kirim Ulang</u>") : qsTr("   00:" + timer.time_sms)
        font.pixelSize:22
        color:(timer.time_sms==0 && kirim_ulang>0) ? "#ff524f" : "#3a3a3a"
        font.family: fontStyle.medium
        MouseArea{
            anchors.fill: parent
            onClicked: {
                if(timer.time_sms<=0){
                    if(kirim_ulang<=0){
                        kirim_ulang = 0
                        txt_kirim_ulang.color="#3a3a3a"
                    }else{
                        slot_handler.send_request_otp(phone_number, "resend","")
                        kirim_ulang -= 1
                        console.log("ACTIVE" + kirim_ulang)
                        timer.time_sms=30
                    }
                }
            }
        }
    }

    Text {
        id: maks_kirim_ulang
        x:675
        y:360
        // text: (kirim_ulang<=0) ? qsTr("(Sudah Batas Maksimal)") : qsTr("(Maksimal " + kirim_ulang + "x)")
        text: qsTr("(Maksimal ") + kirim_ulang + ("x)")
        font.pixelSize:18
        color:"#3a3a3a"
        font.family: fontStyle.book
    }

    KeyBoardNumber{
        id:keyboard_number

        Component.onCompleted: {
            keyboard_number.letter_button_clicked.connect(show_validate_code);
        }
        NumButton{
            id: reset_button
            x: 298
            y: 245
            width: 125
            height: 65
            show_img: true
            source_img: "asset/keyboard/delete.png"
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    if (count == 1) first_box.show_text = '';
                    if (count == 2) second_box.show_text = '';
                    if (count == 3) third_box.show_text = '';
                    if (count == 4) fourth_box.show_text = '';
                    if (count == 5) fifth_box.show_text = '';
                    if (count == 6) sixth_box.show_text = '';
                    if (count>0) {
                        count--;
                        button_inactive();
                    } else {
                        count=0;
                    }
                }
            }
        }

        NumButton{
            id: find_button
            x: 574
            y: 245
            width: 125
            height: 65
            show_text: qsTr('OKE')
            num_fontsize : 30
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    if (press!='0') return;
                    press = '1';
                    pin_code = '';
                    pin_code += first_box.show_text;
                    pin_code += second_box.show_text;
                    pin_code += third_box.show_text;
                    pin_code += fourth_box.show_text;
                    pin_code += fifth_box.show_text;
                    pin_code += sixth_box.show_text;
                    console.log('OK Button Processing with : ', pin_code);
                }
            }
        }

        function on_function_button_clicked(str){
            if(str == "ok"){
                if(press != "0") return;
                press = "1";
                pin_code = '';
                pin_code += first_box.show_text;
                pin_code += second_box.show_text;
                pin_code += third_box.show_text;
                pin_code += fourth_box.show_text;
                pin_code += fifth_box.show_text;
                pin_code += sixth_box.show_text;
                console.log('OK Button Processing with : ', pin_code);
            }
        }

        function show_validate_code(str){
            if (count == 0) first_box.show_text = str;
            if (count == 1) second_box.show_text = str;
            if (count == 2) third_box.show_text = str;
            if (count == 3) fourth_box.show_text = str;
            if (count == 4) fifth_box.show_text = str;
            if (count == 5){
                sixth_box.show_text = str;
                button_active();
            }
            if (count <= 5){
                count++;
            }
            press = '0';
        }
    }

    NumberAnimation {
        id:hide_popsafe
        targets: notif_popsafe
        properties: "y"
        from:248
        to:768
        duration: 500
        easing.type: Easing.InOutBack
    }
    NumberAnimation {
        id:show_popsafe
        targets: notif_popsafe
        properties: "y"
        from:768
        to:248
        duration: 500
        easing.type: Easing.InOutBack
    }

    DimmDisplay{
        id:dimm_display
        visible:false
    }

    NotifSwipe{
        id:notif_popsafe
        img_y:56
        source_img: asset_path + "bgpin.png"
        body_y: 130
        title_text:qsTr("Kode Verifikasi Salah")
        body_text:qsTr("Kode verifikasi yang kamu masukkan salah. <br>Periksa kembali dan masukkan ulang untuk <br>melanjutkan proses")
        NotifButton{
            id:ok_retry
            x:50
            y:351
            r_width:283
            r_radius:2
            buttonText:qsTr("MASUKKAN ULANG")
            modeReverse:false
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    hide_popsafe.start();
                    dimm_display.visible=false
                }
                onEntered:{
                    ok_retry.modeReverse = true
                }
                onExited:{
                    ok_retry.modeReverse = false
                }
            }
        }

        Image{
            x:939
            y:60
            height:33
            width:33
            source:"asset/global/button/close.png"
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    hide_popsafe.start();
                    dimm_display.visible=false
                }

            }
        }
    }

    Component.onCompleted: {
        root.otp_result.connect(get_otp);
    }

    Component.onDestruction: {
        root.otp_result.disconnect(get_otp);
    }

    function button_active(){
        btn_lanjut.buttonColor = "#ff524f"
        first_box.borderColor = "#ff7e7e"
        second_box.borderColor = "#ff7e7e"
        third_box.borderColor = "#ff7e7e"
        fourth_box.borderColor = "#ff7e7e"
        fifth_box.borderColor = "#ff7e7e"
        sixth_box.borderColor = "#ff7e7e"
    }
    function button_inactive(){
        btn_lanjut.buttonColor = "#8c8c8c"
        first_box.borderColor = "#8c8c8c"
        second_box.borderColor = "#8c8c8c"
        third_box.borderColor = "#8c8c8c"
        fourth_box.borderColor = "#8c8c8c"
        fifth_box.borderColor = "#8c8c8c"
        sixth_box.borderColor = "#8c8c8c"
    }

    function get_otp(result) {
        
        console.log(" otp_result: " + result)
        var response = JSON.parse(result)

        if (response.isSuccess == "true") {
            status_code = response.code
            var validation = response.validation
            var type = response.type
            console.log(" status_code: " + status_code + "type: " + type)
            if(type == "validation") {
                if(status_code == "200" && validation == "true"){
                    
                    console.log("lastmile_type: " + lastmile_type)
                    
                    if (lastmile_type == "PRELOAD") {
                        my_stack_view.push(detail_courier_popsend, {no_parcel:no_parcel, phone_cust:customer_phone, phone_courier:phone_number, select_courier:select_courier, id_company: id_company, popsafe_status: "lastmile", drop_by_courier: drop_by_courier, drop_by_other_company: drop_by_other_company})
                    }
                    else {
                        my_stack_view.push(input_phone, {no_parcel:no_parcel, select_courier:select_courier, phone_courier:phone_number, status: "lastmile", title: qsTr("KURIR"), title_body: qsTr("Penerima"), note_body: qsTr("Pastikan nomor Customer yang kamu masukan sudah benar"), id_company: id_company, customer_phone: customer_phone, status_courier: status_courier, drop_by_courier: drop_by_courier, drop_by_other_company: drop_by_other_company, reinput_number: true})
                    }
                }else{
                    dimm_display.visible=true
                    show_popsafe.start()
                }
            }
        } else {
            dimm_display.visible=true
            show_popsafe.start()
        }
        
    }

}
