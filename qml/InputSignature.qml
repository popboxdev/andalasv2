import QtQuick 2.4
import QtQuick.Controls 1.2

Background{
    id: input_signature_page
    property var press: '0'
    property var asset_global: 'asset/global/'
    property var asset_path: 'asset/take_page/'
    property var identity:""
    property var level_user
    property var level_name
    property var txt_time: ""
    property var pin_code: ""
    property var parcel_number: ""
    property var door_type: ""
    property int door_no: 0
    property var nomorLoker: ""
    property var survey_expressNumber : ""
    property var survey_expressId : ""
    property var express_id : ""
    property var customer_name: ""

    property var filename: ''
    property int touch: 1
    property int lastX: 0
    property int lastY: 0
    property var drawSign
    property real xpos
    property real ypos
    property var resetButton: "0"
    property var filepath: "signature/"
    property int push_data: 0
    property var is_passport
    property var recipient_name: ''
    property var identification_number: ''
    // property var idcard_number: ''
    
    

    Stack.onStatusChanged:{
        if(Stack.status == Stack.Activating){
            timer.secon = 90
            timer_note.restart()
            press = '0';
            console.log(identity)
            console.log("Parcel Number " + survey_expressNumber + "Pin : "+ pin_code + "custname : "+ customer_name);
            filename = customer_name.toUpperCase() + "_" + pin_code + "_" + survey_expressNumber
            // filename = "ZAEN" + "_" + "QWEDFG" + "_" + "LKR121331E12"
            push_data=0;
        }
        if(Stack.status==Stack.Deactivating){
            timer_note.stop()
        }
    }

    FontType{
        id:fontStyle
        //Type Font ### bold , extrabold , light , book, medium, black ###
    }

    Header{
        id:header
        title_text:identity
        text_timer : txt_time
        img_vis: false
    }

    Item{
        id: timer
        property int secon  : 90
        Timer{
            id:timer_note
            interval:1000
            repeat:true
            running:true
            triggeredOnStart:true
            onTriggered:{
                timer.secon -= 1
                txt_time = timer.secon
                if(timer.secon < 1){
                    timer_note.stop()
                    my_stack_view.push(time_out)
                }
            }
        }
    }

    function resetGlobalTimer(){
        timer.secon = 90
        timer_note.restart()
    }
    
    Text {
        id: title_page
        x:212
        y:159
        text: qsTr("Submit Your Signature")
        font.pixelSize:40
        color:"#414042"
        font.family: fontStyle.medium
    }

    Text {
        id: body_page
        x:212
        y:572
        text: qsTr("Please draw your signature on the box above.")
        font.pixelSize:18
        color:"#3a3a3a"
        font.family: fontStyle.book
    }

    Rectangle {
        id: rec_preview
        // x: 223
        // y: 250
        x: 212
        y: 230
        // width: 500
        // height: 350
        width: 600
        height: 338
        color: "#ffffff"
        border.color: "#9a9999"
        border.width: 2
        radius: 2
        // anchors.horizontalCenter: text_notif_1.horizontalCenter

        Canvas {
            id: myCanvas
            width: rec_preview.width
            height: rec_preview.height
            antialiasing: false
            smooth: true
            onPaint: {
                drawSign = getContext('2d');
                drawSign.lineJoin = 'round';
                drawSign.lineCap = 'round';
                drawSign.lineWidth = 3;
                if(touch==1){
                    drawSign.fillStyle = 'white';
                    drawSign.fillRect(xpos, ypos, 0, 0);
                }
                if(touch%2==0){
                    drawSign.strokeStyle = "black";
                    if(lastX-xpos==Math.abs(10) || lastY-ypos==Math.abs(10)){
                        drawSign.lineTo(lastX, lastY);
                    }else{
                        drawSign.lineTo(xpos, ypos);
                    }
                    drawSign.stroke();
                }
                if(touch>2 && touch%2!=0){
                    drawSign.moveTo(xpos, ypos);
                    touch += 1;
                }
            }

            function resetPressed(){
                if(resetButton == "1"){
                    drawSign.reset();
                    drawSign.clearRect(0, 0, myCanvas/width, myCanvas.height)
                    myCanvas.requestPaint()
                    continue_button.enabled = false;
                    continue_button.buttonColor = "#c4c4c4";
                }else{
                    return
                }
                resetButton = "0"
                touch = 1
                continue_button.enabled = false
                // continue_button.show_source = "img/button/button2.png"
            }

            MouseArea{
                id: area
                anchors.fill: parent
                onEntered: {
                    touch += 1
                    console.log("touch sensor :", touch)
                    continue_button.enabled = true;
                    continue_button.buttonColor = "#ff524f";
                }
                onPressed: {
                    resetGlobalTimer()
                    press = "1"
                    xpos = area.mouseX
                    ypos = area.mouseY
                    myCanvas.requestPaint()
                }
                onPositionChanged: {
                    resetGlobalTimer()
                    press = "1"
                    xpos = area.mouseX
                    ypos = area.mouseY
                    myCanvas.requestPaint()
                }
                onReleased: {
                    continue_button.enabled = true
                    // continue_button.show_source = "img/button/button1.png"
                    reset_button.enabled = true
                    // reset_button.show_source = "img/button/button1.png"
                    lastX = area.mouseX
                    lastY = area.mouseY
                }
            }

            NotifButton{
                id:reset_button
                x:55
                y:412
                r_radius:2
                buttonText:qsTr("RESET")
                modeReverse:true
                MouseArea{
                    anchors.fill: parent
                    onClicked: {
                        resetButton = "1"
                        if(press == "1"){
                            myCanvas.resetPressed();
                        }else{
                            return
                        }
                    }  
                    onEntered:{
                        reset_button.modeReverse = false;
                    }
                    onExited:{
                        reset_button.modeReverse = true;
                    }
                }
            }

            NotifButton{
                id:continue_button
                x:315
                y:412
                r_radius:2
                buttonText:qsTr("SUBMIT")
                modeReverse:false
                enabled: false
                buttonColor: "#c4c4c4"
                MouseArea{
                    anchors.fill: parent
                    onClicked: {
                        if(press == "1" && touch > 1){
                            // loadingGif.open()
                            continue_button.visible = false;
                            reset_button.visible = false;
                            body_page.visible = false;
                            console.log('parameter to slot function : ' + filename);
                            slot_handler.start_capture_signature(filepath, filename);
                            waiting.visible = true;
                            rec_preview.enabled = false;
                            // dimm_display.visible = true;
                        }else{
                            // error_tips.open()
                            console.log("GAGAL continue_button");
                            
                        }
                        continue_button.enabled = false;
                        reset_button.enabled = false;
                    }
                    onEntered:{
                        continue_button.modeReverse = true;
                    }
                    onExited:{
                        continue_button.modeReverse = false;
                    }
                }
            }

        }
    }

    Component.onCompleted: {
        root.customer_take_express_result.connect(process_take_parcel)
        root.mouth_number_result.connect(get_locker_number)
        root.start_capture_signature_result.connect(signature_result)
    }

    Component.onDestruction: {
        root.customer_take_express_result.disconnect(process_take_parcel)
        root.mouth_number_result.disconnect(get_locker_number)
        root.start_capture_signature_result.disconnect(signature_result)
    }
        

    AnimatedImage{
        id: waiting
        visible:false
        width: 70
        height: 70
        y: 600
        // anchors.verticalCenter: parent.verticalCenter + 100
        anchors.horizontalCenter: parent.horizontalCenter
        source: asset_path + 'loading_sign.gif'
    }

    NumberAnimation {
        id:hide_fail_signature
        targets: notif_fail_signature
        properties: "y"
        from:248
        to:768
        duration: 500
        easing.type: Easing.InOutBack
    }
    NumberAnimation {
        id:show_fail_signature
        targets: notif_fail_signature
        properties: "y"
        from:768
        to:248
        duration: 500
        easing.type: Easing.InOutBack
    }

    NumberAnimation {
        id:hide_doorfail
        targets: door_fail
        properties: "y"
        from:248
        to:768
        duration: 500
        easing.type: Easing.InOutBack
    }
    
    NumberAnimation {
        id:show_doorfail
        targets: door_fail
        properties: "y"
        from:768
        to:248
        duration: 500
        easing.type: Easing.InOutBack
    }

    DimmDisplay{
        id:dimm_display
        visible:false
    }

    NotifSwipe{
        id:door_fail
        img_y:50
        source_img: "asset/send_page/background/unknown_popsafeCopy.PNG"
        title_text:qsTr("Oops pintu gagal dibuka")
        body_text:qsTr("Mohon ulangi kembali proses")
        NotifButton{
            id:btn_door_fail
            x:50
            y:337
            r_radius:2
            buttonText:qsTr("BATAL")
            modeReverse:false
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    hide_doorfail.start();
                    dimm_display.visible=false
                    press=0;
                    my_stack_view.pop(null)
                }
                onEntered:{
                    btn_door_fail.modeReverse = true
                }
                onExited:{
                    btn_door_fail.modeReverse = false
                }
            }
        }

        Image{
            x:939
            y:60
            height:33
            width:33
            source:"asset/global/button/close.png"
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    hide_doorfail.start();
                    dimm_display.visible=false
                    my_stack_view.pop()
                }
            }
        }
    }

    NotifSwipe{
        id:notif_fail_signature
        source_img : asset_path + 'fail_signature.png'
        img_y: 50
        body_y: 137
        title_text:qsTr("Signature Error")
        body_text:qsTr("An error occurred, please re-enter your<br>signature")

        NotifButton{
            id:reinput
            x:50
            y:350
            r_radius:2
            modeReverse: false
            buttonText:qsTr("RE-INPUT")
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    resetButton = "1"
                    myCanvas.resetPressed();
                    continue_button.visible = true;
                    reset_button.visible = true;
                    body_page.visible = true;
                    rec_preview.enabled = true;
                    hide_fail_signature.start();
                    dimm_display.visible=false;
                }
                onEntered:{
                    reinput.modeReverse = true
                }
                onExited:{
                    reinput.modeReverse = false
                }
            }
        }

        Image{
            x:939
            y:60
            height:33
            width:33
            source:"asset/global/button/close.png"
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    continue_button.visible = true;
                    reset_button.visible = true;
                    body_page.visible = true;
                    rec_preview.enabled = true;
                    hide_fail_signature.start();
                    dimm_display.visible=false;
                }

            }
        }
    }

    function process_take_parcel(response) {
        console.log("customer_take_express_customer_name: " + JSON.stringify(response))
        
        var result = JSON.parse(response)
        if (result.isSuccess == "true") {
            slot_handler.start_video_capture('take_express_'+pin_code)
            slot_handler.start_get_express_mouth_number_by_express_id(express_id)
        } 
        else if (result.isSuccess == "false" && result.doorFlag == 'false'){
            waiting.visible = false
            show_doorfail.start()
        }
        else {
            dimm_display.visible=true
            show_popsafe.start()
        }
    }

    function signature_result(text){
        console.log('status start_capture_esignature : ' + text)
        waiting.visible = false;
        if(text == "ERROR" || text == "FAILED"){
            console.log("Failed Signature");
            dimm_display.visible = true;
            show_fail_signature.start();
        }
        if(text == "SUCCESS"){
            slot_handler.set_recipient_name(recipient_name)
            slot_handler.set_identification_number(is_passport, identification_number)
            slot_handler.customer_take_express(pin_code);
            console.log("Sukses Signature");
            
        }
    }

    function get_locker_number(argument) {
        console.log("locker_number: " + argument)
        var result = JSON.parse(argument)
        if (result.isSuccess == "true") {
            console.log("locker_number_number: " + result.locker_number)
            door_no = result.locker_number
            door_type = result.locker_number_size
            my_stack_view.push(open_door, {nomorLoker: door_no, door_type: door_type, header_title: qsTr("AMBIL"), survey_expressNumber: survey_expressNumber, survey_expressId: survey_expressId, express_id: express_id, access_door: true, button_type:1})
        }
    }
    
    function clear_all() {
        press = 0;
        count = 0;
        del_img.visible=false;
    }

}