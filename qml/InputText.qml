import QtQuick 2.4
import QtQuick.Controls 1.2

Rectangle{
    id: rectangle_txt
    property int r_width: 616
    property int text_width: 616
    width: r_width
    height: 76
    color: "#ffffff"
    radius: 5
    // property bool password: false
    // property var font_type: "SourceSansPro-Regular"
    property var show_image:""
    property var show_text:""
    property var bool:true
    property var borderColor
    property bool text_password:false
    border.width: 3
    border.color: borderColor
    FontType{
        id:fontStyle
        //Type Font ### bold , extrabold , light , book, medium, black ###
    }

    TextInput{
        id:text_input
        enabled:true
        x:25
        y:2
        width: text_width
        height: 39
        echoMode:(text_password==true) ? TextInput.Password : TextInput.Normal
        anchors.verticalCenter: parent.verticalCenter
        text:show_text
        clip: true
        visible: true
        cursorVisible: bool
        color:"#414042"
        focus: true
        font.family:fontStyle.medium
        font.pixelSize:32
        // font.bold: true
    }


    Image{
        x:10
        y:15
        width:25
        height:30
        source:show_image
    }

    // Image{
    //     x:40
    //     y:15
    //     width:5
    //     height:30
    //     source:"img/apservice/courier/cut.png"
    // }
}
