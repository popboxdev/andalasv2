import QtQuick 2.4
import QtQuick.Controls 1.2

Rectangle{
    id:keyboard_alphanumeric
    x:0
    y:768
    signal letter_button_clicked(string str)
    signal function_button_clicked(string str)
    property var name: "keyboard"
    property var page_calling_keyboard: "default"
    color: "transparent"
    width: parent.width
    height: 400
    property var color_space: "#BEBEBE"
    property int ynumber: 16
    property int yrow2: 94
    property int yrow3: 172
    property int yrow4: 250
    property bool closeButton: true
    property var color_disable: "#FFFFFF"
    property bool btn_enable: true
    property var button_ok: qsTr("OKE")
    
    FontType{
        id:fontStyle
        //Type Font ### bold , extrabold , light , book, medium, black ###
    }

    Rectangle{
        x:874
        height: 72
        width: 150
        color: color_space
        visible: closeButton

        Rectangle{
            x:17
            y:8
            height: 65
            width: 124
            color: "#fbfcfc"
            Text {
                id: tutup
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: parent.verticalCenter
                font.pixelSize:30
                color:"#37474f"
                font.family: fontStyle.medium
                text: qsTr("TUTUP")
            }
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    if (page_calling_keyboard == "select_payment") {
                        select_payment.hide_animation(name);
                    }else if (page_calling_keyboard=="reason_cancel"){
                        reason_cancel.hide_animation(name);
                    }else if (page_calling_keyboard=="store_parcel"){
                        store_parcel.hide_animation(name);
                    }else {
                        send_parcel.hide_animation(name);
                    }
                }

            }
        }

    }

    Rectangle{
        y:72
        visible: true
        height: 328
        width: root.width
        color: color_space

//            ROW 1
        LetterButton{
            x:55
            y:ynumber
            show_text: "1"
        }

        LetterButton{
            x:147
            y:ynumber
            show_text: "2"
        }

        LetterButton{
            x:240
            y:ynumber
            show_text: "3"
        }

        LetterButton{
            x:332
            y:ynumber
            show_text: "4"
        }

        LetterButton{
            x:424
            y:ynumber
            show_text: "5"
        }
        LetterButton{
            x:516
            y:ynumber
            show_text: "6"
        }
        LetterButton{
            x:608
            y:ynumber
            show_text: "7"
        }
        LetterButton{
            x:700
            y:ynumber
            show_text: "8"
        }
        LetterButton{
            x:793
            y:ynumber
            show_text: "9"
        }
        LetterButton{
            x:885
            y:ynumber
            show_text: "0"
        }

//            ROW2
        LetterButton{
            x:17
            y:yrow2
            show_text: "Q"
            enabled: btn_enable
            color: color_disable
        }
        LetterButton{
            x:109
            y:yrow2
            show_text: "W"
            enabled: btn_enable
            color: color_disable
        }
        LetterButton{
            x:201
            y:yrow2
            show_text: "E"
            enabled: btn_enable
            color: color_disable
        }
        LetterButton{
            x:293
            y:yrow2
            show_text: "R"
            enabled: btn_enable
            color: color_disable
        }
        LetterButton{
            x:385
            y:yrow2
            show_text: "T"
            enabled: btn_enable
            color: color_disable
        }
        LetterButton{
            x:478
            y:yrow2
            show_text: "Y"
            enabled: btn_enable
            color: color_disable
        }
        LetterButton{
            x:570
            y:yrow2
            show_text: "U"
            enabled: btn_enable
            color: color_disable
        }
        LetterButton{
            x:662
            y:yrow2
            show_text: "I"
            enabled: btn_enable
            color: color_disable
        }
        LetterButton{
            x:754
            y:yrow2
            show_text: "O"
            enabled: btn_enable
            color: color_disable
        }
        LetterButton{
            x:846
            y:yrow2
            show_text: "P"
            enabled: btn_enable
            color: color_disable
        }
        LetterButton{
            x:933
            y:yrow2
            show_img: true
            source_img: "asset/keyboard/delete.png"
        }

//            ROW3

        LetterButton{
            x:54
            y:yrow3
            show_text: "A"
            enabled: btn_enable
            color: color_disable
        }
        LetterButton{
            x:146
            y:yrow3
            show_text: "S"
            enabled: btn_enable
            color: color_disable
        }
        LetterButton{
            x:238
            y:yrow3
            show_text: "D"
            enabled: btn_enable
            color: color_disable
        }
        LetterButton{
            x:330
            y:yrow3
            show_text: "F"
            enabled: btn_enable
            color: color_disable
        }
        LetterButton{
            x:422
            y:yrow3
            show_text: "G"
            enabled: btn_enable
            color: color_disable
        }
        LetterButton{
            x:515
            y:yrow3
            show_text: "H"
            enabled: btn_enable
            color: color_disable
        }
        LetterButton{
            x:607
            y:yrow3
            show_text: "J"
            enabled: btn_enable
            color: color_disable
        }
        LetterButton{
            x:699
            y:yrow3
            show_text: "K"
            enabled: btn_enable
            color: color_disable
        }
        LetterButton{
            x:791
            y:yrow3
            show_text: "L"
            enabled: btn_enable
            color: color_disable
        }
        LetterButton{
            x:883
            y:yrow3
            show_text: button_ok
        }


//            ROW4
        LetterButton{
            x:55
            y:yrow4
            show_text: "."
            enabled: btn_enable
            color: color_disable
        }
        LetterButton{
            x:155
            y:yrow4
            show_text: "Z"
            enabled: btn_enable
            color: color_disable
        }
        LetterButton{
            x:247
            y:yrow4
            show_text: "X"
            enabled: btn_enable
            color: color_disable
        }
        LetterButton{
            x:339
            y:yrow4
            show_text: "C"
            enabled: btn_enable
            color: color_disable
        }
        LetterButton{
            x:431
            y:yrow4
            show_text: "V"
            enabled: btn_enable
            color: color_disable
        }
        LetterButton{
            x:524
            y:yrow4
            show_text: "B"
            enabled: btn_enable
            color: color_disable
        }
        LetterButton{
            x:616
            y:yrow4
            show_text: "N"
            enabled: btn_enable
            color: color_disable
        }
        LetterButton{
            x:708
            y:yrow4
            show_text: "M"
            enabled: btn_enable
            color: color_disable
        }
        LetterButton{
            x:800
            y:yrow4
            show_text: "_"
            enabled: btn_enable
            color: color_disable
        }
        LetterButton{
            x:892
            y:yrow4
            show_text: "-"
            enabled: btn_enable
            color: color_disable
        }

    }

}

