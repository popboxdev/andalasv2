import QtQuick 2.0
import QtQuick.Controls 1.2

Rectangle{
    id:keyboard_number
    y:444.4
    visible: true
    height: 323.6
    width: root.width
    color: "#c1c1c2"

    property var name: "keyboard"
    property int yrow1: 11
    property int yrow2: 89
    property int yrow3: 167
    property int yrow4: 245
    property int xrow1: 298
    property int xrow2: 436
    property int xrow3: 574
    signal letter_button_clicked(string str)
    signal function_button_clicked(string str)

//            ROW 1
    NumButton{
        x:xrow1
        y:yrow1
        width: 125
        height: 65
        show_text: "1"
    }

    NumButton{
        x:xrow2
        y:yrow1
        width: 125
        height: 65
        show_text: "2"
    }

    NumButton{
        x:xrow3
        y:yrow1
        width: 125
        height: 65
        show_text: "3"
    }

//    ROW2
    NumButton{
        x:xrow1
        y:yrow2
        width: 125
        height: 65
        show_text: "4"
    }

    NumButton{
        x:xrow2
        y:yrow2
        width: 125
        height: 65
        show_text: "5"
    }

    NumButton{
        x:xrow3
        y:yrow2
        width: 125
        height: 65
        show_text: "6"
    }

    NumButton{
        x:xrow1
        y:yrow3
        width: 125
        height: 65
        show_text: "7"
    }

    NumButton{
        x:xrow2
        y:yrow3
        width: 125
        height: 65
        show_text: "8"
    }

    NumButton{
        x:xrow3
        y:yrow3
        width: 125
        height: 65
        show_text: "9"
    }

    // NumButton{
    //     x:xrow1
    //     y:yrow4
    //     width: 125
    //     height: 65
    //     show_img: true
    //     source_img: "asset/keyboard/delete.png"
    // }

    NumButton{
        x:xrow2
        y:yrow4
        width: 125
        height: 65
        show_text: "0"
    }

    // NumButton{
    //     x:xrow3
    //     y:yrow4
    //     width: 125
    //     height: 65
    //     show_text: "OKE"
    // }

}


