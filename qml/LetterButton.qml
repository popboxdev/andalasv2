import QtQuick 2.4
import QtQuick.Controls 1.2

Rectangle{
    id:letter_button
    width: 79
    height: 65
    property var show_text:""
    property bool show_img: false
    property var source_img:""
    // color: "#fbfcfc"
    color: "#ffffff"
    FontType{
        id:fontStyle
        //Type Font ### bold , extrabold , light , book, medium, black ###
    }
    Text {
        id: text_button
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter
        font.pixelSize:30
        color:"#37474f"
        font.family: fontStyle.medium
        text: qsTr(show_text)
    }
    Image {
        id: img_button
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter
        visible: (show_img==true) ? true : false
        source: source_img
    }

    MouseArea {
        anchors.fill: letter_button
        onClicked: {
            keyboard_alphanumeric.letter_button_clicked(show_text)
            }
        onEntered:{
            letter_button.color = "silver"
            }
        onExited:{
            letter_button.color = "#ffffff"
        }
    }
}
