import QtQuick 2.4

Rectangle {
    id: main_rectangle
    property var wBox: 62
    property var hS: 11
    property var hL: 57
    property var hM: 29

    //Baris Per Slot / Per 50 CM
    property var xSlot1: 4
    property var xSlot2: 73
    property var xSlot3: 138
    property var xSlot4: 207
    property var xSlot5: 272
    property var xSlot6: 341
    property var xSlot7: 406
    property var xSlot8: 475
    property var xSlot9: 540
    property var xSlot10: 609
    property var xSlot11: 674
    property var xSlot12: 743

    //Baris Khusus CU
    property var yCU1: 3
    property var yCU2: 62
    property var yCU3: 175
    property var yCU4: 188
    property var yCU5: 201
    property var yCU6: 214
    property var yCU7: 227

    //Baris Khusus SU
    property var yBaris1: 3
    property var yBaris2: 62
    property var yBaris3: 92
    property var yBaris4: 105
    property var yBaris5: 118
    property var yBaris6: 131
    property var yBaris7: 144
    property var yBaris8: 157
    property var yBaris9: 170
    property var yBaris10: 183
    property var yBaris11: 196
    property var yBaris12: 227

    property var jLock: 1
    property var doorClick: 0

    property var rec_color: "#ffa400"
    color:"transparent"
    // width: 138
    // height: 266
    // property var show_source: ""
    property var door_no: 0

//############################################################################//
    //###########################// 6 METER //##########################//

    Image{
        id: img_6meter
        verticalAlignment: Text.AlignVCenter
        visible: (jLock==6) ? true : false
        source:"asset/map_loker/TW6m.png"
        MouseArea {
            anchors.fill: parent
            onClicked: {
                doorClick++;
                door_no = doorClick;
                if(doorClick>=139) doorClick=0;
            }
         }

        //Nomer 1 - 24
        //Slot Kanan 12

        Rectangle{
            color: rec_color 
            visible: (door_no==1) ? true : false
            x: xSlot1
            y: yBaris1
            width:wBox
            height:hL
        }

        Rectangle{
            color: rec_color 
            visible: (door_no==2) ? true : false
            x: xSlot1
            y: yBaris2
            width:wBox
            height:hM
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==3) ? true : false
            x: xSlot1
            y: yBaris3
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==4) ? true : false
            x: xSlot1
            y: yBaris4
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==5) ? true : false
            x: xSlot1
            y: yBaris5
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==6) ? true : false
            x: xSlot1
            y: yBaris6
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==7) ? true : false
            x: xSlot1
            y: yBaris7
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==8) ? true : false
            x: xSlot1
            y: yBaris8
            width:wBox
            height:hS
        }

        Rectangle{
            color: rec_color 
            visible: (door_no==9) ? true : false
            x: xSlot1
            y: yBaris9
            width:wBox
            height:hS
        }

        Rectangle{
            color: rec_color 
            visible: (door_no==10) ? true : false
            x: xSlot1
            y: yBaris10
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==11) ? true : false
            x: xSlot1
            y: yBaris11
            width:wBox
            height:hM
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==12) ? true : false
            x: xSlot1
            y: yBaris12
            width:wBox
            height:hM
        }

        //slot Kiri 12
        Rectangle{
            color: rec_color 
            visible: (door_no==13) ? true : false
            x: xSlot2
            y: yBaris1
            width:wBox
            height:hL
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==14) ? true : false
            x: xSlot2
            y: yBaris2
            width:wBox
            height:hM
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==15) ? true : false
            x: xSlot2
            y: yBaris3
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==16) ? true : false
            x: xSlot2
            y: yBaris4
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==17) ? true : false
            x: xSlot2
            y: yBaris5
            width:wBox
            height:hS
        }

        Rectangle{
            color: rec_color 
            visible: (door_no==18) ? true : false
            x: xSlot2
            y: yBaris6
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==19) ? true : false
            x: xSlot2
            y: yBaris7
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==20) ? true : false
            x: xSlot2
            y: yBaris8
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==21) ? true : false
            x: xSlot2
            y: yBaris9
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==22) ? true : false
            x: xSlot2
            y: yBaris10
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==23) ? true : false
            x: xSlot2
            y: yBaris11
            width:wBox
            height:hM
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==24) ? true : false
            x: xSlot2
            y: yBaris12
            width:wBox
            height:hM
        }

        //Nomer 25 - 48
        //Slot Kanan 12

        Rectangle{
            color: rec_color 
            visible: (door_no==25) ? true : false
            x: xSlot3
            y: yBaris1
            width:wBox
            height:hL
        }

        Rectangle{
            color: rec_color 
            visible: (door_no==26) ? true : false
            x: xSlot3
            y: yBaris2
            width:wBox
            height:hM
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==27) ? true : false
            x: xSlot3
            y: yBaris3
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==28) ? true : false
            x: xSlot3
            y: yBaris4
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==29) ? true : false
            x: xSlot3
            y: yBaris5
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==30) ? true : false
            x: xSlot3
            y: yBaris6
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==31) ? true : false
            x: xSlot3
            y: yBaris7
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==32) ? true : false
            x: xSlot3
            y: yBaris8
            width:wBox
            height:hS
        }

        Rectangle{
            color: rec_color 
            visible: (door_no==33) ? true : false
            x: xSlot3
            y: yBaris9
            width:wBox
            height:hS
        }

        Rectangle{
            color: rec_color 
            visible: (door_no==34) ? true : false
            x: xSlot3
            y: yBaris10
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==35) ? true : false
            x: xSlot3
            y: yBaris11
            width:wBox
            height:hM
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==36) ? true : false
            x: xSlot3
            y: yBaris12
            width:wBox
            height:hM
        }

        //slot Kiri 12
        Rectangle{
            color: rec_color 
            visible: (door_no==37) ? true : false
            x: xSlot4
            y: yBaris1
            width:wBox
            height:hL
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==38) ? true : false
            x: xSlot4
            y: yBaris2
            width:wBox
            height:hM
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==39) ? true : false
            x: xSlot4
            y: yBaris3
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==40) ? true : false
            x: xSlot4
            y: yBaris4
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==41) ? true : false
            x: xSlot4
            y: yBaris5
            width:wBox
            height:hS
        }

        Rectangle{
            color: rec_color 
            visible: (door_no==42) ? true : false
            x: xSlot4
            y: yBaris6
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==43) ? true : false
            x: xSlot4
            y: yBaris7
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==44) ? true : false
            x: xSlot4
            y: yBaris8
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==45) ? true : false
            x: xSlot4
            y: yBaris9
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==46) ? true : false
            x: xSlot4
            y: yBaris10
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==47) ? true : false
            x: xSlot4
            y: yBaris11
            width:wBox
            height:hM
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==48) ? true : false
            x: xSlot4
            y: yBaris12
            width:wBox
            height:hM
        }

        //Nomer 49 - 72
        //Slot Kanan 12

        Rectangle{
            color: rec_color 
            visible: (door_no==49) ? true : false
            x: xSlot5
            y: yBaris1
            width:wBox
            height:hL
        }

        Rectangle{
            color: rec_color 
            visible: (door_no==50) ? true : false
            x: xSlot5
            y: yBaris2
            width:wBox
            height:hM
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==51) ? true : false
            x: xSlot5
            y: yBaris3
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==52) ? true : false
            x: xSlot5
            y: yBaris4
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==53) ? true : false
            x: xSlot5
            y: yBaris5
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==54) ? true : false
            x: xSlot5
            y: yBaris6
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==55) ? true : false
            x: xSlot5
            y: yBaris7
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==56) ? true : false
            x: xSlot5
            y: yBaris8
            width:wBox
            height:hS
        }

        Rectangle{
            color: rec_color 
            visible: (door_no==57) ? true : false
            x: xSlot5
            y: yBaris9
            width:wBox
            height:hS
        }

        Rectangle{
            color: rec_color 
            visible: (door_no==58) ? true : false
            x: xSlot5
            y: yBaris10
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==59) ? true : false
            x: xSlot5
            y: yBaris11
            width:wBox
            height:hM
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==60) ? true : false
            x: xSlot5
            y: yBaris12
            width:wBox
            height:hM
        }

        //slot Kiri 12
        Rectangle{
            color: rec_color 
            visible: (door_no==61) ? true : false
            x: xSlot6
            y: yBaris1
            width:wBox
            height:hL
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==62) ? true : false
            x: xSlot6
            y: yBaris2
            width:wBox
            height:hM
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==63) ? true : false
            x: xSlot6
            y: yBaris3
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==64) ? true : false
            x: xSlot6
            y: yBaris4
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==65) ? true : false
            x: xSlot6
            y: yBaris5
            width:wBox
            height:hS
        }

        Rectangle{
            color: rec_color 
            visible: (door_no==66) ? true : false
            x: xSlot6
            y: yBaris6
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==67) ? true : false
            x: xSlot6
            y: yBaris7
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==68) ? true : false
            x: xSlot6
            y: yBaris8
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==69) ? true : false
            x: xSlot6
            y: yBaris9
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==70) ? true : false
            x: xSlot6
            y: yBaris10
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==71) ? true : false
            x: xSlot6
            y: yBaris11
            width:wBox
            height:hM
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==72) ? true : false
            x: xSlot6
            y: yBaris12
            width:wBox
            height:hM
        }

        
        //CU Monitor
        // 73 - 91
        Rectangle{
            color: rec_color 
            visible: (door_no==73) ? true : false
            x: xSlot7
            y: yCU1
            width:wBox
            height:hL
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==74) ? true : false
            x: xSlot7
            y: yCU2
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==75) ? true : false
            x: xSlot7
            y: yCU3
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==76) ? true : false
            x: xSlot7
            y: yCU4
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==77) ? true : false
            x: xSlot7
            y: yCU5
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==78) ? true : false
            x: xSlot7
            y: yCU6
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==79) ? true : false
            x: xSlot7
            y: yCU7
            width:wBox
            height:hM
        }

        //CU kiri 12

        Rectangle{
            color: rec_color 
            visible: (door_no==80) ? true : false
            x: xSlot8
            y: yBaris1
            width:wBox
            height:hL
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==81) ? true : false
            x: xSlot8
            y: yBaris2
            width:wBox
            height:hM
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==82) ? true : false
            x: xSlot8
            y: yBaris3
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==83) ? true : false
            x: xSlot8
            y: yBaris4
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==84) ? true : false
            x: xSlot8
            y: yBaris5
            width:wBox
            height:hS
        }

        Rectangle{
            color: rec_color 
            visible: (door_no==85) ? true : false
            x: xSlot8
            y: yBaris6
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==86) ? true : false
            x: xSlot8
            y: yBaris7
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==87) ? true : false
            x: xSlot8
            y: yBaris8
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==88) ? true : false
            x: xSlot8
            y: yBaris9
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==89) ? true : false
            x: xSlot8
            y: yBaris10
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==90) ? true : false
            x: xSlot8
            y: yBaris11
            width:wBox
            height:hM
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==91) ? true : false
            x: xSlot8
            y: yBaris12
            width:wBox
            height:hM
        }

        //Number 92 - 115
        //Slot Kanan 12

        Rectangle{
            color: rec_color 
            visible: (door_no==92) ? true : false
            x: xSlot9
            y: yBaris1
            width:wBox
            height:hL
        }

        Rectangle{
            color: rec_color 
            visible: (door_no==93) ? true : false
            x: xSlot9
            y: yBaris2
            width:wBox
            height:hM
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==94) ? true : false
            x: xSlot9
            y: yBaris3
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==95) ? true : false
            x: xSlot9
            y: yBaris4
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==96) ? true : false
            x: xSlot9
            y: yBaris5
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==97) ? true : false
            x: xSlot9
            y: yBaris6
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==98) ? true : false
            x: xSlot9
            y: yBaris7
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==99) ? true : false
            x: xSlot9
            y: yBaris8
            width:wBox
            height:hS
        }

        Rectangle{
            color: rec_color 
            visible: (door_no==100) ? true : false
            x: xSlot9
            y: yBaris9
            width:wBox
            height:hS
        }

        Rectangle{
            color: rec_color 
            visible: (door_no==101) ? true : false
            x: xSlot9
            y: yBaris10
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==102) ? true : false
            x: xSlot9
            y: yBaris11
            width:wBox
            height:hM
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==103) ? true : false
            x: xSlot9
            y: yBaris12
            width:wBox
            height:hM
        }

        //slot Kiri 12
        Rectangle{
            color: rec_color 
            visible: (door_no==104) ? true : false
            x: xSlot10
            y: yBaris1
            width:wBox
            height:hL
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==105) ? true : false
            x: xSlot10
            y: yBaris2
            width:wBox
            height:hM
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==106) ? true : false
            x: xSlot10
            y: yBaris3
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==107) ? true : false
            x: xSlot10
            y: yBaris4
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==108) ? true : false
            x: xSlot10
            y: yBaris5
            width:wBox
            height:hS
        }

        Rectangle{
            color: rec_color 
            visible: (door_no==109) ? true : false
            x: xSlot10
            y: yBaris6
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==110) ? true : false
            x: xSlot10
            y: yBaris7
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==111) ? true : false
            x: xSlot10
            y: yBaris8
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==112) ? true : false
            x: xSlot10
            y: yBaris9
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==113) ? true : false
            x: xSlot10
            y: yBaris10
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==114) ? true : false
            x: xSlot10
            y: yBaris11
            width:wBox
            height:hM
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==115) ? true : false
            x: xSlot10
            y: yBaris12
            width:wBox
            height:hM
        }

        //Number 116 - 139
        //Slot Kanan 12

        Rectangle{
            color: rec_color 
            visible: (door_no==116) ? true : false
            x: xSlot11
            y: yBaris1
            width:wBox
            height:hL
        }

        Rectangle{
            color: rec_color 
            visible: (door_no==117) ? true : false
            x: xSlot11
            y: yBaris2
            width:wBox
            height:hM
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==118) ? true : false
            x: xSlot11
            y: yBaris3
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==119) ? true : false
            x: xSlot11
            y: yBaris4
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==120) ? true : false
            x: xSlot11
            y: yBaris5
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==121) ? true : false
            x: xSlot11
            y: yBaris6
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==122) ? true : false
            x: xSlot11
            y: yBaris7
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==123) ? true : false
            x: xSlot11
            y: yBaris8
            width:wBox
            height:hS
        }

        Rectangle{
            color: rec_color 
            visible: (door_no==124) ? true : false
            x: xSlot11
            y: yBaris9
            width:wBox
            height:hS
        }

        Rectangle{
            color: rec_color 
            visible: (door_no==125) ? true : false
            x: xSlot11
            y: yBaris10
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==126) ? true : false
            x: xSlot11
            y: yBaris11
            width:wBox
            height:hM
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==127) ? true : false
            x: xSlot11
            y: yBaris12
            width:wBox
            height:hM
        }

        //slot Kiri 12
        Rectangle{
            color: rec_color 
            visible: (door_no==128) ? true : false
            x: xSlot12
            y: yBaris1
            width:wBox
            height:hL
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==129) ? true : false
            x: xSlot12
            y: yBaris2
            width:wBox
            height:hM
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==130) ? true : false
            x: xSlot12
            y: yBaris3
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==131) ? true : false
            x: xSlot12
            y: yBaris4
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==132) ? true : false
            x: xSlot12
            y: yBaris5
            width:wBox
            height:hS
        }

        Rectangle{
            color: rec_color 
            visible: (door_no==133) ? true : false
            x: xSlot12
            y: yBaris6
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==134) ? true : false
            x: xSlot12
            y: yBaris7
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==135) ? true : false
            x: xSlot12
            y: yBaris8
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==136) ? true : false
            x: xSlot12
            y: yBaris9
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==137) ? true : false
            x: xSlot12
            y: yBaris10
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==138) ? true : false
            x: xSlot12
            y: yBaris11
            width:wBox
            height:hM
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==139) ? true : false
            x: xSlot12
            y: yBaris12
            width:wBox
            height:hM
        }

    }


//############################################################################//
    //###########################// 5 METER //##########################//

    Image{
        id: img_5meter
        verticalAlignment: Text.AlignVCenter
        visible: (jLock==5) ? true : false
        source:"asset/map_loker/TW5m.png"
        MouseArea {
            anchors.fill: parent
            onClicked: {
                doorClick++;
                door_no = doorClick;
                if(doorClick>=115) doorClick=0;
            }
         }

        //Nomer 1 - 24
        //Slot Kanan 12

        Rectangle{
            color: rec_color 
            visible: (door_no==1) ? true : false
            x: xSlot1
            y: yBaris1
            width:wBox
            height:hL
        }

        Rectangle{
            color: rec_color 
            visible: (door_no==2) ? true : false
            x: xSlot1
            y: yBaris2
            width:wBox
            height:hM
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==3) ? true : false
            x: xSlot1
            y: yBaris3
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==4) ? true : false
            x: xSlot1
            y: yBaris4
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==5) ? true : false
            x: xSlot1
            y: yBaris5
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==6) ? true : false
            x: xSlot1
            y: yBaris6
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==7) ? true : false
            x: xSlot1
            y: yBaris7
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==8) ? true : false
            x: xSlot1
            y: yBaris8
            width:wBox
            height:hS
        }

        Rectangle{
            color: rec_color 
            visible: (door_no==9) ? true : false
            x: xSlot1
            y: yBaris9
            width:wBox
            height:hS
        }

        Rectangle{
            color: rec_color 
            visible: (door_no==10) ? true : false
            x: xSlot1
            y: yBaris10
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==11) ? true : false
            x: xSlot1
            y: yBaris11
            width:wBox
            height:hM
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==12) ? true : false
            x: xSlot1
            y: yBaris12
            width:wBox
            height:hM
        }

        //slot Kiri 12
        Rectangle{
            color: rec_color 
            visible: (door_no==13) ? true : false
            x: xSlot2
            y: yBaris1
            width:wBox
            height:hL
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==14) ? true : false
            x: xSlot2
            y: yBaris2
            width:wBox
            height:hM
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==15) ? true : false
            x: xSlot2
            y: yBaris3
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==16) ? true : false
            x: xSlot2
            y: yBaris4
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==17) ? true : false
            x: xSlot2
            y: yBaris5
            width:wBox
            height:hS
        }

        Rectangle{
            color: rec_color 
            visible: (door_no==18) ? true : false
            x: xSlot2
            y: yBaris6
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==19) ? true : false
            x: xSlot2
            y: yBaris7
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==20) ? true : false
            x: xSlot2
            y: yBaris8
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==21) ? true : false
            x: xSlot2
            y: yBaris9
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==22) ? true : false
            x: xSlot2
            y: yBaris10
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==23) ? true : false
            x: xSlot2
            y: yBaris11
            width:wBox
            height:hM
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==24) ? true : false
            x: xSlot2
            y: yBaris12
            width:wBox
            height:hM
        }

        //Nomer 25 - 48
        //Slot Kanan 12

        Rectangle{
            color: rec_color 
            visible: (door_no==25) ? true : false
            x: xSlot3
            y: yBaris1
            width:wBox
            height:hL
        }

        Rectangle{
            color: rec_color 
            visible: (door_no==26) ? true : false
            x: xSlot3
            y: yBaris2
            width:wBox
            height:hM
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==27) ? true : false
            x: xSlot3
            y: yBaris3
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==28) ? true : false
            x: xSlot3
            y: yBaris4
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==29) ? true : false
            x: xSlot3
            y: yBaris5
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==30) ? true : false
            x: xSlot3
            y: yBaris6
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==31) ? true : false
            x: xSlot3
            y: yBaris7
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==32) ? true : false
            x: xSlot3
            y: yBaris8
            width:wBox
            height:hS
        }

        Rectangle{
            color: rec_color 
            visible: (door_no==33) ? true : false
            x: xSlot3
            y: yBaris9
            width:wBox
            height:hS
        }

        Rectangle{
            color: rec_color 
            visible: (door_no==34) ? true : false
            x: xSlot3
            y: yBaris10
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==35) ? true : false
            x: xSlot3
            y: yBaris11
            width:wBox
            height:hM
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==36) ? true : false
            x: xSlot3
            y: yBaris12
            width:wBox
            height:hM
        }

        //slot Kiri 12
        Rectangle{
            color: rec_color 
            visible: (door_no==37) ? true : false
            x: xSlot4
            y: yBaris1
            width:wBox
            height:hL
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==38) ? true : false
            x: xSlot4
            y: yBaris2
            width:wBox
            height:hM
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==39) ? true : false
            x: xSlot4
            y: yBaris3
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==40) ? true : false
            x: xSlot4
            y: yBaris4
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==41) ? true : false
            x: xSlot4
            y: yBaris5
            width:wBox
            height:hS
        }

        Rectangle{
            color: rec_color 
            visible: (door_no==42) ? true : false
            x: xSlot4
            y: yBaris6
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==43) ? true : false
            x: xSlot4
            y: yBaris7
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==44) ? true : false
            x: xSlot4
            y: yBaris8
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==45) ? true : false
            x: xSlot4
            y: yBaris9
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==46) ? true : false
            x: xSlot4
            y: yBaris10
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==47) ? true : false
            x: xSlot4
            y: yBaris11
            width:wBox
            height:hM
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==48) ? true : false
            x: xSlot4
            y: yBaris12
            width:wBox
            height:hM
        }

        //CU Monitor
        // 49 - 67
        Rectangle{
            color: rec_color 
            visible: (door_no==49) ? true : false
            x: xSlot5
            y: yCU1
            width:wBox
            height:hL
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==50) ? true : false
            x: xSlot5
            y: yCU2
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==51) ? true : false
            x: xSlot5
            y: yCU3
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==52) ? true : false
            x: xSlot5
            y: yCU4
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==53) ? true : false
            x: xSlot5
            y: yCU5
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==54) ? true : false
            x: xSlot5
            y: yCU6
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==55) ? true : false
            x: xSlot5
            y: yCU7
            width:wBox
            height:hM
        }

        //CU kiri 12

        Rectangle{
            color: rec_color 
            visible: (door_no==56) ? true : false
            x: xSlot6
            y: yBaris1
            width:wBox
            height:hL
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==57) ? true : false
            x: xSlot6
            y: yBaris2
            width:wBox
            height:hM
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==58) ? true : false
            x: xSlot6
            y: yBaris3
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==59) ? true : false
            x: xSlot6
            y: yBaris4
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==60) ? true : false
            x: xSlot6
            y: yBaris5
            width:wBox
            height:hS
        }

        Rectangle{
            color: rec_color 
            visible: (door_no==61) ? true : false
            x: xSlot6
            y: yBaris6
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==62) ? true : false
            x: xSlot6
            y: yBaris7
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==63) ? true : false
            x: xSlot6
            y: yBaris8
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==64) ? true : false
            x: xSlot6
            y: yBaris9
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==65) ? true : false
            x: xSlot6
            y: yBaris10
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==66) ? true : false
            x: xSlot6
            y: yBaris11
            width:wBox
            height:hM
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==67) ? true : false
            x: xSlot6
            y: yBaris12
            width:wBox
            height:hM
        }

        //Number 68 - 91
        //Slot Kanan 12

        Rectangle{
            color: rec_color 
            visible: (door_no==68) ? true : false
            x: xSlot7
            y: yBaris1
            width:wBox
            height:hL
        }

        Rectangle{
            color: rec_color 
            visible: (door_no==69) ? true : false
            x: xSlot7
            y: yBaris2
            width:wBox
            height:hM
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==70) ? true : false
            x: xSlot7
            y: yBaris3
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==71) ? true : false
            x: xSlot7
            y: yBaris4
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==72) ? true : false
            x: xSlot7
            y: yBaris5
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==73) ? true : false
            x: xSlot7
            y: yBaris6
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==74) ? true : false
            x: xSlot7
            y: yBaris7
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==75) ? true : false
            x: xSlot7
            y: yBaris8
            width:wBox
            height:hS
        }

        Rectangle{
            color: rec_color 
            visible: (door_no==76) ? true : false
            x: xSlot7
            y: yBaris9
            width:wBox
            height:hS
        }

        Rectangle{
            color: rec_color 
            visible: (door_no==77) ? true : false
            x: xSlot7
            y: yBaris10
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==78) ? true : false
            x: xSlot7
            y: yBaris11
            width:wBox
            height:hM
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==79) ? true : false
            x: xSlot7
            y: yBaris12
            width:wBox
            height:hM
        }

        //slot Kiri 12
        Rectangle{
            color: rec_color 
            visible: (door_no==80) ? true : false
            x: xSlot8
            y: yBaris1
            width:wBox
            height:hL
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==81) ? true : false
            x: xSlot8
            y: yBaris2
            width:wBox
            height:hM
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==82) ? true : false
            x: xSlot8
            y: yBaris3
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==83) ? true : false
            x: xSlot8
            y: yBaris4
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==84) ? true : false
            x: xSlot8
            y: yBaris5
            width:wBox
            height:hS
        }

        Rectangle{
            color: rec_color 
            visible: (door_no==85) ? true : false
            x: xSlot8
            y: yBaris6
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==86) ? true : false
            x: xSlot8
            y: yBaris7
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==87) ? true : false
            x: xSlot8
            y: yBaris8
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==88) ? true : false
            x: xSlot8
            y: yBaris9
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==89) ? true : false
            x: xSlot8
            y: yBaris10
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==90) ? true : false
            x: xSlot8
            y: yBaris11
            width:wBox
            height:hM
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==91) ? true : false
            x: xSlot8
            y: yBaris12
            width:wBox
            height:hM
        }

        //Number 68 - 91
        //Slot Kanan 12

        Rectangle{
            color: rec_color 
            visible: (door_no==92) ? true : false
            x: xSlot9
            y: yBaris1
            width:wBox
            height:hL
        }

        Rectangle{
            color: rec_color 
            visible: (door_no==93) ? true : false
            x: xSlot9
            y: yBaris2
            width:wBox
            height:hM
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==94) ? true : false
            x: xSlot9
            y: yBaris3
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==95) ? true : false
            x: xSlot9
            y: yBaris4
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==96) ? true : false
            x: xSlot9
            y: yBaris5
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==97) ? true : false
            x: xSlot9
            y: yBaris6
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==98) ? true : false
            x: xSlot9
            y: yBaris7
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==99) ? true : false
            x: xSlot9
            y: yBaris8
            width:wBox
            height:hS
        }

        Rectangle{
            color: rec_color 
            visible: (door_no==100) ? true : false
            x: xSlot9
            y: yBaris9
            width:wBox
            height:hS
        }

        Rectangle{
            color: rec_color 
            visible: (door_no==101) ? true : false
            x: xSlot9
            y: yBaris10
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==102) ? true : false
            x: xSlot9
            y: yBaris11
            width:wBox
            height:hM
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==103) ? true : false
            x: xSlot9
            y: yBaris12
            width:wBox
            height:hM
        }

        //slot Kiri 12
        Rectangle{
            color: rec_color 
            visible: (door_no==104) ? true : false
            x: xSlot10
            y: yBaris1
            width:wBox
            height:hL
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==105) ? true : false
            x: xSlot10
            y: yBaris2
            width:wBox
            height:hM
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==106) ? true : false
            x: xSlot10
            y: yBaris3
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==107) ? true : false
            x: xSlot10
            y: yBaris4
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==108) ? true : false
            x: xSlot10
            y: yBaris5
            width:wBox
            height:hS
        }

        Rectangle{
            color: rec_color 
            visible: (door_no==109) ? true : false
            x: xSlot10
            y: yBaris6
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==110) ? true : false
            x: xSlot10
            y: yBaris7
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==111) ? true : false
            x: xSlot10
            y: yBaris8
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==112) ? true : false
            x: xSlot10
            y: yBaris9
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==113) ? true : false
            x: xSlot10
            y: yBaris10
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==114) ? true : false
            x: xSlot10
            y: yBaris11
            width:wBox
            height:hM
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==115) ? true : false
            x: xSlot10
            y: yBaris12
            width:wBox
            height:hM
        }

    }

//############################################################################//
    //###########################// 4 METER //##########################//

    Image{
        id: img_4meter
        verticalAlignment: Text.AlignVCenter
        visible: (jLock==4) ? true : false
        source:"asset/map_loker/TW4m.png"
        MouseArea {
            anchors.fill: parent
            onClicked: {
                doorClick++;
                door_no = doorClick;
                if(doorClick>=91) doorClick=0;
            }
         }

        //Nomer 1 - 24
        //Slot Kanan 12

        Rectangle{
            color: rec_color 
            visible: (door_no==1) ? true : false
            x: xSlot1
            y: yBaris1
            width:wBox
            height:hL
        }

        Rectangle{
            color: rec_color 
            visible: (door_no==2) ? true : false
            x: xSlot1
            y: yBaris2
            width:wBox
            height:hM
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==3) ? true : false
            x: xSlot1
            y: yBaris3
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==4) ? true : false
            x: xSlot1
            y: yBaris4
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==5) ? true : false
            x: xSlot1
            y: yBaris5
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==6) ? true : false
            x: xSlot1
            y: yBaris6
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==7) ? true : false
            x: xSlot1
            y: yBaris7
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==8) ? true : false
            x: xSlot1
            y: yBaris8
            width:wBox
            height:hS
        }

        Rectangle{
            color: rec_color 
            visible: (door_no==9) ? true : false
            x: xSlot1
            y: yBaris9
            width:wBox
            height:hS
        }

        Rectangle{
            color: rec_color 
            visible: (door_no==10) ? true : false
            x: xSlot1
            y: yBaris10
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==11) ? true : false
            x: xSlot1
            y: yBaris11
            width:wBox
            height:hM
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==12) ? true : false
            x: xSlot1
            y: yBaris12
            width:wBox
            height:hM
        }

        //slot Kiri 12
        Rectangle{
            color: rec_color 
            visible: (door_no==13) ? true : false
            x: xSlot2
            y: yBaris1
            width:wBox
            height:hL
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==14) ? true : false
            x: xSlot2
            y: yBaris2
            width:wBox
            height:hM
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==15) ? true : false
            x: xSlot2
            y: yBaris3
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==16) ? true : false
            x: xSlot2
            y: yBaris4
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==17) ? true : false
            x: xSlot2
            y: yBaris5
            width:wBox
            height:hS
        }

        Rectangle{
            color: rec_color 
            visible: (door_no==18) ? true : false
            x: xSlot2
            y: yBaris6
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==19) ? true : false
            x: xSlot2
            y: yBaris7
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==20) ? true : false
            x: xSlot2
            y: yBaris8
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==21) ? true : false
            x: xSlot2
            y: yBaris9
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==22) ? true : false
            x: xSlot2
            y: yBaris10
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==23) ? true : false
            x: xSlot2
            y: yBaris11
            width:wBox
            height:hM
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==24) ? true : false
            x: xSlot2
            y: yBaris12
            width:wBox
            height:hM
        }

        //Nomer 25 - 48
        //Slot Kanan 12

        Rectangle{
            color: rec_color 
            visible: (door_no==25) ? true : false
            x: xSlot3
            y: yBaris1
            width:wBox
            height:hL
        }

        Rectangle{
            color: rec_color 
            visible: (door_no==26) ? true : false
            x: xSlot3
            y: yBaris2
            width:wBox
            height:hM
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==27) ? true : false
            x: xSlot3
            y: yBaris3
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==28) ? true : false
            x: xSlot3
            y: yBaris4
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==29) ? true : false
            x: xSlot3
            y: yBaris5
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==30) ? true : false
            x: xSlot3
            y: yBaris6
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==31) ? true : false
            x: xSlot3
            y: yBaris7
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==32) ? true : false
            x: xSlot3
            y: yBaris8
            width:wBox
            height:hS
        }

        Rectangle{
            color: rec_color 
            visible: (door_no==33) ? true : false
            x: xSlot3
            y: yBaris9
            width:wBox
            height:hS
        }

        Rectangle{
            color: rec_color 
            visible: (door_no==34) ? true : false
            x: xSlot3
            y: yBaris10
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==35) ? true : false
            x: xSlot3
            y: yBaris11
            width:wBox
            height:hM
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==36) ? true : false
            x: xSlot3
            y: yBaris12
            width:wBox
            height:hM
        }

        //slot Kiri 12
        Rectangle{
            color: rec_color 
            visible: (door_no==37) ? true : false
            x: xSlot4
            y: yBaris1
            width:wBox
            height:hL
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==38) ? true : false
            x: xSlot4
            y: yBaris2
            width:wBox
            height:hM
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==39) ? true : false
            x: xSlot4
            y: yBaris3
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==40) ? true : false
            x: xSlot4
            y: yBaris4
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==41) ? true : false
            x: xSlot4
            y: yBaris5
            width:wBox
            height:hS
        }

        Rectangle{
            color: rec_color 
            visible: (door_no==42) ? true : false
            x: xSlot4
            y: yBaris6
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==43) ? true : false
            x: xSlot4
            y: yBaris7
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==44) ? true : false
            x: xSlot4
            y: yBaris8
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==45) ? true : false
            x: xSlot4
            y: yBaris9
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==46) ? true : false
            x: xSlot4
            y: yBaris10
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==47) ? true : false
            x: xSlot4
            y: yBaris11
            width:wBox
            height:hM
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==48) ? true : false
            x: xSlot4
            y: yBaris12
            width:wBox
            height:hM
        }

        //CU Monitor
        // 49 - 67
        Rectangle{
            color: rec_color 
            visible: (door_no==49) ? true : false
            x: xSlot5
            y: yCU1
            width:wBox
            height:hL
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==50) ? true : false
            x: xSlot5
            y: yCU2
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==51) ? true : false
            x: xSlot5
            y: yCU3
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==52) ? true : false
            x: xSlot5
            y: yCU4
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==53) ? true : false
            x: xSlot5
            y: yCU5
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==54) ? true : false
            x: xSlot5
            y: yCU6
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==55) ? true : false
            x: xSlot5
            y: yCU7
            width:wBox
            height:hM
        }

        //CU kiri 12

        Rectangle{
            color: rec_color 
            visible: (door_no==56) ? true : false
            x: xSlot6
            y: yBaris1
            width:wBox
            height:hL
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==57) ? true : false
            x: xSlot6
            y: yBaris2
            width:wBox
            height:hM
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==58) ? true : false
            x: xSlot6
            y: yBaris3
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==59) ? true : false
            x: xSlot6
            y: yBaris4
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==60) ? true : false
            x: xSlot6
            y: yBaris5
            width:wBox
            height:hS
        }

        Rectangle{
            color: rec_color 
            visible: (door_no==61) ? true : false
            x: xSlot6
            y: yBaris6
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==62) ? true : false
            x: xSlot6
            y: yBaris7
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==63) ? true : false
            x: xSlot6
            y: yBaris8
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==64) ? true : false
            x: xSlot6
            y: yBaris9
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==65) ? true : false
            x: xSlot6
            y: yBaris10
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==66) ? true : false
            x: xSlot6
            y: yBaris11
            width:wBox
            height:hM
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==67) ? true : false
            x: xSlot6
            y: yBaris12
            width:wBox
            height:hM
        }

        //Number 68 - 91

        Rectangle{
            color: rec_color 
            visible: (door_no==68) ? true : false
            x: xSlot7
            y: yBaris1
            width:wBox
            height:hL
        }

        Rectangle{
            color: rec_color 
            visible: (door_no==69) ? true : false
            x: xSlot7
            y: yBaris2
            width:wBox
            height:hM
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==70) ? true : false
            x: xSlot7
            y: yBaris3
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==71) ? true : false
            x: xSlot7
            y: yBaris4
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==72) ? true : false
            x: xSlot7
            y: yBaris5
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==73) ? true : false
            x: xSlot7
            y: yBaris6
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==74) ? true : false
            x: xSlot7
            y: yBaris7
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==75) ? true : false
            x: xSlot7
            y: yBaris8
            width:wBox
            height:hS
        }

        Rectangle{
            color: rec_color 
            visible: (door_no==76) ? true : false
            x: xSlot7
            y: yBaris9
            width:wBox
            height:hS
        }

        Rectangle{
            color: rec_color 
            visible: (door_no==77) ? true : false
            x: xSlot7
            y: yBaris10
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==78) ? true : false
            x: xSlot7
            y: yBaris11
            width:wBox
            height:hM
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==79) ? true : false
            x: xSlot7
            y: yBaris12
            width:wBox
            height:hM
        }

        //slot Kiri 12
        Rectangle{
            color: rec_color 
            visible: (door_no==80) ? true : false
            x: xSlot8
            y: yBaris1
            width:wBox
            height:hL
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==81) ? true : false
            x: xSlot8
            y: yBaris2
            width:wBox
            height:hM
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==82) ? true : false
            x: xSlot8
            y: yBaris3
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==83) ? true : false
            x: xSlot8
            y: yBaris4
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==84) ? true : false
            x: xSlot8
            y: yBaris5
            width:wBox
            height:hS
        }

        Rectangle{
            color: rec_color 
            visible: (door_no==85) ? true : false
            x: xSlot8
            y: yBaris6
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==86) ? true : false
            x: xSlot8
            y: yBaris7
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==87) ? true : false
            x: xSlot8
            y: yBaris8
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==88) ? true : false
            x: xSlot8
            y: yBaris9
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==89) ? true : false
            x: xSlot8
            y: yBaris10
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==90) ? true : false
            x: xSlot8
            y: yBaris11
            width:wBox
            height:hM
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==91) ? true : false
            x: xSlot8
            y: yBaris12
            width:wBox
            height:hM
        }

    }
//############################################################################//
    //###########################// 3 METER //##########################//

    Image{
        id: img_3meter
        verticalAlignment: Text.AlignVCenter
        visible: (jLock==3) ? true : false
        source:"asset/map_loker/TW3m.png"
        MouseArea {
            anchors.fill: parent
            onClicked: {
                doorClick++;
                door_no = doorClick;
                if(doorClick>=67) doorClick=0;
            }
         }

        //Nomer 1 - 24
        //Slot Kanan 12

        Rectangle{
            color: rec_color 
            visible: (door_no==1) ? true : false
            x: xSlot1
            y: yBaris1
            width:wBox
            height:hL
        }

        Rectangle{
            color: rec_color 
            visible: (door_no==2) ? true : false
            x: xSlot1
            y: yBaris2
            width:wBox
            height:hM
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==3) ? true : false
            x: xSlot1
            y: yBaris3
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==4) ? true : false
            x: xSlot1
            y: yBaris4
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==5) ? true : false
            x: xSlot1
            y: yBaris5
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==6) ? true : false
            x: xSlot1
            y: yBaris6
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==7) ? true : false
            x: xSlot1
            y: yBaris7
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==8) ? true : false
            x: xSlot1
            y: yBaris8
            width:wBox
            height:hS
        }

        Rectangle{
            color: rec_color 
            visible: (door_no==9) ? true : false
            x: xSlot1
            y: yBaris9
            width:wBox
            height:hS
        }

        Rectangle{
            color: rec_color 
            visible: (door_no==10) ? true : false
            x: xSlot1
            y: yBaris10
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==11) ? true : false
            x: xSlot1
            y: yBaris11
            width:wBox
            height:hM
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==12) ? true : false
            x: xSlot1
            y: yBaris12
            width:wBox
            height:hM
        }

        //slot Kiri 12
        Rectangle{
            color: rec_color 
            visible: (door_no==13) ? true : false
            x: xSlot2
            y: yBaris1
            width:wBox
            height:hL
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==14) ? true : false
            x: xSlot2
            y: yBaris2
            width:wBox
            height:hM
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==15) ? true : false
            x: xSlot2
            y: yBaris3
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==16) ? true : false
            x: xSlot2
            y: yBaris4
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==17) ? true : false
            x: xSlot2
            y: yBaris5
            width:wBox
            height:hS
        }

        Rectangle{
            color: rec_color 
            visible: (door_no==18) ? true : false
            x: xSlot2
            y: yBaris6
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==19) ? true : false
            x: xSlot2
            y: yBaris7
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==20) ? true : false
            x: xSlot2
            y: yBaris8
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==21) ? true : false
            x: xSlot2
            y: yBaris9
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==22) ? true : false
            x: xSlot2
            y: yBaris10
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==23) ? true : false
            x: xSlot2
            y: yBaris11
            width:wBox
            height:hM
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==24) ? true : false
            x: xSlot2
            y: yBaris12
            width:wBox
            height:hM
        }

        //Nomer 25 - 48
        //Slot Kanan 12

        Rectangle{
            color: rec_color 
            visible: (door_no==25) ? true : false
            x: xSlot3
            y: yBaris1
            width:wBox
            height:hL
        }

        Rectangle{
            color: rec_color 
            visible: (door_no==26) ? true : false
            x: xSlot3
            y: yBaris2
            width:wBox
            height:hM
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==27) ? true : false
            x: xSlot3
            y: yBaris3
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==28) ? true : false
            x: xSlot3
            y: yBaris4
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==29) ? true : false
            x: xSlot3
            y: yBaris5
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==30) ? true : false
            x: xSlot3
            y: yBaris6
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==31) ? true : false
            x: xSlot3
            y: yBaris7
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==32) ? true : false
            x: xSlot3
            y: yBaris8
            width:wBox
            height:hS
        }

        Rectangle{
            color: rec_color 
            visible: (door_no==33) ? true : false
            x: xSlot3
            y: yBaris9
            width:wBox
            height:hS
        }

        Rectangle{
            color: rec_color 
            visible: (door_no==34) ? true : false
            x: xSlot3
            y: yBaris10
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==35) ? true : false
            x: xSlot3
            y: yBaris11
            width:wBox
            height:hM
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==36) ? true : false
            x: xSlot3
            y: yBaris12
            width:wBox
            height:hM
        }

        //slot Kiri 12
        Rectangle{
            color: rec_color 
            visible: (door_no==37) ? true : false
            x: xSlot4
            y: yBaris1
            width:wBox
            height:hL
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==38) ? true : false
            x: xSlot4
            y: yBaris2
            width:wBox
            height:hM
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==39) ? true : false
            x: xSlot4
            y: yBaris3
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==40) ? true : false
            x: xSlot4
            y: yBaris4
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==41) ? true : false
            x: xSlot4
            y: yBaris5
            width:wBox
            height:hS
        }

        Rectangle{
            color: rec_color 
            visible: (door_no==42) ? true : false
            x: xSlot4
            y: yBaris6
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==43) ? true : false
            x: xSlot4
            y: yBaris7
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==44) ? true : false
            x: xSlot4
            y: yBaris8
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==45) ? true : false
            x: xSlot4
            y: yBaris9
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==46) ? true : false
            x: xSlot4
            y: yBaris10
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==47) ? true : false
            x: xSlot4
            y: yBaris11
            width:wBox
            height:hM
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==48) ? true : false
            x: xSlot4
            y: yBaris12
            width:wBox
            height:hM
        }

        //CU Monitor
        // 49 - 67
        Rectangle{
            color: rec_color 
            visible: (door_no==49) ? true : false
            x: xSlot5
            y: yCU1
            width:wBox
            height:hL
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==50) ? true : false
            x: xSlot5
            y: yCU2
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==51) ? true : false
            x: xSlot5
            y: yCU3
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==52) ? true : false
            x: xSlot5
            y: yCU4
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==53) ? true : false
            x: xSlot5
            y: yCU5
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==54) ? true : false
            x: xSlot5
            y: yCU6
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==55) ? true : false
            x: xSlot5
            y: yCU7
            width:wBox
            height:hM
        }

        //CU kiri 12

        Rectangle{
            color: rec_color 
            visible: (door_no==56) ? true : false
            x: xSlot6
            y: yBaris1
            width:wBox
            height:hL
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==57) ? true : false
            x: xSlot6
            y: yBaris2
            width:wBox
            height:hM
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==58) ? true : false
            x: xSlot6
            y: yBaris3
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==59) ? true : false
            x: xSlot6
            y: yBaris4
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==60) ? true : false
            x: xSlot6
            y: yBaris5
            width:wBox
            height:hS
        }

        Rectangle{
            color: rec_color 
            visible: (door_no==61) ? true : false
            x: xSlot6
            y: yBaris6
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==62) ? true : false
            x: xSlot6
            y: yBaris7
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==63) ? true : false
            x: xSlot6
            y: yBaris8
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==64) ? true : false
            x: xSlot6
            y: yBaris9
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==65) ? true : false
            x: xSlot6
            y: yBaris10
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==66) ? true : false
            x: xSlot6
            y: yBaris11
            width:wBox
            height:hM
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==67) ? true : false
            x: xSlot6
            y: yBaris12
            width:wBox
            height:hM
        }

    }

//############################################################################//
    //###########################// 2 METER //##########################//

    Image{
        id: img_2meter
        verticalAlignment: Text.AlignVCenter
        visible: (jLock==2) ? true : false
        source:"asset/map_loker/TW2m.png"
        MouseArea {
            anchors.fill: parent
            onClicked: {
                doorClick++;
                door_no = doorClick;
                if(doorClick>=43) doorClick=0;
            }
         }

        //Slot Kanan 12

        Rectangle{
            color: rec_color 
            visible: (door_no==1) ? true : false
            x: xSlot1
            y: yBaris1
            width:wBox
            height:hL
        }

        Rectangle{
            color: rec_color 
            visible: (door_no==2) ? true : false
            x: xSlot1
            y: yBaris2
            width:wBox
            height:hM
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==3) ? true : false
            x: xSlot1
            y: yBaris3
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==4) ? true : false
            x: xSlot1
            y: yBaris4
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==5) ? true : false
            x: xSlot1
            y: yBaris5
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==6) ? true : false
            x: xSlot1
            y: yBaris6
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==7) ? true : false
            x: xSlot1
            y: yBaris7
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==8) ? true : false
            x: xSlot1
            y: yBaris8
            width:wBox
            height:hS
        }

        Rectangle{
            color: rec_color 
            visible: (door_no==9) ? true : false
            x: xSlot1
            y: yBaris9
            width:wBox
            height:hS
        }

        Rectangle{
            color: rec_color 
            visible: (door_no==10) ? true : false
            x: xSlot1
            y: yBaris10
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==11) ? true : false
            x: xSlot1
            y: yBaris11
            width:wBox
            height:hM
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==12) ? true : false
            x: xSlot1
            y: yBaris12
            width:wBox
            height:hM
        }

        //slot Kiri 12
        Rectangle{
            color: rec_color 
            visible: (door_no==13) ? true : false
            x: xSlot2
            y: yBaris1
            width:wBox
            height:hL
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==14) ? true : false
            x: xSlot2
            y: yBaris2
            width:wBox
            height:hM
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==15) ? true : false
            x: xSlot2
            y: yBaris3
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==16) ? true : false
            x: xSlot2
            y: yBaris4
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==17) ? true : false
            x: xSlot2
            y: yBaris5
            width:wBox
            height:hS
        }

        Rectangle{
            color: rec_color 
            visible: (door_no==18) ? true : false
            x: xSlot2
            y: yBaris6
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==19) ? true : false
            x: xSlot2
            y: yBaris7
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==20) ? true : false
            x: xSlot2
            y: yBaris8
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==21) ? true : false
            x: xSlot2
            y: yBaris9
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==22) ? true : false
            x: xSlot2
            y: yBaris10
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==23) ? true : false
            x: xSlot2
            y: yBaris11
            width:wBox
            height:hM
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==24) ? true : false
            x: xSlot2
            y: yBaris12
            width:wBox
            height:hM
        }

        //CU Monitor
        Rectangle{
            color: rec_color 
            visible: (door_no==25) ? true : false
            x: xSlot3
            y: yCU1
            width:wBox
            height:hL
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==26) ? true : false
            x: xSlot3
            y: yCU2
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==27) ? true : false
            x: xSlot3
            y: yCU3
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==28) ? true : false
            x: xSlot3
            y: yCU4
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==29) ? true : false
            x: xSlot3
            y: yCU5
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==30) ? true : false
            x: xSlot3
            y: yCU6
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==31) ? true : false
            x: xSlot3
            y: yCU7
            width:wBox
            height:hM
        }

        //CU kiri 12

        Rectangle{
            color: rec_color 
            visible: (door_no==32) ? true : false
            x: xSlot4
            y: yBaris1
            width:wBox
            height:hL
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==33) ? true : false
            x: xSlot4
            y: yBaris2
            width:wBox
            height:hM
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==34) ? true : false
            x: xSlot4
            y: yBaris3
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==35) ? true : false
            x: xSlot4
            y: yBaris4
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==36) ? true : false
            x: xSlot4
            y: yBaris5
            width:wBox
            height:hS
        }

        Rectangle{
            color: rec_color 
            visible: (door_no==37) ? true : false
            x: xSlot4
            y: yBaris6
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==38) ? true : false
            x: xSlot4
            y: yBaris7
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==39) ? true : false
            x: xSlot4
            y: yBaris8
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==40) ? true : false
            x: xSlot4
            y: yBaris9
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==41) ? true : false
            x: xSlot4
            y: yBaris10
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==42) ? true : false
            x: xSlot4
            y: yBaris11
            width:wBox
            height:hM
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==43) ? true : false
            x: xSlot4
            y: yBaris12
            width:wBox
            height:hM
        }

    }

//############################################################################//
    //###########################// 1 METER //##########################//

    Image{
        id: img_1meter
        verticalAlignment: Text.AlignVCenter
        visible: (jLock==1) ? true : false
        source:"asset/map_loker/TW1m.png"
         MouseArea {
            anchors.fill: parent
            onClicked: {
                doorClick++;
                door_no = doorClick;
                if(doorClick>=19) doorClick=0;
            }
         }

        Rectangle{
            color: rec_color 
            visible: (door_no==1) ? true : false
            x: xSlot1
            y: yCU1
            width:wBox
            height:hL
        }

        Rectangle{
            color: rec_color 
            visible: (door_no==2) ? true : false
            x: xSlot1
            y: yCU2
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==3) ? true : false
            x: xSlot1
            y: yCU3
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==4) ? true : false
            x: xSlot1
            y: yCU4
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==5) ? true : false
            x: xSlot1
            y: yCU5
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==6) ? true : false
            x: xSlot1
            y: yCU6
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==7) ? true : false
            x: xSlot1
            y: yCU7
            width:wBox
            height:hM
        }

        // Kiri LCD
        Rectangle{
            color: rec_color 
            visible: (door_no==8) ? true : false
            x: xSlot2
            y: 3
            width:wBox
            height:hL
        }

        Rectangle{
            color: rec_color 
            visible: (door_no==9) ? true : false
            x: xSlot2
            y: 62
            width:wBox
            height:hM
        }

        Rectangle{
            color: rec_color 
            visible: (door_no==10) ? true : false
            x: xSlot2
            y: 92
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==11) ? true : false
            x: xSlot2
            y: 105
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==12) ? true : false
            x: xSlot2
            y: 118
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==13) ? true : false
            x: xSlot2
            y: 131
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==14) ? true : false
            x: xSlot2
            y: 144
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==15) ? true : false
            x: xSlot2
            y: 157
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==16) ? true : false
            x: xSlot2
            y: 170
            width:wBox
            height:hS
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==17) ? true : false
            x: xSlot2
            y: 183
            width:wBox
            height:hS
        }

        Rectangle{
            color: rec_color 
            visible: (door_no==18) ? true : false
            x: xSlot2
            y: 196
            width:wBox
            height:hM
        }
        Rectangle{
            color: rec_color 
            visible: (door_no==19) ? true : false
            x: xSlot2
            y: 227
            width:wBox
            height:hM
        }

    }

}

