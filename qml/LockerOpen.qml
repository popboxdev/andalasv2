import QtQuick 2.4
import QtQuick.Controls 1.2
// import "Helper.js" as Helper

Rectangle {
    id: main_rectangle
    
    property var asset_path: "asset/map_loker/"

    property var type_box: ""
    property var wBox: 62
    property var wXS: 30
    property var hXS: 29
    property var hS: 11
    property var hL: 57
    property var hM: 29

    //Baris Per Slot / Per 50 CM
    // property var j: 0
    property var xSlot1: 4
    property var xSlot2: 73
    property var xSlot3: 138
    property var xSlot4: 207
    property var xSlot5: 272
    property var xSlot6: 341
    property var xSlot7: 406
    property var xSlot8: 475
    property var xSlot9: 540
    property var xSlot10: 609
    property var xSlot11: 674
    property var xSlot12: 743

    //Baris Per Slot Khusus XS = ketika YCU maka w: 30 h: 29 
    property var xSlotXS1: 35
    property var xSlotXS2: 170
    property var xSlotXS3: 302
    property var xSlotXS4: 438

    //Baris Khusus CU WITH XS
    property var yCUXS1: 4
    property var yCUXS2: 63
    property var yCUXS3: 175
    property var yCUXS4: 206
    property var yCUXS5: 237

    //Baris Khusus SU WITH XS
    property var yBarisXS1: 4
    property var yBarisXS2: 63
    property var yBarisXS3: 93
    property var yBarisXS4: 123
    property var yBarisXS5: 136
    property var yBarisXS6: 149
    property var yBarisXS7: 162
    property var yBarisXS8: 175
    property var yBarisXS9: 206
    property var yBarisXS10: 237

    //Baris Khusus CU
    property var yCU1: 3
    property var yCU2: 62
    property var yCU3: 175
    property var yCU4: 188
    property var yCU5: 201
    property var yCU6: 214
    property var yCU7: 227

    //Baris Khusus SU
    property var yBaris1: 3
    property var yBaris2: 62
    property var yBaris3: 92
    property var yBaris4: 105
    property var yBaris5: 118
    property var yBaris6: 131
    property var yBaris7: 144
    property var yBaris8: 157
    property var yBaris9: 170
    property var yBaris10: 183
    property var yBaris11: 196
    property var yBaris12: 227

    // Property 1 Meter 32"
    property var row32_1_1m: [6]
    property var row32_2_1m: [7]
    property var row32_3_1m: [8]
    property var row32_4_1m: [9]
    property var row32_5_1m: [10]
    property var row32_6_1m: [11]
    property var row32_7_1m: [12]
    property var row32_8_1m: [13]
    property var row32_9_1m: [14]
    property var row32_10_1m: [15]
    property var row32_11_1m: [16]
    property var row32_12_1m: [17]
    property var row32_cu_1m: [1,2,3,4,5]

    // Property 2 Meter 32"
    property var row32_1_2m: [1,13,30]
    property var row32_2_2m: [2,14,31]
    property var row32_3_2m: [3,15,32]
    property var row32_4_2m: [4,16,33]
    property var row32_5_2m: [5,17,34]
    property var row32_6_2m: [6,18,35]
    property var row32_7_2m: [7,19,36]
    property var row32_8_2m: [8,20,37]
    property var row32_9_2m: [9,21,38]
    property var row32_10_2m: [10,22,39]
    property var row32_11_2m: [11,23,40]
    property var row32_12_2m: [12,24,41]
    property var row32_cu_2m: [25,26,27,28,29]

    // Property 3 Meter 32"
    property var row32_1_3m: [1,13,25,37,54]
    property var row32_2_3m: [2,14,26,38,55]
    property var row32_3_3m: [3,15,27,39,56]
    property var row32_4_3m: [4,16,28,40,57]
    property var row32_5_3m: [5,17,29,41,58]
    property var row32_6_3m: [6,18,30,42,59]
    property var row32_7_3m: [7,19,31,43,60]
    property var row32_8_3m: [8,20,32,44,61]
    property var row32_9_3m: [9,21,33,45,62]
    property var row32_10_3m: [10,22,34,46,63]
    property var row32_11_3m: [11,23,35,47,64]
    property var row32_12_3m: [12,24,36,48,65]
    property var row32_cu_3m: [49,50,51,52,53]

    // Property 4 Meter 32"
    property var row32_1_4m: [1,13,25,37,54,66,78]
    property var row32_2_4m: [2,14,26,38,55,67,79]
    property var row32_3_4m: [3,15,27,39,56,68,80]
    property var row32_4_4m: [4,16,28,40,57,69,81]
    property var row32_5_4m: [5,17,29,41,58,70,82]
    property var row32_6_4m: [6,18,30,42,59,71,83]
    property var row32_7_4m: [7,19,31,43,60,72,84]
    property var row32_8_4m: [8,20,32,44,61,73,85]
    property var row32_9_4m: [9,21,33,45,62,74,86]
    property var row32_10_4m: [10,22,34,46,63,75,87]
    property var row32_11_4m: [11,23,35,47,64,76,88]
    property var row32_12_4m: [12,24,36,48,65,77,89]
    property var row32_cu_4m: [49,50,51,52,53]

    // Property 5 Meter 32"
    property var row32_1_5m: [1,13,25,37,54,66,78,90,102]
    property var row32_2_5m: [2,14,26,38,55,67,79,91,103]
    property var row32_3_5m: [3,15,27,39,56,68,80,92,104]
    property var row32_4_5m: [4,16,28,40,57,69,81,93,105]
    property var row32_5_5m: [5,17,29,41,58,70,82,94,106]
    property var row32_6_5m: [6,18,30,42,59,71,83,95,107]
    property var row32_7_5m: [7,19,31,43,60,72,84,96,108]
    property var row32_8_5m: [8,20,32,44,61,73,85,97,109]
    property var row32_9_5m: [9,21,33,45,62,74,86,98,110]
    property var row32_10_5m: [10,22,34,46,63,75,87,99,111]
    property var row32_11_5m: [11,23,35,47,64,76,88,100,112]
    property var row32_12_5m: [12,24,36,48,65,77,89,101,113]
    property var row32_cu_5m: [49,50,51,52,53]

    // Property 6 Meter 32"
    property var row32_1_6m: [1,13,25,37,49,61,78,90,102,114,126]
    property var row32_2_6m: [2,14,26,38,50,62,79,91,103,115,127]
    property var row32_3_6m: [3,15,27,39,51,63,80,92,104,116,128]
    property var row32_4_6m: [4,16,28,40,52,64,81,93,105,117,129]
    property var row32_5_6m: [5,17,29,41,53,65,82,94,106,118,130]
    property var row32_6_6m: [6,18,30,42,54,66,83,95,107,119,131]
    property var row32_7_6m: [7,19,31,43,55,67,84,96,108,120,132]
    property var row32_8_6m: [8,20,32,44,56,68,85,97,109,121,133]
    property var row32_9_6m: [9,21,33,45,57,69,86,98,110,122,134]
    property var row32_10_6m: [10,22,34,46,58,70,87,99,111,123,135]
    property var row32_11_6m: [11,23,35,47,59,71,88,100,112,124,136]
    property var row32_12_6m: [12,24,36,48,60,72,89,101,113,125,137]
    property var row32_cu_6m: [73,74,75,76,77]


    // Property 1 Meter
    property var row_1_1m: [1,8]
    property var row_2_1m: [2,9]
    property var row_3_1m: [10]
    property var row_4_1m: [11]
    property var row_5_1m: [12]
    property var row_6_1m: [13]
    property var row_7_1m: [14]
    property var row_8_1m: [15]
    property var row_9_1m: [16]
    property var row_10_1m: [17]
    property var row_11_1m: [18]
    property var row_12_1m: [19]
    property var row_cu_1m: [3,4,5,6,7]


    // Property 2 Meter
    property var row_1_2m: [1,13,25,32]
    property var row_2_2m: [2,14,26,33]
    property var row_3_2m: [3,15,34]
    property var row_4_2m: [4,16,35]
    property var row_5_2m: [5,17,36]
    property var row_6_2m: [6,18,37]
    property var row_7_2m: [7,19,38]
    property var row_8_2m: [8,20,39]
    property var row_9_2m: [9,21,40]
    property var row_10_2m: [10,22,41]
    property var row_11_2m: [11,23,42]
    property var row_12_2m: [12,24,43]
    property var row_cu_2m: [27,28,29,30,31]


    // Property 3 Meter
    property var row_1_3m: [1,13,25,37,49,56]
    property var row_2_3m: [2,14,26,38,50,57]
    property var row_3_3m: [3,15,27,39,58]
    property var row_4_3m: [4,16,28,40,59]
    property var row_5_3m: [5,17,29,41,60]
    property var row_6_3m: [6,18,30,42,61]
    property var row_7_3m: [7,19,31,43,62]
    property var row_8_3m: [8,20,32,44,63]
    property var row_9_3m: [9,21,33,45,64]
    property var row_10_3m: [10,22,34,46,65]
    property var row_11_3m: [11,23,35,47,66]
    property var row_12_3m: [12,24,36,48,67]
    property var row_cu_3m: [51,52,53,54,55]


    // Property 4 Meter
    property var row_1_4m: [1,13,25,37,49,56,68,80]
    property var row_2_4m: [2,14,26,38,50,57,69,81]
    property var row_3_4m: [3,15,27,39,58,70,82]
    property var row_4_4m: [4,16,28,40,59,71,83]
    property var row_5_4m: [5,17,29,41,60,72,84]
    property var row_6_4m: [6,18,30,42,61,73,85]
    property var row_7_4m: [7,19,31,43,62,74,86]
    property var row_8_4m: [8,20,32,44,63,75,87]
    property var row_9_4m: [9,21,33,45,64,76,88]
    property var row_10_4m: [10,22,34,46,65,77,89]
    property var row_11_4m: [11,23,35,47,66,78,90]
    property var row_12_4m: [12,24,36,48,67,79,91]
    property var row_cu_4m: [51,52,53,54,55]


    // Property 5 Meter
    property var row_1_5m: [1,13,25,37,49,56,68,80,92,104]
    property var row_2_5m: [2,14,26,38,50,57,69,81,93,105]
    property var row_3_5m: [3,15,27,39,58,70,82,94,106]
    property var row_4_5m: [4,16,28,40,59,71,83,95,107]
    property var row_5_5m: [5,17,29,41,60,72,84,96,108]
    property var row_6_5m: [6,18,30,42,61,73,85,97,109]
    property var row_7_5m: [7,19,31,43,62,74,86,98,110]
    property var row_8_5m: [8,20,32,44,63,75,87,99,111]
    property var row_9_5m: [9,21,33,45,64,76,88,100,112]
    property var row_10_5m: [10,22,34,46,65,77,89,101,113]
    property var row_11_5m: [11,23,35,47,66,78,90,102,114]
    property var row_12_5m: [12,24,36,48,67,79,91,103,115]
    property var row_cu_5m: [51,52,53,54,55]
    

    // Property 6 Meter
    property var row_1_6m: [1,13,25,37,49,61,73,80,92,104,116,128] 
    property var row_2_6m: [2,14,26,38,50,62,74,81,93,105,117,129] 
    property var row_3_6m: [3,15,27,39,51,63,82,94,106,118,130] 
    property var row_4_6m: [4,16,28,40,52,64,83,95,107,119,131] 
    property var row_5_6m: [5,17,29,41,53,65,84,96,108,120,132] 
    property var row_6_6m: [6,18,30,42,54,66,85,97,109,121,133] 
    property var row_7_6m: [7,19,31,43,55,67,86,98,110,122,134] 
    property var row_8_6m: [8,20,32,44,56,68,87,99,111,123,135] 
    property var row_9_6m: [9,21,33,45,57,69,88,100,112,124,136] 
    property var row_10_6m: [10,22,34,46,58,70,89,101,113,125,137] 
    property var row_11_6m: [11,23,35,47,59,71,90,102,114,126,138] 
    property var row_12_6m: [12,24,36,48,60,72,91,103,115,127,139] 
    property var row_cu_6m: [75,76,77,78,79] 


    //Property 1 Meter XS
    property var rowXS_1_1m: [1,7]
    property var rowXS_2_1m: [2,8]
    property var rowXS_3_1m: [9]
    property var rowXS_4_1m: [10]
    property var rowXS_5_1m: [11]
    property var rowXS_6_1m: [12]
    property var rowXS_7_1m: [13]
    property var rowXS_8_1m: [14]
    property var rowXS_9_1m: [15]
    property var rowXS_10_1m: [16]
    property var rowXS_cu_1m: [4,5,6]

    //Property 2 Meter XS
    property var rowXS_1_2m: [1,11,21,27]
    property var rowXS_2_2m: [2,12,22,28]
    property var rowXS_3_2m: [3,13,29]
    property var rowXS_4_2m: [4,14,30]
    property var rowXS_5_2m: [5,15,31]
    property var rowXS_6_2m: [6,16,32]
    property var rowXS_7_2m: [7,17,33]
    property var rowXS_8_2m: [8,18,34]
    property var rowXS_9_2m: [9,19,35]
    property var rowXS_10_2m: [10,20,36]
    property var rowXS_cu_2m: [23,24,25,26]

    //Property 3 Meter XS
    property var rowXS_1_3m: [1,11,21,31,41,47]
    property var rowXS_2_3m: [2,12,22,32,42,48]
    property var rowXS_3_3m: [3,13,23,33,49]
    property var rowXS_4_3m: [4,14,24,34,50]
    property var rowXS_5_3m: [5,15,25,35,51]
    property var rowXS_6_3m: [6,16,26,36,52]
    property var rowXS_7_3m: [7,17,27,37,53]
    property var rowXS_8_3m: [8,18,28,38,54]
    property var rowXS_9_3m: [9,19,29,39,55]
    property var rowXS_10_3m: [10,20,30,40,56]
    property var rowXS_cu_3m: [43,44,45,46]

    //Property 4 Meter XS
    property var rowXS_1_4m: [1,11,21,31,41,47,57,67]
    property var rowXS_2_4m: [2,12,22,32,42,48,58,68]
    property var rowXS_3_4m: [3,13,23,33,49,59,69]
    property var rowXS_4_4m: [4,14,24,34,50,60,70]
    property var rowXS_5_4m: [5,15,25,35,51,61,71]
    property var rowXS_6_4m: [6,16,26,36,52,62,72]
    property var rowXS_7_4m: [7,17,27,37,53,63,73]
    property var rowXS_8_4m: [8,18,28,38,54,64,74]
    property var rowXS_9_4m: [9,19,29,39,55,65,75]
    property var rowXS_10_4m: [10,20,30,40,56,66,76]
    property var rowXS_cu_4m: [43,44,45,46]


    //Property 5 Meter XS
    property var rowXS_1_5m: [1,11,21,31,41,47,57,67,77,87]
    property var rowXS_2_5m: [2,12,22,32,42,48,58,68,78,88]
    property var rowXS_3_5m: [3,13,23,33,49,59,69,79,89]
    property var rowXS_4_5m: [4,14,24,34,50,60,70,80,90]
    property var rowXS_5_5m: [5,15,25,35,51,61,71,81,91]
    property var rowXS_6_5m: [6,16,26,36,52,62,72,82,92]
    property var rowXS_7_5m: [7,17,27,37,53,63,73,83,93]
    property var rowXS_8_5m: [8,18,28,38,54,64,74,84,94]
    property var rowXS_9_5m: [9,19,29,39,55,65,75,85,95]
    property var rowXS_10_5m: [10,20,30,40,56,66,76,86,96]
    property var rowXS_cu_5m: [43,44,45,46]

    //Property 6 Meter XS
    property var rowXS_1_6m: [1,11,21,31,41,51,61,67,77,87,97,107]
    property var rowXS_2_6m: [2,12,22,32,42,52,62,68,78,88,98,108]
    property var rowXS_3_6m: [3,13,23,33,43,53,69,79,89,99,109]
    property var rowXS_4_6m: [4,14,24,34,44,54,70,80,90,100,110]
    property var rowXS_5_6m: [5,15,25,35,45,55,71,81,91,101,111]
    property var rowXS_6_6m: [6,16,26,36,46,56,72,82,92,102,112]
    property var rowXS_7_6m: [7,17,27,37,47,57,73,83,93,103,113]
    property var rowXS_8_6m: [8,18,28,38,48,58,74,84,94,104,114]
    property var rowXS_9_6m: [9,19,29,39,49,59,75,85,95,105,115]
    property var rowXS_10_6m: [10,20,30,40,50,66,76,86,96,106,116]
    property var rowXS_cu_6m: [63,64,65,66]
    


    property var rec_color: "#ffa400"
    color:"transparent"
    property var jLock: 6
    property var doorClick: 0
    property var img_source: ''
    property var doorType: ''
    property var height_type: ''
    property var lockerType: ""
    property var door_no: 0
    property var data_locker: ''
    property var imgX: 0
    property var imgY: 0
    property var row_Cu: 0
    property var pos
    property int dataX: 0
    property int dataY: 0
    property int dataH: 0

    function __construct() {

        if(lockerType=="32_TWIN"){
            if (jLock == 1) {
                img_source = 'asset/map_loker/TW32_1m.png'
            } else if (jLock == 2) {
                img_source = 'asset/map_loker/TW32_2m.png'
            } else if (jLock == 3) { 
                img_source = 'asset/map_loker/TW32_3m.png'
            } else if (jLock == 4) {
                img_source = 'asset/map_loker/TW32_4m.png'
            } else if (jLock == 5) {
                img_source = 'asset/map_loker/TW32_5m.png'
            } else {
                img_source = 'asset/map_loker/TW32_6m.png'
            }
            position();
        }else if(lockerType=="15_TWIN"){
            if (jLock == 1) {
                img_source = 'asset/map_loker/TW1m.png'
            } else if (jLock == 2) {
                img_source = 'asset/map_loker/TW2m.png'
            } else if (jLock == 3) { 
                img_source = 'asset/map_loker/TW3m.png'
            } else if (jLock == 4) {
                img_source = 'asset/map_loker/TW4m.png'
            } else if (jLock == 5) {
                img_source = 'asset/map_loker/TW5m.png'
            } else {
                img_source = 'asset/map_loker/TW6m.png'
            }
            position();
        }else if(lockerType=="15_TWIN_XS"){
            if (jLock == 1) {
                img_source = 'asset/map_loker/TWXS_1m.png'
            } else if (jLock == 2) {
                img_source = 'asset/map_loker/TWXS_2m.png'
            } else if (jLock == 3) { 
                img_source = 'asset/map_loker/TWXS_3m.png'
            } else if (jLock == 4) {
                img_source = 'asset/map_loker/TWXS_4m.png'
            } else if (jLock == 5) {
                img_source = 'asset/map_loker/TWXS_5m.png'
            } else {
                img_source = 'asset/map_loker/TWXS_6m.png'
            }
            position();
        }else{
            //DEFAULT JIKA LOKER MASIH TIDAK SESUAI
            img_source = 'asset/map_loker/TW5m.png'
            imgX = 4
            imgY = 4
            height_type = 57
        }
    }
    
    function position() {
        console.log("door_no.length: "+ door_no.length)
        
        if (door_no.length > 1) {
            for(var i = 0; i < door_no.length; i++) {
                var door_number = door_no[i].number
                var size = door_no[i].size
                checking_door_location(jLock, door_number, size)
            }
            // console.log("JSON.stringify(data_locker) s: "+ JSON.stringify(data_locker))
            if(data_locker != ''){
                var result= JSON.parse(JSON.stringify(data_locker));
                imgX = result.x
                imgY = result.y
                height_type = result.h 
                console.log(" > 1 imgX: " + imgX + " imgY:" + imgY + " height: " + height_type)
            }else{
                console.log("Memuat data...")
            }
            
        } else {
            checking_door_location(jLock, door_no, doorType)
            if(data_locker != ''){
                var result= JSON.parse(JSON.stringify(data_locker));
                // console.log("RESULT : "+ JSON.stringify(data_locker))
                if (result != undefined) {
                    imgX = result.x
                    imgY = result.y
                    height_type = result.h
                    console.log("imgX: " + imgX + " imgY:" + imgY + " height: " + height_type)
                }
            }else{
                console.log("Memuat data...")
            }
            
        }
    }
    
    function checking_door_location(LengthLocker, number_locker, size) {
        if(lockerType=="32_TWIN"){
            twin_32(LengthLocker, number_locker, size)
        }else if(lockerType=="15_TWIN"){
            twin_15(LengthLocker, number_locker, size)
        }else if(lockerType=="15_TWIN_XS"){
            twin_15_xs(LengthLocker, number_locker, size)
        }
    }
    
    function set_Yrow(door_num) {
        var res = false;
        var row_num1, row_num2, row_num3, row_num4, row_num5, row_num6, row_num7, row_num8, row_num9, row_num10, row_num11, row_num12, row_cu
        if(lockerType=="32_TWIN"){
            if(jLock==1){
                row_num1 = row32_1_1m
                row_num2 = row32_2_1m
                row_num3 = row32_3_1m
                row_num4 = row32_4_1m
                row_num5 = row32_5_1m
                row_num6 = row32_6_1m
                row_num7 = row32_7_1m
                row_num8 = row32_8_1m
                row_num9 = row32_9_1m
                row_num10 = row32_10_1m
                row_num11 = row32_11_1m
                row_num12 = row32_12_1m
                row_cu = row32_cu_1m
            } else if(jLock==2){
                row_num1 = row32_1_2m
                row_num2 = row32_2_2m
                row_num3 = row32_3_2m
                row_num4 = row32_4_2m
                row_num5 = row32_5_2m
                row_num6 = row32_6_2m
                row_num7 = row32_7_2m
                row_num8 = row32_8_2m
                row_num9 = row32_9_2m
                row_num10 = row32_10_2m
                row_num11 = row32_11_2m
                row_num12 = row32_12_2m
                row_cu = row32_cu_2m
            } else if(jLock==3){
                row_num1 = row32_1_3m
                row_num2 = row32_2_3m
                row_num3 = row32_3_3m
                row_num4 = row32_4_3m
                row_num5 = row32_5_3m
                row_num6 = row32_6_3m
                row_num7 = row32_7_3m
                row_num8 = row32_8_3m
                row_num9 = row32_9_3m
                row_num10 = row32_10_3m
                row_num11 = row32_11_3m
                row_num12 = row32_12_3m
                row_cu = row32_cu_3m
            } else if(jLock==4){
                row_num1 = row32_1_4m
                row_num2 = row32_2_4m
                row_num3 = row32_3_4m
                row_num4 = row32_4_4m
                row_num5 = row32_5_4m
                row_num6 = row32_6_4m
                row_num7 = row32_7_4m
                row_num8 = row32_8_4m
                row_num9 = row32_9_4m
                row_num10 = row32_10_4m
                row_num11 = row32_11_4m
                row_num12 = row32_12_4m
                row_cu = row32_cu_4m
            } else if(jLock==5){
                row_num1 = row32_1_5m
                row_num2 = row32_2_5m
                row_num3 = row32_3_5m
                row_num4 = row32_4_5m
                row_num5 = row32_5_5m
                row_num6 = row32_6_5m
                row_num7 = row32_7_5m
                row_num8 = row32_8_5m
                row_num9 = row32_9_5m
                row_num10 = row32_10_5m
                row_num11 = row32_11_5m
                row_num12 = row32_12_5m
                row_cu = row32_cu_5m
            } else if(jLock==6){
                row_num1 = row32_1_6m
                row_num2 = row32_2_6m
                row_num3 = row32_3_6m
                row_num4 = row32_4_6m
                row_num5 = row32_5_6m
                row_num6 = row32_6_6m
                row_num7 = row32_7_6m
                row_num8 = row32_8_6m
                row_num9 = row32_9_6m
                row_num10 = row32_10_6m
                row_num11 = row32_11_6m
                row_num12 = row32_12_6m
                row_cu = row32_cu_6m
            }

        }else if(lockerType=="15_TWIN"){
            if(jLock==1){
                row_num1 = row_1_1m
                row_num2 = row_2_1m
                row_num3 = row_3_1m
                row_num4 = row_4_1m
                row_num5 = row_5_1m
                row_num6 = row_6_1m
                row_num7 = row_7_1m
                row_num8 = row_8_1m
                row_num9 = row_9_1m
                row_num10 = row_10_1m
                row_num11 = row_11_1m
                row_num12 = row_12_1m
                row_cu = row_cu_1m
            } else if(jLock==2){
                row_num1 = row_1_2m
                row_num2 = row_2_2m
                row_num3 = row_3_2m
                row_num4 = row_4_2m
                row_num5 = row_5_2m
                row_num6 = row_6_2m
                row_num7 = row_7_2m
                row_num8 = row_8_2m
                row_num9 = row_9_2m
                row_num10 = row_10_2m
                row_num11 = row_11_2m
                row_num12 = row_12_2m
                row_cu = row_cu_2m
            } else if(jLock==3){
                row_num1 = row_1_3m
                row_num2 = row_2_3m
                row_num3 = row_3_3m
                row_num4 = row_4_3m
                row_num5 = row_5_3m
                row_num6 = row_6_3m
                row_num7 = row_7_3m
                row_num8 = row_8_3m
                row_num9 = row_9_3m
                row_num10 = row_10_3m
                row_num11 = row_11_3m
                row_num12 = row_12_3m
                row_cu = row_cu_3m
            } else if(jLock==4){
                row_num1 = row_1_4m
                row_num2 = row_2_4m
                row_num3 = row_3_4m
                row_num4 = row_4_4m
                row_num5 = row_5_4m
                row_num6 = row_6_4m
                row_num7 = row_7_4m
                row_num8 = row_8_4m
                row_num9 = row_9_4m
                row_num10 = row_10_4m
                row_num11 = row_11_4m
                row_num12 = row_12_4m
                row_cu = row_cu_4m
            } else if(jLock==5){
                row_num1 = row_1_5m
                row_num2 = row_2_5m
                row_num3 = row_3_5m
                row_num4 = row_4_5m
                row_num5 = row_5_5m
                row_num6 = row_6_5m
                row_num7 = row_7_5m
                row_num8 = row_8_5m
                row_num9 = row_9_5m
                row_num10 = row_10_5m
                row_num11 = row_11_5m
                row_num12 = row_12_5m
                row_cu = row_cu_5m
            } else if(jLock==6){
                row_num1 = row_1_6m
                row_num2 = row_2_6m
                row_num3 = row_3_6m
                row_num4 = row_4_6m
                row_num5 = row_5_6m
                row_num6 = row_6_6m
                row_num7 = row_7_6m
                row_num8 = row_8_6m
                row_num9 = row_9_6m
                row_num10 = row_10_6m
                row_num11 = row_11_6m
                row_num12 = row_12_6m
                row_cu = row_cu_6m
            }
        }else if(lockerType=="15_TWIN_XS"){
            if(jLock==1){
                row_num1 = rowXS_1_1m
                row_num2 = rowXS_2_1m
                row_num3 = rowXS_3_1m
                row_num4 = rowXS_4_1m
                row_num5 = rowXS_5_1m
                row_num6 = rowXS_6_1m
                row_num7 = rowXS_7_1m
                row_num8 = rowXS_8_1m
                row_num9 = rowXS_9_1m
                row_num10 = rowXS_10_1m
                row_cu = rowXS_cu_1m
            } else if(jLock==2){
                row_num1 = rowXS_1_2m
                row_num2 = rowXS_2_2m
                row_num3 = rowXS_3_2m
                row_num4 = rowXS_4_2m
                row_num5 = rowXS_5_2m
                row_num6 = rowXS_6_2m
                row_num7 = rowXS_7_2m
                row_num8 = rowXS_8_2m
                row_num9 = rowXS_9_2m
                row_num10 = rowXS_10_2m
                row_cu = rowXS_cu_2m
            } else if(jLock==3){
                row_num1 = rowXS_1_3m
                row_num2 = rowXS_2_3m
                row_num3 = rowXS_3_3m
                row_num4 = rowXS_4_3m
                row_num5 = rowXS_5_3m
                row_num6 = rowXS_6_3m
                row_num7 = rowXS_7_3m
                row_num8 = rowXS_8_3m
                row_num9 = rowXS_9_3m
                row_num10 = rowXS_10_3m
                row_cu = rowXS_cu_3m
            } else if(jLock==4){
                row_num1 = rowXS_1_4m
                row_num2 = rowXS_2_4m
                row_num3 = rowXS_3_4m
                row_num4 = rowXS_4_4m
                row_num5 = rowXS_5_4m
                row_num6 = rowXS_6_4m
                row_num7 = rowXS_7_4m
                row_num8 = rowXS_8_4m
                row_num9 = rowXS_9_4m
                row_num10 = rowXS_10_4m
                row_cu = rowXS_cu_4m
            } else if(jLock==5){
                row_num1 = rowXS_1_5m
                row_num2 = rowXS_2_5m
                row_num3 = rowXS_3_5m
                row_num4 = rowXS_4_5m
                row_num5 = rowXS_5_5m
                row_num6 = rowXS_6_5m
                row_num7 = rowXS_7_5m
                row_num8 = rowXS_8_5m
                row_num9 = rowXS_9_5m
                row_num10 = rowXS_10_5m
                row_cu = rowXS_cu_5m
            } else if(jLock==6){
                row_num1 = rowXS_1_6m
                row_num2 = rowXS_2_6m
                row_num3 = rowXS_3_6m
                row_num4 = rowXS_4_6m
                row_num5 = rowXS_5_6m
                row_num6 = rowXS_6_6m
                row_num7 = rowXS_7_6m
                row_num8 = rowXS_8_6m
                row_num9 = rowXS_9_6m
                row_num10 = rowXS_10_6m
                row_cu = rowXS_cu_6m
            }
        }
        if (lockerType=="15_TWIN_XS"){
            for(var i = 1; i <= 11; i++) {
                if (i == 1) {
                    res = checking_array_exist(row_num1, door_num)
                    if (res) return yBarisXS1
                    else continue 
                } else if (i == 2) {
                    res = checking_array_exist(row_num2, door_num)
                    if (res) return yBarisXS2
                    else continue               
                } else if (i == 3) {
                    res = checking_array_exist(row_num3, door_num)
                    if (res) return yBarisXS3
                    else continue 
                } else if (i == 4) {
                    res = checking_array_exist(row_num4, door_num)
                    if (res) return yBarisXS4
                    else continue 
                } else if (i == 5) {
                    res = checking_array_exist(row_num5, door_num)
                    if (res) return yBarisXS5
                    else continue 
                } else if (i == 6) {
                    res = checking_array_exist(row_num6, door_num)
                    if (res) return yBarisXS6
                    else continue 
                } else if (i == 7) {
                    res = checking_array_exist(row_num7, door_num)
                    if (res) return yBarisXS7
                    else continue 
                } else if (i == 8) {
                    res = checking_array_exist(row_num8, door_num)
                    if (res) return yBarisXS8
                    else continue 
                } else if (i == 9) {
                    res = checking_array_exist(row_num9, door_num)
                    if (res) return yBarisXS9
                    else continue 
                } else if (i == 10) {
                    res = checking_array_exist(row_num10, door_num)
                    if (res) return yBarisXS10
                    else continue 
                } else if (i == 11) {
                    var result = checking_array_exist(row_cu, door_num)
                    return result
                }
            }
            
        } else{
            for(var i = 1; i <= 13; i++) {
                if (i == 1) {
                    res = checking_array_exist(row_num1, door_num)
                    if (res) return yBaris1
                    else continue 
                } else if (i == 2) {
                    res = checking_array_exist(row_num2, door_num)
                    if (res) return yBaris2
                    else continue               
                } else if (i == 3) {
                    res = checking_array_exist(row_num3, door_num)
                    if (res) return yBaris3
                    else continue 
                } else if (i == 4) {
                    res = checking_array_exist(row_num4, door_num)
                    if (res) return yBaris4
                    else continue 
                } else if (i == 5) {
                    res = checking_array_exist(row_num5, door_num)
                    if (res) return yBaris5
                    else continue 
                } else if (i == 6) {
                    res = checking_array_exist(row_num6, door_num)
                    if (res) return yBaris6
                    else continue 
                } else if (i == 7) {
                    res = checking_array_exist(row_num7, door_num)
                    if (res) return yBaris7
                    else continue 
                } else if (i == 8) {
                    res = checking_array_exist(row_num8, door_num)
                    if (res) return yBaris8
                    else continue 
                } else if (i == 9) {
                    res = checking_array_exist(row_num9, door_num)
                    if (res) return yBaris9
                    else continue 
                } else if (i == 10) {
                    res = checking_array_exist(row_num10, door_num)
                    if (res) return yBaris10
                    else continue 
                } else if (i == 11) {
                    res = checking_array_exist(row_num11, door_num)
                    if (res) return yBaris11
                    else continue 
                } else if (i == 12) {
                    res = checking_array_exist(row_num12, door_num)
                    if (res) return yBaris12
                    else continue 
                } else if (i == 13) {
                    var result = checking_array_exist(row_cu, door_num)
                    return result
                } 
            }
        }
    }

    function checking_array_exist(arr, door_number) {
    
        for(var j = 0; j < arr.length; j++) {
            if (door_number == arr[j]) { 
                if(lockerType=="32_TWIN"){
                    if(jLock==1){
                        if(door_number == 1 || door_number == 2 || door_number == 3 || door_number == 4 || door_number == 5){
                            return ycu_32(j);
                        } else {
                            return true
                        }

                    } else if(jLock==2){
                        if(door_number == 25 || door_number == 26 || door_number == 27 || door_number == 28 || door_number == 29){
                            return ycu_32(j);
                        } else {
                            return true
                        }
                    } else if(jLock==3 || jLock==4 || jLock==5){
                        if(door_number == 25 || door_number == 26 || door_number == 27 || door_number == 28 || door_number == 29){
                            return ycu_32(j);
                        } else {
                            return true
                        }
                    } else if(jLock==6){
                        if(door_number == 73 || door_number == 74 || door_number == 75 || door_number == 76 || door_number == 77){
                            return ycu_32(j);
                        } else {
                            return true
                        }
                    }
                } else if(lockerType=="15_TWIN"){
                    if(jLock==1){
                        if(door_number == 3 || door_number == 4 || door_number == 5 || door_number == 6 || door_number == 7){
                            return ycu_15(j);
                        } else {
                            return true
                        }

                    } else if(jLock==2){
                        if(door_number == 27 || door_number == 28 || door_number == 29 || door_number == 30 || door_number == 31){
                            return ycu_15(j);
                        } else {
                            return true
                        }
                    } else if(jLock==3 || jLock==4 || jLock==5){
                        if(door_number == 51 || door_number == 52 || door_number == 53 || door_number == 54 || door_number == 55){
                            return ycu_15(j);
                        } else {
                            return true
                        }
                    } else if(jLock==6){
                        if(door_number == 75 || door_number == 76 || door_number == 77 || door_number == 78 || door_number == 79){
                            return ycu_15(j);
                        } else {
                            return true
                        }
                    }
                } else if(lockerType=="15_TWIN_XS"){
                    if(jLock==1){
                        if(door_number == 3 || door_number == 4 || door_number == 5 || door_number == 6){
                            return ycu_15_xs(j);
                        } else {
                            return true
                        }

                    } else if(jLock==2){
                        if(door_number == 23 || door_number == 24 || door_number == 25 || door_number == 26){
                            return ycu_15_xs(j);
                        } else {
                            return true
                        }
                    } else if(jLock==3 || jLock==4 || jLock==5){
                        if(door_number == 43 || door_number == 44 || door_number == 45 || door_number == 46){
                            return ycu_15_xs(j);
                        } else {
                            return true
                        }
                    } else if(jLock==6){
                        if(door_number == 63 || door_number == 64 || door_number == 65 || door_number == 66){
                            return ycu_15_xs(j);
                        } else {
                            return true
                        }
                    }
                }
            }
        }
    }
    
    function choose_door_type(type) {
        
        console.log("DOOOR TYYPEEE: " + type)
        type_box = type;
        
        if (type == 'XL') {
           return height_type = ''
        } else if (type == 'L') {
            return height_type = hL
        } else if (type == 'M') {
            return height_type = hM
        } else if (type == 'S') {
            return height_type = hS
        } else if (type == 'MINI') {
            return height_type = hM
        } else {
            return height_type = hS
        }
    }

    Image{
        id: img_locker
        verticalAlignment: Text.AlignVCenter
        visible: true
        source: img_source
        MouseArea {
            anchors.fill: parent
            onClicked: {
                // debugging locker position
                doorClick++;
                door_no = doorClick;
                if(doorClick>=139) doorClick=0;
            }
        }

        Rectangle{
            color: rec_color 
            visible: true
            x: imgX
            y: imgY
            width:(type_box=="MINI") ? wXS : wBox
            height: height_type
            // AnimatedImage{
            //     id: loading_image
            //     x: 0
            //     y: 10
            //     width: 100
            //     height: 50
            //     source: asset_path + 'arrow.gif'
            //     // fillMode: Image.PreserveAspectFit
            // }
        }
    }

    
    function twin_15_xs(LengthLocker, number_locker, size) {
        if (LengthLocker == 1){
            if (number_locker >= 1 && number_locker <= 6) { //ada XS
                var xSlot;
                if(number_locker==4){
                    xSlot = xSlotXS1;
                }else{
                    xSlot = xSlot1;
                }
                locker_data(xSlot,number_locker,size);

            } else if (number_locker >= 7 && number_locker <= 16) {
                var xSlot = xSlot2;
                locker_data(xSlot,number_locker,size);

            }
        } else if (LengthLocker == 2 || LengthLocker == 3 || LengthLocker == 4 || LengthLocker == 5 || LengthLocker == 6){

            if(LengthLocker == 2 && (number_locker >=21 && number_locker <= 26)){ //ada XS
                var xSlot;
                if(number_locker==24){
                    xSlot = xSlotXS2
                }else{
                    xSlot = xSlot3;
                }
                locker_data(xSlot,number_locker,size);

            } else if(LengthLocker == 2 && (number_locker >=27 && number_locker <= 36)){
                var xSlot = xSlot4;
                locker_data(xSlot,number_locker,size);

            } else if(LengthLocker==6 && (number_locker >= 41 && number_locker <= 50 )){
                var xSlot = xSlot5;
                locker_data(xSlot,number_locker,size);

            } else if(LengthLocker==6 && (number_locker >= 51 && number_locker <= 60 )){
                var xSlot = xSlot6;
                locker_data(xSlot,number_locker,size);

            } else if(LengthLocker==6 && (number_locker >= 61 && number_locker <= 66 )){ //ada XS
                var xSlot;
                if(number_locker==64){
                    xSlot = xSlotXS4;
                }else{
                    xSlot = xSlot7;
                }
                locker_data(xSlot,number_locker,size);

            } else{
                if (number_locker >= 1 && number_locker <= 10) {
                    var xSlot = xSlot1;
                    locker_data(xSlot,number_locker,size);
                    
                } else if (number_locker >= 10 && number_locker <= 20) {
                    var xSlot = xSlot2;
                    locker_data(xSlot,number_locker,size);
                    
                } else if (number_locker >= 21 && number_locker <= 30) {
                    var xSlot = xSlot3;
                    locker_data(xSlot,number_locker,size);
                    
                } else if (number_locker >= 31 && number_locker <= 40) {
                    var xSlot = xSlot4;
                    locker_data(xSlot,number_locker,size);
                    
                } else if (number_locker >= 41 && number_locker <= 46) { //ADA XS
                    var xSlot;
                    if(number_locker==44){
                        xSlot = xSlotXS3
                    }else{
                        xSlot = xSlot5;
                    }
                    locker_data(xSlot,number_locker,size);
                    
                } else if (number_locker >= 47 && number_locker <= 56) {
                    var xSlot = xSlot6;
                    locker_data(xSlot,number_locker,size);
                    
                } else if (number_locker >= 57 && number_locker <= 66) {
                    var xSlot = xSlot7;
                    locker_data(xSlot,number_locker,size);
                    
                } else if (number_locker >= 67 && number_locker <= 76) {
                    var xSlot = xSlot8;
                    locker_data(xSlot,number_locker,size);
                    
                } else if (number_locker >= 77 && number_locker <= 86) {
                    var xSlot = xSlot9;
                    locker_data(xSlot,number_locker,size);
                    
                } else if (number_locker >= 87 && number_locker <= 96) {
                    var xSlot = xSlot10;
                    locker_data(xSlot,number_locker,size);
                    
                } else if (number_locker >= 97 && number_locker <= 106) {
                    var xSlot = xSlot11;
                    locker_data(xSlot,number_locker,size);
                    
                } else if (number_locker >= 107 && number_locker <= 116) {
                    var xSlot = xSlot12;
                    locker_data(xSlot,number_locker,size);
                    
                }
            }
        }
    }
    
    function twin_15(LengthLocker, number_locker, size) {
        if (LengthLocker == 1){
            if (number_locker >= 1 && number_locker <= 7) {
                var xSlot = xSlot1;
                locker_data(xSlot,number_locker,size);

            } else if (number_locker >= 8 && number_locker <= 19) {
                var xSlot = xSlot2;
                locker_data(xSlot,number_locker,size);

            }
        } else if (LengthLocker == 2 || LengthLocker == 3 || LengthLocker == 4 || LengthLocker == 5 || LengthLocker == 6){

            if(LengthLocker == 2 && (number_locker >=25 && number_locker <= 31)){
                var xSlot = xSlot3;
                locker_data(xSlot,number_locker,size);

            } else if(LengthLocker == 2 && (number_locker >=32 && number_locker <= 43)){
                var xSlot = xSlot4;
                locker_data(xSlot,number_locker,size);

            } else if(LengthLocker==6 && (number_locker >= 49 && number_locker <= 60 )){
                var xSlot = xSlot5;
                locker_data(xSlot,number_locker,size);

            } else if(LengthLocker==6 && (number_locker >= 61 && number_locker <= 72 )){
                var xSlot = xSlot6;
                locker_data(xSlot,number_locker,size);

            } else if(LengthLocker==6 && (number_locker >= 73 && number_locker <= 79 )){
                var xSlot = xSlot7;
                locker_data(xSlot,number_locker,size);

            } else{
                if (number_locker >= 1 && number_locker <= 12) {
                    var xSlot = xSlot1;
                    locker_data(xSlot,number_locker,size);
                    
                } else if (number_locker >= 13 && number_locker <= 24) {
                    var xSlot = xSlot2;
                    locker_data(xSlot,number_locker,size);
                    
                } else if (number_locker >= 25 && number_locker <= 36) {
                    var xSlot = xSlot3;
                    locker_data(xSlot,number_locker,size);
                    
                } else if (number_locker >= 37 && number_locker <= 48) {
                    var xSlot = xSlot4;
                    locker_data(xSlot,number_locker,size);
                    
                } else if (number_locker >= 49 && number_locker <= 55) {
                    var xSlot = xSlot5;
                    locker_data(xSlot,number_locker,size);
                    
                } else if (number_locker >= 56 && number_locker <= 67) {
                    var xSlot = xSlot6;
                    locker_data(xSlot,number_locker,size);
                    
                } else if (number_locker >= 68 && number_locker <= 79) {
                    var xSlot = xSlot7;
                    locker_data(xSlot,number_locker,size);
                    
                } else if (number_locker >= 80 && number_locker <= 91) {
                    var xSlot = xSlot8;
                    locker_data(xSlot,number_locker,size);
                    
                } else if (number_locker >= 92 && number_locker <= 103) {
                    var xSlot = xSlot9;
                    locker_data(xSlot,number_locker,size);
                    
                } else if (number_locker >= 104 && number_locker <= 115) {
                    var xSlot = xSlot10;
                    locker_data(xSlot,number_locker,size);
                    
                } else if (number_locker >= 116 && number_locker <= 127) {
                    var xSlot = xSlot11;
                    locker_data(xSlot,number_locker,size);
                    
                } else if (number_locker >= 128 && number_locker <= 139) {
                    var xSlot = xSlot12;
                    locker_data(xSlot,number_locker,size);
                    
                }
            }
        }
    }

    function twin_32(LengthLocker, number_locker, size) {
        // console.log("checking_door_location LengthLocker : " + LengthLocker + " number_locker : "+ number_locker + "size : " + size + " lockerType: " + lockerType)
        
        // 32_1meter
        if (LengthLocker == 1) {
            if (number_locker >= 1 && number_locker <= 5) {
                var xSlot = xSlot1;
                locker_data(xSlot,number_locker,size);
              
            } else if (number_locker >= 6 && number_locker <= 17) {
                var xSlot = xSlot2;
                locker_data(xSlot,number_locker,size);
            }
        } else if (LengthLocker == 2 || LengthLocker == 3 || LengthLocker == 4 || LengthLocker == 5 || LengthLocker == 6) {

            if(LengthLocker == 2 && (number_locker >=25 && number_locker <= 29)){
                var xSlot = xSlot3;
                locker_data(xSlot,number_locker,size);

            } else if(LengthLocker == 2 && (number_locker >=30 && number_locker <= 41)){
                var xSlot = xSlot4;
                locker_data(xSlot,number_locker,size);

            } else if(LengthLocker == 6 && (number_locker >=49 && number_locker <= 60)){
                var xSlot = xSlot5;
                locker_data(xSlot,number_locker,size);

            } else if(LengthLocker == 6 && (number_locker >= 61 && number_locker <= 72)){
                var xSlot = xSlot6;
                locker_data(xSlot,number_locker,size);

            } else if(LengthLocker == 6 && (number_locker >= 73 && number_locker <= 77)){
                var xSlot = xSlot7;
                locker_data(xSlot,number_locker,size);
                
            } else {
                if (number_locker >= 1 && number_locker <= 12) {
                    var xSlot = xSlot1;
                    locker_data(xSlot,number_locker,size);

                } else if (number_locker >= 13 && number_locker <= 24) {
                    var xSlot = xSlot2;
                    locker_data(xSlot,number_locker,size);

                } else if (number_locker >= 25 && number_locker <= 36) {
                    var xSlot = xSlot3;
                    locker_data(xSlot,number_locker,size);

                } else if (number_locker >= 37 && number_locker <= 48) {
                    var xSlot = xSlot4;
                    locker_data(xSlot,number_locker,size);

                } else if (number_locker >= 49 && number_locker <= 53) {
                    var xSlot = xSlot5;
                    locker_data(xSlot,number_locker,size);

                } else if (number_locker >= 54 && number_locker <= 65) {
                    var xSlot = xSlot6;
                    locker_data(xSlot,number_locker,size);

                } else if (number_locker >= 66 && number_locker <= 77) {
                    var xSlot = xSlot7;
                    locker_data(xSlot,number_locker,size);

                } else if (number_locker >= 78 && number_locker <= 89) {
                    var xSlot = xSlot8;
                    locker_data(xSlot,number_locker,size);

                } else if (number_locker >= 90 && number_locker <= 101) {
                    var xSlot = xSlot9;
                    locker_data(xSlot,number_locker,size);

                } else if (number_locker >= 102 && number_locker <= 113) {
                    var xSlot = xSlot10;
                    locker_data(xSlot,number_locker,size);

                } else if (number_locker >= 114 && number_locker <= 125) {
                    var xSlot = xSlot11;
                    locker_data(xSlot,number_locker,size);

                } else if (number_locker >= 126 && number_locker <= 137) {
                    var xSlot = xSlot12;
                    locker_data(xSlot,number_locker,size);

                }
            }
        }
    }

    
    function ycu_32(j) {
        if (j == 0) {
            return yCU3
        } else if (j == 1) {
            return yCU4
        } else if (j == 2) {
            return yCU5
        } else if (j == 3) {
            return yCU6
        } else if (j == 4) {
            return yCU7
        }
    }

    function ycu_15(j) {
        if (j == 0) {
            return yCU3
        } else if (j == 1) {
            return yCU4
        } else if (j == 2) {
            return yCU5
        } else if (j == 3) {
            return yCU6
        } else if (j == 4) {
            return yCU7
        }
    }

    function ycu_15_xs(j) {
        if (j == 0) {
            return yCUXS3
        } else if (j == 1) {
            return yCUXS3
        } else if (j == 2) {
            return yCUXS4
        } else if (j == 3) {
            return yCUXS5
        }
    }
    
    function locker_data(xslot, number, size) {
        data_locker = {
            x : xslot,
            y : set_Yrow(number),
            h : choose_door_type(size)
        }
    }

}