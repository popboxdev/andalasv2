import QtQuick 2.4
import QtQuick.Controls 1.2
import "configQML.js" as CONFIG


Rectangle {
    id:root
    width: 1024
    height: 768
    color: 'transparent'

    signal customer_take_express_result(string str)
    signal mouth_number_result(string str)
    signal overdue_cost_result(string str)
    signal user_login_result(string str)
    signal get_version_result(string str)
    signal barcode_result(string str)
    signal store_express_result(string str)
    signal phone_number_result(string str)
    signal paid_amount_result(string str)
    signal user_info_result(string str)
    signal customer_store_express_result(string str)
    signal mouth_status_result(string str)
    signal overdue_express_list_result(string str)
    signal overdue_express_count_result(int i)
    signal load_express_list_result(string str)
    signal init_client_result(string str)
    signal courier_take_overdue_express_result(string str)
    signal customer_store_express_cost_result(string str)
    signal customer_express_cost_insert_coin_result(string str)
    signal store_customer_express_result_result(string str)
    signal send_express_list_result(string str)
    signal send_express_count_result(int i)
    signal take_send_express_result(string str)
    signal manager_mouth_count_result(int i)
    signal load_mouth_list_result(string str)
    signal mouth_list_result(string str)
    signal manager_set_mouth_result(string str)
    signal manager_open_mouth_by_id_result(string str)
    signal courier_get_user_result(string str)
    signal manager_open_all_mouth_result(string str)
    signal choose_mouth_result(string str)
    signal free_mouth_result(string str)
    signal free_mouth_by_size_result(string str)
    signal imported_express_result(string str)
    signal customer_reject_express_result(string str)
    signal reject_express_count_result(int i)
    signal take_reject_express_result(string str)
    signal reject_express_list_result(string str)
    signal reject_express_result(string str)
    signal manager_get_box_info_result(string str)
    signal ad_download_result(string str)
    signal ad_source_result(string str)
    signal ad_source_number_result(string str)
    signal delete_result(string str)
    signal product_file_result(string str)
    signal open_main_box_result(string str)
    signal start_get_express_info_result(string str)
    signal start_get_pakpobox_express_info_result(string str)
    signal start_get_customer_info_by_card_result(string str)
    signal start_payment_by_card_result(string str)
    signal start_get_cod_status_result(string str)
    signal start_get_popbox_express_info_result(string str)
    signal start_reset_partial_transaction_result(string str)
    signal start_post_capture_file_result(string str)
    signal start_get_capture_file_location_result(string str)
    signal start_get_locker_name_result(string str)
    signal start_global_payment_emoney_result(string str)
    signal start_get_sepulsa_product_result(string str)
    signal start_sepulsa_transaction_result(string str)
    signal start_get_gui_version_result(string str)
    signal start_customer_scan_qr_code_result(string str)
    signal stop_customer_scan_qr_code_result(string str)
    signal get_popshop_product_result(string str)
    signal get_query_member_popsend_result(string str)
    signal start_popsend_topup_result(string str)
    signal get_locker_data_result(string str)
    signal start_popshop_transaction_result(string str)
    signal get_popsend_button_result(string str)
    signal sepulsa_trx_check_result(string str)
    signal box_start_migrate_result(string str)
    signal get_ads_images_result(string str)
    signal start_post_subscribe_data_result(string str)
    signal start_get_file_dir_result(string str)
    signal start_get_detection_result(string str)
    signal start_create_payment_result(string str)
    signal check_trans_global_result(string str)
    signal get_sim_card_result(string str)
    signal check_addon_result(string str)
    signal book_transaction_result(string str)
    signal confirm_transaction_result(string str)
    signal callback_result(string str)
    signal create_transaction_result(string str)
    signal add_payment_result(string str)
    signal get_payment_result(string str)
    signal get_transaction_result(string str)
    signal flag_emoney_result(string str)
    signal ppob_button_result(string str)
    signal apexpress_result(string str)
    signal update_apexpress_result(string str)
    signal start_check_member_result(string str)
    // UPDATE SINCE ANDALAS V2
    signal check_awb_global_result(string str)
    signal start_checking_return_result(string str)
    signal start_checking_popsend_result(string str)
    signal start_hour_overdue_result(string str)
    signal otp_result(string str)
    signal otp_validality_result(string str)
    signal start_time_overdue_result(string str)
    signal all_express_list_result(string str)
    signal ovo_no_transaction_result(string str)
    signal start_get_locker_type_result(string str)
    signal start_internet_status_result(string str)
    signal get_parcel_data_bypin_result(string str)
    signal start_get_tvc_timer_result(int i)
    signal start_get_tvc32_timer_result(int i)
    signal start_get_file_banner_result(string str)
    signal start_open_door_id_express_result(string str)
    signal choose_mouth_for_change_size_result(string str)
    signal update_mouth_id_express_result(string str)
    signal set_value_change_size_result(string str)
    signal return_express_count_result(int i)
    signal popsafe_express_count_result(int i)
    signal popsend_express_count_result(int i)
    signal maintenance_status_result(string str)
    signal cancel_order_result(string str)
    signal start_get_language_result(string str)
    // signal popsafe_express_list_result(string str)
    signal start_capture_signature_result(string str)
    signal start_get_contactless_result(string str)




    //==================================================================================================//
    Image{
        id: base_background
        anchors.fill: parent
        source: "asset/base/background/bg.png"
        fillMode: Image.Stretch
        Background{
            logo_vis:true
        }
    }
    //==================================================================================================//

    property var reference: "none"
    property int media_idx: 0
    property bool isEmergency: (CONFIG.isEmergency=='0') ? false : true
    property var staticFile: CONFIG.staticFile
    property int screenSize: 15

    StackView {
        id: my_stack_view
        // anchors.fill: root
        anchors.fill: parent
        initialItem: select_service
//        initialItem: simcard_ppob_view

        delegate: StackViewDelegate {
            function transitionFinished(properties)
            {
                properties.exitItem.opacity = 1
            }

            pushTransition: StackViewTransition {
                PropertyAnimation {
                    target: enterItem
                    property: "opacity"
                    from: 0
                    to: 1
                }
                PropertyAnimation {
                    target: exitItem
                    property: "opacity"
                    from: 1
                    to: 0
                }
            }
        }
    }

    //Start Andalas
    // kirim_view

    Component{id:success_return
        SuccessReturn{}
    }

    Component{id:send_parcel
        SendParcel{}
    }
    Component{id:detail_popsend
        DetailPopSend{}
    }
    Component{id:detail_return
        DetailReturn{}
    }
    Component{id:detail_ondemand
        DetailOnDemand{}
    }
    Component{id:send_parcel_return
        SendParcelReturn{}
    }

    Component{id:store_parcel
        StoreParcel{}
    }
    Component{id:take_parcel
        TakeParcel{}
    }

    Component{id:select_service
        SelectService{}
    }

    Component{id: detail_parcel
        DetailStoreParcel{}
    }
    Component{id: input_phone
        InputPhoneNumber{}
    }

    Component{id: select_payment
        SelectPaymentPage{}
    }
    Component{id: select_courier
        SelectCourier{}
    }
    Component{id: open_door
        OpenDoorPage{}
    }    
    Component{id: input_pin
        InputPin{}
    }
    Component{id: detail_courier_popsend
        DetailCourierPopSend{}
    }
    Component{id: box_size
        BoxSizePage{}
    }

    Component{id: courier_login_page
        CourierLoginPage{}
    }
    Component{id: courier_home_page
        CourierHomePage{}
    }
    Component{id: detail_courier_store
        DetailCourierStore{}
    }
    Component{id: courier_store
        CourierStore{}
    }
    Component{id: survey_page
        SurveyPage{}
    }

    Component{id: courier_take
        CourierTake{}
    }

    Component{id: contact_us
        ContactUs{}
    }
    Component{id: time_out
        TimeOut{}
    }
    Component{id: input_door_page
        InputDoor{}
    }
    Component{id:courier_service_view
        CourierServicePage{}
    }
    Component{id:cabinet_page
        CabinetPage{}
    }
    Component{id:other_courier
        OtherCourier{}
    }
    Component{id:input_recepient_name
        InputCustomerName{}
    }
    Component{id:ad_page
        Advertisement{}
    }
    Component{id:under_maintenance
        UnderMaintenance{}
    }
    Component{id:under_maintenance32
        UnderMaintenance32{}
    }
    Component{id:reason_cancel
        ReasonCancel{}
    }
    Component{id:subscribe_page
        SubscribePage{}
    }
    Component{id:input_email
        InputEmail{}
    }
    Component{id:input_idcard
        InputIdCard{}
    }
    Component{id:input_passport
        InputPassport{}
    }
    Component{id:input_signature
        InputSignature{}
    }

}




