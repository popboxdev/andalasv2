import QtQuick 2.4

Rectangle {
    id: main_rectangle
    property var r_width: 439
    property var buttonColor: '#ff524f'
    // property var font_type: "SourceSansPro-Regular"
    property bool insertImage: false
    property bool modeDisable: false
    property var buttonText: ""
    property var sizeLockerText: ""
    property var show_source: ""
    property int img_width: 82
    property int img_height: 75
    property var lefttext : 20
    property var img_y: ""
    width: r_width
    height: 85
    color: (modeDisable==true) ? "#c4c4c4" : buttonColor
    FontType{
        id:fontStyle
        //Type Font ### bold , extrabold , light , book, medium, black ###
    }
    Text {
        id: button_text
        x:30
        y:6
        text: qsTr(buttonText)
        font.family: fontStyle.medium
        font.pixelSize: 60
        color: "#f1f1f1"
    }

    Text {
        id: size_locker
        x:115
        y:35
        text:qsTr(sizeLockerText)
        font.family: font_type
        font.pixelSize: 18
        color: "#f1f1f1"
    }
    Image{
        id: img_button
        x: 60
        y: img_y
        width:img_width
        height:img_height
        anchors.verticalCenter: parent.verticalCenter
        visible: (insertImage==true) ? true : false
        source:show_source
    }

    Rectangle {
        x:339
        width: 100
        height: parent.height
        color: "#9c9c9c"
    }
}
