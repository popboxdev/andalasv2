import QtQuick 2.4

Rectangle {
    id: main_rectangle
    property var r_width: 551
    property var r_height: 70
    property var buttonColor: '#f6f6f6'
    // property var font_type: "SourceSansPro-Regular"
    property int r_radius: 20
    property bool insertImage: true
    property bool modeReverse: false
    property var buttonText: ""
    // property var show_source: "asset/global/button/tap.png"
    property var show_source: ""
    property int font_size: 28
    // property int img_width: 82
    // property int img_height: 75
    property int img_width: 90
    property int img_height: 22
    property var lefttext : 20
    property var img_y: 0
    // property var img_x: 60
    // property var img_y: 824
    property var img_x: 471
    property var color_font: "#3d3d3d"
    property bool xCenter: false

    width: r_width
    height: r_height
    color: (modeReverse==true) ? "#FFFFFF" : buttonColor
    radius: r_radius
    border.width: (modeReverse==true) ? 3 : 0
    border.color: buttonColor
    FontType{
        id:fontStyle
        //Type Font ### bold , extrabold , light , book, medium, black ###
    }
    Text {
        id: button_text
        anchors.fill: parent
        text: (insertImage==true) ? qsTr("  "+ buttonText) : qsTr(buttonText)
        verticalAlignment: Text.AlignVCenter 
        anchors.leftMargin: lefttext
        font.family: fontStyle.medium
        font.pixelSize: font_size
        color: color_font
    }
    Image{
        id: img_button
        x: img_x
        y: img_y
        width:img_width
        height:img_height
        anchors.verticalCenter: parent.verticalCenter
        anchors.horizontalCenter: (xCenter== true) ? parent.horizontalCenter : undefined
        visible: (insertImage==true) ? true : false
        source:show_source
    }
}
