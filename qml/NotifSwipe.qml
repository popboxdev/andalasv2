import QtQuick 2.4
import QtQuick.Controls 1.2

Item {
    id:notif_swipe
    x:0
    y:768
    // property var font_type: "SourceSansPro-Regular"
    // property var font_type_bold: "SourceSansPro-SemiBold"
    property var asset_font: 'asset/font/'
    property var font_color:"#4a4a4a"
    property var title_text: ""
    property var body_text: ""
    property var source_img: ""
    property int title_size: 40
    property int body_size: 24
    property int titlebody_x: 50
    property int title_y: 60
    property int body_y: 180
    property int img_y: 150
    property int r_height: 520
    property int img_height: 470
    property bool title_bold : false

    width: parent.width
    height: r_height
    clip: true

    FontType{
        id:fontStyle
        //Type Font ### bold , extrabold , light , book, medium, black ###
    }

    Rectangle {
        anchors.fill: parent
        anchors.bottomMargin: -radius
        radius: 30
        color: "white"
    }

    Image{
        id:background
        y:img_y
        height: img_height
        width: parent.width
        source:source_img
    }

    Text {
        id: title
        x:titlebody_x
        y:title_y
        // font.bold: true
        font.bold: true
        font.pixelSize:title_size
        color:font_color
        font.family: fontStyle.medium
        text: qsTr(title_text)
    }
    Text {
        id: body
        x:titlebody_x
        y:body_y
        font.pixelSize:body_size
        color:font_color
        font.family: fontStyle.book
        text: qsTr(body_text)
    }
}

