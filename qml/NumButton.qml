import QtQuick 2.4
import QtQuick.Controls 1.2

Rectangle{
    id:num_button
    width: 26//79
    height: 65
    property var show_text:""
    property bool show_img: false
    property var source_img:""
    property var num_fontsize: 46
    // color: "#fbfcfc"
    color: "#ffffff"
    FontType{
        id:fontStyle
        //Type Font ### bold , extrabold , light , book, medium, black ###
    }

    Text {
        id: text_button
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter
        font.pixelSize:num_fontsize
        color:"#37474f"
        font.family: fontStyle.medium
        text: qsTr(show_text)
    }
    Image {
        id: img_button
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter
        visible: (show_img==true) ? true : false
        source: source_img
    }

    MouseArea {
        anchors.fill: num_button
        onClicked: {
            keyboard_number.letter_button_clicked(show_text)
            }
        onEntered:{
            num_button.color = "silver"
            }
        onExited:{
            num_button.color = "#ffffff"
        }
    }
}
