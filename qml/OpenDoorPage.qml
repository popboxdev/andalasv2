import QtQuick 2.4
import QtQuick.Controls 1.2

Background{
    id: detail_parcel
    property var press: '0'
    property var asset_path: 'asset/send_page/'
    property var asset_survey: 'asset/survey/'
    property var red1: "#ff524f"
    property var step_choose:''
    property var locker_type: ""
    property var locker_length: ""
    property var nomorLoker: '1' 
    property var door_type: ''
    property var opendoor_status: "default"
    property var header_title: qsTr("BUKA PINTU")
    property bool logistic: false
    property var list_locker_number: ''
    property var arr_number_locker : []
    property var loker_no : []
    property int availableClick: 2
    property var txt_time: ""
    property var popsafe_status: ""
    property var popsafe_param: undefined
    property bool changebox_state: false
    property bool batal_state: true
    property bool isPopDeposit: false
    property var survey_expressNumber : ""
    property var survey_expressId : ""
    property var express_id : ""
    property bool change_locker : false
    property bool drop_by_kirim : false

    property var id_company: ""
    property var status_courier: ""
    property var select_courier: ""
    property var phone_courier: ""
    property var drop_by_other_company: 0
    property var drop_by_courier: 0
    property var access_door: false
    property int button_type: 0
    property bool contactless: false

    show_img: 'asset/base/background/bg.png'

    Stack.onStatusChanged:{
        if(Stack.status == Stack.Activating){
            press = '0';
            timer_note.start()
            slot_handler.start_get_locker_type();
            button_access();
            // console.log("nomorLoker: " + nomorLoker.length)
            // console.log("door_type: " + door_type)
            // console.log("opendoor_status: " + opendoor_status)
            // console.log("logistic : " + logistic)
            // console.log("changebox_state : " + changebox_state)
            // console.log(" drop_by_courier: " + drop_by_courier)
            // console.log(" popsafe_status: " + popsafe_status)
            // if (popsafe_status == "lastmile" || popsafe_status == "courier_store") {
            //     slot_handler.set_drop_by_courier(drop_by_courier)
            // }
            if (access_door==true) {
                open_door_access();
            }
            
            if (nomorLoker.length > 1)  {
                for(var i = 0; i < nomorLoker.length ; i++) {
                    arr_number_locker[i] = nomorLoker[i].number
                }
                loker_no = (arr_number_locker.join(" ")).toString()
            } else if (nomorLoker.length == undefined) {
                loker_no = nomorLoker
                door_type = door_type
            }
        
            // if (popsafe_status == 'poptitip') {
            //     open_door_popsafe_direct(express_id)
            // } else {
            //     if (change_locker == false) {
            //         // store to db when finish
            //         store_to_db()
            //     } else {
            //         slot_handler.start_change_mouth_id_by_express_id(express_id)
            //     }
            // }
            if (header_title == "AMBIL"){
                cancelorder.visible = false;
            }
            if (contactless==true) {
                timer.secon = 15;
            }
        }
        if(Stack.status==Stack.Deactivating){
            timer_note.stop()
        }
    }

    FontType{
        id:fontStyle
        //Type Font ### bold , extrabold , light , book, medium, black ###
    }

    Header{
        id:header
        title_text: header_title
        text_timer : txt_time
        img_vis: false
    }

    Item{
        id: timer
        property int secon  : 45
        Timer{
            id:timer_note
            interval:1000
            repeat:true
            running:true
            triggeredOnStart:true
            onTriggered:{
                timer.secon -= 1
                txt_time = timer.secon
                if(timer.secon == 2){
                    // store_to_db()
                }
                if(timer.secon < 1){
                    slot_handler.save_survey_data_byexpress(header_title, '0', survey_expressId, survey_expressNumber)
                    timer_note.stop()
                    my_stack_view.pop(null)
                }
            }
        }
    }

    Text {
        id: txt_title
        // x:295
        anchors.horizontalCenter: parent.horizontalCenter
        y:115
        text: qsTr("Pintu Loker Telah Terbuka")
        // text: qsTr("Loker No. ") + loker_no  + qsTr(" Telah Terbuka")
        font.pixelSize:45
        // color:"#ff524f"
        color: "#ff332f"
        font.family: fontStyle.black
    }

    // Text {
    //     id:idloker1
    //     x:262
    //     y: 172
    //     text: qsTr("Silakan Ambil/Letakan Barang/Paket kamu di loker No. ") + loker_no
    //     font.pixelSize:24
    //     color:"#6b6a6c"
    //     font.family: fontStyle.book
        
    // }

    Text {
        id:txt_locker_num
        anchors.horizontalCenter: parent.horizontalCenter
        y: 165
        text: qsTr("No. ") + loker_no
        font.pixelSize:50
        color:"#272727"
        font.family: fontStyle.bold
        
    }

    Rectangle{
        id: rec_note
        anchors.horizontalCenter: parent.horizontalCenter
        y: 230
        height: 35
        width: 763
        color: "#ffc107"
        radius: width * 0.5

        Image{
            anchors.verticalCenter: parent.verticalCenter
            x: 160
            source: "asset/point_icon/lamp2.png"
        }

        Text {
            id:txt_note
            anchors.verticalCenter: parent.verticalCenter
            anchors.horizontalCenter: parent.horizontalCenter
            text: qsTr("Pastikan kamu menutup pintu loker setelah selesai")
            font.pixelSize:15
            color:"#ffffff"
            font.family: fontStyle.bold
            
        }
    }

    LockerOpen{
        id:locker_number
        x:108
        y:280
        jLock: locker_length
        door_no: nomorLoker
        doorType: door_type
        lockerType: locker_type
    }
    

    // Text{
    //     id: txt_buka
    //     x:59
    //     y:(logistic==true) ? 650 : 590
    //     visible: (logistic==true || changebox_state==true) ? false : true
    //     text: qsTr("<b>BUKA KEMBALI</b>") + " : " + availableClick + qsTr(" Kali")
    //     font.pixelSize:18
    //     font.letterSpacing:0
    //     color:"#4a4a4a"
    //     font.family: fontStyle.medium
    // }

    // Text{
    //     x:59
    //     y:658
    //     visible: (opendoor_status=="courier")? false : logistic
    //     text: qsTr("Tekan <b>AMBIL LAGI</b> untuk kembali ke halaman ambil/simpan paket.")
    //     font.pixelSize:18
    //     font.letterSpacing:0
    //     color:"#4a4a4a"
    //     font.family: fontStyle.medium
    // }

    // Text{
    //     x:59
    //     y:(logistic==true) ? 688 : 672
    //     text: qsTr("Tekan <b>BUKA KEMBALI</b> untuk membuka kembali loker.")
    //     font.pixelSize:18
    //     font.letterSpacing:0
    //     color:"#4a4a4a"
    //     font.family: fontStyle.medium
    // }

    // Text{
    //     x:59
    //     y:(logistic==true) ? 718 : 695
    //     text: qsTr("Tekan <b>SELESAI</b> untuk menyelesaikan proses.")
    //     font.pixelSize:18
    //     font.letterSpacing:0
    //     color:"#4a4a4a"
    //     font.family: fontStyle.medium
    // }

    // Text{
    //     x:59
    //     y:715
    //     text: qsTr("Tekan <b>GANTI LOKER</b> untuk mengubah ukuran loker.")
    //     font.pixelSize:18
    //     font.letterSpacing:0
    //     color:"#4a4a4a"
    //     font.family: fontStyle.medium
    //     visible: (changebox_state==true)? true : false
    // }

    //////////// BUTTON UTAMA ////////////

     NotifButton{
        id: cancelorder
        x:268
        y:647
        buttonText:qsTr("BATAL") // Batal simpan
        modeReverse:true
        buttonColor: "#c4c4c4"
        r_radius:2
        visible: false
        enabled: false
        MouseArea{
            anchors.fill: parent
            onClicked: {
                console.log("exprss_id_locker_batal: " + express_id)
                my_stack_view.push(reason_cancel, {express_id: express_id, locker_length: locker_length, nomorLoker: nomorLoker,door_type: door_type, locker_type: locker_type, survey_expressNumber: survey_expressNumber, survey_expressId: survey_expressId, logistic: logistic, changebox_state: changebox_state, opendoor_status: opendoor_status, batal_state: batal_state})
            }
            onEntered:{
                cancelorder.modeReverse = false
            }
            onExited:{
                cancelorder.modeReverse = true
            }
        }
    }


    NotifButton{
        id: changebox
        x:152
        y:558
        enabled: false
        buttonText:qsTr("GANTI LOKER")
        modeReverse:true
        // buttonColor: red1
        buttonColor: "#c4c4c4"
        r_radius:2
        visible: false
        MouseArea{
            anchors.fill: parent
            onClicked: {
                // my_stack_view.pop();
                // console.log("express_id: " + survey_expressId)
                // my_stack_view.push(box_size, {change_locker: true, express_id: survey_expressId, opendoor_status: opendoor_status})
                slot_handler.set_value_change_size(change_locker, survey_expressId, opendoor_status)
                
                console.log(" popsafe_status: " + popsafe_status + "drop_by_other_company: " + drop_by_other_company)
                my_stack_view.pop()
                // if (popsafe_status == "lastmile") {
                //     if (drop_by_other_company == 1) {
                //         my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 8) return true })); // back to select size page
                //     } else {
                //         my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 7) return true })); // back to select size page
                //     }

                // } else {
                //     my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 6) return true })); // back to select size page
                // }
            }
            onEntered:{
                changebox.modeReverse = false
            }
            onExited:{
                changebox.modeReverse = true
            }
        }
    }

    NotifButton{
        id: sendagain
        x:397
        y:558
        buttonText:qsTr("KIRIM LAGI")
        modeReverse:true
        buttonColor: red1
        // buttonColor: "#c4c4c4"
        enabled: true
        r_radius:2
        visible: false
        MouseArea{
            anchors.fill: parent
            onClicked: {
                
                my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 1)
                    return item.id_company = id_company,
                    item.status_courier = status_courier,
                    item.drop_by_courier = drop_by_courier,
                    item.drop_by_other_company = drop_by_other_company,
                    item.kirimLagi = true,
                    item.param_choice = "courier_send_awb",
                    item.flagPopUp = true,
                    item.courier_selected = select_courier,
                    item.phone_courier = phone_courier,
                    item.kirimLagi = true,
                    item.show_img = show_img
                }))
            }
            onEntered:{
                sendagain.modeReverse = false
            }
            onExited:{
                sendagain.modeReverse = true
            }
        }
    }

    NotifButton{
        id: openagain
        x:513
        y:647
        buttonText:qsTr("BUKA KEMBALI")
        modeReverse:true
        buttonColor: red1
        r_radius:2
        visible: false
        MouseArea{
            anchors.fill: parent
            onClicked: {
                if(availableClick>0){
                    availableClick -=1;
                    
                    open_door_again(express_id) // disable for checking the function
                    // slot_handler.start_open_mouth_again(); //Hanya 1 yg terbuka. jika kondisi pintu banyak
                    // console.log("OKE:  BUKA AGAIN" + availableClick)
                    if(availableClick==0) {
                        openagain.buttonColor= "#c4c4c4";
                        openagain.enabled = false;
                        availableClick = 0;
                    }
                }
            }
            onEntered:{
                openagain.modeReverse = false
            }
            onExited:{
                openagain.modeReverse = true
            }
        }
    }

    Rectangle {
        id:txt_openagain
        x: 722
        y: 653
        width:34
        height:34
        color: "#ff524f"
        radius: width*0.5
        visible: false
        Text {
            id:sumsizeL
            anchors.verticalCenter: parent.verticalCenter
            anchors.horizontalCenter: parent.horizontalCenter
            color: "#ffffff"
            text: availableClick
            font.pixelSize:22
            font.family: fontStyle.book
        }
    }

    NotifButton{
        id: done
        x:642
        y:558
        buttonText:qsTr("SELESAI")
        buttonColor: red1
        r_radius:2
        modeReverse:false
        visible: false
        MouseArea{
            anchors.fill: parent
            onClicked: {
                dimm_display.visible = true;

                if(opendoor_status=="default") {
                    if(logistic==true){
                        my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 2) return true }))
                    }else{
                        // my_stack_view.push(survey_page)
                        my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
                        // show_survey.start();
                    }
                }else if(opendoor_status=="return") {
                    show_sukses_return.start();
                }else if(opendoor_status=="courier") {
                    show_logistic.start();
                }
            }
            onEntered:{
                done.modeReverse = true
            }
            onExited:{
                done.modeReverse = false
            }
        }
    }
    NotifButton{
        id: takeagain
        x:153
        y:558
        buttonText:qsTr("AMBIL LAGI")
        modeReverse:true
        buttonColor: red1
        r_radius:2
        visible: false
        MouseArea{
            anchors.fill: parent
            onClicked: {
                my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 3) return true }));
            }
            onEntered:{
                takeagain.modeReverse = false
            }
            onExited:{
                takeagain.modeReverse = true
            }
        }
    }
    //////////// BUTTON UTAMA ////////////

    DimmDisplay{
        id:dimm_display
        visible:false
    }

    NotifSwipe{
        id:notif_survey
        // source_img: asset_path + "background/success_store.png"
        img_y:50
        titlebody_x:50
        title_y:60
        body_y:130
        title_text:qsTr("Terima Kasih Telah Menggunakan PopBox")
        body_text:qsTr("Bantu kami untuk terus memberikan layanan terbaik, pilih tingkat <br>kepuasan kamu")

        Image{
            height:145
            width:145
            x:161
            y:215
            source:asset_survey + "kesal.png"
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    slot_handler.save_survey_data_byexpress(header_title, '1', survey_expressId, survey_expressNumber)
                    my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
                }
            }
        }

        Image{
            height:145
            width:145
            x:440
            y:215
            source:asset_survey + "biasa.png"
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    slot_handler.save_survey_data_byexpress(header_title, '2', survey_expressId, survey_expressNumber)
                    my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
                }
            }
        }

        Image{
            height:145
            width:145
            x:718
            y:215
            source:asset_survey + "suka.png"
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    slot_handler.save_survey_data_byexpress(header_title, '3', survey_expressId, survey_expressNumber)
                    my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
                }
            }
        }

        Text {
            id: txt_suka
            x:172
            y:360
            text: qsTr("Tidak Suka")
            font.pixelSize:28
            color:"#4a4a4a"
            font.family: fontStyle.book
        }

        Text {
            id: txt_biasa
            x:450
            y:360
            text: qsTr("Biasa Saja")
            font.pixelSize:28
            color:"#4a4a4a"
            font.family: fontStyle.book
        }

        Text {
            id: txt_kesal
            x:738
            y:360
            text: qsTr("Senang")
            font.pixelSize:28
            color:"#4a4a4a"
            font.family: fontStyle.book
        }

        // NotifButton{
        //     id:oke_logistic
        //     x:50
        //     y:351
        //     r_radius:0
        //     r_width:260
        //     modeReverse:true
        //     buttonText:"SIMPAN PAKET LAGI"
        //     MouseArea{
        //         anchors.fill: parent
        //         onClicked: {
        //             // my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
        //             dimm_display.visible = false;
        //             hide_logistic.start();
        //             my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 3) return true }))
        //             // my_stack_view.push(survey_page);
        //         }
        //         onEntered:{
        //             oke_logistic.modeReverse = false
        //         }
        //         onExited:{
        //             oke_logistic.modeReverse = true
        //         }
        //     }
    }

    NotifSwipe{
        id:notif_logistic
        source_img: asset_path + "background/success_store.png"
        img_y:50
        titlebody_x:50
        title_y:60
        body_y:180
        title_text:qsTr("Terima Kasih Telah Menggunakan <br>Layanan PopBox")
        body_text:qsTr("WhatsApp notifikasi telah dikirimkan <br>ke no. pelanggan.")

        NotifButton{
            id:oke_logistic
            x:50
            y:351
            r_radius:0
            r_width:260
            modeReverse:true
            buttonText:qsTr("SIMPAN PAKET LAGI")
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    // my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
                    dimm_display.visible = false;
                    hide_logistic.start();
                    my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 3) return true }))
                    // my_stack_view.push(survey_page);
                }
                onEntered:{
                    oke_logistic.modeReverse = false
                }
                onExited:{
                    oke_logistic.modeReverse = true
                }
            }
        }

        NotifButton{
            id:selesai_logistic
            x:335
            y:351
            r_radius:0
            modeReverse:false
            buttonText:qsTr("SELESAI")
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    // my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
                    dimm_display.visible = false;
                    hide_logistic.start();
                    my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 2) return true }))
                    // my_stack_view.push(survey_page);
                }
                onEntered:{
                    selesai_logistic.modeReverse = true
                }
                onExited:{
                    selesai_logistic.modeReverse = false
                }
            }
        }
    }

    NotifSwipe{
        id:notif_sukses_return
        source_img: asset_path + "background/success_store.png"
        img_y:50
        titlebody_x:50
        title_y:60
        body_y:180
        title_text:qsTr("Barang Telah Kami Terima")
        body_text:qsTr("Hai Poppers, pengembalian barang kamu telah <br>kami terima dan akan kami kirimkan ke <br>warehouse toko online tempat kamu belanja")

        NotifButton{
            id:oke_sukses
            x:50
            y:351
            r_radius:0
            modeReverse:false
            buttonText:qsTr("OKE")
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    // my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
                    // dimm_display.visible = false;
                    hide_sukses_return.start();
                    // my_stack_view.push(survey_page);
                    // show_survey.start()
                    my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
                }
                onEntered:{
                    oke_sukses.modeReverse = true
                }
                onExited:{
                    oke_sukses.modeReverse = false
                }
            }
        }
    }

    NotifSwipe{
        id:notif_selesai
        source_img: asset_path + "return/isiform.png"
        img_y:50
        titlebody_x:50
        title_y:60
        body_y:180
        title_text:qsTr("Terima Kasih Kamu Telah <br>Menggunakan PopBox")
        body_text:qsTr("Bantu kami untuk terus memberikan layanan <br>terbaik dengan mengisi form kepuasan <br>pelanggan.")
        NotifButton{
            id:ok_sls
            x:50
            y:304
            r_radius:0
            modeReverse:true
            buttonText:qsTr("SELESAI")
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
                }
                onEntered:{
                    ok_sls.modeReverse = false
                }
                onExited:{
                    ok_sls.modeReverse = true
                }
            }
        }
        NotifButton{
            id:btn_form
            x:300
            y:304
            r_radius:0
            buttonText:qsTr("ISI FORM")
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    hide_selesai.start();
                    dimm_display.visible=false;
                    my_stack_view.push(survey_page)
                }
                onEntered:{
                    btn_form.modeReverse = false
                }
                onExited:{
                    btn_form.modeReverse = true
                }

            }
        }
    }
    NotifSwipe{
        id:door_fail
        img_y:50
        source_img: "asset/send_page/background/unknown_popsafeCopy.PNG"
        title_text:qsTr("Oops pintu gagal dibuka")
        body_text:qsTr("Mohon ulangi kembali proses")
        NotifButton{
            id:btn_door_fail
            x:50
            y:337
            r_radius:2
            buttonText:qsTr("BATAL")
            modeReverse:false
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    hide_doorfail.start();
                    dimm_display.visible=false
                    press=0;
                    if (opendoor_status == "courier"){ // popsafe_status == "lastmile" || 
                        my_stack_view.pop(my_stack_view.find(function(item){return item.Stack.index==2}));
                    }
                    else{
                        my_stack_view.pop(null)
                    }
                }
                onEntered:{
                    btn_door_fail.modeReverse = true
                }
                onExited:{
                    btn_door_fail.modeReverse = false
                }
            }
        }

        Image{
            x:939
            y:60
            height:33
            width:33
            source:"asset/global/button/close.png"
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    hide_doorfail.start();
                    dimm_display.visible=false
                }
            }
        }
    }

    NumberAnimation {
        id:hide_logistic
        targets: notif_logistic
        properties: "y"
        from:248
        to:768
        duration: 500
        easing.type: Easing.InOutBack
    }
    NumberAnimation {
        id:show_logistic
        targets: notif_logistic
        properties: "y"
        from:768
        to:248
        duration: 500
        easing.type: Easing.InOutBack
    }

    NumberAnimation {
        id:hide_sukses_return
        targets: notif_sukses_return
        properties: "y"
        from:248
        to:768
        duration: 500
        easing.type: Easing.InOutBack
    }
    NumberAnimation {
        id:show_sukses_return
        targets: notif_sukses_return
        properties: "y"
        from:768
        to:248
        duration: 500
        easing.type: Easing.InOutBack
    }

    NumberAnimation {
        id:hide_selesai
        targets: notif_selesai
        properties: "y"
        from:248
        to:768
        duration: 500
        easing.type: Easing.InOutBack
    }
    NumberAnimation {
        id:show_selesai
        targets: notif_selesai
        properties: "y"
        from:768
        to:248
        duration: 500
        easing.type: Easing.InOutBack
    }

    NumberAnimation {
        id:hide_survey
        targets: notif_survey
        properties: "y"
        from:248
        to:768
        duration: 500
        easing.type: Easing.InOutBack
    }
    
    NumberAnimation {
        id:show_survey
        targets: notif_survey
        properties: "y"
        from:768
        to:248
        duration: 500
        easing.type: Easing.InOutBack
    }

    NumberAnimation {
        id:hide_doorfail
        targets: door_fail
        properties: "y"
        from:248
        to:768
        duration: 500
        easing.type: Easing.InOutBack
    }
    
    NumberAnimation {
        id:show_doorfail
        targets: door_fail
        properties: "y"
        from:768
        to:248
        duration: 500
        easing.type: Easing.InOutBack
    }



     Component.onCompleted: {
        root.start_get_locker_type_result.connect(show_locker_type)
        root.store_express_result.connect(store_lastmile_result)
        root.store_customer_express_result_result.connect(response_store_reject)
        root.start_open_door_id_express_result.connect(open_door_result)
        root.update_mouth_id_express_result.connect(open_door_after_change_size)
    }

    Component.onDestruction: {
        root.start_get_locker_type_result.disconnect(show_locker_type)
        root.store_express_result.disconnect(store_lastmile_result)
        root.store_customer_express_result_result.disconnect(response_store_reject)
        root.start_open_door_id_express_result.disconnect(open_door_result)
        root.update_mouth_id_express_result.disconnect(open_door_after_change_size)
    }

    
    function store_to_db(argument) {
        
        console.log("store "+ popsafe_status +" to db on process ...: ")
        
        if (popsafe_status == "lastmile" || popsafe_status == "courier_store") {
            slot_handler.start_store_express();
        } else if (popsafe_status == "return") {
            slot_handler.start_store_customer_reject_for_electronic_commerce()
        } else if (popsafe_status == "ondemand") {
            slot_handler.start_store_customer_express()
        } else if (popsafe_status == "popsend") {
            slot_handler.start_store_customer_express();
        } else if (popsafe_status=="app") {
            if(isPopDeposit){
                if (popsafe_param != undefined){
                    console.log('masuk popsafe direct')
                    console.log('popsafe_param', popsafe_param)
                    slot_handler.store_direct_popsafe(popsafe_param)
                } else {
                    console.log('masuk popsafe app')
                    slot_handler.start_deposit_express()
                }
            } else {
                console.log("POPSAFE STATUS : " , popsafe_param)
            }
        }
    }

    function show_locker_type(text) {
        var obj = JSON.parse(text)
        var result = obj.locker_type
        locker_type = result['type']
        locker_length = result['length']
        if (locker_type==null){
            locker_type = "15_TWIN"
        }else if(locker_length==null || locker_length<=0 || locker_length>6){
            locker_length = 2
        }
        console.log("LT : " + locker_length + " nomerLoker : " + nomorLoker + " doorType : "+ door_type + " lockerType : "+ locker_type)
        locker_number.__construct()
        
    }

    function store_lastmile_result(response) { 
        console.log(" response_lastmile_or_courier_drop: " + response)
        var result = JSON.parse(response)
        if (result.isSuccess == "true") {
            survey_expressNumber = result.expressNumber
            survey_expressId = result.id_express
            express_id = result.id_express
            if (express_id != "") {
                if(popsafe_status == "lastmile" || popsafe_status== "app"){
                    console.log("DATA LASTMILE OR APP : SAVED")
                    // open door by data exist
                    slot_handler.start_open_door_by_express_id(survey_expressId)
                }else if(popsafe_status == "courier_store"){
                    console.log("DATA COURIER STORE : SAVED")
                    // open door by data exist
                    slot_handler.start_open_door_by_express_id(survey_expressId)
                }
            } else {
                dimm_display.visible = true
                show_doorfail.start()
            }

        }else{
            dimm_display.visible = true
            show_doorfail.start()
        }
    }
    
    function open_door_result(argument) {
        var result = JSON.parse(argument)
        console.log("result_open_door" + argument)
        if (result.isSuccess == "true") {
            console.log('a')
            cancelorder.enabled = true;
            cancelorder.buttonColor =  red1;
            // if(header_title=="AMBIL"){
            //     timer.secon = 5;
            // }
        } else {
            dimm_display.visible = true
            show_doorfail.start()
        }  
    }

    function response_store_reject(response) {
        console.log(" response_store_reject: " + response)
        var result = JSON.parse(response)
        if (result.isSuccess == "true"){
            survey_expressNumber = result.expressNumber
            survey_expressId = result.id_express
            express_id = result.id_express
            if (result.id_express != "") {
                if(popsafe_status == "return"){
                    // my_stack_view.push(open_door,{opendoor_status:popsafe_status, nomorLoker: locker_number, header_title: "RETURN", door_type: door_type})
                    
                    slot_handler.start_open_door_by_express_id(survey_expressId)
                }else if(popsafe_status == "popsend"){
                    // my_stack_view.push(open_door,{nomorLoker: locker_number, header_title: "POPSEND", door_type: door_type})
                    
                    slot_handler.start_open_door_by_express_id(survey_expressId)
                }else{
                    console.log("DATA response_store_reject : UNDEFINED")
                }

            } else {
                dimm_display.visible = true
                show_doorfail.start()     
            }
        } else {
            dimm_display.visible = true
            show_doorfail.start()
        }
    }

    function access_button() {    
        if(opendoor_status=="default") {
            // my_stack_view.push(survey_page)
            show_survey.start();
        }else if(opendoor_status=="return") {
            show_sukses_return.start();
        }else if(opendoor_status=="courier") {
            show_logistic.start();
        }
    }

    function open_door_after_change_size(response) {
        var result = JSON.parse(response)
        if (result.isSuccess == "true") {
            slot_handler.start_open_door_by_express_id(result.id_express)
        } else {
            dimm_display.visible = true
            show_doorfail.start()
        }
    }
    
    function open_door_popsafe_direct(express_id) {
        slot_handler.start_open_door_by_express_id(express_id)        
    }

    function open_door_again(express_id) {
        if ( express_id != '' ) {
            slot_handler.start_open_mouth_again_by_id(express_id);
        } else {
            console.log('express_id_is' + express_id)
        }
    }
    function open_door_access(){
        
        if (access_door==false) {
            if (popsafe_status == "lastmile" || popsafe_status == "courier_store") {
                slot_handler.set_drop_by_courier(drop_by_courier)
            }
        }

        if (popsafe_status == 'poptitip') {
            open_door_popsafe_direct(express_id)
        } else {
            if (change_locker == false) {
                // store to db when finish
                store_to_db()
            } else {
                slot_handler.start_change_mouth_id_by_express_id(express_id)
            }
        }
    }

    //BUTTON FUNCTION//

    function button_type_satu() { // BUKA KEMBALI - SELESAI
        txt_openagain.visible = true
        txt_openagain.x = 477
        txt_openagain.y = 563
        openagain.visible = true
        openagain.x = 265
        openagain.y = 558
        done.visible = true
        done.x = 526
        done.y = 558
    }

    function button_type_dua() { // BUKA KEMBALI - AMBIL LAGI - SELESAI //Biasanya dari halaman ambil login kurir
        txt_openagain.visible = true
        txt_openagain.x = 355
        txt_openagain.y = 563
        openagain.visible = true
        openagain.x = 146
        openagain.y = 558
        takeagain.visible = true
        takeagain.x = 404
        takeagain.y = 558
        done.visible = true
        done.x = 649
        done.y = 558
    }
    
    function button_type_tiga() { // BATAL - BUKA KEMBALI - GANTI LOKER - SELESAI
        cancelorder.visible = true
        cancelorder.x = 23
        cancelorder.y = 558
        txt_openagain.visible = true
        txt_openagain.x = 477
        txt_openagain.y = 563
        openagain.visible = true
        openagain.x = 268
        openagain.y = 558
        changebox.visible = true
        changebox.x = 526
        changebox.y = 558
        done.visible = true
        done.x = 771
        done.y = 558
    }

    function button_type_empat() { // GANTI LOCKER - KIRIM LAGI - SELESAI - BATAL - BUKA KEMBALI
        changebox.visible = true
        changebox.x = 152
        changebox.y = 558
        sendagain.visible = true
        sendagain.x = 397
        sendagain.y = 558
        done.visible = true
        done.x = 642
        done.y = 558
        cancelorder.visible = true
        cancelorder.x = 268
        cancelorder.y = 647
        txt_openagain.visible = true
        txt_openagain.x = 722
        txt_openagain.y = 653
        openagain.visible = true
        openagain.x = 513
        openagain.y = 647
    }

    //BUTTON FUNCTION//

    function button_access() {

        if(button_type==1){
            button_type_satu();
        }else if(button_type==2) {
            button_type_dua();
        }else if(button_type==3) {
            button_type_tiga();
        }else if(button_type==4) {
            button_type_empat();
        }else{
            button_type_satu();
        }
        
    }

}
