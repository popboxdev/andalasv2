import QtQuick 2.4
import QtQuick.Controls 1.2

Background{
    id: courier_store
    property var press: '0'
    property var asset_global: 'asset/global/'
    property var asset_path: 'asset/send_page/'
    property var path_background: 'asset/send_page/background/'
    property var param_choice: ""
    property var status: ""
    show_img:""
    img_vis:true
    property int character:26
    property var ch:1  
    property var title:""
    property var id_company:""
    property var param_parcel: ""
    property var no_parcel: ""
    property var courier_name: ''
    property var select_courier:''
    property var count:0
    property var customer_phone:''
    property var lastmile_type:''
    property var other_company_name: ''
    property var txt_time: ""
    property var drop_by_courier: 0
    property var drop_by_other_company: 1
    

    Stack.onStatusChanged:{
        if(Stack.status == Stack.Activating){
            press = '0';
            timer.secon = 90;
            timer_note.restart();
            show_key.start();
            console.log('drop_by_courier =>' + drop_by_courier)
        }
        if(Stack.status==Stack.Deactivating){
            timer_note.stop();
        }
    }

    FontType{
        id:fontStyle
        //Type Font ### bold , extrabold , light , book, medium, black ###
    }

    Header{
        id:header
        title_text: title
        text_timer : txt_time
    }

    Item{
        id: timer
        property int secon  : 90
        Timer{
            id:timer_note
            interval:1000
            repeat:true
            running:true
            triggeredOnStart:true
            onTriggered:{
                timer.secon -= 1
                txt_time = timer.secon
                if(timer.secon < 1){
                    timer_note.stop()
                    my_stack_view.push(time_out)
                }
            }
        }
    }
    
    Text {
        id: krm
        x:50
        y:145
        text: qsTr("Nama Perusahaan Kurir Logistik")
        font.pixelSize:40
        color:"#414042"
        font.family: fontStyle.medium
    }

    Image{
        id: img_bg
        width:359
        height:293
        x:50
        y:463
        source:path_background+"bglogo.png"
    }

    Text {
        id:a
        y:325
        x:51
        text: qsTr("Pastikan nama perusahaan logistik sudah benar")
        font.pixelSize:22
        color:"#3a3a3a"
        font.family: fontStyle.book
    }

    NotifButton{
        id:btn_lanjut
        x:690
        y:230
        buttonText:qsTr("LANJUT")
        buttonColor: "#8c8c8c"
        r_radius:0
        modeReverse:false
        MouseArea{
            anchors.fill: parent
            onClicked: {
                if(logistic_name.show_text != ""){
                    console.log("nama perusahaan: " + logistic_name.show_text)

                    slot_handler.set_other_company_name(logistic_name.show_text)
                    
                    console.log("drop_by_other_company: " + drop_by_other_company)
                    
                    my_stack_view.push(input_phone, {no_parcel:no_parcel, select_courier:logistic_name.show_text, status: "courier", title: qsTr("KURIR"), title_body: qsTr("Kamu (Kurir)"), id_company: id_company, customer_phone: customer_phone, lastmile_type: lastmile_type, other_company_name: logistic_name.show_text, status_courier: "others", drop_by_courier: drop_by_courier, drop_by_other_company: drop_by_other_company}) 
                }
            }
            onEntered:{
                btn_lanjut.modeReverse = true
            }
            onExited:{
                btn_lanjut.modeReverse = false
            }
        }
    }

    Rectangle{
        x:50
        y:230
        width: 620
        height: 80
        color: "transparent"
        InputText{
            id:logistic_name
            x:2
            y:2
            borderColor : "#8c8c8c"
            // password: false
            // show_image:"img/apservice/courier/user_name.png"
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    if(press==0){
                        // show_key.start();
                        ch=1
                        logistic_name.bool = true
                        press=1
                        // courier_login_psw.bool = false
                    }
                }
            }
        }
    }

    Image{
        id: del_img
        x:613
        y:252
        width:36
        height:36
        visible:false
        source:asset_global + "button/delete.png"
        MouseArea {
            anchors.fill: parent
            onClicked: {
                logistic_name.show_text = "";
                count=0;
                touch_keyboard.input_text("")
            }
        }
    }


    KeyBoardAlphanumeric{
        id:touch_keyboard
        property var validate_c
        closeButton: false
        Component.onCompleted: {
            touch_keyboard.letter_button_clicked.connect(input_text)
            // touch_keyboard.function_button_clicked.connect(on_function_button_clicked)
        }

        function input_text(str){
            if(ch==1){
                if (str == "" && logistic_name.show_text.length > 0){
                    logistic_name.show_text.length--
                    logistic_name.show_text=logistic_name.show_text.substring(0,logistic_name.show_text.length-1)
                    count--;                 
                }
                if(str == "OKE" || str == "OK"){
                    str = "";
                    if(logistic_name.show_text != ""){
                        my_stack_view.push(input_phone, {no_parcel:no_parcel, select_courier:logistic_name, status: "courier", title: "KURIR", title_body: "Kamu (Kurir)", id_company: id_company, status_courier: "others"}) 
                    }
                }
                if (str != "" && logistic_name.show_text.length< character){
                    logistic_name.show_text.length++
                    count++;
                }
                if (logistic_name.show_text.length>=character){
                    str=""
                    logistic_name.show_text.length=character
                }else{
                    logistic_name.show_text += str
                }
                if(count>0){
                    btn_lanjut.buttonColor = "#ff524f"
                    logistic_name.borderColor = "#ff7e7e"
                    del_img.visible=true
                } else{
                    btn_lanjut.buttonColor = "#c4c4c4"
                    logistic_name.borderColor = "#8c8c8c"
                    del_img.visible=false
                }

            }
        }
    }

    NumberAnimation {
        id:hide_key
        targets: touch_keyboard
        properties: "y"
        from:368
        to:768
        duration: 500
        easing.type: Easing.InOutBack
    }
    NumberAnimation {
        id:show_key
        targets: touch_keyboard
        properties: "y"
        from:768
        to:368
        duration: 500
        easing.type: Easing.InOutBack
    }

    DimmDisplay{
        id:dimm_display
        visible:false
    }

    Component.onCompleted: {

    }

    Component.onDestruction: {

    }

}
