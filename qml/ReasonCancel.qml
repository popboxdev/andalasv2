import QtQuick 2.4
import QtQuick.Controls 1.2
import QtQuick 2.4
import QtQuick.Controls 1.2

Background{
    id: reason_cancel
    property var path_background: 'asset/send_page/background/'
    property var count:0
    property var ch:1  
    property int character:45
    show_img: ""
    img_vis: true
    property var txt_time: ""
    property bool ketik : false
    property var express_id : ""
    property var reasontext : ""
    property var locker_length
    property var nomorLoker
    property var door_type
    property var locker_type
    property var survey_expressNumber
    property var survey_expressId
    property bool changebox_state: false
    property bool change_locker: false
    property bool batal_state: true
    property bool logistic: false
    property bool batal_enabled: false
    property var opendoor_status: ""

    Stack.onStatusChanged:{
        if(Stack.status == Stack.Activating){
            timer.secon = 90
            timer_note.restart()
        }
        if(Stack.status==Stack.Deactivating){
            timer_note.stop()
        }
    }

    FontType{
        id:fontStyle
        //Type Font ### bold , extrabold , light , book, medium, black ###
    }

    Header{
        id:header
        title_text:qsTr("Alasan Pembatalan")
        text_timer : txt_time
        img_vis:false
    }

    Item{
        id: timer
        property int secon  : 90
        Timer{
            id:timer_note
            interval:1000
            repeat:true
            running:true
            triggeredOnStart:true
            onTriggered:{
                timer.secon -= 1
                txt_time = timer.secon
                if(timer.secon < 1){
                    timer_note.stop()
                    reasontext = "timeout"
                    slot_handler.start_cancel_express_by_id(express_id, reasontext)
                    my_stack_view.push(time_out)
                }
            }
        }
    }
    NotifButtonLong{
        id:reason1
        x:50
        y:115
        buttonText:qsTr("Barang tidak cukup masuk ke loker")
        r_width: 915
        r_height: 79
        r_radius:2
        MouseArea{
            anchors.fill: parent
            onClicked: {
                reasontext = "Barang tidak cukup masuk ke loker"
                ok_select.buttonColor = "#ff524f"
                ok_select.enabled = true
                reason1.modeReverse=true
                reason1.buttonColor="#ff524f"
                reason3.modeReverse=false
                reason3.buttonColor="#f6f6f6"
                reason2.modeReverse=false
                reason2.buttonColor="#f6f6f6"
                reason3.color_font="#3d3d3d"
                reason_text.visible = false
                reason_text_background.visible = true
                reason_text_background.enabled = false
                if (ketik == true){
                    hide_animation("keyboard")
                }
                ketik = false
            }
        }
    }

    NotifButtonLong{
        id:reason2
        x:50
        y:205
        buttonText:qsTr("Pintu loker tidak terbuka")
        r_width: 915
        r_height: 79
        r_radius:2
        MouseArea{
            anchors.fill: parent
            onClicked: {
                reasontext = "Pintu loker tidak terbuka"
                ok_select.buttonColor = "#ff524f"
                ok_select.enabled = true
                reason2.modeReverse=true
                reason2.buttonColor="#ff524f"
                reason1.modeReverse=false
                reason1.buttonColor="#f6f6f6"
                reason3.modeReverse=false
                reason3.buttonColor="#f6f6f6"
                reason3.color_font="#3d3d3d"
                reason_text.visible = false
                reason_text_background.visible = true
                reason_text_background.enabled = false
                if (ketik == true){
                    hide_animation("keyboard")
                }
                ketik = false
            }
        }
    }

    NotifButtonLong{
        id:reason3
        x:50
        y:296
        // buttonText:qsTr("Masukkan Alasan Lainnya")
        r_width: 915
        r_height: 79
        r_radius:2
        MouseArea{
            anchors.fill: parent
            onClicked: {
                ok_select.buttonColor = "#c4c4c4"
                ok_select.enabled = false
                reason3.modeReverse=true
                reason3.buttonColor="#c4c4c4"
                reason1.modeReverse=false
                reason1.buttonColor="#f6f6f6"
                reason2.modeReverse=false
                reason2.buttonColor="#f6f6f6"
                reason_text_background.enabled = true
            }
        }
        Text {
            id: reason_text_background
            x:30
            y:20
            text: qsTr("Masukkan alasan lainnya")
            verticalAlignment: Text.AlignVCenter 
            anchors.leftMargin: 20
            font.family: fontStyle.medium
            font.pixelSize: 28
            enabled:false
            color: "#dddddd"
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    ketik = true
                    reason_text_background.visible = false
                    reason_text.visible = true
                    show_key.start();
                    ch=1
                }
            }
        }
    }
    NotifButton{
        id:ok_select
        x:744
        y:395
        buttonText:qsTr("LANJUT")
        r_radius:2
        buttonColor: "#c4c4c4"
        enabled:false
        modeReverse:false
        MouseArea{
            anchors.fill: parent
            onClicked: {
                // console.log("VALUE REASON: "+reasontext)
                if(ketik==true) reasontext = reason_text.show_text
                slot_handler.start_cancel_express_by_id(express_id, reasontext)
            }
            onEntered:{
                ok_select.modeReverse = true
            }
            onExited:{
                ok_select.modeReverse = false
            }
        }
    }
    NotifButton{
        id:cancel_select
        x:484
        y:395
        buttonText:qsTr("KEMBALI")
        r_radius:2
        buttonColor: "#ff524f"
        enabled:true
        modeReverse:true
        MouseArea{
            anchors.fill: parent
            onClicked: {
                // my_stack_view.pop({reason_cancel_status:"Testing"})
                my_stack_view.push(open_door, {express_id: express_id, locker_length: locker_length, nomorLoker: nomorLoker,door_type: door_type, locker_type: locker_type, access_door: false, survey_expressNumber: survey_expressNumber, survey_expressId: survey_expressId, logistic: logistic, changebox_state: changebox_state, opendoor_status: opendoor_status, button_type:1})
            }
            onEntered:{
                cancel_select.modeReverse = true
            }
            onExited:{
                cancel_select.modeReverse = false
            }
        }
    }

    InputText{
        id:reason_text
        x:60
        y:296
        borderColor : "transparent"
        color: "transparent"
        text_width: 860
        visible: false
        MouseArea {
            anchors.fill: parent
            onClicked: {
                ketik = true
                show_key.start();
                ch=1
                reason_text.bool = true
                
            }
        }
    }

    KeyBoardAlphanumeric{
        id:touch_keyboard
        button_ok: qsTr("SPASI")
        property var validate_c
        page_calling_keyboard: "reason_cancel"
        Component.onCompleted: {
            touch_keyboard.letter_button_clicked.connect(input_text)
        }

        function input_text(str){
            if(ch==1){
                if (str == "" && reason_text.show_text.length > 0){
                    reason_text.show_text.length--
                    reason_text.show_text=reason_text.show_text.substring(0,reason_text.show_text.length-1)
                    count--;                 
                }
                if(str == "OKE" || str == "OK"){
                    str = "";
                    if(reason_text.show_text != ""){
                        slot_handler.start_get_internet_status()
                    }
                }
                if(str == "SPASI" || str == "SPACE"){
                    str = " ";
                }
                if (str != "" && reason_text.show_text.length< character){
                    reason_text.show_text.length++
                    count++;
                }
                if (reason_text.show_text.length>=character){
                    str=""
                    reason_text.show_text.length=character
                }else{
                    reason_text.show_text += str
                }
                if(count>0){
                    ok_select.buttonColor = "#ff524f"
                    ok_select.enabled = true
                    reason3.modeReverse=true
                    reason3.buttonColor="#ff524f"
                } else{
                    ok_select.buttonColor = "#c4c4c4"
                    ok_select.enabled = false
                    reason3.modeReverse=true
                    reason3.buttonColor="#c4c4c4"
                    
                }

            }
        }
    }
    DimmDisplay{
        id:dimm_display
        visible:false
    }
    NotifSwipe{
        id: notif_cancel_succes
        source_img: path_background + "unknown_popsafe.png"
        titlebody_x:50
        title_y:60
        body_y:130
        img_y:100
        title_text:qsTr("Pembatalan Berhasil")
        body_text:qsTr("Proses pembatalan yang kamu lakukan berhasil")

        NotifButton{
            id:btn_again
            x:310
            y:254
            r_radius:2
            buttonText:qsTr("KEMBALI KE AWAL")
            modeReverse:false
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    hide_cancel_success.start();
                    dimm_display.visible = false;
                    my_stack_view.pop(null)
                }
                onEntered:{
                    btn_again.modeReverse = true
                }
                onExited:{
                    btn_again.modeReverse = false
                }
            }
        }
        NotifButton{
            id: btn_cs2
            x:50
            y:254
            r_radius:2
            buttonText:qsTr("KIRIM LAGI")
            modeReverse:true
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    hide_cancel_success.start();
                    my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 2) return true }));
                    // my_stack_view.push(contact_us);
                }
                onEntered:{
                    btn_cs2.modeReverse = false
                }
                onExited:{
                    btn_cs2.modeReverse = true
                }
            }
        }

        Image{
            x:939
            y:60
            height:33
            width:33
            source:"asset/global/button/close.png"
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    hide_cancel_success.start();
                    dimm_display.visible = false;
                    my_stack_view.pop(null);
                }

            }
        }
    }

    NumberAnimation{
        id:show_cancel_success
        targets: notif_cancel_succes
        properties: "y"
        from:768
        to:248
        duration: 500
        easing.type: Easing.InOutBack
    }
    NumberAnimation{
        id:hide_cancel_success
        targets: notif_cancel_succes
        properties: "y"
        from:248
        to:768
        duration: 500
        easing.type: Easing.InOutBack
    }

    NumberAnimation {
        id:hide_key
        targets: touch_keyboard
        properties: "y"
        from:368
        to:768
        duration: 500
        easing.type: Easing.InOutBack
    }
    NumberAnimation {
        id:show_key
        targets: touch_keyboard
        properties: "y"
        from:768
        to:368
        duration: 500
        easing.type: Easing.InOutBack
    }

    Component.onCompleted: {
        root.cancel_order_result.connect(status_cancel)

    }

    Component.onDestruction: {
        root.cancel_order_result.disconnect(status_cancel)
    }

    function status_cancel(res){
        console.log("balikan cancel order"+res)
        var result = JSON.parse(res)
        if(result.isSuccess == "true"){
            show_cancel_success.start();
            dimm_display.visible = true
        }
    }


    function hide_animation(param){
        if(param=="keyboard") {
            hide_key.start();
        }
        if(param=="cancel_order"){
            hide_cancel_success.start();
        }
    }
}
