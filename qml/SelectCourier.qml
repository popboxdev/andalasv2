import QtQuick 2.4
import QtQuick.Controls 1.2

Background{
    id: select_courier
    property var press: '0'
    property var asset_path: 'asset/courier/'
    property var font_type1: "SourceSansPro-Regular"
    property var font_type2: "SourceSansPro-SemiBold"
    property var font_type3: "SourceSansPro-Light"
    property var param_choice: ""
    property var param_parcel: ""
    property var no_parcel: ""
    property var color_button: "#c4c4c4"
    property var id_company: ""
    property int pos_x: 100
    property var txt_time: ""
    property var lastmile_type: ""
    property var customer_phone: ""
    property var drop_by_courier: 0
    property var language: 'ID'
    //show_img: asset_path + "background/bg.png"
    show_img: ""
    img_vis: true

    Stack.onStatusChanged:{
        if(Stack.status == Stack.Activating){
            timer.secon = 90
            timer_note.restart()
            slot_handler.start_get_language();
            // slot_handler.start_get_file_dir(path);
            // slot_handler.start_get_locker_name();
            // timer_clock.start();
            // timer_startup.start();
            // hide_key.start();
            console.log('select courier page called')
            console.log("nomer parcel : " + no_parcel)
            press = '0';
            console.log('drop_by_courier =>' + drop_by_courier)
            
        }
        if(Stack.status==Stack.Deactivating){
            timer_note.stop()
            // timer_clock.stop();
            // slider_timer.stop();
            // timer_startup.stop();
        }
    }

    FontType{
        id:fontStyle
        //Type Font ### bold , extrabold , light , book, medium, black ###
    }

    Header{
        id:header
        title_text:qsTr("KURIR")
        text_timer : txt_time
    }

    Item{
        id: timer
        property int secon  : 90
        Timer{
            id:timer_note
            interval:1000
            repeat:true
            running:true
            triggeredOnStart:true
            onTriggered:{
                timer.secon -= 1
                txt_time = timer.secon
                if(timer.secon < 1){
                    timer_note.stop()
                    my_stack_view.push(time_out)
                }
            }
        }
    }

    Text {
        x:50
        y:125
        text: qsTr("Pilih Nama Kurir Logistik Kamu?")
        font.pixelSize:40
        color:"#414042"
        font.family: fontStyle.medium
    }
//BARIS PERTAMA
    NotifButtonLong{
        id: popexpress
        x:50
        y:230
        // buttonText:"| PopExpress"
        insertImage:true
        show_source:asset_path + "popexpress.png"
        img_width:113
        img_height:54
        img_y : 12
        img_x : pos_x
        lefttext: 150
        font_size:16
        r_width :300
        r_radius:2
        // buttonColor:"#c4c4c4"
        // MouseArea{
        //     anchors.fill: parent
        //     onClicked: {
        //         param_choice = "popexpress"
        //         id_company = "161e5ed1140f11e5bdbd0242ac110001"
                // button_inactive(param_choice)
        //     }
        // }
    }

    NotifButtonLong{
        id: pos
        x:50
        y:320
        // buttonText:"| POS INDONESIA"
        insertImage:true
        show_source:asset_path + "pos.png"
        img_width:51
        img_height:34
        img_y : 12
        img_x : pos_x
        lefttext: 150
        font_size:16
        r_width :300
        r_radius:2
        MouseArea{
            anchors.fill: parent
            onClicked: {
                param_choice="pos"
                id_company = "2c9180c151a8c1a70151ae15ecc80207"
                button_inactive(param_choice)
                choose_courier();
            }
        }
    }

    NotifButtonLong{
        id: grab
        x:50
        y:410
        // buttonText:"| GRAB"
        insertImage:true
        show_source:asset_path + "grab.png"
        img_width:132
        img_height:25
        img_y : 12
        img_x : pos_x
        lefttext: 150
        font_size:16
        r_width :300
        r_radius:2
        MouseArea{
            anchors.fill: parent
            onClicked: {
                param_choice="grab"
                id_company = "402880835778f18f015791fd48fb4b40"
                button_inactive(param_choice)
                choose_courier();
            }
        }
    }

    NotifButtonLong{
        id: others
        x:50
        y:500
        buttonText:qsTr("Kurir lainnya")
        insertImage:false
        // show_source:asset_path + "lex.png"
        // img_width:62
        // img_height:47
        // img_y : 12
        // img_x : pos_x
        lefttext: 80
        font_size:24
        r_width :300
        r_radius:2
        MouseArea{
            anchors.fill: parent
            onClicked: {
                param_choice="others"
                id_company = "f54530d1a9ae4983b3871fd43a187fa9"
                button_inactive(param_choice)
                choose_courier();
            }
        }
    }

    //BARIS KEDUA

    NotifButtonLong{
        id: jne
        x:362
        y:230
        // buttonText:"| JNE"
        insertImage:true
        show_source:asset_path + "jne.png"
        img_width:84
        img_height:34
        img_y : 12
        img_x : pos_x
        lefttext: 150
        font_size:16
        r_width :300
        r_radius:2
        MouseArea{
            anchors.fill: parent
            onClicked: {
                param_choice="jne"
                id_company = "2c9180af513872b7015147fa973301bd"
                button_inactive(param_choice)
                choose_courier();
            }
        }
    }

    NotifButtonLong{
        id: jnt
        x:362
        y:320
        // buttonText:"| J&T"
        insertImage:true
        show_source:asset_path + "jnt.png"
        img_width:92
        img_height:21
        img_y : 12
        img_x : pos_x
        lefttext: 150
        font_size:16
        r_width :300
        r_radius:2
        MouseArea{
            anchors.fill: parent
            onClicked: {
                param_choice="jnt"
                id_company="402880835659d5280156685affc71502"
                button_inactive(param_choice)
                choose_courier();
            }
        }
    }

    NotifButtonLong{
        id: wahana
        x:362
        y:410
        // buttonText:"| Wahana"
        insertImage:true
        show_source:asset_path + "wahana.png"
        img_width:90
        img_height:28
        img_y : 12
        img_x : pos_x
        lefttext: 150
        font_size:16
        r_width :300
        r_radius:2
        MouseArea{
            anchors.fill: parent
            onClicked: {
                param_choice="wahana"
                id_company="402880835af9d733015afb5218080371"
                button_inactive(param_choice)
                choose_courier();
            }
        }
    }

//BARIS KETIGA 

    NotifButtonLong{
        id: tiki
        x:674
        y:230
        // buttonText:"| TIKI"
        insertImage:true
        show_source:asset_path + "tiki.png"
        img_width:89
        img_height:24
        img_y : 12
        img_x : pos_x
        lefttext: 150
        font_size:16
        r_width :300
        r_radius:2
        MouseArea{
            anchors.fill: parent
            onClicked: {
                param_choice="tiki"
                id_company="30577083560cfae601562b68240b32e4"
                button_inactive(param_choice)
                choose_courier();
            }
        }
    }

    NotifButtonLong{
        id: gosend
        x:674
        y:320
        // buttonText:"| GOSEND"
        insertImage:true
        show_source:asset_path + "gosend.png"
        img_width:84
        img_height:14
        img_y : 12
        img_x : pos_x
        lefttext: 150
        font_size:16
        r_width :300
        r_radius:2
        MouseArea{
            anchors.fill: parent
            onClicked: {
                param_choice="gosend"
                id_company="202880839978f18f015791fd48fb4b40"
                button_inactive(param_choice)
                choose_courier();
            }
        }
    }

    NotifButtonLong{
        id: sicepat
        x:674
        y:410
        // buttonText:"| SICEPAT"
        insertImage:true
        show_source:asset_path + "sicepat.png"
        img_width:91
        img_height:25
        img_y : 12
        img_x : pos_x
        lefttext: 150
        font_size:16
        r_width :300
        r_radius:2
        MouseArea{
            anchors.fill: parent
            onClicked: {
                param_choice="sicepat"
                id_company="402880825c2ddbf5015c426a14cb2527"
                button_inactive(param_choice)
                choose_courier();
            }
        }
    }

    //BARIS PERTAMA MALAY
    NotifButtonLong{
        id: poslaju
        x:50
        y:230
        insertImage:true
        show_source:asset_path + "poslaju.png"
        img_width:115
        img_height:32
        img_y : 12
        img_x : pos_x
        lefttext: 150
        font_size:16
        r_width :300
        r_radius:2
        buttonColor:"#c4c4c4"
        MouseArea{
            anchors.fill: parent
            onClicked: {
                param_choice = "poslaju"
                id_company = "800640693001567595171qEYKp7s4CTR"
                button_inactive(param_choice)
                choose_courier();
            }
        }
    }

    NotifButtonLong{
        id: gdex
        x:50
        y:320
        insertImage:true
        show_source:asset_path + "gdex.png"
        img_width:81
        img_height:45
        img_y : 12
        img_x : pos_x
        lefttext: 150
        font_size:16
        r_width :300
        r_radius:2
        MouseArea{
            anchors.fill: parent
            onClicked: {
                param_choice="gdex"
                id_company = "4028808c57d5e92c0157dd41a1114014"

                button_inactive(param_choice)
                choose_courier();
            }
        }
    }

    //BARIS KEDUA MALAY

    NotifButtonLong{
        id: posmalay
        x:362
        y:230
        insertImage:true
        show_source:asset_path + "posmalay.png"
        img_width:68
        img_height:40
        img_y : 12
        img_x : pos_x
        lefttext: 150
        font_size:16
        r_width :300
        r_radius:2
        MouseArea{
            anchors.fill: parent
            onClicked: {
                param_choice="posmalay"
                id_company = "800640693001567595171qEYKp7s4CTR"
                button_inactive(param_choice)
                choose_courier();
            }
        }
    }

    NotifButtonLong{
        id: lelexpress
        x:362
        y:320
        insertImage:true
        show_source:asset_path + "lelexpress.png"
        img_width:125
        img_height:44
        img_y : 12
        img_x : pos_x
        lefttext: 150
        font_size:16
        r_width :300
        r_radius:2
        MouseArea{
            anchors.fill: parent
            onClicked: {
                param_choice="lelexpress"
                id_company="4028808c57d5e92c0157ff23890f1c21"
                button_inactive(param_choice)
                choose_courier();
            }
        }
    }

//BARIS KETIGA MALAY

    NotifButtonLong{
        id: ninjavan
        x:674
        y:230
        insertImage:true
        show_source:asset_path + "ninjavan.png"
        img_width:136
        img_height:37
        img_y : 12
        img_x : pos_x
        lefttext: 150
        font_size:16
        r_width :300
        r_radius:2
        MouseArea{
            anchors.fill: parent
            onClicked: {
                param_choice="ninjavan"
                id_company="402880825ea763b8015fa3ceef090bf6"
                button_inactive(param_choice)
                choose_courier();
            }
        }
    }

    NotifButtonLong{
        id: jnt_malay
        x:674
        y:320
        // buttonText:"| J&T"
        insertImage:true
        show_source:asset_path + "jnt.png"
        img_width:92
        img_height:21
        img_y : 12
        img_x : pos_x
        lefttext: 150
        font_size:16
        r_width :300
        r_radius:2
        MouseArea{
            anchors.fill: parent
            onClicked: {
                param_choice="jnt_malay"
                id_company="402880835659d5280156685affc71502"
                button_inactive(param_choice)
                choose_courier();
            }
        }
    }

   // ####################### JANGAN DI HAPUSSS #############################

//     NotifButtonLong{
//         id: anteraja
//         x:50
//         y:480
            // buttonText:"| ANTERAJA"
//         insertImage:true
//         show_source:asset_path + "anteraja.png"
//         img_width:86
//         img_height:35
//         img_y : 12
//         img_x : 42
//         lefttext: 150
//         font_size:16
//         r_width :300
//         r_radius:2
//         MouseArea{
//             anchors.fill: parent
//             onClicked: {
//                 param_choice="anteraja"
                // button_inactive(param_choice)
                // choose_courier();
//             }
//         }
//     }

//     NotifButtonLong{
//         id: lex
//         x:50
//         y:569
//         buttonText:"| LEX"
//         insertImage:true
//         show_source:asset_path + "lex.png"
//         img_width:62
//         img_height:47
//         img_y : 12
//         img_x : 42
//         lefttext: 150
//         font_size:16
//         r_width :300
//         r_radius:2
//         MouseArea{
//             anchors.fill: parent
//             onClicked: {
//                 param_choice="lex"
//                 button_inactive(param_choice)
//             }
//         }
//     }

//     //Baris 2


//     NotifButtonLong{
//         id: ninja
//         x:360
//         y:390
//         buttonText:"| NINJA XPRESS"
//         insertImage:true
//         show_source:asset_path + "ninja.png"
//         img_width:78
//         img_height:36
//         img_y : 12
//         img_x : 42
//         lefttext: 150
//         font_size:16
//         r_width :300
//         r_radius:2
//         MouseArea{
//             anchors.fill: parent
//             onClicked: {
//                 param_choice="ninja"
//                 button_inactive(param_choice)
//             }
//         }
//     }
// ///////
//     NotifButtonLong{
//         id: rcl
//         x:360
//         y:480
//         buttonText:"| RCL"
//         insertImage:true
//         show_source:asset_path + "rcl.png"
//         img_width:74
//         img_height:34
//         img_y : 12
//         img_x : 42
//         lefttext: 150
//         font_size:16
//         r_width :300
//         r_radius:2
//         MouseArea{
//             anchors.fill: parent
//             onClicked: {
//                 param_choice="rcl"
//                 button_inactive(param_choice)
//             }
//         }
//     }

//     NotifButtonLong{
//         id: blibli
//         x:360
//         y:569
//         buttonText:"| BLIBLI EXPRESS"
//         insertImage:true
//         show_source:asset_path + "blibli.png"
//         img_width:81
//         img_height:29
//         img_y : 12
//         img_x : 42
//         lefttext: 150
//         font_size:16
//         r_width :300
//         r_radius:2
//         MouseArea{
//             anchors.fill: parent
//             onClicked: {
//                 param_choice="blibli"
//                 button_inactive(param_choice)
//             }
//         }
//     }

//     //Baris 3

    

    
// ///////
//     NotifButtonLong{
//         id: jx
//         x:669
//         y:480
//         buttonText:"| JX"
//         insertImage:true
//         show_source:asset_path + "jx.png"
//         img_width:41
//         img_height:35
//         img_y : 12
//         img_x : 42
//         lefttext: 150
//         font_size:16
//         r_width :300
//         r_radius:2
//         MouseArea{
//             anchors.fill: parent
//             onClicked: {
//                 param_choice="jx"
//                 button_inactive(param_choice)
//             }
//         }
//     }

// ####################### JANGAN DI HAPUSSS #############################


    

    // NotifButton{
    //     id:btn_select
    //     x:744
    //     y:590
    //     r_width:230
    //     r_height:79
    //     buttonText:qsTr("PILIH")
    //     buttonColor: color_button
    //     r_radius:2
    //     modeReverse:false
    //     MouseArea{
    //         anchors.fill: parent
    //         onClicked: {
    //             console.log(param_choice, id_company)
    //             slot_handler.set_id_logistic_company(id_company)
    //             if(param_choice=="others"){
    //                 my_stack_view.push(other_courier, {no_parcel:no_parcel, status: "courier", title: qsTr("KURIR"), id_company: id_company, lastmile_type: lastmile_type, customer_phone: customer_phone, drop_by_courier: drop_by_courier})
    //             }else{
    //                 my_stack_view.push(input_phone, {no_parcel:no_parcel, select_courier:param_choice, status: "courier", title: qsTr("KURIR"), title_body: qsTr("Kamu (Kurir)"), id_company: id_company, lastmile_type: lastmile_type, customer_phone: customer_phone, drop_by_courier: drop_by_courier})
    //             }
    //         }
    //         onEntered:{
    //             btn_select.modeReverse = true
    //         }
    //         onExited:{
    //             btn_select.modeReverse = false
    //         }
    //     }
    // }

    Component.onCompleted: {
        root.start_get_language_result.connect(locker_language);
    }

    Component.onDestruction: {
        root.start_get_language_result.disconnect(locker_language);
    }

    function locker_language(result){
        // console.log("locker_language : ",result)
        language = result;
        if(language=="MY"){
            // console.log("bahasa : Malaysia")
            courier_my();
        }else{
            // console.log("bahasa  = Indonesia")
            courier_id();
        }
    }

    function courier_my() {
        popexpress.visible=false
        jnt.visible=false
        wahana.visible=false
        tiki.visible=false
        pos.visible=false
        jne.visible=false
        grab.visible=false
        sicepat.visible=false
        gosend.visible=false
        //Malaysia
        jnt_malay.visible=true
        poslaju.visible=true
        posmalay.visible=true
        lelexpress.visible=true
        ninjavan.visible=true
        gdex.visible=true
        jnt.x = 674
        others.y = 410
    }

    function courier_id() {
        popexpress.visible=true;
        jnt.visible=true;
        wahana.visible=true;
        tiki.visible=true;
        pos.visible=true;
        jne.visible=true;
        grab.visible=true;
        sicepat.visible=true;
        gosend.visible=true;
        //Malaysia
        jnt_malay.visible=false
        poslaju.visible=false;
        posmalay.visible=false;
        lelexpress.visible=false;
        ninjavan.visible=false;
        gdex.visible=false;
        // jnt.x = 674;
        // others.y = 410;
    }

    function button_inactive(param){
        console.log(param)
        // popexpress.modeReverse=false
        // popexpress.buttonColor="#f6f6f6"
        //MALAY
        poslaju.modeReverse=false
        poslaju.buttonColor="#f6f6f6"
        posmalay.modeReverse=false
        posmalay.buttonColor="#f6f6f6"
        lelexpress.modeReverse=false
        lelexpress.buttonColor="#f6f6f6"
        gdex.modeReverse=false
        gdex.buttonColor="#f6f6f6"
        ninjavan.modeReverse=false
        ninjavan.buttonColor="#f6f6f6"
        jnt_malay.modeReverse=false
        jnt_malay.buttonColor="#f6f6f6"

        //ID
        jnt.modeReverse=false
        jnt.buttonColor="#f6f6f6"
        wahana.modeReverse=false
        wahana.buttonColor="#f6f6f6"
        tiki.modeReverse=false
        tiki.buttonColor="#f6f6f6"
        pos.modeReverse=false
        pos.buttonColor="#f6f6f6"
        jne.modeReverse=false
        jne.buttonColor="#f6f6f6"
        grab.modeReverse=false
        grab.buttonColor="#f6f6f6"
        sicepat.modeReverse=false
        sicepat.buttonColor="#f6f6f6"
        gosend.modeReverse=false
        gosend.buttonColor="#f6f6f6"
        others.modeReverse=false
        others.buttonColor="#f6f6f6"
        color_button="#ff524f"

        // ####################### JANGAN DI HAPUSSS #############################
        // anteraja.modeReverse=false
        // anteraja.buttonColor="#f6f6f6"
        // lex.modeReverse=false
        // lex.buttonColor="#f6f6f6"
        // ninja.modeReverse=false
        // ninja.buttonColor="#f6f6f6"
        // rcl.modeReverse=false
        // rcl.buttonColor="#f6f6f6"
        // blibli.modeReverse=false
        // blibli.buttonColor="#f6f6f6"
        // jx.modeReverse=false
        // jx.buttonColor="#f6f6f6"

        if(param=="popexpress"){
            popexpress.modeReverse=true
            popexpress.buttonColor="#ff524f"
        }
        if(param=="jne"){
            jne.modeReverse=true
            jne.buttonColor="#ff524f"
        }
        if(param=="jnt"){
            jnt.modeReverse=true
            jnt.buttonColor="#ff524f"
        }
        if(param=="wahana"){
            wahana.modeReverse=true
            wahana.buttonColor="#ff524f"
        }
        if(param=="tiki"){
            tiki.modeReverse=true
            tiki.buttonColor="#ff524f"
        }
        if(param=="gosend"){
            gosend.modeReverse=true
            gosend.buttonColor="#ff524f"
        }
        if(param=="pos"){
            pos.modeReverse=true
            pos.buttonColor="#ff524f"
        }
        if(param=="grab"){
            grab.modeReverse=true
            grab.buttonColor="#ff524f"
        }
        if(param=="sicepat"){
            sicepat.modeReverse=true
            sicepat.buttonColor="#ff524f"
        }
        if(param=="others"){
            others.modeReverse=true
            others.buttonColor="#ff524f"
        }

        if(param=="poslaju"){
            poslaju.modeReverse=true
            poslaju.buttonColor="#ff524f"
        }
        if(param=="posmalay"){
            posmalay.modeReverse=true
            posmalay.buttonColor="#ff524f"
        }
        if(param=="lelexpress"){
            lelexpress.modeReverse=true
            lelexpress.buttonColor="#ff524f"
        }
        if(param=="ninjavan"){
            ninjavan.modeReverse=true
            ninjavan.buttonColor="#ff524f"
        }
        if(param=="gdex"){
            gdex.modeReverse=true
            gdex.buttonColor="#ff524f"
        }
        if(param=="jnt_malay"){
            jnt_malay.modeReverse=true
            jnt_malay.buttonColor="#ff524f"
        }


        // ####################### JANGAN DI HAPUSSS #############################
        // if(param=="anteraja"){
        //     anteraja.modeReverse=true
        //     anteraja.buttonColor="#ff524f"
        // }
        // if(param=="lex"){
        //     lex.modeReverse=true
        //     lex.buttonColor="#ff524f"
        // }
        // if(param=="ninja"){
        //     ninja.modeReverse=true
        //     ninja.buttonColor="#ff524f"
        // }
        // if(param=="rcl"){
        //     rcl.modeReverse=true
        //     rcl.buttonColor="#ff524f"
        // }
        // if(param=="blibli"){
        //     blibli.modeReverse=true
        //     blibli.buttonColor="#ff524f"
        // }
        // if(param=="jx"){
        //     jx.modeReverse=true
        //     jx.buttonColor="#ff524f"
        // }
    }

    function choose_courier(){
        console.log(param_choice, id_company)
        slot_handler.set_id_logistic_company(id_company)
        if(param_choice=="others"){
            my_stack_view.push(other_courier, {no_parcel:no_parcel, status: "courier", title: qsTr("KURIR"), id_company: id_company, lastmile_type: lastmile_type, customer_phone: customer_phone, drop_by_courier: drop_by_courier})
        }else{
            my_stack_view.push(input_phone, {no_parcel:no_parcel, select_courier:param_choice, status: "courier", title: qsTr("KURIR"), title_body: qsTr("Kamu (Kurir)"), id_company: id_company, lastmile_type: lastmile_type, customer_phone: customer_phone, drop_by_courier: drop_by_courier})
        }
    }

    DimmDisplay{
        id:dimm_display
        visible:false
    }

}
