import QtQuick 2.4
import QtQuick.Controls 1.2
import QtQuick.Controls.Styles 1.2
import 'door_price.js' as PRICE

Background{
    id: select_payment
    property var press: '0'
    property int count: 0
    property var path_background: 'asset/send_page/background/'
    property var asset_path: 'asset/send_page/'
    property var asset_payment : "asset/payment/logo/"
    property var asset_global: 'asset/global/'
    property var grey1: "#c4c4c4"
    property var grey2: "#f6f6f6"
    property var red1: "#ff524f"
    property var parcel_number: ''
    property var response_ovo: ""
    property int character:12
    property var ch:1 
    property var phone_number: ""
    property var phone_number_ovo: ""
    property var step_choose: ''
    property var durationOverdue: ''
    property var amount: 0
    property var locker_name: ''
    property var door_no: ''
    property var no_order: ''
    property var total_pay: ''
    property var logopayment: ''
    property var box_size: ''
    property var duration: ''
    property var payment_method_name: ''
    property var locker_number: ''
    property var storeExtend: false
    property var takeduration: ""
    property var durationHour: ""
    property var data_overdue: ""
    property var locale: Qt.locale()
    property date currentDate: new Date()
    property var validate_code: new Date()
    property var paymentResult: ''
    property var isExtending: false
    property var testingMode: false
    property var door_type: ''
    property var express_number: ''
    property var txt_time: ""
    property var item: ""
    property var customer: ""
    property var customer_ovo: ""
    property int presspay : 0
    property var paymentParam: ""
    property int limitpush: 0
    property bool modelate: false
    property var cancelStatus: ""
    property bool flagQr: false
    property var survey_expressNumber : ""
    property var survey_expressId : ""
    property var express_id : ""
    property var popsafe_status : undefined
    property int countOvo: 0
    property var masking_phone_ovo: ""
    property var real_phone_ovo: ""
    property var language: 'ID'
    property bool status_loading: false

    //show_img: asset_path + "background/bg.png"
    show_img: ""
    img_vis: true

    property variant check_number: ['0814','0815', '0816', '0855', '0856', '0857', '0858', '0811', '0812', '0813', '0821', '0822', '0823', '0851', '0852', '0853',
    '0817', '0818', '0819', '0859', '0877', '0878', '0879', '0831', '0832', '0838', '9991', '9992', '9993', '9994', '9995', '9988', '0881', '0882', '0883', '0884', '0885', '0886', '0887', '0888', '0889', '0895', '0896', '0897', '0898', '0899']

    Stack.onStatusChanged:{
        if(Stack.status == Stack.Activating){
            timer.secon = 120
            timer_note.restart()
            timer_ovo.stop()
            timer_ovo_notif.stop()
            logopayment = logopayment
            press = '0';
            slot_handler.start_get_language();
            slot_handler.start_hour_overdue()
            slot_handler.start_get_locker_name()
            amount = PRICE.popsafe_price
            total_pay = amount
            timer_payment.stop()
            if (flagQr){
                slot_handler.start_check_trans_global()
                // hide_qr_payment.start()
                hide_cancel_store.start()
                waiting.visible =true
            }
            if (storeExtend == true) {
                var parse_overdue = JSON.parse(data_overdue)
                phone_number = parse_overdue.takeUserPhoneNumber
                var tgl = new Date(parse_overdue.date_overdue);
                var date = Qt.formatDateTime(tgl, "ddd, dd MMM yyyy hh:mm");
                takeduration = date;
                durationHour = parse_overdue.parcel_duration + " Jam " + parse_overdue.parcel_minutes_duration + " Menit"
                amount = parse_overdue.cost_overdue
                validate_code = parse_overdue.validateCode
                express_number = parse_overdue.expressNumber
                isExtending = true
                console.log("data_overdue: " + data_overdue)
            }
            
            item = 'LOCAL_DEPOSIT @ ' + locker_name + ' : ' + door_no;    
            console.log("LOCAL DEPOSIT DOOR NO XX: ", item)
            customer = 'USER_ANDALAS||'+ phone_number +'||it@popbox.asia'; 

            // untuk phone ovo
            // phone_number_ovo.borderColor ="#c4c4c4";
            // nextButtonInputPhone.buttonColor="#c4c4c4"
            // countOvo = phone_number_ovo.length;
            

            if ( countOvo > 0 )
            {
                nextButtonInputPhone.buttonColor = "#ff524f"
                // phonebox.border.color = "#8c8c8c"
                del_img.visible=true
            }
        }
        if(Stack.status==Stack.Deactivating){
            timer_note.stop()
            timer_ovo.stop()
            timer_ovo_notif.stop()
        }
    }

    FontType{
        id:fontStyle
        //Type Font ### bold , extrabold , light , book, medium, black ###
    }

    Header{
        id:header
        title_text:(storeExtend == false) ? qsTr("TITIP") : qsTr("AMBIL")
        text_timer : txt_time
    }
    Item{
        id: timer
        property int secon  : 120
        Timer{
            id:timer_note
            interval:1000
            repeat:true
            running:true
            triggeredOnStart:true
            onTriggered:{
                timer.secon -= 1
                txt_time = timer.secon
                if(timer.secon < 1){
                    timer_note.stop()
                    timer_ovo.stop()
                    timer_ovo_notif.stop()
                    my_stack_view.push(time_out, {flagQr:flagQr})
                }
            }
        }
    }
    Item{
        id: timerovo
        property int detik :45
        Timer{
            id:timer_ovo
            interval:1000
            repeat:true
            running:true
            triggeredOnStart:true
            onTriggered:{
                timerovo.detik -= 1
                // if(timerovo.detik % 7 == 0 && timerovo.detik > 15){
                //     slot_handler.start_create_payment_ovo(amount, customer, item, locker_name, no_order);        
                // }
                // else if(timerovo.detik % 5 == 0 && timerovo.detik < 15 ){
                //     slot_handler.start_create_payment_ovo(amount, customer, item, locker_name, no_order);
                // }
                if(timerovo.detik == 15){
                    // slot_handler.start_create_payment_ovo(amount, customer, item, locker_name, no_order);
                    dimm_display.visible=true
                    // hide_qr_payment.start()
                    // show_check_ovo.start()
                }
                if(timerovo.detik<1){
                    timer_ovo.stop()
                }
            }
        }
    }

    Text {
        id: txt_method
        x:50
        y:125
        text: (storeExtend == false) ? qsTr("Pilih Metode Pembayaran") : qsTr("Detail Extends")
        font.pixelSize:40
        color:"#414042"
        font.family: fontStyle.medium
    }

    Text {
        id:nohp
        x:50
        y:(storeExtend == false) ? 216 : 210
        text: qsTr("No. Handphone") + ("\t:    ") + phone_number
        font.pixelSize:24
        color:"#3a3a3a"
        font.family: fontStyle.book
        
    }
    Text {
        id:changeNohp
        x:502
        y:219
        text: qsTr("<u>Ubah No Handphone<u>")
        font.pixelSize:18
        color: red1
        font.family: fontStyle.book
        visible: (storeExtend == false) ? true : false
        MouseArea {
            anchors.fill: parent
            onClicked: {
                slot_handler.start_clear_transaction();
                my_stack_view.pop(my_stack_view.find(function(item){return item.Stack.index==2}));
            }
        }
    }

    Text {
        id:durasi
        x:50
        y: 267
        text: qsTr("Durasi   ") + ("\t\t:    ") +  durationOverdue + qsTr(" Jam")
        font.pixelSize:24
        color:"#3a3a3a"
        font.family: fontStyle.book
        visible: (storeExtend == false) ? true : false
    }

    Text {
        id:batasambil
        x:50
        y: 271
        text: qsTr("Batas Pengambilan") + ("\t:    ") +  takeduration
        font.pixelSize:24
        color:"#3a3a3a"
        font.family: fontStyle.book
        visible: (storeExtend == false) ? false : true
    }

    Text {
        id:durasiperpanjangan
        x:50
        y: 332
        text: qsTr("Durasi Perpanjangan") + ("\t:    ") +  durationHour
        font.pixelSize:24
        color:"#3a3a3a"
        font.family: fontStyle.book
        visible: (storeExtend == false) ? false : true
    }



    Text {
        id:harga
        x:50
        y:(storeExtend == false) ? 318 : 393
        text: qsTr("Harga") + ("\t\t\t:    ") + qsTr("Rp ") + amount
        font.pixelSize:24
        color:"#3a3a3a"
        font.family: fontStyle.book
    }

    Text {
        id:method
        x:50
        y:(storeExtend == false) ? 398 : 469
        text: qsTr("Metode Pembayaran") + ("\t:  ")
        font.pixelSize:24
        color:"#3a3a3a"
        font.family: fontStyle.book
    }

    Image{
        id:selectedpayment
        x:360
        y:(storeExtend == false) ? 380 : 454
        width:106
        height:60
        source: logopayment
        visible: source!=""? true:false
    }

    Text {
        id:ask_voucher
        x:50
        y:(storeExtend == false) ? 430 : 534
        text: qsTr("Kamu Punya Kode Voucher ?")
        font.pixelSize:24
        color:"#3a3a3a"
        font.family: fontStyle.book
        visible: false
    }
    
    Text {
        id:voucher
        x:368
        y:(storeExtend == false) ? 433 : 538
        text: qsTr("<u>Masukkan Kode Voucher<u>")
        font.pixelSize:18
        color:red1
        font.family: fontStyle.book
        visible:false
    }

    Rectangle{
        id : chooseMethod
        x:330
        y:(storeExtend == false) ? 381 : 455
        width: 337
        height: 60
        color: "transparent"
        border.color: grey1
        border.width: 2
        //radius : 5
        Text {
            id: choose_button
            //anchors.horizontalCenter: parent.horizontalCenter
            x: 14
            anchors.verticalCenter: parent.verticalCenter
            font.pixelSize:20
            color:red1
            font.family: fontStyle.book
            text: qsTr("PILIH METODE PEMBAYARAN")
        }
        Image{
            x:291
            y:24
            width:22
            height: 15
            source: "asset/payment/logo/choose.png"
        }   
        MouseArea {
            anchors.fill: parent
            onClicked: {
                modelate=false
                show_metode_bayar.start();
                dimm_display.visible=true;
            }
        }

    }
    NotifButton{
        id: nextButton
        x:329
        y:(storeExtend == false) ? 550 : 620
        buttonText:qsTr("LANJUT")
        buttonColor: grey1
        enabled: false
        r_radius:2
        modeReverse:false
        MouseArea{
            anchors.fill: parent
            onClicked: {
                presspay = 0
                if(step_choose=="emoney")
                {
                    create_payment(step_choose)
                    show_emoney_payment.start();
                    dimm_display.visible = true;
                }
                // else if(step_choose=="ovo")
                // {
                //     show_confirm_ovo.start()
                //     dimm_display.visible = true;
                //     create_payment(step_choose)
                // }
                else{
                    if (step_choose == 'ovo'){
                        // timer_ovo.start()
                        waiting_payment.visible = false;
                        
                        //  ovo notif
                        show_input_hp_ovo.start()
                        create_payment(step_choose)
                        dimm_display.visible = true;
                    }else{
                        
                        waiting_payment.visible = true;
                        show_qr_payment.start();
                        dimm_display.visible = true;
                        create_payment(step_choose)
                    }
                }
            }
            onEntered:{
                nextButton.modeReverse = true
            }
            onExited:{
                nextButton.modeReverse = false
            }
        }
    }
    NotifButton{
        id: cancelpay
        x:51
        y:(storeExtend == false) ? 550 : 620
        buttonText:qsTr("BATAL")
        modeReverse:true
        buttonColor: red1
        r_radius:2
        MouseArea{
            anchors.fill: parent
            onClicked: {
                // my_stack_view.pop(null)
                cancelStatus = "cancelpay";
                show_cancel_store.start();
                dimm_display.visible = true;
            }
            onEntered:{
                cancelpay.modeReverse = false
            }
            onExited:{
                cancelpay.modeReverse = true
            }
        }
    }

    Text{
        x:50
        y:(storeExtend == false) ? 644 : 715
        text: qsTr("Pastikan nomor yang dimasukkan aktif, notifikasi akan dikirimkan ke nomor tersebut.")
        font.pixelSize:22
        color:"#3a3a3a"
        font.family: fontStyle.book
    }

    NumberAnimation {
        id:hide_metode_bayar
        targets: notif_metode_bayar
        properties: "y"
        from:83
        to:768
        duration: 500
        easing.type: Easing.InOutBack
    }

    NumberAnimation {
        id:show_metode_bayar
        targets: notif_metode_bayar
        properties: "y"
        from:768
        to:83
        duration: 500
        easing.type: Easing.InOutBack
    }

    NumberAnimation{
        id:show_qr_payment
        targets: notif_qr_payment
        properties: "y"
        from:768
        to:218
        duration: 500
        easing.type: Easing.InOutBack
    }
    NumberAnimation{
        id:hide_qr_payment
        targets: notif_qr_payment
        properties: "y"
        from:218
        to:768
        duration: 500
        easing.type: Easing.InOutBack
    }

    NumberAnimation{
        id: show_emoney_payment
        targets: notif_emoney_payment
        properties: "y"
        from:768
        to:218
        duration: 500
        easing.type: Easing.InOutBack
    }
    NumberAnimation{
        id: hide_emoney_payment
        targets: notif_emoney_payment
        properties: "y"
        from:218
        to:768
        duration: 500
        easing.type: Easing.InOutBack
    }

    NumberAnimation{
        id: show_payment_failed
        targets: notif_payment_failed
        properties: "y"
        from:768
        to:248
        duration: 500
        easing.type: Easing.InOutBack
    }
    NumberAnimation{
        id: hide_payment_failed
        targets: notif_payment_failed
        properties: "y"
        from:248
        to:768
        duration: 500
        easing.type: Easing.InOutBack
    }

    NumberAnimation{
        id: show_payment_check
        targets: notif_payment_check
        properties: "y"
        from:768
        to:248
        duration: 500
        easing.type: Easing.InOutBack
    }
    NumberAnimation{
        id: hide_payment_check
        targets: notif_payment_check
        properties: "y"
        from:248
        to:768
        duration: 500
        easing.type: Easing.InOutBack
    }

    NumberAnimation{
        id: show_loading_payment
        targets: notif_loading_payment
        properties: "y"
        from:768
        to:248
        duration: 500
        easing.type: Easing.InOutBack
    }
    NumberAnimation{
        id: hide_loading_payment
        targets: notif_loading_payment
        properties: "y"
        from:248
        to:768
        duration: 500
        easing.type: Easing.InOutBack
    }

    NumberAnimation{
        id: show_payment_choice
        targets: notif_payment_choice
        properties: "y"
        from:768
        to:248
        duration: 500
        easing.type: Easing.InOutBack
    }
    NumberAnimation{
        id: hide_payment_choice
        targets: notif_payment_choice
        properties: "y"
        from:248
        to:768
        duration: 500
        easing.type: Easing.InOutBack
    }

    NumberAnimation{
        id: show_payment_success
        targets: notif_payment_success
        properties: "y"
        from:768
        to:218
        duration: 500
        easing.type: Easing.InOutBack
    }
    NumberAnimation{
        id: hide_payment_success
        targets: notif_payment_success
        properties: "y"
        from:218
        to:768
        duration: 500
        easing.type: Easing.InOutBack
    }

    NumberAnimation{
        id: show_confirm_ovo
        targets: notif_confirm_ovo
        properties: "y"
        from:768
        to:218
        duration: 500
        easing.type: Easing.InOutBack
    }
    NumberAnimation{
        id: hide_confirm_ovo
        targets: notif_confirm_ovo
        properties: "y"
        from:218
        to:768
        duration: 500
        easing.type: Easing.InOutBack
    }

    NumberAnimation{
        id: show_check_ovo
        targets: notif_check_ovo
        properties: "y"
        from:768
        to:218
        duration: 500
        easing.type: Easing.InOutBack
    }
    NumberAnimation{
        id: hide_check_ovo
        targets: notif_check_ovo
        properties: "y"
        from:218
        to:768
        duration: 500
        easing.type: Easing.InOutBack
    }

    NumberAnimation {
        id:show_cancel_store
        targets: cancel_store
        properties: "y"
        from:768
        to:248
        duration: 500
        easing.type: Easing.InOutBack
    }

    NumberAnimation {
        id:hide_cancel_store
        targets: cancel_store
        properties: "y"
        from:248
        to:768
        duration: 500
        easing.type: Easing.InOutBack
    }

    DimmDisplay{
        id:dimm_display
        visible:false
    }

    AnimatedImage{
        id: waiting
        visible:false
        width: 200
        height: 200
        anchors.verticalCenter: parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter
        source: 'asset/global/animation/loading.gif'
    }

    NotifSwipe{
        id: notif_check_ovo
        source_img: path_background + "check_ovo.png"
        titlebody_x:50
        title_y:60
        body_y:130
        img_y:80
        img_height:440
        r_height: 550
        title_text:qsTr("Cek Notifikasi di HP Kamu")
        body_text:qsTr("Silakan cek aplikasi OVO di HP kamu dan <br>konfirmasi pembayaran untuk melanjutkan <br>proses. <br>Pastikan No. Hp Sudah benar")

        Rectangle{
            id:ovo_info_phone_number
            x:50
            y: 270
            width: 497
            height: 82
            color: "#e8e8e8"
            border.color: '#e8e8e8'
            border.width: 3
            radius : 5
            
            Text {
                id:phone_number_ovo_text
                y:25
                x:45
                text: qsTr("<b>"+masking_phone_ovo+"</b>")
                font.pixelSize: 28
                color:"#414042"
                font.family: fontStyle.bold
            }
        }

        // Image{
        //     id: icon_reinput
        //     x:50
        //     y:359
        //     source: asset_global + "icon/exchange.png"
        // }

        // Text {
        //     id:text_reinput
        //     y:360
        //     x:85
        //     text: qsTr("<b><u>Salah no. hp? Ubah di sini.</u></b>")
        //     font.pixelSize:22
        //     color:"#3a3a3a"
        //     font.family: fontStyle.book
        //     MouseArea {
        //         anchors.fill: parent
        //         onClicked: {
        //             hide_animation("check_ovo");
        //             show_input_hp_ovo.start()
        //             show_key.start()
        //             dimm_display.visible=true  
        //         }
        //     }
        // }

        Image{
            x:939
            y:60
            height:33
            width:33
            source:"asset/global/button/close.png"
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    hide_animation("check_ovo");
                    // payprocessnotif.buttonColor = grey1
                    // payprocessnotif.enabled =false
                    gopay.buttonColor = grey2
                    gopay.modeReverse = false
                    emoney.buttonColor = grey2
                    emoney.modeReverse = false
                    linkaja.buttonColor = grey2
                    linkaja.modeReverse = false
                    dana.buttonColor = grey2
                    dana.modeReverse = false
                    ottopay.buttonColor = grey2
                    ottopay.modeReverse = false
                }
            }
        }
    }


    NotifSwipe{
        id: notif_confirm_ovo
        source_img: path_background + "confirm_ovo.png"
        titlebody_x:50
        title_y:90
        body_y:180
        img_y:80
        img_height:440
        r_height: 550
        title_text:qsTr("Bayar Dengan OVO")
        body_text:qsTr("Pastikan No.HP kamu sama dengan No.HP yang <br>terdaftar di aplikasi OVO")

        Text{
            x:50
            y:245
            text: phone_number
            font.pixelSize:24
            color:"#3a3a3a"
            font.family: fontStyle.medium
        }

        Item{
            id: timerovo_notif
            property int detik :45
            Timer{
                id:timer_ovo_notif
                interval:1000
                repeat:true
                running:true
                triggeredOnStart:true
                onTriggered:{
                    timerovo.detik -= 1
                    if(timerovo.detik % 7 == 0 && timerovo.detik > 15){
                        slot_handler.start_create_payment_ovo(amount, customer, item, locker_name, no_order);        
                    }
                    else if(timerovo.detik % 5 == 0 && timerovo.detik < 15 ){
                        slot_handler.start_create_payment_ovo(amount, customer, item, locker_name, no_order);
                    }
                    if(timerovo.detik == 15){
                        slot_handler.start_create_payment_ovo(amount, customer, item, locker_name, no_order);
                        dimm_display.visible=true
                        // hide_qr_payment.start()
                        // show_check_ovo.start()
                    }
                    if(timerovo.detik<1){
                        timer_ovo_notif.stop()            
                    }
                }
            }
        }

        NotifButton{
            id:ok_change
            x:51
            y:400
            r_width: 405
            r_radius:2
            buttonText:qsTr("UBAH METODE PEMBAYARAN")
            modeReverse:true
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    hide_animation("pay_ovo");
                }
                onEntered:{
                    ok_change.modeReverse = true
                }
                onExited:{
                    ok_change.modeReverse = false
                }
            }
        }
        NotifButton{
            id:ok_confirm
            x:486
            y:400
            r_radius:2
            buttonText:"OKE"
            modeReverse:false
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    qr_image.source = "asset/payment/qr_ovo.png"
                    timer_ovo.start()
                    timer_ovo_notif.start()
                    hide_confirm_ovo.start();
                    // show_qr_payment.start();
                }
                onEntered:{
                    ok_confirm.modeReverse = false
                }
                onExited:{
                    ok_confirm.modeReverse = true
                }
            }
        }
    }

    NotifSwipe{
        id: notif_payment_success
        source_img: path_background + "payment_success.png"
        titlebody_x:50
        title_y:60
        body_y:130
        img_y:80
        img_height:440
        r_height:550
        title_text:qsTr("Selamat Pembayaran Kamu Berhasil")
        body_text: (flagQr)? qsTr("Mohon maaf, transaksi kamu tidak dapat <br>dibatalkan, karena status pembayaran sudah <br>berhasil.") : ""

        Rectangle{
            id: timer_set
            x:50
            y:50
            QtObject{
                id:secon
                property int counter
                Component.onCompleted:{
                    secon.counter = 3
                }
            }

            Timer{
                id:timer_payment
                interval:1000
                repeat:true
                running:true
                triggeredOnStart:true
                onTriggered:{
                    secon.counter -= 1
                    if(secon.counter == 0){
                        slot_handler.start_choose_mouth_size(box_size, "customer_store_express")
                        timer_payment.stop()
                        hide_payment_success.start();
                    }
                }
            }
        }

        Text {
            id: txt_qrno_success
            x:50
            y:155
            text: qsTr("No Order") + "\t: " + no_order
            font.pixelSize:24
            color:"#414042"
            font.family: fontStyle.medium
            visible: (flagQr)? false:true
        }
        Text {
            id: txt_qrtotal_success
            x:50
            y:219
            text: qsTr("Jumlah") + "\t: " + qsTr("Rp ") + amount
            font.pixelSize:24
            color:"#414042"
            font.family: fontStyle.medium
            visible: (flagQr)? false:true
        }
        Text {
            id: txt_qrpay_success
            x:50
            y:291
            text: qsTr("Bayar")+ "\t\t:" 
            font.pixelSize:24
            color:"#414042"
            font.family: fontStyle.medium
            visible: (flagQr)? false:true
        }
        Image{
            id: qrcode_ground_success
            x:227
            y:282
            width:106
            height:60
            source: ""
            visible: (flagQr)? false:true
        }
        NotifButton{
            x: 49
            y: 371
            buttonText: qsTr("LANJUT")
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    slot_handler.start_choose_mouth_size(box_size, "customer_store_express")
                }
            }
            visibleflag: (flagQr)? true:false
        }
    }

    NotifSwipe{
        id: notif_loading_payment
        source_img: path_background + "payment_failed.png"
        titlebody_x:50
        title_y:60
        body_y:130
        img_y:80
        img_height:440
        title_text:qsTr("Pengecekan Pembayaran")
        body_text:qsTr("Mohon menunggu, saat ini loker sedang <br>melakukan pengecekan pembayaran kamu.")
        // AnimatedImage{
        //     width: 200
        //     height: 200
        //     anchors.verticalCenter: parent.verticalCenter
        //     anchors.horizontalCenter: parent.horizontalCenter
        //     source: 'asset/global/animation/loading.gif'
        // }
    }

    NotifSwipe{
        id: notif_payment_choice
        source_img: path_background + "payment_failed.png"
        titlebody_x:50
        title_y:60
        body_y:130
        img_y:80
        img_height:440
        title_text:qsTr("Konfirmasi Pembayaran")
        body_text:qsTr("Apakah kamu sudah melakukan pembayaran <br>pada aplikasi payment di smartphone kamu?")

        NotifButton{
            id: check_yes
            x:310
            y:250
            r_radius:0
            modeReverse:false
            buttonText:qsTr("YA")
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    hide_animation("pay_choice");
                    slot_handler.start_check_trans_global()
                    show_loading_payment.start();
                    dimm_display.visible = true;
                    status_loading = true;
                }
                onEntered:{
                    check_yes.modeReverse = true
                }
                onExited:{
                    check_yes.modeReverse = false
                }
            }
        }

        NotifButton{
            id:check_no
            x:50
            y:250
            r_radius:0
            buttonText:qsTr("TIDAK")
            modeReverse:true
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    hide_animation("pay_choice");
                    show_payment_check.start();
                    dimm_display.visible = true;
                }
                onEntered:{
                    check_no.modeReverse = false
                }
                onExited:{
                    check_no.modeReverse = true
                }
            }
        }
    }

    NotifSwipe{
        id: notif_payment_check
        source_img: path_background + "payment_failed.png"
        titlebody_x:50
        title_y:60
        body_y:130
        img_y:80
        img_height:440
        title_text:qsTr("Pembayaran Tidak Terverifikasi")
        body_text:qsTr("Silakan tekan tombol CEK PEMBAYARAN untuk <br>mengecek kembali status pembayaran kamu. <br>Tekan tombol GANTI PEMBAYARAN jika ingin <br>melakukan pergantian metode pembayaran.")

        NotifButton{
            id:check_ok
            x:330
            y:350
            r_width: 250
            r_radius:2
            buttonText:qsTr("CEK PEMBAYARAN")
            modeReverse:false
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    hide_animation("pay_check");
                    slot_handler.start_check_trans_global()
                    show_loading_payment.start();
                    dimm_display.visible = true;
                    status_loading = true;
                }
                onEntered:{
                    check_ok.modeReverse = true
                }
                onExited:{
                    check_ok.modeReverse = false
                }
            }
        }

        NotifButton{
            id:check_retry
            x:50
            y:350
            r_width: 250
            r_radius:2
            buttonText:qsTr("GANTI PEMBAYARAN")
            modeReverse:true
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    hide_animation("pay_check");
                    modelate = false
                    timerovo.detik = 45
                    limitpush = 0
                    no_order=''
                }
                onEntered:{
                    check_retry.modeReverse = false
                }
                onExited:{
                    check_retry.modeReverse = true
                }
            }
        }
    }

    NotifSwipe{
        id: notif_payment_failed
        source_img: path_background + "payment_failed.png"
        titlebody_x:50
        title_y:60
        body_y:130
        img_y:80
        img_height:440
        title_text:qsTr("Pembayaran Kamu Gagal")
        body_text:qsTr("Mohon maaf terjadi kendala dalam transaksi anda <br>silakan ulangi pembayaran kamu, jika kamu sudah <br>membayar harap hubungi Customer Service kami")

        NotifButton{
            id:ok_retry
            x:310
            y:290
            r_width: 283
            r_radius:2
            buttonText:qsTr("ULANGI PEMBAYARAN")
            modeReverse:false
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    hide_animation("pay_fail");
                    modelate = false
                    timerovo.detik = 45
                    limitpush = 0
                    no_order=''
                }
                onEntered:{
                    ok_retry.modeReverse = true
                }
                onExited:{
                    ok_retry.modeReverse = false
                }
            }
        }
        NotifButton{
            id:ok_batal
            x:50
            y:290
            r_radius:2
            buttonText:qsTr("BATAL")
            modeReverse:true
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    slot_handler.start_clear_transaction();
                    my_stack_view.pop(null);
                }
                onEntered:{
                    ok_batal.modeReverse = false
                }
                onExited:{
                    ok_batal.modeReverse = true
                }
            }
        }

        Image{
            x:939
            y:60
            height:33
            width:33
            source:"asset/global/button/close.png"
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    hide_animation("pay_fail");
                }
            }
        }
    }


    NotifSwipe{
        id:notif_metode_bayar
        title_text:qsTr("Pilih Metode Pembayaran")
        titlebody_x:440
        title_y: 50
        body_y:67
        r_height: 685

        Image{
            x:15.2
            y:197
            width:403
            height:303.3
            source: "asset/payment/method_payment.jpg"
        }

        NotifButtonLong{
            id: gopay
            x:440
            y:152
            show_source:"asset/payment/logo/gopay.png"
            xCenter: true
            img_width:106
            img_height:60
            img_x : 60//24
            insertImage:true
            lefttext: 334
            font_size:22
            r_width : 535//915
            r_radius:2

            MouseArea{
                anchors.fill: parent
                onClicked: {
                    step_choose = "gopay"
                    compressor(step_choose)
                    lanjut_payment_notif()
                }
            }

        }

        NotifButtonLong{
            id: emoney
            x:440
            y:232
            show_source:"asset/payment/logo/emoney.png"
            xCenter: true
            img_width:106
            img_height:60
            img_x : 60
            insertImage:true
            lefttext: 334
            font_size:22
            r_width : 535
            r_radius:2
            enabled: false
            color: "#ABABAB"

            MouseArea{
                anchors.fill: parent
                onClicked: {
                    step_choose = "emoney"
                    compressor(step_choose)
                    lanjut_payment_notif()
                }
            }
        }

        NotifButtonLong{
            id: boost
            x:440
            y:152
            show_source:"asset/payment/logo/boost.png"
            xCenter: true
            img_width:166
            img_height:90
            img_x : 60//24
            insertImage:true
            lefttext: 334
            font_size:22
            r_width : 535//915
            r_radius:2

            MouseArea{
                anchors.fill: parent
                onClicked: {
                    step_choose = "boost"
                    compressor(step_choose)
                    lanjut_payment_notif()
                }
            }

        }

        NotifButtonLong{
            id: grabpay
            x:440
            y:232
            show_source:"asset/payment/logo/grabpay.png"
            xCenter: true
            img_width:166
            img_height:90
            img_x : 60
            insertImage:true
            lefttext: 334
            font_size:22
            r_width : 535
            r_radius:2

            MouseArea{
                anchors.fill: parent
                onClicked: {
                    step_choose = "grabpay"
                    compressor(step_choose)
                    lanjut_payment_notif()
                }
            }
        }

        NotifButtonLong{
            id: linkaja
            x:440
            y:312
            show_source:"asset/payment/logo/linkaja.png"
            xCenter: true
            img_width:106
            img_height:60
            img_x : 60
            insertImage:true
            lefttext: 334
            font_size:22
            r_width : 535
            r_radius:2
            color: "#ABABAB"
            enabled: false

            MouseArea{
                anchors.fill: parent
                onClicked: {
                    step_choose = "linkaja"
                    compressor(step_choose)
                    lanjut_payment_notif()
                }
            }
        }
        NotifButtonLong{
            id: ottopay
            x:440
            y:392
            lefttext: 334
            show_source:"asset/payment/logo/otto.png"
            xCenter: true
            img_width:106
            img_height:60
            img_x : 60
            insertImage:true
            font_size:22
            r_width : 535
            r_radius:2
            color: "#ABABAB"
            enabled: false

            MouseArea{
                anchors.fill: parent
                onClicked: {
                    step_choose = "otto"
                    compressor(step_choose)
                    lanjut_payment_notif()
                }
            }
        }

        NotifButtonLong{
            id:ovo
            x:440
            y:472
            lefttext: 334
            show_source:"asset/payment/logo/ovo.png"
            xCenter: true
            img_width:106
            img_height:60
            img_x : 60
            insertImage:true
            font_size:22
            r_width : 535
            r_radius:2

            
            // Rectangle {
            //     id: ovo_inactive
            //     width: 535
            //     height: 70
            //     color: "#AAb3b3b3"

            //     Text{
            //         font.family: fontStyle.medium
            //         color:"#ffffff"
            //         font.pixelSize:24
            //         // x: 20
            //         anchors.verticalCenter: parent.verticalCenter
            //         anchors.horizontalCenter: parent.horizontalCenter
            //         text: qsTr("Coming Soon")
            //     }
            // }

            //  OVO DISABLE
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    step_choose = "ovo"
                    compressor(step_choose)
                    lanjut_payment_notif()
                }
            }
        }

        // NotifButton{
        //     id: payprocessnotif
        //     x:735
        //     y:567//487
        //     buttonText:qsTr("LANJUT")
        //     buttonColor: grey1
        //     r_radius:2
        //     enabled: false
        //     modeReverse:false
        //     MouseArea{
        //         anchors.fill: parent
        //         onClicked: {

        //             hide_metode_bayar.start()
        //             dimm_display.visible=false
        //             press=0;
        //             nextButton.color = red1
        //             nextButton.enabled = true
        //         }
        //         onEntered:{
        //             payprocessnotif.modeReverse = true
        //         }
        //         onExited:{
        //             payprocessnotif.modeReverse = false
        //         }
        //     }
        // }

        Image{
            x:939
            y:60
            height:33
            width:33
            source:"asset/global/button/close.png"
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    hide_animation("bayar");
                    // payprocessnotif.buttonColor = grey1
                    // payprocessnotif.enabled =false
                    gopay.buttonColor = grey2
                    gopay.modeReverse = false
                    emoney.buttonColor = grey2
                    emoney.modeReverse = false
                    linkaja.buttonColor = grey2
                    linkaja.modeReverse = false
                    ottopay.buttonColor = grey2
                    ottopay.modeReverse = false
                }
            }
        }
    }

    NotifSwipe{
        id:cancel_store
        title_text:qsTr("Apakah Kamu Ingin <br>Membatalkan Proses?")
        body_text:qsTr("Jika kamu membatalkan proses, maka akan <br>diarahkan ke halaman awal.")
        titlebody_x: 50
        body_y: 189
        r_height: 535
        source_img:"asset/box/cancel.png"
        img_y:40
        img_height: 470

        NotifButton{
            x: 50
            y: 350
            buttonText: qsTr("TIDAK")
            modeReverse: true
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    dimm_display.visible= false;
                    hide_cancel_store.start();
                    if (cancelStatus != "cancelpay"){
                        show_qr_payment.start();
                        dimm_display.visible = true;
                    }
                }
            }
        }

        NotifButton{
            x: 310
            y: 350
            buttonText: qsTr("YA")
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    if (cancelStatus != "cancelpay"){
                        dimm_display.visible= false
                        hide_cancel_store.start()
                        // waiting.visible =true;
                        // slot_handler.start_check_trans_global()
                    }
                    else{
                        dimm_display.visible= false
                        slot_handler.start_clear_transaction();
                        my_stack_view.pop(null)
                    }
                }
            }
        }
    }

    NotifSwipe{
        id:notif_qr_payment
        title_text:(modelate == false) ? qsTr("Bayar Dengan Scan QR") : qsTr("Ayo Segera Scan QR-nya!")
        font_color: (modelate == false) ? "#4a4a4a" : "#ff524f"
        // font_type: "SourceSansPro-SemiBold"
        title_bold: false
        titlebody_x:50
        body_y:60
        r_height: 550
        Text {
            id: txt_qrno
            x:50
            y:155
            text: qsTr("No Order     ") + "\t: " + no_order
            font.pixelSize:24
            color:"#414042"
            font.family: fontStyle.medium
        }
        Text {
            id: txt_qrtotal
            x:50
            y:219
            text: qsTr("Jumlah") + "\t: " + qsTr("Rp ") + amount //malaysia
            font.pixelSize:24
            color:"#414042"
            font.family: fontStyle.medium
        }
        Text {
            id: txt_qrpay
            x:50
            y:291
            text: qsTr("Bayar     ")+ "\t:" 
            font.pixelSize:24
            color:"#414042"
            font.family: fontStyle.medium
        }
        Image{
            id: qrcode_ground
            x:227
            y:282
            width:106
            height:60
            source: ""
        }
        Rectangle{
            id: rec_qr
            //x: 362
            color: "white"
            radius: 20
            anchors.top: parent.top
            anchors.topMargin: 159
            anchors.right: parent.right
            anchors.rightMargin: 80
            width: 300
            height: 300

            Image{
                id: qr_image
                scale: 0.95
                opacity: 1
                fillMode: Image.PreserveAspectFit
                anchors.fill: parent
                source : (step_choose == 'ovo') ? "asset/payment/qr_ovo.png" : ""
                //onStatusChanged: if(qr_image.status == Image.Ready) loadingPopUp.close()
            }
            AnimatedImage{
                id: waiting_payment
                visible:false
                width: 100
                height: 100
                anchors.verticalCenter: parent.verticalCenter
                anchors.horizontalCenter: parent.horizontalCenter
                source: 'asset/payment/loading_payment.gif'
            }
        }
        NotifButton{
            id: notifcancelqr
            x:51
            y:391
            buttonText:qsTr("BATAL")
            modeReverse:true
            buttonColor: red1
            r_radius:2
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    cancelStatus = "notif_qr";
                    hide_qr_payment.start();
                    show_cancel_store.start();
                }
                onEntered:{
                    notifcancelqr.modeReverse = false
                }
                onExited:{
                    notifcancelqr.modeReverse = true
                }
            }
        }
        NotifButton{
            id: notifdoneqr
            x:311
            y:392
            buttonText:qsTr("SUDAH BAYAR")
            buttonColor: red1
            r_radius:2
            modeReverse:false
            visible: (step_choose == "ovo")? false : true
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    if(presspay == 0){
                        // dimm_display.visible=false
                        flagQr=false
                        slot_handler.start_check_trans_global() // dimatikan untuk testing timer
                        hide_qr_payment.start();
                        waiting.visible = true
                    }
                    presspay=1
                }
                onEntered:{
                    notifdoneqr.modeReverse = true
                }
                onExited:{
                    notifdoneqr.modeReverse = false
                }
            }
        }

    }

    NotifSwipe{
        id:notif_emoney_payment
        title_text:qsTr("Tempelkan Kartu Emoney Di Reader")
        // font_type: "SourceSansPro-SemiBold"
        title_bold: false
        titlebody_x:50
        body_y:60
        img_y:50
        source_img: asset_payment+"bground_mon.png"
        r_height: 550
        Text {
            id: txt_emoneyno
            x:50
            y:155
            text: qsTr("No Order") + "\t: " + no_order
            font.pixelSize:24
            color:"#414042"
            font.family: fontStyle.medium
        }
        Text {
            id: txt_emoneytotal
            x:50
            y:219
            text: qsTr("Jumlah") + "\t\t: " + qsTr("Rp ") + amount
            font.pixelSize:24
            color:"#414042"
            font.family: fontStyle.medium
        }
        Text {
            id: txt_emoneypay
            x:50
            y:291
            text: qsTr("Bayar")+ "\t:" 
            font.pixelSize:24
            color:"#414042"
            font.family: fontStyle.medium
        }
        Image{
            x:227
            y:282
            width:106
            height:60
            source: asset_payment+"emoney.png"
        }
        // Image{
        //     x:723
        //     y:182
        //     //width:250
        //     //height:232
        //     source: asset_payment+"bground_mon.png"
        // }
        // NotifButton{
        //     id: notifcancel
        //     x:51
        //     y:402
        //     buttonText:"BATAL"
        //     modeReverse:true
        //     buttonColor: red1
        //     r_radius:2
        //     MouseArea{
        //         anchors.fill: parent
        //         onClicked: {
        //             // my_stack_view.pop(null);
        //             cancelStatus = "emoney";
        //             show_cancel_store.start();
        //             dimm_display.visible = true;
        //         }
        //         onEntered:{
        //             notifcancel.modeReverse = false
        //         }
        //         onExited:{
        //             notifcancel.modeReverse = true
        //         }
        //     }
        // }

    }

    NotifSwipe{
        id:door_fail
        img_y:50
        source_img: "asset/send_page/background/unknown_popsafeCopy.PNG"
        title_text:qsTr("Open Door Failed")
        body_text:qsTr("Mohon maaf, untuk saat ini pintunya tidak dapat dibuka. <br>Terimakasih")
        NotifButton{
            id:btn_door_fail
            x:50
            y:337
            r_radius:2
            buttonText:qsTr("BATAL")
            modeReverse:false
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    hide_doorfail.start();
                    dimm_display.visible=false
                    press=0;
                    slot_handler.start_clear_transaction();
                    my_stack_view.pop(my_stack_view.find(function(item){return item.Stack.index==1}));
                    
                }
                onEntered:{
                    btn_door_fail.modeReverse = true
                }
                onExited:{
                    btn_door_fail.modeReverse = false
                }
            }
        }

        Image{
            x:939
            y:60
            height:33
            width:33
            source:"asset/global/button/close.png"
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    hide_doorfail.start();
                    dimm_display.visible=false
                }
            }
        }
    }

    NumberAnimation {
        id:hide_doorfail
        targets: door_fail
        properties: "y"
        from:248
        to:768
        duration: 500
        easing.type: Easing.InOutBack
    }
    
    NumberAnimation {
        id:show_doorfail
        targets: door_fail
        properties: "y"
        from:768
        to:248
        duration: 500
        easing.type: Easing.InOutBack
    }

    NotifSwipe{
        id:ovo_input_no_hp
        img_y:50
        r_height: 768
        // img_height: 768
        // source_img: "asset/send_page/background/unknown_popsafeCopy.PNG"
        title_text:"No. Handphone Kamu"
       
        Rectangle{
            id:phonebox
            x:50
            y: 130
            width: 620
            height: 80
            color: "transparent"
            border.color: '#8c8c8c'
            border.width: 3
            radius : 5
            // borderColor : "#c4c4c4"
            

            InputText{
                id:phone_number_ovo
                x:2
                y:2
                border.color: '#e8e8e8'
                // password: false
                // show_image:"img/apservice/courier/user_name.png"
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        if(press==0){
                            show_key.start();
                            ch=1
                            phone_number_ovo.bool = true
                            press=0
                            // courier_login_psw.bool = false
                        }
                    }
                }
            }

            Rectangle{
                id:ovo_info
                x:0
                y: 130
                width: 871
                height: 250
                color: "#e8e8e8"
                border.color: '#e8e8e8'
                border.width: 3
                radius : 5
                
                Text {
                    id:title_ovo
                    y:25
                    x:20
                    text: qsTr("<b>Info Pembayaran dengan Aplikasi OVO</b>")
                    font.pixelSize: 28
                    color:"#414042"
                    font.family: fontStyle.bold
                }
                Text {
                    id:isi_info_ovo
                    y:75
                    x:20
                    text: qsTr("1. Pastikan kamu sudah login dengan aplikasi OVO \n")
                    font.pixelSize: 22
                    color:"#414042"
                    font.family: fontStyle.book
                }
                Text {
                    id:isi_info_ovo_one
                    y:105
                    x:20
                    text: qsTr("2. Pembayaran dengan OVO akan kadaluarsa dalam 30 detik setelah kamu klik \"Lanjut\" \n")
                    font.pixelSize: 21
                    color:"#414042"
                    font.family: fontStyle.book
                }
                Text {
                    id:isi_info_ovo_two
                    y:140
                    x:20
                    text: qsTr("3. Buka notifikasi OVO untuk melakukan pembayaran \n")
                    font.pixelSize: 21
                    color:"#414042"
                    font.family: fontStyle.book
                }

                 Text {
                    id:isi_info_ovo_three
                    y:170
                    x:20
                    text: qsTr("4. Pilih cara pembayaran dengan \"OVO Cash\" atau\"OVO Point\" atau kombinasi \n keduanya, kemudian klik \"Bayar\" \n")
                    font.pixelSize: 21
                    color:"#414042"
                    font.family: fontStyle.book
                }
                
            }
        }

       Image{
            id: del_img
            x:613
            y:155
            width:36
            height:36
            visible:false
            source:asset_global + "button/delete.png"
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    phone_number_ovo.show_text = "";
                    real_phone_ovo = ""
                    countOvo=0;
                    touch_keyboard.input_text("")
                }
            }
        }

        NotifButton{
            id:nextButtonInputPhone
            x:690
            y:130
            buttonText:"LANJUT"
            buttonColor: "#8c8c8c"
            r_radius:0
            modeReverse:false
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    if (press!='0') return
                    press = '1'
                    if(phone_number_ovo.show_text != ""){
                        console.log('Phone Number : ', phone_number_ovo.show_text);
                        if(check_number.indexOf(phone_number_ovo.show_text.substring(0,4)) > -1 && phone_number_ovo.show_text.length > 9){
                            // show_verif01.start();
                            dimm_display.visible=true;
                            count = 0;
                            
                            masking_phone_ovo = phone_number_ovo.show_text

                            console.log("Begin push to pay ovo" + no_order)
                            customer_ovo = 'USER_ANDALAS||'+ real_phone_ovo +'||it@popbox.asia'
                            
                            console.log("phone_number: " + real_phone_ovo)
                            
                            slot_handler.start_create_payment_ovo(amount, customer_ovo, item, locker_name, no_order);
                            hide_input_hp_ovo.start()
                            hide_key.start()
                            show_check_ovo.start()
                            dimm_display.visible=true

                        }else{
                            dimm_display_front.visible= true
                            show_wrong_number.start()
                            
                        }
                    }
                    // else{
                    //     dimm_display.visible= true
                    //     show_wrong_number.start()
                    // }
                }
                onEntered:{
                    nextButton.modeReverse = true
                }
                onExited:{
                    nextButton.modeReverse = false
                }
            }
        }


        Image{
            x:939
            y:60
            height:33
            width:33
            source:"asset/global/button/close.png"
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    hide_input_hp_ovo.start();
                    hide_key.start();
                    dimm_display.visible=false
                }
            }
        }

        Image{
            x:30
            y:550
            height:70
            width:145
            source:"asset/payment/logo/ovo.png"
            // MouseArea{
            //     anchors.fill: parent
            //     onClicked: {
            //         hide_doorfail.start();
            //         dimm_display.visible=false
            //     }
            // }
        }
    }

    DimmDisplay{
        id:dimm_display_front
        visible:false
    }

    NumberAnimation {
        id:hide_input_hp_ovo
        targets: ovo_input_no_hp
        properties: "y"
        from:105
        to:768
        duration: 500
        easing.type: Easing.InOutBack
    }
    
    NumberAnimation {
        id:show_input_hp_ovo
        targets: ovo_input_no_hp
        properties: "y"
        from:768
        to:105
        duration: 500
        easing.type: Easing.InOutBack
    }

    KeyBoardAlphanumeric{
        id:touch_keyboard
        property var validate_c
        page_calling_keyboard: "select_payment"
        color_disable: "#8c8c8c"
        btn_enable: false
        Component.onCompleted: {
            touch_keyboard.letter_button_clicked.connect(input_text)
            // touch_keyboard.function_button_clicked.connect(on_function_button_clicked)
        }

        function input_text(str){
            if(ch==1){
                if (str == "" && phone_number_ovo.show_text.length > 0){
                    
                    console.log("tes del: ")
                    
                    
                    phone_number_ovo.show_text.length--
                    phone_number_ovo.show_text=phone_number_ovo.show_text.substring(0,phone_number_ovo.show_text.length-1)

                    real_phone_ovo.length--
                    real_phone_ovo=real_phone_ovo.substring(0,real_phone_ovo.length-1)
                    countOvo--;                 
                    console.log("phone_number_ovo.showtext : ", phone_number_ovo.show_text);
                    console.log("countOvo : ", countOvo);
                    console.log("real_phone_ovo : ", real_phone_ovo);
                }
                if(str == "OKE"){
                    str = "";
                    real_phone_ovo = ""
                    if(phone_number_ovo.show_text != "" && phone_number_ovo >= character){
                        // phone_number_ovo.show_text = "PLLIDTFKXFFK"
                        // slot_handler.checking_awb(phone_number_ovo.show_text)
                        console.log("Begin push to pay ovo")
                        slot_handler.start_create_payment_ovo(amount, customer, item, locker_name, no_order);
                        hide_key.start()
                    }
                }
                if (str != "" && phone_number_ovo.show_text.length< character){
                    phone_number_ovo.show_text.length++
                    countOvo++;
                }
                if (phone_number_ovo.show_text.length>=character){
                    str=""
                    phone_number_ovo.show_text.length=character
                }else{
                    if(countOvo <=9 && str != ""){
                        real_phone_ovo += str
                        phone_number_ovo.show_text += str
                    }else if (countOvo > 9 && str != ""){
                        real_phone_ovo += str
                        phone_number_ovo.show_text += "X"
                    }
                }
                if(countOvo>0){
                    nextButtonInputPhone.buttonColor = "#ff524f"
                    phone_number_ovo.border.color = "#ff7e7e"
                    del_img.visible=true
                } else{
                    nextButtonInputPhone.buttonColor = "#c4c4c4"
                    phone_number_ovo.border.color = "#8c8c8c"
                    del_img.visible=false
                }

            }
            
        }
    }

    NumberAnimation {
        id:hide_key
        targets: touch_keyboard
        properties: "y"
        from:368
        to:768
        duration: 500
        easing.type: Easing.InOutBack
    }
    NumberAnimation {
        id:show_key
        targets: touch_keyboard
        properties: "y"
        from:768
        to:368
        duration: 500
        easing.type: Easing.InOutBack
    }

    NotifSwipe{
        id:notif_wrongnumber
        img_y:50
        source_img: asset_path + "wrong_number.png"
        title_text:"Periksa Kembali No. Handphone \nKamu"
        body_text:"Mohon periksa kembali, format no. handphone yang \nkamu masukkan salah."
        NotifButton{
            id:ok_back
            x:50
            y:337
            buttonText:"KEMBALI"
            modeReverse:false
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    hide_animation("wrong_number");
                }
                onEntered:{
                    ok_back.modeReverse = true
                }
                onExited:{
                    ok_back.modeReverse = false
                }
            }
        }
    }

    NumberAnimation {
        id:hide_wrong_number
        targets: notif_wrongnumber
        properties: "y"
        from:287
        to:768
        duration: 500
        easing.type: Easing.InOutBack
    }
    NumberAnimation {
        id:show_wrong_number
        targets: notif_wrongnumber
        properties: "y"
        from:768
        to:287
        duration: 500
        easing.type: Easing.InOutBack
    }

    Component.onCompleted: {
        root.start_hour_overdue_result.connect(get_maximum_overdue)
        root.start_get_locker_name_result.connect(show_locker_name);
        root.start_create_payment_result.connect(create_trans_global_result);
        root.check_trans_global_result.connect(payment_transaction_result);
        root.choose_mouth_result.connect(open_door_locker)        
        root.mouth_number_result.connect(door_locker_choosen)
        root.update_apexpress_result.connect(result_update_extend);
        root.customer_take_express_result.connect(signal_result_take_express);
        root.start_global_payment_emoney_result.connect(deduct_emoney_result);
        root.apexpress_result.connect(store_data_parcel_result)
        root.ovo_no_transaction_result.connect(ovo_no_transaction)
        root.start_get_language_result.connect(locker_language);
    }

    Component.onDestruction: {
        root.start_hour_overdue_result.disconnect(get_maximum_overdue)
        root.start_get_locker_name_result.disconnect(show_locker_name);
        root.start_create_payment_result.disconnect(create_trans_global_result);
        root.check_trans_global_result.disconnect(payment_transaction_result);
        root.choose_mouth_result.disconnect(open_door_locker)   
        root.mouth_number_result.disconnect(door_locker_choosen)
        root.update_apexpress_result.disconnect(result_update_extend);
        root.customer_take_express_result.disconnect(signal_result_take_express);
        root.start_global_payment_emoney_result.disconnect(deduct_emoney_result);
        root.apexpress_result.disconnect(store_data_parcel_result)
        root.ovo_no_transaction_result.disconnect(ovo_no_transaction)
        root.start_get_language_result.disconnect(locker_language);
    }

    
    
    function ovo_no_transaction(transaction_id) {
        
        console.log("no_transaction_order: " + transaction_id)
        
        if (step_choose == "ovo") {
            no_order = transaction_id
        }
    }

    function show_locker_name(lockername) {
        
        console.log("Locker_name: " + lockername)
        if (lockername == "") {
            locker_name = "Locker PopBox" 
        } else {
            locker_name = lockername
        }
    }
    
    function get_maximum_overdue(param) {
        console.log(" overdue_time: " + param)
        durationOverdue = parseInt(param)
    }

    function hide_animation(param){
        if (param=="bayar") {
            hide_metode_bayar.start();
            dimm_display.visible=false;
           
        }
        if (param=="pay_fail") {
            hide_payment_failed.start();
            dimm_display.visible=false;
           
        }
        if (param=="pay_check") {
            hide_payment_check.start();
            dimm_display.visible=false;
        }
        
        if (param=="pay_choice") {
            hide_payment_choice.start();
            dimm_display.visible=false;
        }

        if (param=="pay_loading") {
            hide_loading_payment.start();
            dimm_display.visible=false;
        }
        // if(param=="pay_ovo")hide_confirm_ovo.start();
        if (param=="check_ovo") {
            hide_check_ovo.start();
            dimm_display.visible=false;
           
        } 
        if (param=="keyboard") {
            hide_key.start();
        }
        if (param=="wrong_number") {
            dimm_display.visible=false;
            hide_wrong_number.start();
            dimm_display_front.visible = false
        }
        waiting.visible = false;
        press=0;
    }
    
    function create_payment(payment_method) {
        console.log("metode_pembayaran: " + step_choose)
        
        if (payment_method == "gopay") {
            slot_handler.start_create_payment_gopay(amount, customer, item, locker_name)
            payment_method_name = "MID-GOPAY"
        } else if (payment_method == "yap") {
            slot_handler.start_create_payment_yap(amount, customer, item, locker_name)
        } else if (payment_method == "linkaja") {
            slot_handler.start_create_payment_tcash(amount, customer, item, locker_name)
            payment_method_name = "TCASH"
        } else if (payment_method == "otto") {
            slot_handler.start_create_payment_ottoqr(amount, customer, item, locker_name)
            payment_method_name = "OTTO-QR"
        } else if (payment_method == "emoney") {
            payment_method_name = "EMONEY"
            slot_handler.start_global_payment_emoney(amount);
        } else if (payment_method == "ovo"){
            payment_method_name = "OVO"
            // slot_handler.start_create_qr_ovo();
            slot_handler.generate_ovo_transaction();
        } else if (payment_method == "boost"){
            slot_handler.start_create_payment_boost(amount, customer, item, locker_name)
            payment_method_name = "BOOST"
        } else if (payment_method == "grabpay"){
            slot_handler.start_create_payment_grabpay(amount, customer, item, locker_name)
            payment_method_name = "GRABPAY"
        }
        
    }

    // Response payment request 
    function create_trans_global_result(response) {
        
        console.log("response_payment: " + response)
        flagQr = true
        paymentParam = response
        if(response=="ERROR"){
            console.log("JSON ERROR BOSSSS")
        }else{
            var result = JSON.parse(response)
            if (result.isSuccess == "true") {
                timer_ovo.stop()
                timer_ovo_notif.stop()
                waiting_payment.visible = false;

                if (step_choose != "emoney" && step_choose != "ovo") {
                    //hide animasi taruh sini
                    qr_image.source = "../qr-payment/" + result.payment_id + ".png"
                } else if (step_choose == "emoney" ) {
                    //&& result.status == "CREATED"
                    if (storeExtend || isExtending) {
                        console.log(" extend_pincode: " + validate_code)
                        slot_handler.start_update_apexpress(validate_code, JSON.stringify(result));
                        push_emoney_settlement(express_number)
                    } else {
                        // if (!flagQr){
                        //     timer_payment.start()
                        // }
                        timer_payment.start()
                        // waiting.visible = true
                        // dimm_display.visible=true
                         //dont delete this timer, this is  for open door triggered
                        // hide_emoney_payment.start();
                        show_payment_success.start();                    
                    }
                } else if (step_choose == "ovo") {
                    if (result.status == "success" || result.status == "PAID") {
                        // hide_qr_payment.start()
                        if (storeExtend || isExtending) {
                            console.log(" extend_pincode: " + validate_code)
                            dimm_display.visible=true
                            show_payment_success.start();
                            hide_check_ovo.start()
                            slot_handler.start_update_apexpress(validate_code, JSON.stringify(result));
                        }else{
                            timer_payment.start() //dont delete this timer, this is  for open door triggered
                            //slot_handler.start_video_capture('store_popsafe_locker_'+parcel_number)
                            dimm_display.visible=true
                            show_payment_success.start();
                            hide_check_ovo.start()
                            // slot_handler.start_choose_mouth_size(box_size, "customer_store_express")
                        }
                    } else {
                        notif_payment_failed.body_text = result.message
                        // response_ovo = "Mohon maaf Anda telah melebihi batas waktu pembayaran, \nsilakan ulangi kembali pembayaran kamu.";
                        // notif_payment_failed.body_text = response_ovo;
                        console.log("RESPONSE OVO : ",result.message);
                        
                        dimm_display.visible=true
                        // hide_qr_payment.start()
                        show_payment_failed.start(); //contoh penggunaan pembayaran GAGAL
                        flagQr=false
                        hide_check_ovo.start()
                    }
                } else {
                    dimm_display.visible=true
                    show_payment_failed.start();
                    flagQr=false
                }
                no_order = result.transaction_id
            }else{
                if (step_choose == 'ovo'){
                    limitpush +=1
                    if (limitpush==5){
                        modelate = true
                        hide_check_ovo.start()
                        // show_qr_payment.start();
                    }
                    else if (limitpush==8){
                        hide_check_ovo.start()
                        dimm_display.visible=true
                        show_payment_failed.start();
                        flagQr=false
                        // hide_qr_payment.start();
                    } else {
                        hide_check_ovo.start()
                        dimm_display.visible=true
                        show_payment_failed.start();
                        flagQr=false
                        // hide_qr_payment.start();
                    }
                } else {
                    console.log("issukssess : FALSEEE")
                    dimm_display.visible=true
                    hide_qr_payment.start()
                    // dimm_display.visible=true
                    show_payment_failed.start(); //contoh penggunaan pembayaran GAGAL

                }
            }
        }
    }

    
    function payment_transaction_result(result) {
        var response = JSON.parse(result)
        if(status_loading==true) {
            hide_animation("pay_loading");
            status_loading = false;
        }
        if (response.isSuccess == "true") {
            if (response.status == "PAID"  || response.status == "SUCCESS") { //Bermasalah 
                slot_handler.start_clear_transaction();
                if (storeExtend) {
                    console.log(" extend_pincode: " + validate_code)
                    if(step_choose=="ovo" && response.status == "success"){
                        slot_handler.start_update_apexpress(validate_code, result);
                    } else {
                        
                        console.log("update_ap_express : ", result)
                        
                        slot_handler.start_update_apexpress(validate_code, result);
                    }
                } else {
                    if(!flagQr){
                        timer_payment.start()
                    }
                     //dont delete this timer, this is  for open door triggered
                    //slot_handler.start_video_capture('store_popsafe_locker_'+parcel_number)
                    dimm_display.visible=true
                    show_payment_success.start();
                    
                    if(step_choose=='ovo'){
                        hide_check_ovo.start();
                        slot_handler.start_choose_mouth_size(box_size, "customer_store_express");
                    }
                    //slot_handler.start_choose_mouth_size(box_size, "customer_store_express")
                }
            } else if(response.status == "pending" || response.status == "CREATED"){ // Gopay & Dana issue
                dimm_display.visible=true
                show_payment_check.start(); //contoh penggunaan pembayaran GAGAL
                flagQr=false
                waiting.visible =false;
            } else {
                dimm_display.visible=true
                show_payment_check.start(); //contoh penggunaan pembayaran GAGAL
                flagQr=false
                waiting.visible =false;
                if(step_choose=='ovo'){
                    hide_check_ovo.start()
                }
            }
        }else{
            if(step_choose=='ovo'){
                hide_check_ovo.start()
            }
            dimm_display.visible=true
            show_payment_check.start();
            flagQr=false
        }
        
    }

    
    function open_door_locker(result) {
        console.log("choose_mouth_result_ss: " + result)
        if (result == 'Success') {
            console.log("payment_param_open_door: " + paymentParam)
            slot_handler.start_store_apexpress(phone_number, durationOverdue, no_order, payment_method_name, amount, box_size, paymentParam)
        }
    }

    
    function store_data_parcel_result(argument) {
        console.log("store_data_parcel_result: " + argument)
        var result = JSON.parse(argument)
        survey_expressNumber = result.express
        survey_expressId = result.id_express
        if (result.isSuccess == "true") {
            waiting.visible = false;
            dimm_display.visible = false;
            if (step_choose == "emoney"){
                push_emoney_settlement(result.express)
            }
            // SCRIPT BERMASALAH , OPEN DOOR BERKALI KALI
            slot_handler.start_clear_transaction();
            my_stack_view.push(open_door, {nomorLoker: locker_number, door_type: door_type, header_title: qsTr("TITIP"), survey_expressNumber: survey_expressNumber, survey_expressId:survey_expressId, express_id: result.id_express, popsafe_status: "poptitip", access_door: true, button_type:3})

            console.log("TERPANGGIL : store_data_parcel_result <br> <br>")
            
        } else {
            // slot_handler.start_store_apexpress(phone_number, durationOverdue, no_order, payment_method_name, amount, box_size, paymentParam)
            // console.log("GAGAL DDD : store_data_parcel_result \n \n")
            dimm_display.visible = true
            show_doorfail.start()
        }
    }

    function door_locker_choosen(data) {
        var result = JSON.parse(data)
        if (result.isSuccess == "true") {
            locker_number = result.locker_number
            door_type = result.locker_number_size
            console.log(" locker_number_choosen: " + locker_number + "door_type : " + door_type)
        }
    }
    
    function result_update_extend(result) {
        console.log("result_update_extend: " + result)
        if (result=='Success') {
            var parse_overdue = JSON.parse(data_overdue)
            var data_taken = {
                "transactionRecord": parse_overdue.transactionId, 
                "extendTimes": parse_overdue.extend_overdue, 
                "paymentMethod": payment_method_name, 
                "overdueTime": parse_overdue.overdueTime, 
                "isExtend": true, 
                "duration": parse_overdue.parcel_duration, 
                "minutes_duration": parse_overdue.parcel_minutes_duration, 
                "cost_overdue": parse_overdue.cost_overdue, 
                "timestamp_duration": parse_overdue.date_overdue, 
                "extendPincode": validate_code, 
            }
            var send_data = JSON.stringify(data_taken) + "||" + validate_code 
            slot_handler.customer_take_express(send_data);
        } else {
            console.log("hasil update extend" + result)
        }
    }

    
    function signal_result_take_express(result) {
        console.log("signal_result_take_express: " + result)
        
    }

    function compressor(data){
        if (data == "gopay"){
            gopay.modeReverse = true
            gopay.buttonColor = red1
        }else{
            gopay.buttonColor = grey2
            gopay.modeReverse = false
        }
        if (data =="emoney"){
            emoney.buttonColor = red1
            emoney.modeReverse = true
        }else{
            emoney.buttonColor = grey2
            emoney.modeReverse = false
        }
        if(data =="linkaja"){
            linkaja.buttonColor = red1
            linkaja.modeReverse = true
        }else{
            linkaja.buttonColor = grey2
            linkaja.modeReverse = false
        }
        if(data=="otto"){
            ottopay.buttonColor = red1
            ottopay.modeReverse = true
        }else{
            ottopay.buttonColor = grey2
            ottopay.modeReverse = false
        }
        if(data=="ovo"){
            ovo.buttonColor = red1
            ovo.modeReverse = true
        }else{
            ovo.buttonColor = grey2
            ovo.modeReverse = false
        }if(data=="boost"){
            boost.buttonColor = red1
            boost.modeReverse = true
        }else{
            boost.buttonColor = grey2
            boost.modeReverse = false
        }
        if(data=="grabpay"){
            grabpay.buttonColor = red1
            grabpay.modeReverse = true
        }else{
            grabpay.buttonColor = grey2
            grabpay.modeReverse = false
        }
        // payprocessnotif.buttonColor = red1
        // payprocessnotif.enabled=true
        logopayment = asset_payment+data+".png"
        qrcode_ground.source = asset_payment+data+".png"
        qrcode_ground_success.source = asset_payment+data+".png"
        choose_button.visible = false
    }

    function deduct_emoney_result(result){
        // var result = '{"card_no": "12345678987654321", "locker": "KAI Kemayoran", "terminal_id": "03260100", "last_balance": "99999", "show_date": 1563355597000, "raw": "60000123123123123123123123123123123123123|1563355597000"}';
        console.log('emoney_payment_result', result);

        if(result == "UNFINISHED"){
            trial_count += 1;
            payment_page.close();
            nonfinished_notif.open();
            show_timer_payment.stop();
            show_timer_nf.restart();
            ok_button.enabled = false;
            trial_count_text.text = trial_count;
            time_nf.counter = show_timer_value_nf;
            if(trial_count >= 4){
                my_timer.stop();
                slot_handler.start_clear_transaction();
                my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }));
            }
        }else if(result == "WRONG-CARD" || result == "ERROR" || result == "FAILED"){
            hide_emoney_payment.start();
            dimm_display.visible=true
            show_payment_failed.start(); //contoh penggunaan pembayaran GAGAL

            return
        }

        var obj = JSON.parse(result)
        if(obj.length <= 0){
            hide_emoney_payment.start();
            dimm_display.visible=true
            show_payment_failed.start(); //contoh penggunaan pembayaran GAGAL
            return
        }else{
            hide_emoney_payment.start();
            waiting.visible =true
            paymentResult = result;
            var item = 'LOCAL_DEPOSIT @ ' + locker_name + ' : ' + door_no;
            
            console.log("LOCAL DEPOSIT DOOR NO: " , item)
            
            var customer = 'USER_LOCKER||'+phone_number+'||a@a.com';
            slot_handler.start_create_payment_emoney(amount, customer, item, locker_name, result)
            
            console.log(" isExtending____: " + isExtending)
            
            if (isExtending==false || storeExtend == false){
                if (testingMode==true){
                    slot_handler.start_choose_mouth_size("M","staff_store_express");
                } else {
                    var locker_size = (box_size == "" || box_size == null) ? box_size : door_type;
                }
            }
        }
    }

    function push_emoney_settlement(awb) {
        console.log("data_push_settlement: " + phone_number +'-'+ awb +'-'+ 'USER_LOCKER' )
        slot_handler.start_push_data_settlement(phone_number +'-'+ awb +'-'+ 'USER_LOCKER');
    }

    function locker_language(result){
        // console.log("locker_language : ",result)
        language = result;
        if(language=="MY"){
            // console.log("bahasa : Malaysia")
            payment_my();
        }else{
            // console.log("bahasa  = Indonesia")
            payment_id();
        }
    }

    function payment_my() {
        boost.visible = true;
        grabpay.visible = true;
        emoney.visible = false;
        gopay.visible = false;
        ovo.visible = false;
        ottopay.visible = false;
        linkaja.visible = false;
    }

    function payment_id() {
        boost.visible = false;
        grabpay.visible = false;
        emoney.visible = true;
        gopay.visible = true;
        ovo.visible = true;
        ottopay.visible = true;
        linkaja.visible = true;
    }

    function lanjut_payment_notif(){
        hide_metode_bayar.start()
        dimm_display.visible=false
        press=0;
        nextButton.color = red1
        nextButton.enabled = true
    }

}
