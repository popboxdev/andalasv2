import QtQuick 2.4
import QtQuick.Controls 1.2

Background{
    property var press: '0'
    // property var path: '/advertisement/apservice/'
    property var path: ''
    property int num_pic: 0
    property variant qml_pic: []
    property variant pic_source: []
    // property var img_path: '..' + path
    property var img_path
    property var language: 'ID'
    property var path_tvc: ''
    property var path_kirim: ''
    property var path_titip: ''
    property var path_ambil: ''
    property var path_kirim_click: ''
    property var path_titip_click: ''
    property var path_ambil_click: ''
    property var path_logistik: ''
    property var path_poppy: ''
    property var asset_path: 'asset/home_page/'
    property var path_button: 'asset/home_page/button15/'
    property var path_background: 'asset/home_page/background/'
    property var clock_color: "#595353"
    property bool availableUse: false
    property bool slider_status: false
    property bool tvc_status: false
    property var size_XS: 0
    property var size_S: 0
    property var size_M: 0
    property var size_L: 0
    property var size_XL: 0
    property var doorClick: 0
    property int tvc_timeout: 0
    property bool vss_mode: true // To control 15 inch VSS Scrensaver mode
    property bool button_active : false
    property var count_: 0
    property int timer_scanner: 20
    property bool scanner_status: false
    property var status_qr: "HOME"
    // property var pin_code
    // property var status_page
    img_vis : true
    show_img: 'asset/base/background/bg.png'
    property var contactless_status: "disable"

    Stack.onStatusChanged:{
        if(Stack.status == Stack.Activating){
            status_qr = "HOME";
            scanner_restart.stop();
            button_active = false;
            if(vss_mode==true){
                slider_status = false;
                tvc_status = false;
            }
            // slot_handler.start_get_file_banner(path);
            slot_handler.start_get_locker_name();
            slot_handler.start_get_language();
            slot_handler.start_get_free_mouth_mun();
            slot_handler.start_get_contactless();
            timer_clock.start();
            timer_startup.start();
            slot_handler.start_idle_mode();
            slot_handler.set_logout_user();
            slider_timer.start();
            press = '0';
            doorClick = 0;
        }
        if(Stack.status==Stack.Deactivating){
            scanner_restart.stop();
            slot_handler.stop_courier_scan_barcode();
            show_tvc_loading.stop();
            timer_clock.stop();
            slider_timer.stop();
            timer_startup.stop();
        }
    }

    Component.onCompleted: {
        root.start_get_gui_version_result.connect(get_gui_version);
        root.start_get_file_banner_result.connect(init_images);
        root.start_get_locker_name_result.connect(show_locker_name);
        root.free_mouth_result.connect(show_free_mouth_num);
        root.start_get_tvc_timer_result.connect(tvc_timer);
        root.maintenance_status_result.connect(maintenance_status);
        root.start_get_language_result.connect(locker_language);
        root.barcode_result.connect(handle_text)
        root.start_get_contactless_result.connect(contactless_result);
    }

    Component.onDestruction: {
        root.start_get_gui_version_result.disconnect(get_gui_version);
        root.start_get_file_banner_result.disconnect(init_images);
        root.start_get_locker_name_result.disconnect(show_locker_name);
        root.free_mouth_result.disconnect(show_free_mouth_num);
        root.start_get_tvc_timer_result.disconnect(tvc_timer);
        root.maintenance_status_result.disconnect(maintenance_status);
        root.start_get_language_result.disconnect(locker_language);
        root.barcode_result.disconnect(handle_text)
        root.start_get_contactless_result.disconnect(contactless_result);
    }

    FontType{
        id:fontStyle
        //Type Font ### bold , extrabold , light , book, medium, black ###
    }

    Loader {
        id: slider
        x: 0
        y: 0
        width: parent.width
        height: 310
    }

    Component {
        id: component
        Rectangle {
            id:banner_img
            AnimatedImage {
                id:ad_pic
                source: (slider_status==true) ? pic_source : ""
                anchors.fill: parent
                fillMode: Image.PreserveAspectCrop
            }
        }
    }

    Image{
        id: bg_logo
        y:250
        width:parent.width
        height:300
        source:asset_path + "button/rec_logo.png"
    }

    // Image{
    //     id: rlogo_showing
    //     x:40
    //     y:325
    //     visible: true
    //     width:125
    //     height:40
    //     source:"asset/base/logo/popbox.png"
    // }

    Text {
        id: gui_version
        x: 40
        y: 748
        height: 40
        width: 150
        text: "-"
        font.family: fontStyle.light
        font.pixelSize:10
        color:"GREY"
    }

    Text {
        id: locker_name_text
        x: 494
        y: 315
        height: 40
        width: 207
        text: "POPBOX LOCKER" //Maks Nama Locker 35 Karakter
        font.family: fontStyle.bold
        font.pixelSize:21
        color:"#404040"
        // MouseArea{
        //     anchors.fill: parent
        //     onClicked: {
        //         doorClick++;
        //         if(doorClick>=5){
        //             my_stack_view.push(input_door_page);
        //             doorClick=0;
        //         }
        //     }
        // }
    }

    Image{
        id: clock_base_img
        y: 20
        width: parent.width
        height: 42
        // source: path_background + "clock_base.png"
        Text {
            id: timeText
            text: ""
            anchors.verticalCenter: parent.verticalCenter
            anchors.right: parent.right
            anchors.rightMargin:45
            font.family: fontStyle.medium
            font.pixelSize: 16
            color: clock_color
        }
    }

    Image{
        id: btn_logistik
        x:40
        y:315
        width: 432
        height: 86
        source: (button_active==true) ? path_button + path_logistik : ""
        enabled: true
        // Text{
        //     color: "#ffffff"
        //     font.pixelSize: 17
        //     font.family: fontStyle.medium
        //     text:"LOGIN KURIR"
        //     anchors.verticalCenter: parent.verticalCenter
        //     anchors.horizontalCenter: parent.horizontalCenter
        // }
        MouseArea{
            anchors.fill: parent
            onClicked: {
                console.log("courier login pressed")
                if (press == '0'){
                    stop_scanner();
                    if(language=="MY"){
                        my_stack_view.push(subscribe_page);
                    }else{
                        my_stack_view.push(courier_login_page,{identity:"LOGISTICS_COMPANY_USER"});
                    }
                    slot_handler.stop_idle_mode();
                }
                press = '1'
            }
        }
    }    

    // Image{
    //     id: btn_showlocker
    //     x:555
    //     y:330
    //     width: 300
    //     height: 37
    //     source: path_button + "btn_showlocker.png"
    //     Text{
    //         color: "#ffffff"
    //         font.pixelSize: 18
    //         font.family: fontStyle.book
    //         text:"XS : "+size_XS+" | S : "+size_S+" | M : "+size_M+" | L : "+size_L+" | XL : "+size_XL+""
    //         // text:"XS : "+"99"+" | S : "+"99"+" | M : "+"99"+" | L : "+"9"+" | XL : "+"9"+""
    //         anchors.verticalCenter: parent.verticalCenter
    //         anchors.horizontalCenter: parent.horizontalCenter
    //     }
    // }  

    Rectangle{
        id: text_available
        border.width: 1
        border.color: "#c0bdbd"
        radius: 3
        x: 494
        y: 355
        width: 171
        height: 38
        color: "#f4f5f5"
        Text{
            color: "#525252"
            font.pixelSize: 14
            font.family: fontStyle.medium
            text:qsTr("Ketersediaan Loker")
            anchors.verticalCenter: parent.verticalCenter
            anchors.horizontalCenter: parent.horizontalCenter
        }
        MouseArea {
            anchors.fill: parent
            onClicked: {
                count_ +=1;
                if (count_==5){
                    my_stack_view.push(under_maintenance,{language: language})
                    count_ = 0;
                }
                
            }
        }
    }

    Rectangle{
        id: locker_available
        border.width: 1
        border.color: "#c0bdbd"
        radius: 3
        x: 664
        y: 355
        width: 320
        height: 38
        color: "#ffffff"
        Text{
            color: "#717171"
            font.pixelSize: 16
            font.family: fontStyle.medium
            text:"XS : "+size_XS+"     S : "+size_S+"     M : "+size_M+"     L : "+size_L+"     XL : "+size_XL+""
            // text:"XS : "+"99"+" | S : "+"99"+" | M : "+"99"+" | L : "+"9"+" | XL : "+"9"+""
            anchors.verticalCenter: parent.verticalCenter
            anchors.horizontalCenter: parent.horizontalCenter
        }

    }

    Rectangle{
        id: btn_kirim
        x:40
        y:428
        Image{
            id:img_kirim
            // source: path_button + ((language=='MY') ? "kirim_my.png" : "kirim.png")
            source: (button_active==true) ? path_button + path_kirim : ""
            // height: 374
            // width: 334
            height: 315
            width: 300
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    if (language=="MY") {
                        if (press==0){
                            stop_scanner();
                            my_stack_view.push(courier_login_page,{identity:"LOGISTICS_COMPANY_USER"})
                            slot_handler.stop_idle_mode();
                            press=1
                        }
                    }else{
                        if(availableUse){
                            if (press==0){
                                stop_scanner();
                                my_stack_view.push(send_parcel, {show_img: show_img})
                                slot_handler.stop_idle_mode();
                                press=1
                            }
                        }else{
                            dimm_display.visible=true;
                            show_available.start(); 
                        }
                    }
                }
                onEntered:{
                    img_kirim.source= (button_active==true) ? path_button + path_kirim_click : ""
                }
                onExited:{
                    img_kirim.source= (button_active==true) ? path_button + path_kirim : ""
                }
            }
        }
    }

    Rectangle{
        id: btn_titip
        // x:362
        x:362
        y:428
        Image{
            id:img_titip
            source: (button_active==true) ? path_button + path_titip : ""
            height: 315
            width: 300
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    if(availableUse){
                        if (press==0){
                            stop_scanner();
                            console.log("btn_titip pressed")
                            my_stack_view.push(store_parcel, {show_img: show_img, language:language})
                            slot_handler.stop_idle_mode();
                            press=1
                        }
                    }else{
                        dimm_display.visible=true;
                        show_available.start(); 
                    }
                }
                onEntered:{
                    img_titip.source= (button_active==true) ? path_button + path_titip_click : ""
                }
                onExited:{
                    img_titip.source= (button_active==true) ? path_button + path_titip : ""
                }
            }
        }
    }

    Rectangle{
        id: btn_ambil
        x:683
        y:428
        Image{
            id: img_ambil
            source: (button_active==true) ? path_button + path_ambil : ""
            height: 315
            width: 300
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    if(press==0){
                        stop_scanner();
                        console.log("btn_ambil pressed")
                        slot_handler.stop_courier_scan_barcode();
                        my_stack_view.push(take_parcel, {language: language});
                        // my_stack_view.push(take_parcel, {language: language, pin_code: "123456", contactless: true,qr_content: "123456"});
                        slot_handler.stop_idle_mode();
                        press=1
                    }
                }
                onEntered:{
                    img_ambil.source= (button_active==true) ? path_button + path_ambil_click : ""
                }
                onExited:{
                    img_ambil.source= (button_active==true) ? path_button + path_ambil : ""
                }
            }
        }
    }

    Image{
        id: poppy_icon
        x:684
        y:681
        width:300
        height:95
        source: (button_active==true) ? path_poppy : ""
        MouseArea{
            anchors.fill: parent
                onClicked: {
                    stop_scanner();
                    my_stack_view.push(contact_us,{language: language});
                }
        }
    }

    // AnimatedImage{
    //     id: popy_live
    //     // visible:timer_view
    //     width: 120
    //     height: 120
    //     x:850
    //     y:660
    //     source: 'asset/home_page/background/popy.gif'
    //     fillMode: Image.PreserveAspectFit
    //     Text{
    //         id:text_anim
    //         color:"#f15f78"
    //         anchors.verticalCenter: parent.verticalCenter
    //         anchors.horizontalCenter: parent.horizontalCenter
    //         // text: timer_countdown
    //         font.pixelSize:18
    //         font.family: fontStyle.medium
    //     }

    //     MouseArea{
    //         anchors.fill: parent
    //             onClicked: {
    //                 my_stack_view.push(contact_us);
    //             }
    //     }
    // }

    Rectangle{
        id: timer_tvc
        width: 10
        height: 10
        x:0
        y:0
        visible: false
        QtObject{
            id:tvc_loading
            property int counter
            Component.onCompleted:{
                tvc_loading.counter = tvc_timeout
            }
        }
        Timer{
            id:show_tvc_loading
            interval:1000
            repeat:true
            running:false
            triggeredOnStart:true
            onTriggered:{
                tvc_loading.counter -= 1
                if(tvc_loading.counter == 0){
                    slider_status = false;
                    tvc_status = true;
                    show_tvc_loading.stop();
                    console.log("starting tvc player...")
                    my_stack_view.push(ad_page, {mode:"mediaPlayer", tvc_timeout: tvc_timeout,img_path: path_tvc, language: language, contactless_status: contactless_status})
                }
            }
        }
    }

    Timer {
        id: scanner_restart
        interval: 1000
        repeat: true
        running: true
        onTriggered:{   
            timer_scanner -=1;
            // console.log("timer_scanner = ", timer_scanner);
            if(timer_scanner == 10){
                slot_handler.stop_courier_scan_barcode();
            }
            if(timer_scanner == 0){
                timer_scanner = 20;
                slot_handler.start_courier_scan_barcode();
            }
        }
    }


    Timer {
        id: timer_startup
        interval: 1000
        repeat: false
        running: true
        onTriggered:{   
            slot_handler.start_get_locker_name();
            slot_handler.start_get_free_mouth_mun();
            slot_handler.start_get_tvc_timer();
            slot_handler.start_get_language();
            slot_handler.start_get_gui_version();
            slot_handler.start_get_contactless();
            if(path==''){
                timer_startup.start();
            }else{
                slot_handler.start_get_file_banner(path);
            }
        }
    }

    Timer {
        id: timer_clock
        interval: 1000
        repeat: true
        running: true
        onTriggered:{   
            var tanggal
            if(language=="MY"){
                tanggal = new Date().toLocaleDateString(Qt.locale("en_US"), Locale.LongFormat) + "  " + new Date().toLocaleTimeString(Qt.locale("en_US"), "hh:mm:ss") //MY Locale
            }else{
                tanggal = new Date().toLocaleDateString(Qt.locale("id_ID"), Locale.LongFormat) + "  " + new Date().toLocaleTimeString(Qt.locale("id_ID"), "hh:mm:ss")
            }
            timeText.text = tanggal.toUpperCase()
            press = '0';
        }
    }

    Timer{
        id:slider_timer
        interval:5000
        repeat:true
        running:false
        triggeredOnStart:false
        onTriggered:{
            if(num_pic < qml_pic.length){
                num_pic += 1;
                if(num_pic == qml_pic.length){
                    slider_timer.restart();
                    slider.sourceComponent = slider.Null;
                    pic_source = img_path + qml_pic[0];
                    slider.sourceComponent = component;
                    num_pic = 0;
                }else{
                    slider_timer.restart();
                    slider.sourceComponent = slider.Null;
                    pic_source = img_path + qml_pic[num_pic];
                    slider.sourceComponent = component;
                }
            }
        }
    }

    DimmDisplay{
        id:dimm_display
        visible:false
    }

    NotifSwipe{
        id:notif_available
        source_img: asset_path + "background/full.png"
        img_y:50
        titlebody_x:50
        title_y:60
        body_y:180
        title_text:qsTr("Yahh ,, Lokernya Sudah Penuh")
        body_text:qsTr("Silakan mencoba lagi beberapa saat lagi.")

        NotifButton{
            id:ok_yes
            x:50
            y:351
            r_radius:2
            buttonText:qsTr("YA")
            modeReverse:false
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    hide_available.start();
                    dimm_display.visible=false;
                }
                onEntered:{
                    ok_yes.modeReverse = true
                }
                onExited:{
                    ok_yes.modeReverse = false
                }
            }
        }

        Image{
            x:939
            y:60
            height:33
            width:33
            source:"asset/global/button/close.png"
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    hide_available.start();
                    dimm_display.visible=false;
                }
            }
        }
    }

    NumberAnimation {
        id:hide_available
        targets: notif_available
        properties: "y"
        from:248
        to:768
        duration: 500
        easing.type: Easing.InOutBack
    }
    NumberAnimation {
        id:show_available
        targets: notif_available
        properties: "y"
        from:768
        to:248
        duration: 500
        easing.type: Easing.InOutBack
    }

    function maintenance_status(rest_){
        if (rest_ == "True"){
            my_stack_view.push(under_maintenance,{language: language});
        }
    }

    function get_gui_version(vers){
        gui_version.text = "Version : " + vers
    }

    function init_images(result){
        console.log(result);
        if(result=='') return;
        var i = JSON.parse(result);
        qml_pic = i.output;
        slider.sourceComponent = slider.Null;
        pic_source = img_path + qml_pic[0];
        slider.sourceComponent = component;
        if(tvc_status==true){
            slider_status=false;
        }else{
            slider_status=true;
        }
    }


    function show_locker_name(text){
        console.log("locker_name" + text)
        if(text == ""){
            return
        }
        locker_name_text.text = text.toUpperCase()
    }

    function show_free_mouth_num(locker_size){
        var obj = JSON.parse(locker_size)
        for(var i in obj){
            if(i == "XL"){
                size_XL = obj[i]
            }
            if(i == "L"){
                size_L = obj[i]
            }
            if(i == "M"){
                size_M = obj[i]
            }
            if(i == "S"){
                size_S = obj[i]
            }
            if(i == "MINI"){
                size_XS = obj[i]
            }
        }
        if (size_XL <= 0 && size_L <= 0 && size_M <= 0 && size_S <= 0 && size_XS <=0 ){
            availableUse = false;
        }else{
            availableUse = true;
        }
    }

    
    function tvc_timer(result) {
        console.log("Result TVC TIMER: ", result)
        if(result>0){
            tvc_timeout = result
            if(vss_mode==true){
                tvc_loading.counter = tvc_timeout
                show_tvc_loading.start()
            }
            console.log("Diatas 0 : ", tvc_timeout)
        }else{
            tvc_timeout = 0
            console.log("TVC ==  0 : ", tvc_timeout)
        }
    }

    function contactless_result(result) {
        contactless_status = result;
        // console.log("Get Contactless", contactless_status);
        
        if(contactless_status=="enable"){
            slot_handler.start_courier_scan_barcode();
            scanner_status = true;
            scanner_restart.restart();
        }else{
            scanner_status = false;
            scanner_restart.stop();
        }
    }

    function locker_language(result){
        // console.log("locker_language : ",result)
        language = result;
        if(language=="MY"){
            // console.log("bahasa : Malaysia")
            change_my();
        }else{
            // console.log("bahasa  = Indonesia")
            change_id();
        }
        button_active = true;
    }

    function change_my(){
        path = '/advertisement/myservice/'
        path_tvc = '/advertisement/myvideo/'
        img_path = '..' + path;
        path_poppy = "asset/home_page/background/poppy_my.png";
        poppy_icon.width = 330;
        path_kirim = "kirim_my.png";
        path_titip = "titip_my.png";
        path_ambil = "ambil_my.png";
        path_kirim_click = "kirimClick_my.png";
        path_titip_click = "titipClick_my.png";
        path_ambil_click = "ambilClick_my.png";
        path_logistik = "logistik_my.png";
        btn_kirim.x = 362;
        btn_titip.x = 683;
        btn_ambil.x = 40;
        // btn_logistik.enabled = false;
    }

    function change_id(){
        path = '/advertisement/apservice/'
        path_tvc = '/advertisement/video/'
        img_path = '..' + path;
        path_poppy = "asset/home_page/background/poppy.png";
        path_kirim = "kirim.png";
        path_titip = "titip.png";
        path_ambil = "ambil.png";
        path_kirim_click = "kirimClick.png";
        path_titip_click = "titipClick.png";
        path_ambil_click = "ambilClick.png";
        path_logistik = "logistik.png";
        // btn_logistik.enabled = true;
    }

    function handle_text(text){
        slot_handler.stop_courier_scan_barcode();
        if(status_qr=="HOME"){
            check_pattern(text);
        }else{
            console.log("not me");
        }
    }

    function check_pattern(barcode_text){
        var input = barcode_text;
        var result;
        var page;
        var code;
        var length_string = input.length;
        var sc_separator = input.search("#");
        if(sc_separator > -1){
            result = input.split("#");
            page = result[0];
            code = result[1];
        }else{
            code = barcode_text;
        }
        if((page=="AMBIL" || length_string==6)){
            if(scanner_status==true){
                stop_scanner();
                my_stack_view.push(take_parcel, {language: language, pin_code: code, contactless: true,qr_content: barcode_text});
                scanner_status = false;
            }
        }else{
            console.log("KIRIM");
            slot_handler.start_courier_scan_barcode();
        }
        barcode_text = "";
        result = "";
        page = "";
        code = "";
    }

    function stop_scanner() {
        slot_handler.stop_courier_scan_barcode();
        scanner_restart.stop();
        status_qr = "";
    }
}
