import QtQuick 2.4
import QtQuick.Controls 1.2

Background{
    property var press: '0'
    property var asset_path: 'asset/home_page/'
    property var path_button: 'asset/home_page/button32/'
    property var path_background: 'asset/home_page/background/'
    property var clock_color: "#595353"
    property bool availableUse: false
    property var size_XS: 0
    property var size_S: 0
    property var size_M: 0
    property var size_L: 0
    property var size_XL: 0
    property int tvc_timeout: 60
    property bool vss_mode: true // To control 15 inch VSS Scrensaver mode
    property var count_: 0
    property var language: 'ID'
    property int timer_scanner: 20
    property bool scanner_status: true
    property var status_qr: "HOME"
    img_vis : true
    show_img: 'asset/base/background/bg.png'
    property var contactless_status: "disable"

    Stack.onStatusChanged:{
        if(Stack.status == Stack.Activating){
            status_qr = "HOME";
            scanner_restart.stop();
            slot_handler.start_get_locker_name();
            slot_handler.start_get_language();
            slot_handler.start_get_free_mouth_mun();
            slot_handler.start_get_contactless();
            timer_clock.start();
            timer_startup.start();
            slot_handler.start_idle_mode();
            press = '0';
        }
        if(Stack.status==Stack.Deactivating){
            scanner_restart.stop();
            show_tvc_loading.stop()
            timer_clock.stop();
            timer_startup.stop();
        }
    }

    Component.onCompleted: {
        root.start_get_gui_version_result.connect(get_gui_version);
        root.start_get_locker_name_result.connect(show_locker_name);
        root.free_mouth_result.connect(show_free_mouth_num);
        root.start_get_tvc32_timer_result.connect(tvc_timer);
        root.maintenance_status_result.connect(maintenance_status);
        root.start_get_language_result.connect(locker_language);
        root.barcode_result.connect(handle_text)
        root.start_get_contactless_result.connect(contactless_result);
    }

    Component.onDestruction: {
        root.start_get_gui_version_result.disconnect(get_gui_version);
        root.start_get_locker_name_result.disconnect(show_locker_name);
        root.free_mouth_result.disconnect(show_free_mouth_num);
        root.start_get_tvc32_timer_result.disconnect(tvc_timer);
        root.maintenance_status_result.disconnect(maintenance_status);
        root.start_get_language_result.disconnect(locker_language);
        root.barcode_result.disconnect(handle_text)
        root.start_get_contactless_result.disconnect(contactless_result);
    }

    FontType{
        id:fontStyle
        //Type Font ### bold , extrabold , light , book, medium, black ###
    }

    Image{
        id: rlogo_showing
        x:53
        y:28
        visible: true
        width:140
        height:43
        source:"asset/base/logo/popbox.png"
    }

    // Text {
    //     id: locker_name_text
    //     x: 70
    //     y: 155
    //     height: 40
    //     width: 207
    //     text: "POPBOX LOCKER"
    //     font.family: fontStyle.medium
    //     font.pixelSize:20
    //     color:"#404040"
    //     // MouseArea{
    //     //     anchors.fill: parent
    //     //     onClicked: {
    //     //         my_stack_view.push(input_door_page)
    //     //     }
    //     // }
    // }

    Text {
        id: gui_version
        x: 53
        y: 728
        height: 40
        width: 150
        text: "-"
        font.family: fontStyle.light
        font.pixelSize:10
        color:"GREY"
    }

    Text {
        id: locker_name_text
        x: 534
        y: 110
        height: 40
        width: 207
        text: "POPBOX LOCKER"
        font.family: fontStyle.bold
        font.pixelSize:21
        color:"#404040"
        // MouseArea{
        //     anchors.fill: parent
        //     onClicked: {
        //         doorClick++;
        //         if(doorClick>=5){
        //             my_stack_view.push(input_door_page);
        //             doorClick=0;
        //         }
        //     }
        // }
    }

    // Image{
    //     id: clock_base_img
    //     width: parent.width
    //     height: 42
    //     source: path_background + "clock_base32.png"
    //     Text {
    //         id: timeText
    //         text: ""
    //         anchors.verticalCenter: parent.verticalCenter
    //         anchors.right: parent.right
    //         anchors.rightMargin:18
    //         font.family: fontStyle.medium
    //         font.pixelSize: 18
    //         color: clock_color
    //     }
    // }

    Image{
        id: clock_base_img
        y: 20
        width: parent.width
        height: 42
        // source: path_background + "clock_base.png"
        Text {
            id: timeText
            text: ""
            anchors.verticalCenter: parent.verticalCenter
            anchors.right: parent.right
            anchors.rightMargin:45
            font.family: fontStyle.medium
            font.pixelSize: 16
            color: clock_color
        }
    }

    Image{
        id: btn_logistik
        x:50
        y:99
        width: 476
        height: 100
        source: path_button + "logistik.png"
        // Text{
        //     color: "#ffffff"
        //     font.pixelSize: 17
        //     font.family: fontStyle.medium
        //     text:"LOGIN KURIR"
        //     anchors.verticalCenter: parent.verticalCenter
        //     anchors.horizontalCenter: parent.horizontalCenter
        // }
        MouseArea{
            anchors.fill: parent
            onClicked: {
                console.log("courier login pressed")
                if (press == '0'){
                    stop_scanner();
                    my_stack_view.push(courier_login_page,{identity:"LOGISTICS_COMPANY_USER"})
                    slot_handler.stop_idle_mode();
                }
                press = '1'
            }
        }
    }

    // Image{
    //     id: btn_showlocker
    //     x:540
    //     y:90
    //     width: 325
    //     height: 50
    //     source: path_button + "btn_showlocker.png"
    //     Text{
    //         color: "#ffffff"
    //         font.pixelSize: 18
    //         font.family: fontStyle.book
    //         text:"XS : "+size_XS+" | S : "+size_S+" | M : "+size_M+" | L : "+size_L+" | XL : "+size_XL+""
    //         // text:"XS : "+"99"+" | S : "+"99"+" | M : "+"99"+" | L : "+"9"+" | XL : "+"9"+""
    //         anchors.verticalCenter: parent.verticalCenter
    //         anchors.horizontalCenter: parent.horizontalCenter
    //     }
    // } 

    Rectangle{
        id: text_available
        border.width: 1
        border.color: "#c0bdbd"
        radius: 3
        x: 534
        y: 151
        width: 171
        height: 38
        color: "#f4f5f5"
        Text{
            color: "#525252"
            font.pixelSize: 14
            font.family: fontStyle.medium
            text:qsTr("Ketersediaan Loker")
            anchors.verticalCenter: parent.verticalCenter
            anchors.horizontalCenter: parent.horizontalCenter
        }
        MouseArea {
            anchors.fill: parent
            onClicked: {
                count_ +=1;
                if (count_==5){
                    // slot_handler.start_get_screen_resolution()
                    my_stack_view.push(under_maintenance,{language: language})
                    count_ = 0;
                }
                
            }
        }

    }

    Rectangle{
        id: locker_available
        border.width: 1
        border.color: "#c0bdbd"
        radius: 3
        x: 704
        y: 151
        width: 320
        height: 38
        color: "#ffffff"
        Text{
            color: "#717171"
            font.pixelSize: 16
            font.family: fontStyle.medium
            text:"XS : "+size_XS+"     S : "+size_S+"     M : "+size_M+"     L : "+size_L+"     XL : "+size_XL+""
            // text:"XS : "+"99"+" | S : "+"99"+" | M : "+"99"+" | L : "+"9"+" | XL : "+"9"+""
            anchors.verticalCenter: parent.verticalCenter
            anchors.horizontalCenter: parent.horizontalCenter
        }

    }


    Rectangle{
        id: btn_kirim
        x:50
        y:216
        Image{
            id:img_kirim
            source: path_button + "kirim.png"
            height: 500
            width: 315
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    if(availableUse){
                        if (press==0){
                            stop_scanner();
                            my_stack_view.push(send_parcel, {show_img: show_img, language: language})
                            slot_handler.stop_idle_mode();
                            press=1
                        }
                    }else{
                        dimm_display.visible=true;
                        show_available.start(); 
                    }
                }
                onEntered:{
                    img_kirim.source= path_button + "kirim_down.png"
                }
                onExited:{
                    img_kirim.source= path_button + "kirim.png"
                }
            }
        }
    }

    Rectangle{
        id: btn_titip
        x:383
        y:216
        Image{
            id: img_titip
            source: path_button + "titip.png"
            height: 500
            width: 315
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    if(availableUse){
                        if (press==0){
                            stop_scanner();
                            console.log("btn_titip pressed")
                            my_stack_view.push(store_parcel, {show_img: show_img, language: language})
                            slot_handler.stop_idle_mode();
                            press=1
                        }
                    }else{
                        dimm_display.visible=true;
                        show_available.start(); 
                    }
                }
                onEntered:{
                    img_titip.source= path_button + "titip_down.png"
                }
                onExited:{
                    img_titip.source= path_button + "titip.png"
                }
            }
        }
    }

    Rectangle{
        id: btn_ambil
        x:715
        y:216
        Image{
            id: img_ambil
            source: path_button + "ambil.png"
            height: 500
            width: 315
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    console.log("btn_ambil pressed")
                    if (press == '0'){
                        stop_scanner();
                        my_stack_view.push(take_parcel, {language: language});
                        slot_handler.stop_idle_mode();
                    }
                    press = '1'
                }
                onEntered:{
                    img_ambil.source= path_button + "ambil_down.png"
                }
                onExited:{
                    img_ambil.source= path_button + "ambil.png"
                }
            }
        }
    }

    Image{
        id: poppy_icon
        x:750
        y:681
        width:300
        height:95
        source: 'asset/home_page/background/poppy.png'
        MouseArea{
            anchors.fill: parent
                onClicked: {
                    stop_scanner();
                    my_stack_view.push(contact_us, {language: language});
                }
        }
    }

    Rectangle{
        id: timer_tvc
        width: 10
        height: 10
        x:0
        y:0
        visible: false
        QtObject{
            id:tvc_loading
            property int counter
            Component.onCompleted:{
                tvc_loading.counter = tvc_timeout
            }
        }
        Timer{
            id:show_tvc_loading
            interval:1000
            repeat:true
            running:false
            triggeredOnStart:true
            onTriggered:{
                tvc_loading.counter -= 1
                if(tvc_loading.counter == 0){
                    show_tvc_loading.stop();
                    console.log("starting tvc player...");
                    slot_handler.set_tvc_player("START");
                    tvc_loading.counter = tvc_timeout;
                    show_tvc_loading.restart();
                    // scanner_on();
                }
            }
        }
    }

    Timer {
        id: scanner_restart
        interval: 1000
        repeat: true
        running: true
        onTriggered:{   
            timer_scanner -=1;
            // console.log("timer_scanner = ", timer_scanner);
            if(timer_scanner == 10){
                slot_handler.stop_courier_scan_barcode();
            }
            if(timer_scanner == 0){
                timer_scanner = 20;
                slot_handler.start_courier_scan_barcode();
            }
        }
    }


    Timer {
        id: timer_startup
        interval: 1000
        repeat: false
        running: true
        onTriggered:{   
            slot_handler.start_get_locker_name();
            slot_handler.start_get_free_mouth_mun();
            slot_handler.start_get_tvc32_timer();
            slot_handler.start_get_language();
            slot_handler.start_get_gui_version();
            slot_handler.start_get_contactless();
        }
    }

    Timer {
        id: timer_clock
        interval: 1000
        repeat: true
        running: true
        onTriggered:{   
            var tanggal
            if(language=="MY"){
                tanggal = new Date().toLocaleDateString(Qt.locale("en_US"), Locale.LongFormat) + "  " + new Date().toLocaleTimeString(Qt.locale("en_US"), "hh:mm:ss") //MY Locale
            }else{
                tanggal = new Date().toLocaleDateString(Qt.locale("id_ID"), Locale.LongFormat) + "  " + new Date().toLocaleTimeString(Qt.locale("id_ID"), "hh:mm:ss")
            }
            timeText.text = tanggal.toUpperCase()
            press = '0';
        }
    }

    DimmDisplay{
        id:dimm_display
        visible:false
    }

    NotifSwipe{
        id:notif_available
        source_img: asset_path + "background/full.png"
        img_y:50
        titlebody_x:50
        title_y:60
        body_y:180
        title_text: qsTr("Yahh ,, Lokernya Sudah Penuh")
        body_text: qsTr("Silakan mencoba lagi beberapa saat lagi.")

        NotifButton{
            id:ok_yes
            x:50
            y:351
            r_radius:2
            buttonText:qsTr("YA")
            modeReverse:false
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    hide_available.start();
                    dimm_display.visible=false;
                }
                onEntered:{
                    ok_yes.modeReverse = true
                }
                onExited:{
                    ok_yes.modeReverse = false
                }
            }
        }

        Image{
            x:939
            y:60
            height:33
            width:33
            source:"asset/global/button/close.png"
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    hide_available.start();
                    dimm_display.visible=false;
                }
            }
        }
    }

    NumberAnimation {
        id:hide_available
        targets: notif_available
        properties: "y"
        from:248
        to:768
        duration: 500
        easing.type: Easing.InOutBack
    }
    NumberAnimation {
        id:show_available
        targets: notif_available
        properties: "y"
        from:768
        to:248
        duration: 500
        easing.type: Easing.InOutBack
    }

    function get_gui_version(vers){
        gui_version.text = vers
    }

    function maintenance_status(rest_){
        if (rest_ == "True"){
            my_stack_view.push(under_maintenance,{language: language});
        }
    }

    function show_locker_name(text){
        console.log("locker_name" + text)
        if(text == ""){
            return
        }
        locker_name_text.text = text.toUpperCase()
    }

    function show_free_mouth_num(locker_size){
        var obj = JSON.parse(locker_size)
        for(var i in obj){
            if(i == "XL"){
                size_XL = obj[i]
            }
            if(i == "L"){
                size_L = obj[i]
            }
            if(i == "M"){
                size_M = obj[i]
            }
            if(i == "S"){
                size_S = obj[i]
            }
            if(i == "MINI"){
                size_XS = obj[i]
            }
        }
        if (size_XL <= 0 && size_L <= 0 && size_M <= 0 && size_S <= 0 && size_XS <=0 ){
            availableUse = false;
        }else{
            availableUse = true;
        }
    }

    function tvc_timer(result) {
        console.log("Result TVC TIMER: ", result)
        if(result>0){
            tvc_timeout = result
            if(vss_mode==true){
                tvc_loading.counter = tvc_timeout
                show_tvc_loading.start()
            }
            console.log("Diatas 0 : ", tvc_timeout)
        }else{
            tvc_timeout = 0
            console.log("TVC ==  0 : ", tvc_timeout)
        }
    }

    function contactless_result(result) {
        contactless_status = result;
        // console.log("Get Contactless", contactless_status);
        
        if(contactless_status=="enable"){
            slot_handler.start_courier_scan_barcode();
            scanner_status = true;
            scanner_restart.restart();
        }else{
            scanner_status = false;
            scanner_restart.stop();
        }
    }

    function locker_language(result){
        // console.log("locker_language : ",result)
        language = result;
        if(language=="MY"){
            console.log("bahasa : Malaysia")
            // change_my();
        }else{
            console.log("bahasa  = Indonesia")
            // change_id();
        }
        // button_active = true;
    }

    function handle_text(text){
        slot_handler.stop_courier_scan_barcode();
        if(status_qr=="HOME"){
            check_pattern(text);
        }else{
            console.log("not me");
        }
    }

    function check_pattern(barcode_text){
        var input = barcode_text;
        var result;
        var page;
        var code;
        var length_string = input.length;
        var sc_separator = input.search("#");
        if(sc_separator > -1){
            result = input.split("#");
            page = result[0];
            code = result[1];
        }else{
            code = barcode_text;
        }
        if((page=="AMBIL" || length_string==6)){
            if(scanner_status==true){
                stop_scanner();
                my_stack_view.push(take_parcel, {language: language, pin_code: code, contactless: true,qr_content: barcode_text});
                scanner_status = false;
            }
        }else{
            console.log("KIRIM");
            slot_handler.start_courier_scan_barcode();
        }
        barcode_text = "";
        result = "";
        page = "";
        code = "";
    }

    function stop_scanner() {
        slot_handler.stop_courier_scan_barcode();
        scanner_restart.stop();
        status_qr = "";
    }

    function scanner_on() {
        scanner_status = true;
        scanner_restart.restart();
        slot_handler.start_courier_scan_barcode();
    }

}
