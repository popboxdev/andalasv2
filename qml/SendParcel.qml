import QtQuick 2.4
import QtQuick.Controls 1.2

Background{
    id: send_parcel
    property var press: '0'
    property var asset_global: 'asset/global/'
    property var asset_path: 'asset/send_page/'
    property var path_background: 'asset/send_page/background/'
    property var target_swipe: "notif_swipe"
    property var param_choice: ""
    property var status: ""
    show_img: path_background + "bg.png"
    img_vis:true
    property int character:26
    property var ch:1  
    property var param_parcel: ""
    property var no_parcel: ""
    property var count:0
    property var txt_time: ""
    property var takeUserPhoneNumber: ""
    property var lastmile_type: "NOT_PRELOAD"
    property var drop_by_courier: 0
    property var lockername: ''
    property var lockername_local: ''
    property var lockertarget: "Locker Apartemen Kalibata City"
    property var delimiter: " ]==)> "
    property var chargeType: ''
    property bool flagReturn: false
    property string designationSize: "none"
    property bool kirimLagi: false
    property bool flagPopUp: false
    property var id_company: "none"
    property var status_courier: "none"
    property var phone_courier: "none"
    property var drop_by_other_company: 0
    property var courier_selected: "none"
    property var pushing: "none"
    property var status_qr: "SEND"
    property var language: 'ID'
    
    Stack.onStatusChanged:{
        if(Stack.status == Stack.Activating){
            press = '0';
            timer.secon = 90
            timer_note.restart()
            slot_handler.stop_courier_scan_barcode()
            parcel_number.show_text = "";
            popup_page( flagPopUp );
            slot_handler.start_get_locker_name()
            console.log('id_company' + id_company)
            console.log('status_courier' + status_courier)
            console.log('phone_courier' + phone_courier)
            console.log('courier_selected' + courier_selected)
            console.log('kirimLagi' + kirimLagi)

            if ( kirimLagi == true ) {
                press=0
                // ok_select.buttonColor = "#ff524f"
                // ok_select.enabled = true
                btn_with_resi.modeReverse=true
                btn_with_resi.buttonColor="#ff524f"
                btn_popsend.modeReverse=false
                btn_popsend.buttonColor="#f6f6f6"
                btn_resi_return.modeReverse=false
                btn_resi_return.buttonColor="#f6f6f6"
                btn_resi_courier.modeReverse=false
                btn_resi_courier.buttonColor="#f6f6f6"
                param_choice="courier_send_awb"
                parcel_number.show_text =""
                btn_lanjut.buttonColor = "#c4c4c4"
                parcel_number.borderColor = "#8c8c8c"
                del_img.visible=false
                slot_handler.start_courier_scan_barcode();
            }
        }
        if(Stack.status==Stack.Deactivating){
            timer_note.stop()
            slot_handler.stop_courier_scan_barcode()
        }
    }

    FontType{
        id:fontStyle
        //Type Font ### bold , extrabold , light , book, medium, black ###
    }

    Header{
        id:header
        funct: ( kirimLagi == true ) ? "home" : "default"
        title_text:qsTr("KIRIM")
        text_timer : txt_time
    }
    Item{
        id: timer
        property int secon  : 90
        Timer{
            id:timer_note
            interval:1000
            repeat:true
            running:true
            triggeredOnStart:true
            onTriggered:{
                timer.secon -= 1
                txt_time = timer.secon
                if(timer.secon < 1){
                    timer_note.stop()
                    my_stack_view.push(time_out)
                }
            }
        }
    }
    
    Text {
        id: krm
        x:80
        y:145
        text: qsTr("Ketik / Scan No. Resi")
        font.pixelSize:40
        color:"#414042"
        font.family: fontStyle.medium
    }

    Image{
        id: img_help
        width:26
        height:26
        // x:435
        x:50
        y:148
        source:path_background+"help.png"
        visible: true
        MouseArea {
            anchors.fill: parent
            onClicked: {
                if(press==0){
                    show_resi.start();
                    dimm_display.visible=true
                    press=1
                    slot_handler.stop_courier_scan_barcode();
                }
            }
        }
    }

    Image{
        id: img_return
        x: 50
        y: 319
        width: 623//881
        height: 46
        source:path_background+"logo_return.png"
        visible: false
    }

    Image{
        id: img_bg
        width:412
        height:347
        x:0
        y:386
        source:path_background+"bglogo.png"
    }

    Text {
        id:text_helpscan
        y:325
        x:51
        text: qsTr("Lebih gampang scan no. resi kamu, <b><u>lihat caranya di sini</u></b>")
        font.pixelSize:22
        color:"#3a3a3a"
        font.family: fontStyle.book
        MouseArea {
            anchors.fill: parent
            onClicked: {
                if(press==0){
                    show_scan.start();
                    dimm_display.visible=true
                    press=1
                    slot_handler.stop_courier_scan_barcode();
                }
            }
        }
    }
    Text {
        id:ab
        y:548
        x:666
        text: qsTr("Pilih Layanan Lain ?")
        font.pixelSize:30
        color:"#414042"
        font.family: fontStyle.book
    }

    NotifButton{
        id:btn_lanjut
        x:690
        y:230
        buttonText:qsTr("LANJUT")
        buttonColor: "#8c8c8c"
        r_radius:0
        modeReverse: false
        MouseArea{
            anchors.fill: parent
            onClicked: {
                // if(press != "0"){
                //     return
                // }

                if(parcel_number.show_text != ""){
                    slot_handler.start_get_internet_status()
                    // parcel_number.show_text = "PLLIDTFKXFFK"
                    // slot_handler.checking_awb(parcel_number.show_text)
                    // dimm_display.visible = true
                    // waiting.visible = true
                }

                // else{
                    
                // }
                // press = "1"
            }
            onEntered:{
                btn_lanjut.modeReverse = true
            }
            onExited:{
                btn_lanjut.modeReverse = false
            }
        }
    }

    NotifButton{
        id: btn_help
        x:715
        y:604
        width:210
        height:51
        font_size:18
        buttonText:qsTr("TEKAN DI SINI")
        r_radius:5
        modeReverse:true
        MouseArea{
            anchors.fill: parent
            onClicked: {
                if(press==0){
                    // show_lupa_resi.start();
                    // dimm_display.visible=true
                    // press=1
                    // slot_handler.stop_courier_scan_barcode();
                    popup_page();
                }
            }
            onEntered:{
                btn_help.modeReverse = false
            }
            onExited:{
                btn_help.modeReverse = true
            }
        }
    }

    // Rectangle{
    //     x:690
    //     y:230
    //     width: 230
    //     height: 80
    //     color: "#ff7e7e"
    //     Text {
    //         id: text_button
    //         anchors.horizontalCenter: parent.horizontalCenter
    //         anchors.verticalCenter: parent.verticalCenter
    //         font.pixelSize:24
    //         color:"#ffffff"
    //         font.family: font_type
    //         text: qsTr("LANJUT")
    //     }
    //     // MouseArea {
    //     //     anchors.fill: parent
    //     //     onClicked: {
    //     //         ab.text = parcel_number.show_text
    //     //     }
    //     // }
    // }

    Rectangle{
        x:50
        y:230
        width: 620
        height: 80
        color: "transparent"
        InputText{
            id:parcel_number
            x:2
            y:2
            borderColor : "#8c8c8c"
            // password: false
            // show_image:"img/apservice/courier/user_name.png"
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    if(press==0){
                        show_key.start();
                        ch=1
                        parcel_number.bool = true
                        press=1
                        // courier_login_psw.bool = false
                    }
                }
            }
        }
    }

    Image{
        id: del_img
        x:613
        y:252
        width:36
        height:36
        visible:false
        source:asset_global + "button/delete.png"
        MouseArea {
            anchors.fill: parent
            onClicked: {
                parcel_number.show_text = "";
                count=0;
                touch_keyboard.input_text("")
            }
        }
    }


    KeyBoardAlphanumeric{
        id:touch_keyboard
        property var validate_c
        Component.onCompleted: {
            touch_keyboard.letter_button_clicked.connect(input_text)
            // touch_keyboard.function_button_clicked.connect(on_function_button_clicked)
        }

        function input_text(str){
            if(ch==1){
                if (str == "" && parcel_number.show_text.length > 0){
                    parcel_number.show_text.length--
                    parcel_number.show_text=parcel_number.show_text.substring(0,parcel_number.show_text.length-1)
                    count--;                 
                }
                if(str == "OKE" || str == "OK"){
                    str = "";
                    if(parcel_number.show_text != ""){
                        // parcel_number.show_text = "PLLIDTFKXFFK"
                        // slot_handler.checking_awb(parcel_number.show_text)
                        slot_handler.start_get_internet_status()
                    }
                }
                if (str != "" && parcel_number.show_text.length< character){
                    parcel_number.show_text.length++
                    count++;
                }
                if (parcel_number.show_text.length>=character){
                    str=""
                    parcel_number.show_text.length=character
                }else{
                    parcel_number.show_text += str
                }
                if(count>0){
                    btn_lanjut.buttonColor = "#ff524f"
                    parcel_number.borderColor = "#ff7e7e"
                    del_img.visible=true
                } else{
                    btn_lanjut.buttonColor = "#c4c4c4"
                    parcel_number.borderColor = "#8c8c8c"
                    del_img.visible=false
                }

            }
        }
    }

    NumberAnimation{
        id:show_wrong_popsend
        targets: notif_wrong_popsend
        properties: "y"
        from:768
        to:248
        duration: 500
        easing.type: Easing.InOutBack
    }
    NumberAnimation{
        id:hide_wrong_popsend
        targets: notif_wrong_popsend
        properties: "y"
        from:248
        to:768
        duration: 500
        easing.type: Easing.InOutBack
    }

    NumberAnimation {
        id:hide_key
        targets: touch_keyboard
        properties: "y"
        from:368
        to:768
        duration: 500
        easing.type: Easing.InOutBack
    }
    NumberAnimation {
        id:show_key
        targets: touch_keyboard
        properties: "y"
        from:768
        to:368
        duration: 500
        easing.type: Easing.InOutBack
    }

    NumberAnimation {
        id:hide_scan
        targets: notif_scan
        properties: "y"
        from:266
        to:768
        duration: 500
        easing.type: Easing.InOutBack
    }
    NumberAnimation {
        id:show_scan
        targets: notif_scan
        properties: "y"
        from:768
        to:266
        duration: 500
        easing.type: Easing.InOutBack
    }

    NumberAnimation {
        id:hide_resi
        targets: notif_resi
        properties: "y"
        from:266
        to:768
        duration: 500
        easing.type: Easing.InOutBack
    }
    NumberAnimation {
        id:show_resi
        targets: notif_resi
        properties: "y"
        from:768
        to:266
        duration: 500
        easing.type: Easing.InOutBack
    }

    NumberAnimation {
        id:hide_lupa_resi
        targets: notif_lupa_resi
        properties: "y"
        from:90
        to:768
        duration: 500
        easing.type: Easing.InOutBack
    }
    NumberAnimation {
        id:show_lupa_resi
        targets: notif_lupa_resi
        properties: "y"
        from:768
        to:90
        duration: 500
        easing.type: Easing.InOutBack
    }

    NumberAnimation {
        id:hide_return
        targets: notif_return
        properties: "y"
        from:248
        to:768
        duration: 500
        easing.type: Easing.InOutBack
    }
    NumberAnimation {
        id:show_return
        targets: notif_return
        properties: "y"
        from:768
        to:248
        duration: 500
        easing.type: Easing.InOutBack
    }

    NumberAnimation {
        id:hide_popsend
        targets: notif_popsend
        properties: "y"
        from:266
        to:768
        duration: 500
        easing.type: Easing.InOutBack
    }
    NumberAnimation {
        id:show_popsend
        targets: notif_popsend
        properties: "y"
        from:768
        to:266
        duration: 500
        easing.type: Easing.InOutBack
    }
    NumberAnimation {
        id:hide_popsafe
        targets: notif_popsafe
        properties: "y"
        from:248
        to:768
        duration: 500
        easing.type: Easing.InOutBack
    }
    NumberAnimation {
        id:show_popsafe
        targets: notif_popsafe
        properties: "y"
        from:768
        to:248
        duration: 500
        easing.type: Easing.InOutBack
    }

    NumberAnimation {
        id:hide_chkreturn
        targets: notif_chkreturn
        properties: "y"
        from:248
        to:768
        duration: 500
        easing.type: Easing.InOutBack
    }
    
    NumberAnimation {
        id:show_chkreturn
        targets: notif_chkreturn
        properties: "y"
        from:768
        to:248
        duration: 500
        easing.type: Easing.InOutBack
    }

    NumberAnimation {
        id:hide_wrong_return
        targets: notif_wrong_return
        properties: "y"
        from:248
        to:768
        duration: 500
        easing.type: Easing.InOutBack
    }

    NumberAnimation {
        id:show_wrong_return
        targets: notif_wrong_return
        properties: "y"
        from:768
        to:248
        duration: 500
        easing.type: Easing.InOutBack
    }

    NumberAnimation {
        id:hide_nointernet
        targets: notif_nointernet
        properties: "y"
        from:248
        to:768
        duration: 500
        easing.type: Easing.InOutBack
    }

    NumberAnimation {
        id:show_nointernet
        targets: notif_nointernet
        properties: "y"
        from:768
        to:248
        duration: 500
        easing.type: Easing.InOutBack
    }

    NumberAnimation {
        id:hide_wrongawb
        targets: notif_wrongawb
        properties: "y"
        from:248
        to:768
        duration: 500
        easing.type: Easing.InOutBack
    }

    NumberAnimation {
        id:show_wrongawb
        targets: notif_wrongawb
        properties: "y"
        from:768
        to:248
        duration: 500
        easing.type: Easing.InOutBack
    }

    DimmDisplay{
        id:dimm_display
        visible:false
    }

    AnimatedImage{
        id: waiting
        visible:false
        width: 200
        height: 200
        anchors.verticalCenter: parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter
        source: 'asset/global/animation/loading.gif'
    }

    NotifSwipe{
        id: notif_wrong_popsend
        source_img: path_background + "unknown_popsafe.png"
        titlebody_x:50
        title_y:60
        body_y:130
        img_y:100
        title_text:qsTr("Order Kamu Tidak Dikenali")
        body_text:qsTr("Hai Popers, order kamu tidak dikenali. Periksa kembali nomer <br>order kamu dan pastikan kamu telah memasukkan <br>dengan benar")

        NotifButton{
            id:btn_retry
            x:310
            y:254
            r_radius:2
            buttonText:qsTr("MASUKKAN ULANG")
            modeReverse:false
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    slot_handler.start_courier_scan_barcode();
                    hide_wrong_popsend.start();
                    dimm_display.visible = false;
                }
                onEntered:{
                    btn_retry.modeReverse = true
                }
                onExited:{
                    btn_retry.modeReverse = false
                }
            }
        }
        NotifButton{
            id: btn_cs2
            x:50
            y:254
            r_radius:2
            buttonText:qsTr("HUBUNGI CS")
            modeReverse:true
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    hide_wrong_popsend.start();
                    my_stack_view.push(contact_us,{language: language});
                }
                onEntered:{
                    btn_cs2.modeReverse = false
                }
                onExited:{
                    btn_cs2.modeReverse = true
                }
            }
        }

        Image{
            x:939
            y:60
            height:33
            width:33
            source:"asset/global/button/close.png"
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    hide_wrong_popsend.start();
                    dimm_display.visible = false;
                }

            }
        }
    }

    NotifSwipe{
        id:notif_wrongawb
        source_img: asset_global + "handle/resi_salah.png"
        img_y:50
        titlebody_x:50
        title_y:60
        body_y:130
        title_text:qsTr("Format No. Resi Salah")
        body_text:qsTr("Silakan periksa kembali no. resi yang <br>dimasukkan. Pastikan kamu scan ke barcode <br>yang benar.")

        NotifButton{
            id:coba_wrongawb
            x:50
            y:300
            r_radius:0
            r_width:360
            buttonText:qsTr("MASUKKAN ULANG")
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    hide_wrongawb.start();
                    dimm_display.visible = false
                    slot_handler.start_courier_scan_barcode();
                }
                onEntered:{
                    coba_wrongawb.modeReverse = true
                }
                onExited:{
                    coba_wrongawb.modeReverse = false
                }
            }
        }

        Image{
            x:939
            y:60
            height:33
            width:33
            source:"asset/global/button/close.png"
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    hide_wrongawb.start();
                    dimm_display.visible = false
                    slot_handler.start_courier_scan_barcode();
                }
            }
        }
    }

    NotifSwipe{
        id:notif_wrong_return
        source_img: asset_global + "handle/wrong_return.png"
        img_y:50
        titlebody_x:50
        title_y:60
        body_y:130
        title_text:qsTr("Nomor Tidak Dikenal")
        body_text:qsTr("Periksa kembali no. pengembalian barang dan <br>pastikan kamu telah memasukkan nomor yang <br>benar")

        NotifButton{
            id:coba_wrong_return
            x:50
            y:315
            r_width:320
            r_radius:0
            buttonText:qsTr("MASUKKAN ULANG")
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    hide_wrong_return.start();
                    dimm_display.visible = false
                    slot_handler.start_courier_scan_barcode();
                }
                onEntered:{
                    coba_wrong_return.modeReverse = true
                }
                onExited:{
                    coba_wrong_return.modeReverse = false
                }
            }
        }

        Image{
            x:939
            y:60
            height:33
            width:33
            source:"asset/global/button/close.png"
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    hide_wrong_return.start();
                    dimm_display.visible = false
                }
            }
        }
    }

    NotifSwipe{
        id:notif_nointernet
        source_img: asset_global + "handle/internet.png"
        img_y:50
        titlebody_x:50
        title_y:60
        body_y:130
        title_text:qsTr("Yah, Internet tidak stabil")
        body_text:qsTr("Maaf jaringan internet sedang tidak stabil, <br>silakan coba kembali")

        NotifButton{
            id:coba_nointernet
            x:50
            y:221
            r_radius:0
            buttonText:qsTr("COBA KEMBALI")
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    hide_nointernet.start();
                    dimm_display.visible = false
                }
                onEntered:{
                    coba_nointernet.modeReverse = true
                }
                onExited:{
                    coba_nointernet.modeReverse = false
                }
            }
        }

        Image{
            x:939
            y:60
            height:33
            width:33
            source:"asset/global/button/close.png"
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    hide_nointernet.start();
                    dimm_display.visible = false
                }
            }
        }
    }

    NotifSwipe{
        id:notif_chkreturn
        img_y:50
        body_y:130
        source_img: path_background + "check_return.png"
        title_text:qsTr("Cek No. Pengembalian Barang Kamu")
        body_text:qsTr("Hai Popers, untuk no. pengembalian barang bisa kamu <br>dapatkan dari toko online tempat kamu belanja")

        NotifButton{
            id: btn_chkreturn
            x:50
            y:221
            r_radius:2
            buttonText:qsTr("TUTUP")
            modeReverse: false
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    hide_animation("chkreturn");
                }
                onEntered:{
                    btn_chkreturn.modeReverse = true
                }
                onExited:{
                    btn_chkreturn.modeReverse = false
                }
            }
        }

        Image{
            x:939
            y:60
            height:33
            width:33
            source:"asset/global/button/close.png"
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    hide_animation("chkreturn");
                }

            }
        }
    }

    NotifSwipe{
        id:notif_popsafe
        img_y:56
        source_img: path_background + "popsafe.png"
        title_text:qsTr("No. Resi Yang Kamu Masukkan <br>Terindikasi Sebagai Order PopSafe")
        body_text:qsTr("Popers, jika kamu kurir dan ingin kirim barang pilih <br>KURIR. Jika kamu sudah membuat order dari aplikasi <br>PopBox pilih LANJUT.")
        NotifButton{
            id: btl_popsafe
            x:50
            y:350
            r_radius:2
            buttonText:qsTr("BATAL")
            modeReverse:true
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    hide_animation("popsafe");
                }
                onEntered:{
                    btl_popsafe.modeReverse = false
                }
                onExited:{
                    btl_popsafe.modeReverse = true
                }
            }
        }
        NotifButton{
            id: btn_pilkur
            x:295
            y:350
            r_radius:2
            buttonText:qsTr("KURIR")
            modeReverse:true
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    hide_animation("popsafe");
                    console.log('select courier popsafe')
                    my_stack_view.push(select_courier, {param_parcel: param_parcel, no_parcel:no_parcel});
                }
                onEntered:{
                    btn_pilkur.modeReverse = false
                }
                onExited:{
                    btn_pilkur.modeReverse = true
                }
            }
        }
        NotifButton{
            id:ok_popsafe
            x:540
            y:350
            r_radius:2
            buttonText:qsTr("LANJUT")
            modeReverse: false
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    hide_animation("popsafe");
                    // my_stack_view.push(detail_parcel, {show_img: show_img, data_popsafe: param_parcel})
                    // my_stack_view.push(detail_parcel, {show_img: show_img, data_popsafe: param_parcel, parcel_number: parcel_number.show_text})
                    
                    console.log("PARCELL NUMBER : "+param_parcel)
                    

                    my_stack_view.push(store_parcel, {data_send_parcel: "1", parcel_number: no_parcel});
                }
                onEntered:{
                    ok_popsafe.modeReverse = true
                }
                onExited:{
                    ok_popsafe.modeReverse = false
                }
            }
        }

        Image{
            x:939
            y:60
            height:33
            width:33
            source:"asset/global/button/close.png"
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    hide_animation("popsafe");
                }

            }
        }
    }

    NotifSwipe{
        id:notif_popsend
        img_y:50
        source_img: path_background + "resi_info.png"
        body_y:130
        title_text:qsTr("No. Resi Ada di Aplikasi PopBox mu")
        body_text:qsTr("Hai Popers, ayo cek no. resi kamu dengan masuk ke <br>menu ‘History’ buka tab PopSend pilih order lalu pilih <br>klik detailnya.")
        NotifButton{
            id: cb_popsafe
            x:50
            y:254
            buttonText:qsTr("COBA KEMBALI")
            r_radius:2
            modeReverse:false
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    hide_animation("popsend");
                }
                onEntered:{
                    cb_popsafe.modeReverse = true
                }
                onExited:{
                    cb_popsafe.modeReverse = false
                }
            }
        }

        Image{
            x:939
            y:60
            height:33
            width:33
            source:"asset/global/button/close.png"
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    send_parcel.hide_animation("popsend");
                }

            }
        }
    }

    NotifSwipe{
        id:notif_scan
        img_y:50
        source_img: path_background + "notif_scan.png"
        title_text:qsTr("Lebih Gampang dengan Scan <br>No. Resi")
        body_text:qsTr("Cukup arahkan barcode no. resi pada barcode <br>scanner yang berada di bawah layar bagian <br>tengah. Jaga jarak antara barcode dengan <br>barcode scanner agar dapat dibaca.")
        NotifButton{
            id:btn_scan
            x:50
            y:337
            r_radius:2
            buttonText:qsTr("MENGERTI")
            modeReverse:false
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    hide_animation("scan");
                }
                onEntered:{
                    btn_scan.modeReverse = true
                }
                onExited:{
                    btn_scan.modeReverse = false
                }
            }
        }

        Image{
            x:939
            y:60
            height:33
            width:33
            source:"asset/global/button/close.png"
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    send_parcel.hide_animation("scan");
                }

            }
        }
    }

    NotifSwipe{
        id:notif_resi
        img_y:50
        source_img: path_background + "no_resi.png"
        // titlebody_x:311
        title_y:60
        body_y:128
        title_text:qsTr("Apa itu No. Resi ?")
        body_text:qsTr("No. Resi adalah nomor pengiriman yang <br>diberikan oleh logistik sebagai tanda bukti <br>pengiriman. <br> <br>Istilah untuk beberapa layanan: <br>Pengembalian Barang: return number/ nomor <br>Pengembalian <br>PopSend/PopSafe: order number (dapat dicek <br>di aplikasi PopBox)")

        Image{
            x:939
            y:60
            height:33
            width:33
            source:"asset/global/button/close.png"
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    hide_animation("resi");
                }

            }
        }
    }

    NotifSwipe{
        id:notif_return
        source_img: path_background + "lupa_resi_mini.png"
        titlebody_x:423
        title_y:53
        body_y:110
        img_y:51
        r_height:520
        title_text:qsTr("Pilih Layanan")
        body_text:qsTr("Hai Popers, layanan apa <br>yang kamu ingin gunakan?")

        Image{
            x:939
            y:60
            height:33
            width:33
            source:"asset/global/button/close.png"
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    hide_animation("return");
                }

            }
        }

        NotifButton{
            id: ok_return
            x:744
            y:380
            buttonText:qsTr("LANJUT")
            r_radius:2
            buttonColor: "#c4c4c4"
            modeReverse:false
            enabled:false
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    if(param_choice=="return"){
                        console.log("return Barang")
                        console.log("return_detail" + param_parcel)
                        hide_return.start();
                        dimm_display.visible=false;
                        my_stack_view.push(input_phone, {param_return: param_parcel, status: "return", title:qsTr("Pengembalian Barang"), title_body:qsTr("Kamu")}) 
                    }
                    if(param_choice=="courier_send"){
                        if ( flagReturn == true ) {
                            
                            console.log(" return awb but push to lastmile :" + no_parcel)
                            
                            slot_handler.set_express_number( no_parcel ) 
                        } 
                        // if ( kirimLagi == true ) {
                        //     my_stack_view.push(input_phone, {no_parcel:no_parcel, select_courier:select_courier, phone_courier:phone_number, status: "lastmile", title: qsTr("KURIR"), title_body: qsTr("Penerima"), note_body: qsTr("Pastikan nomor Customer yang kamu masukan sudah benar"), id_company: id_company, customer_phone: customer_phone, status_courier: status_courier, drop_by_courier: drop_by_courier, drop_by_other_company: drop_by_other_company})
                        // } else {
                        //     my_stack_view.push(select_courier, {param_parcel: param_parcel, no_parcel:no_parcel});
                        // }
                    }
                }
                onEntered:{
                    ok_return.modeReverse = true
                }
                onExited:{
                    ok_return.modeReverse = false
                }
            }
        }

        NotifButtonLong{
            id:btn_return
            x:423
            y:200
            buttonText:qsTr("Pengembalian Barang")
            r_radius:2
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    ok_return.buttonColor = "#ff524f"
                    btn_return.modeReverse=true
                    btn_return.buttonColor="#ff524f"
                    btn_courier.modeReverse=false
                    btn_courier.buttonColor="#f6f6f6"
                    param_choice="return"
                    ok_return.enabled = true
                }
            }
        }

        NotifButtonLong{
            id:btn_courier
            x:423
            y:290
            buttonText:qsTr("Kurir Kirim Barang")
            r_radius:2
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    ok_return.buttonColor = "#ff524f"
                    btn_courier.modeReverse=true
                    btn_courier.buttonColor="#ff524f"
                    btn_return.modeReverse=false
                    btn_return.buttonColor="#f6f6f6"
                    param_choice="courier_send"
                    ok_return.enabled = true
                }
            }
        }
    }

    NotifSwipe{
        id:notif_lupa_resi
        source_img: path_background + "lupa_resi.png"
        img_y:40
        title_y:60
        body_y:137
        titlebody_x:423
        r_height:685
        img_height:645
        body_size:28
        title_text:qsTr("Pilih Layanan")
        body_text:qsTr("Hai Popers, layanan apa yang kamu <br>ingin gunakan?")

        NotifButtonLong{
            id:btn_with_resi
            x:423
            y:233
            buttonText:qsTr("Kurir Kirim Barang Dengan No. Resi")
            r_radius:2
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    // ok_select.buttonColor = "#ff524f"
                    // ok_select.enabled = true
                    btn_with_resi.modeReverse=true
                    btn_with_resi.buttonColor="#ff524f"
                    btn_popsend.modeReverse=false
                    btn_popsend.buttonColor="#f6f6f6"
                    btn_resi_return.modeReverse=false
                    btn_resi_return.buttonColor="#f6f6f6"
                    btn_resi_courier.modeReverse=false
                    btn_resi_courier.buttonColor="#f6f6f6"
                    param_choice="courier_send_awb"
                    parcel_number.show_text =""
                    btn_lanjut.buttonColor = "#c4c4c4"
                    parcel_number.borderColor = "#8c8c8c"
                    del_img.visible=false
                    lanjut_pilih()
                }
            }
        }

        NotifButtonLong{
            id:btn_resi_courier
            x:423
            y:318
            buttonText:qsTr("Kurir Kirim Barang Tanpa No. Resi")
            r_radius:2
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    // hide_animation("scan");
                    // ok_select.buttonColor = "#ff524f"
                    // ok_select.enabled = true
                    btn_resi_courier.modeReverse=true
                    btn_resi_courier.buttonColor="#ff524f"
                    btn_with_resi.modeReverse=false
                    btn_with_resi.buttonColor="#f6f6f6"
                    btn_popsend.modeReverse=false
                    btn_popsend.buttonColor="#f6f6f6"
                    btn_resi_return.modeReverse=false
                    btn_resi_return.buttonColor="#f6f6f6"
                    param_choice="courier_send"
                    // generate_awb();
                    lanjut_pilih()
                }
            }
        }

        NotifButtonLong{
            id:btn_resi_return
            x:423
            y:410
            buttonText:qsTr("Pengembalian Barang (Return)/ <br>Pengiriman Rayspeed")
            r_radius:2
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    // ok_select.buttonColor = "#ff524f"
                    // ok_select.enabled = true
                    btn_resi_return.modeReverse=true
                    btn_resi_return.buttonColor="#ff524f"
                    btn_with_resi.modeReverse=false
                    btn_with_resi.buttonColor="#f6f6f6"
                    btn_popsend.modeReverse=false
                    btn_popsend.buttonColor="#f6f6f6"
                    btn_resi_courier.modeReverse=false
                    btn_resi_courier.buttonColor="#f6f6f6"
                    param_choice="return"
                    // hide_animation("scan");
                    parcel_number.show_text =""
                    btn_lanjut.buttonColor = "#c4c4c4"
                    parcel_number.borderColor = "#8c8c8c"
                    del_img.visible=false
                    lanjut_pilih()
                }
            }
        }

        NotifButtonLong{
            id:btn_popsend
            x:423
            y:488
            buttonText:qsTr("PopSend (kirim barang via aplikasi)")
            r_radius:2
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    // ok_select.buttonColor = "#ff524f"
                    // ok_select.enabled = true
                    btn_popsend.modeReverse=true
                    btn_popsend.buttonColor="#ff524f"
                    btn_with_resi.modeReverse=false
                    btn_with_resi.buttonColor="#f6f6f6"
                    btn_resi_return.modeReverse=false
                    btn_resi_return.buttonColor="#f6f6f6"
                    btn_resi_courier.modeReverse=false
                    btn_resi_courier.buttonColor="#f6f6f6"
                    param_choice="popsend"
                    parcel_number.show_text =""
                    btn_lanjut.buttonColor = "#c4c4c4"
                    parcel_number.borderColor = "#8c8c8c"
                    del_img.visible=false
                    lanjut_pilih()
                }
            }
        }

        // NotifButton{
        //     id:ok_select
        //     x:744
        //     y:573
        //     buttonText:qsTr("LANJUT")
        //     r_radius:2
        //     buttonColor: "#c4c4c4"
        //     enabled:false
        //     modeReverse:false
        //     MouseArea{
        //         anchors.fill: parent
        //         onClicked: {
        //             // hide_animation("scan");
        //             if(param_choice=="popsend"){
        //                 console.log("PopSend")
        //                 hide_animation("lupa_resi");
        //                 page_default();
        //                 // show_popsend.start();
        //                 dimm_display.visible=false
        //             }else if(param_choice=="return"){
        //                 console.log("return Barang")
        //                 hide_animation("lupa_resi");
        //                 page_return();
        //                 // show_chkreturn.start();
        //                 dimm_display.visible=false
                        
        //             }else if(param_choice=="courier_send"){
        //                 page_default();
        //                 console.log("kurir Barang")
        //                 generate_awb();
        //                 slot_handler.set_logout_user()
        //                 hide_animation("lupa_resi");
        //             }else{
        //                 page_default();
        //                 console.log("kurir Barang dengan no resi")
        //                 hide_animation("lupa_resi");
        //             }
        //             slot_handler.start_courier_scan_barcode();
        //         }
        //         onEntered:{
        //             ok_select.modeReverse = true
        //         }
        //         onExited:{
        //             ok_select.modeReverse = false
        //         }
        //     }
        // }

        Image{
            x:939
            y:60
            height:33
            width:33
            source:"asset/global/button/close.png"
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    hide_animation("lupa_resi");
                    slot_handler.start_courier_scan_barcode();
                }
            }
        }
    }

    NotifSwipe{
        id: notif_wrong_location
        source_img: path_background + "wrong_location.png"
        titlebody_x:50
        title_y:60
        body_y:130
        img_y:50
        title_text:qsTr("Order Kamu Salah Lokasi Loker")
        body_text:qsTr("Hai Popers, order kamu tidak dapat diproses, karena <br>salah lokasi loker. Pastikan kamu telah berada di loker <br>yang sesuai.")
        NotifButton{
            id:btn_cs
            x:310
            y:254
            r_radius:2
            buttonText:qsTr("HUBUNGI CS")
            modeReverse:false
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    hide_animation("wrong_loc");
                    my_stack_view.push(contact_us,{language: language});
                }
                onEntered:{
                    btn_cs.modeReverse = true
                }
                onExited:{
                    btn_cs.modeReverse = false
                }
            }
        }
        NotifButton{
            id:btn_back
            x:50
            y:254
            r_radius:2
            buttonText:qsTr("KEMBALI KE HOME")
            modeReverse:true
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    my_stack_view.pop(null);
                }
                onEntered:{
                    btn_back.modeReverse = false
                }
                onExited:{
                    btn_back.modeReverse = true
                }
            }
        }

        Image{
            x:939
            y:60
            height:33
            width:33
            source:"asset/global/button/close.png"
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    hide_animation("wrong_loc");
                }
            }
        }
    }

    NumberAnimation{
        id:show_wrong_location
        targets: notif_wrong_location
        properties: "y"
        from:768
        to:248
        duration: 500
        easing.type: Easing.InOutBack
    }
    NumberAnimation{
        id:hide_wrong_location
        targets: notif_wrong_location
        properties: "y"
        from:248
        to:768
        duration: 500
        easing.type: Easing.InOutBack
    }

    Component.onCompleted: {
        root.barcode_result.connect(handle_text)
        root.check_awb_global_result.connect(check_awb_result);
        root.start_checking_return_result.connect(checking_return_result);
        root.start_checking_popsend_result.connect(checking_popsend_result);
        root.start_internet_status_result.connect(internet_status_result);
        root.start_get_locker_name_result.connect(show_locker_name);

    }

    Component.onDestruction: {
        root.barcode_result.disconnect(handle_text)
        root.check_awb_global_result.disconnect(check_awb_result);
        root.start_checking_return_result.disconnect(checking_return_result);
        root.start_checking_popsend_result.disconnect(checking_popsend_result);
        root.start_internet_status_result.disconnect(internet_status_result);
        root.start_get_locker_name_result.disconnect(show_locker_name);

    }

    function show_locker_name(result) {
        
        if (result == "") {
            lockername_local = "Locker PopBox" 
        } else {
            lockername_local = result
        }
        
    }

    function popup_page( flag ) {

        if ( !flag ) {
            show_lupa_resi.start();
            dimm_display.visible=true
            press=1
            slot_handler.stop_courier_scan_barcode();
        }
    }

    function page_return(){
        krm.text = qsTr("Ketik No. Pengembalian barang / Kode Rayspeed ");
        // img_help.x = 650;
        img_return.visible = true;
        text_helpscan.visible = false;
    }

    function page_default(){
        krm.text = qsTr("Ketik / Scan No. Resi");
        // img_help.x = 435;
        img_return.visible = false;
        text_helpscan.visible = true;
    }

    function handle_text(text){
        slot_handler.stop_courier_scan_barcode();
        parcel_number.show_text = text;
        dimm_display.visible = true;
        // waiting.visible = true;
        // slot_handler.checking_awb(parcel_number.show_text);
        check_pattern(text);
        count = text.length;
        btn_lanjut.buttonColor = "#ff524f"
        btn_lanjut.enabled = true
    }

    function hide_animation(param){
        if(param=="keyboard") hide_key.start();
        if(param=="scan")hide_scan.start();
        if(param=="resi")hide_resi.start();
        if(param=="lupa_resi")hide_lupa_resi.start();
        if(param=="popsend")hide_popsend.start();
        if(param=="return")hide_return.start();
        if(param=="popsafe")hide_popsafe.start();
        if(param=="chkreturn")hide_chkreturn.start();
        if(param=="wrong_loc")hide_wrong_location.start();
        dimm_display.visible=false
        press=0;
        slot_handler.start_courier_scan_barcode();
    }

    
    function generate_awb() {
        var date = new Date();
        var datetime = date.getFullYear()+""+(date.getMonth()+1)+""+date.getDate()+""+date.getMinutes();
        var rnd_string = "POP" + datetime + makeid(4).toUpperCase();
        console.log(" random string: " + rnd_string)
        parcel_number.show_text = rnd_string
        btn_lanjut.buttonColor = "#ff524f"
        parcel_number.borderColor = "#ff7e7e"
        del_img.visible=true
    }

    function makeid(length) {
        var result           = '';
        var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        var charactersLength = characters.length;
        for ( var i = 0; i < length; i++ ) {
            result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        return result;
    }
    
    function internet_status_result(param) {
        if(param=="SUCCESS"){
            if(parcel_number.show_text != ""){
                // parcel_number.show_text = "PLLIDTFKXFFK"
                check_pattern(parcel_number.show_text)
                // slot_handler.checking_awb(parcel_number.show_text)
                dimm_display.visible = true
                // waiting.visible = true
            }
        }else{
            console.log("INTERNET STATUS : ", param)
            show_nointernet.start();
            dimm_display.visible = true
        }
    }

    function check_pattern(text)
    {
        var pattern = /^[a-zA-Z0-9-_]{1,24}$/gm;
        var parcel_text = text;
        console.log("Parcel oke : "+ parcel_text);
        if(parcel_text.match(pattern)){
            console.log("BENAR : "+ parcel_text);
            if(param_choice=="return"){
                slot_handler.start_checking_return(parcel_text)
                console.log("PARAM RETURN Kepanggil");
            }else if(param_choice=="popsend"){
                //  || param_choice=="courier_send"
                slot_handler.start_checking_popsend(parcel_text)
                console.log("PARAM POPSEND Kepanggil");
            }else{
                slot_handler.checking_awb(parcel_text)
            }
            waiting.visible = true
        }else{
            console.log("SALAH : "+ parcel_text);
            show_wrongawb.start();
            dimm_display.visible=true
            parcel_number.show_text = "";
            count=0;
            touch_keyboard.input_text("")
        }
    }

    function checking_popsend_result(param){
        console.log("hasil_check_awb_popsend: " + param)
        var result = JSON.parse(param)
        if(result.isSuccess == "true") {
            waiting.visible = false
            dimm_display.visible = false
            param_parcel = param
            no_parcel = parcel_number.show_text
            if (result.type == "POPSEND") {
                console.log("popsend_choose: " + result.type + " parcel_number : " + no_parcel)
                slot_handler.start_get_customer_store_express_info(no_parcel)
                slot_handler.start_video_capture('store_popsend_'+no_parcel)
                param_parcel = param
                show_data_popsend(param)
                checking_locker_name_first()
                dimm_display.visible=true
            }else{
                show_wrong_popsend.start();
                dimm_display.visible = true;
                waiting.visible = false;
            }
            
        }else{
            show_wrong_popsend.start();
            dimm_display.visible = true;
            waiting.visible = false;
        }
    }

    function checking_return_result(param){
        console.log("hasil_check_awb_return: " + param)
        var result = JSON.parse(param)
        if (result.isSuccess == "true") {
            waiting.visible = false
            dimm_display.visible = false
            param_parcel = param
            no_parcel = parcel_number.show_text
            if (result.type == "RETURN"){
                var merchant = result.groupName
                slot_handler.start_video_capture('store_reject_express_'+no_parcel)
                console.log("merchant_return_name: " + merchant)
                
                slot_handler.start_customer_reject_select_merchant(merchant)
                my_stack_view.push(input_phone, {param_return: param_parcel, status: "return", title:qsTr("Pengembalian Barang"), title_body:qsTr("Kamu")}) 
                // show_return.start() // change to multiple choice if detect return
                // dimm_display.visible=true
            }
        }else{
            // console.log("RETURN SALAH");
            show_wrong_return.start();
            dimm_display.visible = true;
            waiting.visible = false;
        }
    }

    
    function check_awb_result(param) {
        console.log("hasil_check_awb: " + param)
        var result = JSON.parse(param)
        if (result.isSuccess == "true") {
            waiting.visible = false
            dimm_display.visible = false
            param_parcel = param
            no_parcel = parcel_number.show_text
            if (result.type == "RETURN"){
                flagReturn = true
                var merchant = result.groupName
                slot_handler.start_video_capture('store_reject_express_'+no_parcel)
                console.log("merchant_return_name: " + merchant)
                
                slot_handler.start_customer_reject_select_merchant(merchant)
                show_return.start() // change to multiple choice if detect return
                dimm_display.visible=true
            } else if (result.type == "LASTMILE") {
                console.log(" data_lastmile: " + param)
                slot_handler.set_express_number(no_parcel)
                takeUserPhoneNumber = result.takeUserPhoneNumber
                
                console.log("customer_phone: " + takeUserPhoneNumber)
                
                if (takeUserPhoneNumber != "") {
                    lastmile_type = "PRELOAD"
                }
                console.log("lastmile_type: " + lastmile_type)

                if ( kirimLagi == true ) {
        
                    my_stack_view.push(input_phone, {no_parcel:no_parcel, select_courier:courier_selected, phone_courier:phone_courier, status: "lastmile", title: qsTr("KURIR"), title_body: qsTr("Penerima"), note_body: qsTr("Pastikan nomor Customer yang kamu masukan sudah benar"), id_company: id_company, status_courier: status_courier, drop_by_courier: drop_by_courier, drop_by_other_company: drop_by_other_company})
                } else {
                    console.log('select courier check awb result')
                    my_stack_view.push(select_courier, {param_parcel: param_parcel, no_parcel:no_parcel, customer_phone: takeUserPhoneNumber, lastmile_type:  lastmile_type, drop_by_courier: drop_by_courier})
                }


            } else if (result.type == "ONDEMAND") {
                slot_handler.set_express_number(no_parcel)
                slot_handler.start_get_customer_store_express_info(no_parcel)
                slot_handler.start_video_capture('store_ondemand_'+no_parcel)
                my_stack_view.push(detail_ondemand, {data_ondemand: param_parcel, no_parcel:no_parcel})
            } else if (result.type == "POPSEND") {
                
                console.log("popsend_choose: " + result.type + " parcel_number : " + no_parcel)
                param_parcel = param
                slot_handler.start_get_customer_store_express_info(no_parcel)
                slot_handler.start_video_capture('store_popsend_'+no_parcel)
                show_data_popsend(param)
                // checking_locker_name_first()
                // my_stack_view.push(detail_popsend, {data_popsend: param, parcel_number: no_parcel})
            }else if (result.type == "POPSAFE") {
                show_popsafe.start();
                dimm_display.visible=true
            }
        } else {
            my_stack_view.push(detail_popsend)
        }
    }

    function show_data_popsend(argument) {
        console.log(" detail_popsend: " + argument)
        var result = JSON.parse(argument)
        if (result.isSuccess == "true") {
            var addresses = result.endAddress
            var show_locker = ''
            var show_address = ''
            var split_address = ''

            //Parse From-To
            if (addresses.indexOf(delimiter) > -1){
                split_address = addresses.split(delimiter.toString())
                show_locker = split_address[0]
                show_address = split_address[1]
            }else{
                show_address = addresses
                show_locker = locker_name
            }
        
            lockername = show_locker
            lockertarget = show_address
            chargeType = result.chargeType
            // notarget = result.takeUserPhoneNumber //OLD
        }
        
    }

    function validate_locker(x,y){
        var check1 = x.toLowerCase().replace(/\s+|a|e|i|u|o|-|[.\,\:\+\&\//\;\@\\!]/g, "")
        var check2 = y.toLowerCase().replace(/\s+|a|e|i|u|o|-|[.\,\:\+\&\//\;\@\\!]/g, "")
        if(check1==check2 ||
                check1.slice(-5)==check2.slice(-5) ||
                check1.indexOf(check2) > -1 ||
                check2.indexOf(check1) > -1){
            console.log("MATCH >> " + check1 + ' VS ' + check2)
            return true
        }else{
            console.log("NOT MATCH >> " + check1 + ' VS ' + check2)
            return false
        }
    }

    function checking_locker_name_first() {
        
        console.log("locker_local: " + lockername_local + " locker_popsend : " + lockername)
        
        if (validate_locker(lockername_local, lockername)) {//order vs actual locker comparation
            if (chargeType == "BY_WEIGHT") {
                my_stack_view.push(customer_send_express_view)
            }
            if (chargeType == "NOT_CHARGE") {
                if (designationSize == "none") {
                    my_stack_view.push(detail_popsend, {data_popsend: param_parcel, parcel_number: no_parcel})
                
                }else{
                    slot_handler.start_choose_mouth_size(designationSize, "customer_store_express")
                }
            }
            if (chargeType == "FIXED_VALUE") {
                if (payOfAmount == 0) {
                    no_payOfAmount.open()
                }
                if (payOfAmount != 0) {
                    slot_handler.start_free_mouth_by_size(designationSize)
                }
            }
        }else{
            console.log(" wrong_location " ) 
            show_wrong_location.start()
            dimm_display.visible=true          
        }   
    }

    function lanjut_pilih(){
        if(param_choice=="popsend"){
            console.log("PopSend")
            hide_animation("lupa_resi");
            page_default();
            // show_popsend.start();
            dimm_display.visible=false
        }else if(param_choice=="return"){
            console.log("return Barang")
            hide_animation("lupa_resi");
            page_return();
            // show_chkreturn.start();
            dimm_display.visible=false
            
        }else if(param_choice=="courier_send"){
            page_default();
            console.log("kurir Barang")
            generate_awb();
            slot_handler.set_logout_user()
            hide_animation("lupa_resi");
        }else{
            page_default();
            console.log("kurir Barang dengan no resi")
            hide_animation("lupa_resi");
        }
        slot_handler.start_courier_scan_barcode();
    }
}
