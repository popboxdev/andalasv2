import QtQuick 2.4
import QtQuick.Controls 1.2

Background{
    id: send_parcel_return
    property var press: '0'
    property var asset_path: 'asset/send_page/'
    property var path_background: 'asset/send_page/background/'
    property var font_type: "SourceSansPro-Regular"
    show_img: path_background + "bg.png"
    img_vis:true

    Stack.onStatusChanged:{
        if(Stack.status == Stack.Activating){
            // slot_handler.start_get_file_dir(path);
            // slot_handler.start_get_locker_name();
            // timer_clock.start();
            // timer_startup.start();
            // hide_key.start();s
            press = '0';
        }
        if(Stack.status==Stack.Deactivating){
            // timer_clock.stop();
            // slider_timer.stop();
            // timer_startup.stop();
        }
    }

    Header{
        id:header
        title_text:"Pengembalian Barang"
    }
}
