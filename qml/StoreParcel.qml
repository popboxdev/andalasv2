import QtQuick 2.4
import QtQuick.Controls 1.2

Background{
    id: store_parcel
    property var press: '0'
    property var asset_path: 'asset/send_page/'
    property var asset_global: 'asset/global/'
    property var path_background: 'asset/send_page/background/'
    // property var fontStyle.book: "SourceSansPro-Regular"
    //show_img: asset_path + "background/bg.png"
    show_img : ""
    property var parcel_number: ''
    img_vis:true
    property int character:26
    property var ch:1 
    property var step_choose: ""
    property var count:0
    property var nameowner: ""    
    property var delimiter: " ]==)> "    
    property var data_popsafe: ''
    property var lockername_local: ''
    property var lockername_popsafe: ''
    property var chargeType: ''
    property var lockername: ''
    property var taketime: ""
    property var orderno : ""
    
    property var locker_number: 0
    
    property bool isPopDeposit: false
    property string designationSize: "none"
    
    property var popsafe_param: undefined
    
    property var popsafe_status:""
    property var txt_time: ""
    
    property var data_send_parcel: "0"
    property var language: 'ID'
    property var status_qr: "STORE"
    
    

    Stack.onStatusChanged:{
        if(Stack.status == Stack.Activating){
            // statusPage();
            timer_startup.start();
            timer.secon = 90;
            timer_note.restart();
            // slot_handler.start_get_file_dir(path);
            // slot_handler.start_get_locker_name();
            // timer_clock.start();
            // timer_startup.start();
            press = '0';
            slot_handler.start_get_locker_name();
            // slot_handler.start_courier_scan_barcode()
        }
        if(Stack.status==Stack.Deactivating){
            timer_note.stop()
            slot_handler.stop_courier_scan_barcode()
            // timer_clock.stop();
            // slider_timer.stop();
            timer_startup.stop();
        }
    }

    FontType{
        id:fontStyle
        //Type Font ### bold , extrabold , light , book, medium, black ###
    }
    
    Item{
        id: timer
        property int secon  : 90
        Timer{
            id:timer_note
            interval:1000
            repeat:true
            running:true
            triggeredOnStart:true
            onTriggered:{
                timer.secon -= 1
                txt_time = timer.secon
                if(timer.secon < 1){
                    timer_note.stop()
                    my_stack_view.push(time_out)
                }
            }
        }
    }

    Timer {
        id: timer_startup
        interval: 1000
        repeat: false
        running: true
        onTriggered:{   
            statusPage();
        }
    }

    Header{
        id:header
        title_text:qsTr("TITIP")
        text_timer : txt_time
    }
     
    
    Text {
        id: krm
        x:80
        y:145
        text: qsTr("Ketik / Scan No. Resi")
        font.pixelSize:40
        color:"#414042"
        font.family: fontStyle.medium
    }

    Image{
        id: img_help
        width:26
        height:26
        // x:435
        x:50
        y:148
        source:path_background+"help.png"
        MouseArea {
            anchors.fill: parent
            onClicked: {
                if(press==0){
                    show_resi.start();
                    dimm_display.visible=true
                    press=1
                    slot_handler.stop_courier_scan_barcode();
                }
            }
        }
    }
    Image{
        id: img_bg
        width:359
        height:293
        x:50
        y:463
        source:path_background+"bglogo.png"
    }

    Text {
        id:a
        y:325
        x:51
        text: qsTr("Lebih gampang scan no. resi kamu, <b><u>lihat caranya di sini</u></b>")
        font.pixelSize:22
        color:"#3a3a3a"
        font.family: fontStyle.book
        MouseArea {
            anchors.fill: parent
            onClicked: {
                if(press==0){
                    show_scan.start();
                    dimm_display.visible=true
                    press=1
                    slot_handler.stop_courier_scan_barcode();
                }
            }
        }
    }
    Text {
        id:ab
        y:520
        x:487
        text: qsTr("Kamu Tidak Punya / Lupa No. Resi ?")
        font.pixelSize:30
        color:"#414042"
        font.family: fontStyle.book
    }

    NotifButton{
        id:nextButton
        x:690
        y:230
        buttonText:qsTr("LANJUT")
        buttonColor: "#8c8c8c"
        r_radius:0
        modeReverse:false
        enabled: false
        MouseArea{
            anchors.fill: parent
            onClicked: {
                if(press==0){
                    if(popsafe_number.show_text != "") {
                        dimm_display.visible = true;
                        // waiting.visible = true;
                        check_pattern(popsafe_number.show_text)
                        // slot_handler.start_get_customer_store_express_info(popsafe_number.show_text);
                    }
                    press=1
                }
            }
            onEntered:{
                nextButton.modeReverse = true
            }
            onExited:{
                nextButton.modeReverse = false
            }
        }
    }

    NotifButton{
        id:btn_help
        x:742
        y:577
        width:200
        height:50
        font_size:18
        buttonText:qsTr("TEKAN DI SINI")
        r_radius:5
        modeReverse:true
        MouseArea{
            anchors.fill: parent
            onClicked: {
                if(press==0){
                    show_lupa_resi.start();
                    dimm_display.visible=true
                    press=1
                    slot_handler.stop_courier_scan_barcode();
                }
            }
            onEntered:{
                btn_help.modeReverse = false
            }
            onExited:{
                btn_help.modeReverse = true
            }
        }
    }

    Rectangle{
        x:50
        y:230
        width: 620
        height: 80
        color: "transparent"
        InputText{
            id:popsafe_number
            x:2
            y:2
            borderColor : "#c4c4c4"
            // password: false
            // show_image:"img/apservice/courier/user_name.png"
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    if(press==0){
                        show_key.start();
                        ch=1
                        popsafe_number.bool = true
                        press=1
                        // courier_login_psw.bool = false
                    }
                }
            }
        }
    }
    Image{
        id: del_img
        x:613
        y:252
        width:36
        height:36
        visible:false
        source:asset_global + "button/delete.png"
        MouseArea {
            anchors.fill: parent
            onClicked: {
                popsafe_number.show_text = "";
                count=0;
                touch_keyboard.input_text("")
            }
        }
    }


    KeyBoardAlphanumeric{
        id:touch_keyboard
        property var count:0
        property var validate_c
        page_calling_keyboard: "store_parcel"
        Component.onCompleted: {
            touch_keyboard.letter_button_clicked.connect(input_text)
            // touch_keyboard.function_button_clicked.connect(on_function_button_clicked)
        }

        function input_text(str){
            if(ch==1){
                if (str == "" && popsafe_number.show_text.length > 0){
                    popsafe_number.show_text.length--
                    popsafe_number.show_text=popsafe_number.show_text.substring(0,popsafe_number.show_text.length-1 )
                }
                if(str == "OKE" || str == "OK"){
                    str = "";
                    if(parcel_number.show_text != ""){
                        // slot_handler.start_get_customer_store_express_info(popsafe_number.show_text)
                        check_pattern(popsafe_number.show_text);
                    }
                }
                if (str != "" && popsafe_number.show_text.length< character){
                    popsafe_number.show_text.length++
                }
        
                if (popsafe_number.show_text.length>=character){
                    str=""
                    popsafe_number.show_text.length=character
                }else{
                    popsafe_number.show_text += str
                }
                if (popsafe_number.show_text.length != 0){
                    popsafe_number.borderColor = "#ff524f"
                    nextButton.buttonColor = "#ff524f"
                    nextButton.enabled = true
                    del_img.visible=true
                }
                else{
                    popsafe_number.borderColor = "#c4c4c4"
                    nextButton.buttonColor = "#c4c4c4"
                    nextButton.enabled = false
                    del_img.visible=false
                }
                press=0; 
            }
        }
    }

    NumberAnimation {
        id:hide_key
        targets: touch_keyboard
        properties: "y"
        from:368
        to:768
        duration: 500
        easing.type: Easing.InOutBack
    }
    NumberAnimation {
        id:show_key
        targets: touch_keyboard
        properties: "y"
        from:768
        to:368
        duration: 500
        easing.type: Easing.InOutBack
    }
    NumberAnimation {
        id:hide_scan
        targets: notif_scan
        properties: "y"
        from:266
        to:768
        duration: 500
        easing.type: Easing.InOutBack
    }
    NumberAnimation {
        id:show_scan
        targets: notif_scan
        properties: "y"
        from:768
        to:266
        duration: 500
        easing.type: Easing.InOutBack
    }
    NumberAnimation {
        id:hide_resi
        targets: notif_resi
        properties: "y"
        from:266
        to:768
        duration: 500
        easing.type: Easing.InOutBack
    }
    NumberAnimation {
        id:show_resi
        targets: notif_resi
        properties: "y"
        from:768
        to:266
        duration: 500
        easing.type: Easing.InOutBack
    }
    
    NumberAnimation {
        id:hide_lupa_resi
        targets: notif_lupa_resi
        properties: "y"
        from:248
        to:768
        duration: 500
        easing.type: Easing.InOutBack
    }
    NumberAnimation {
        id:show_lupa_resi
        targets: notif_lupa_resi
        properties: "y"
        from:768
        to:248
        duration: 500
        easing.type: Easing.InOutBack
    }
    NumberAnimation{
        id:show_wrong_popsafe
        targets: notif_wrong_popsafe
        properties: "y"
        from:768
        to:248
        duration: 500
        easing.type: Easing.InOutBack
    }
    NumberAnimation{
        id:hide_wrong_popsafe
        targets: notif_wrong_popsafe
        properties: "y"
        from:248
        to:768
        duration: 500
        easing.type: Easing.InOutBack
    }
    NumberAnimation{
        id:show_wrong_location
        targets: notif_wrong_location
        properties: "y"
        from:768
        to:248
        duration: 500
        easing.type: Easing.InOutBack
    }
    NumberAnimation{
        id:hide_wrong_location
        targets: notif_wrong_location
        properties: "y"
        from:248
        to:768
        duration: 500
        easing.type: Easing.InOutBack
    }

    NumberAnimation {
        id:hide_wrongawb
        targets: notif_wrongawb
        properties: "y"
        from:248
        to:768
        duration: 500
        easing.type: Easing.InOutBack
    }

    NumberAnimation {
        id:show_wrongawb
        targets: notif_wrongawb
        properties: "y"
        from:768
        to:248
        duration: 500
        easing.type: Easing.InOutBack
    }

    DimmDisplay{
        id:dimm_display
        visible:false
    }

    AnimatedImage{
        id: waiting
        visible:false
        width: 200
        height: 200
        anchors.verticalCenter: parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter
        source: 'asset/global/animation/loading.gif'
    }

    NotifSwipe{
        id:notif_wrongawb
        source_img: asset_global + "handle/resi_salah.png"
        img_y:50
        titlebody_x:50
        title_y:60
        body_y:130
        title_text:qsTr("Format No. Resi Salah")
        body_text:qsTr("Silakan periksa kembali no. resi yang <br>dimasukkan. Pastikan kamu scan ke barcode <br>yang benar.")

        NotifButton{
            id:coba_wrongawb
            x:50
            y:300
            r_radius:0
            r_width:360
            buttonText:qsTr("MASUKKAN ULANG")
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    hide_wrongawb.start();
                    dimm_display.visible = false
                    slot_handler.start_courier_scan_barcode();
                }
                onEntered:{
                    coba_wrongawb.modeReverse = true
                }
                onExited:{
                    coba_wrongawb.modeReverse = false
                }
            }
        }

        Image{
            x:939
            y:60
            height:33
            width:33
            source:"asset/global/button/close.png"
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    hide_wrongawb.start();
                    dimm_display.visible = false
                    slot_handler.start_courier_scan_barcode();
                }
            }
        }
    }

    NotifSwipe{
        id: notif_wrong_location
        source_img: path_background + "wrong_location.png"
        titlebody_x:50
        title_y:60
        body_y:130
        img_y:50
        title_text:qsTr("Order Kamu Salah Lokasi Loker")
        body_text:qsTr("Hai Popers, order kamu tidak dapat diproses, karena <br>salah lokasi loker. Pastikan kamu telah berada di loker <br>yang sesuai.")
        NotifButton{
            id:btn_cs
            x:310
            y:254
            r_radius:2
            buttonText:qsTr("HUBUNGI CS")
            modeReverse:false
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    hide_animation("wrong_loc");
                    my_stack_view.push(contact_us,{language: language});
                }
                onEntered:{
                    btn_cs.modeReverse = true
                }
                onExited:{
                    btn_cs.modeReverse = false
                }
            }
        }
        NotifButton{
            id:btn_back
            x:50
            y:254
            r_radius:2
            buttonText:qsTr("KEMBALI KE HOME")
            modeReverse:true
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    my_stack_view.pop(null);
                }
                onEntered:{
                    btn_back.modeReverse = false
                }
                onExited:{
                    btn_back.modeReverse = true
                }
            }
        }

        Image{
            x:939
            y:60
            height:33
            width:33
            source:"asset/global/button/close.png"
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    hide_animation("wrong_loc");
                }
            }
        }
    }

    NotifSwipe{
        id: notif_wrong_popsafe
        source_img: path_background + "unknown_popsafe.png"
        titlebody_x:50
        title_y:60
        body_y:130
        img_y:100
        title_text:qsTr("Order Kamu Tidak Dikenali")
        body_text:qsTr("Hai Popers, order kamu tidak dikenali. Periksa kembali nomer <br>order kamu dan pastikan kamu telah memasukkan <br>dengan benar")

        NotifButton{
            id:btn_retry
            x:310
            y:254
            r_radius:2
            buttonText:qsTr("MASUKKAN ULANG")
            modeReverse:false
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    slot_handler.start_courier_scan_barcode();
                    hide_animation("wrong_pop");
                }
                onEntered:{
                    btn_retry.modeReverse = true
                }
                onExited:{
                    btn_retry.modeReverse = false
                }
            }
        }
        NotifButton{
            id: btn_cs2
            x:50
            y:254
            r_radius:2
            buttonText:qsTr("HUBUNGI CS")
            modeReverse:true
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    hide_animation("wrong_pop");
                    my_stack_view.push(contact_us,{language: language});
                }
                onEntered:{
                    btn_cs2.modeReverse = false
                }
                onExited:{
                    btn_cs2.modeReverse = true
                }
            }
        }

        Image{
            x:939
            y:60
            height:33
            width:33
            source:"asset/global/button/close.png"
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    hide_animation("wrong_pop");
                }

            }
        }
    }

    NotifSwipe{
        id:notif_resi
        img_y:50
        source_img: path_background + "no_resi.png"
        // titlebody_x:311
        title_y:60
        body_y:128
        title_text:qsTr("Apa itu No. Resi ?")
        body_text:qsTr("No. Resi adalah nomor pengiriman yang <br>diberikan oleh logistik sebagai tanda bukti <br>pengiriman. <br> <br>Istilah untuk beberapa layanan: <br>Pengembalian Barang: return number/ nomor <br>Pengembalian <br>PopSend/PopSafe: order number (dapat dicek <br>di aplikasi PopBox)")

        Image{
            x:939
            y:60
            height:33
            width:33
            source:"asset/global/button/close.png"
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    slot_handler.start_courier_scan_barcode();
                    hide_animation("resi");
                }

            }
        }
    }
    NotifSwipe{
        id:notif_scan
        img_y:50
        source_img: path_background + "notif_scan.png"
        title_text:qsTr("Lebih Gampang dengan Scan <br>No. Resi")
        body_text:qsTr("Cukup arahkan barcode no. resi pada barcode <br>scanner yang berada di bawah layar bagian <br>tengah. Jaga jarak antara barcode dengan <br>barcode scanner agar dapat dibaca.")
        NotifButton{
            x:50
            y:337
            r_radius:2
            buttonText:qsTr("MENGERTI")
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    slot_handler.start_courier_scan_barcode();
                    hide_animation("scan");
                }
            }
        }

        Image{
            x:939
            y:60
            height:33
            width:33
            source:"asset/global/button/close.png"
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    slot_handler.start_courier_scan_barcode();
                    store_parcel.hide_animation("scan");
                }

            }
        }
    }
    NotifSwipe{
        id:notif_lupa_resi
        source_img: path_background + "lupa_resi_mini.png"
        title_text:qsTr("Order Titip")
        body_text:qsTr("Kamu sudah punya resi TITIP/ PopSafe?")
        titlebody_x:423
        title_y:53
        body_y:110
        img_y:51
        r_height:520

        NotifButtonLong{
            id: longnotifno
            x:423
            y:200
            buttonText:qsTr("Belum, buat sekarang")
            r_radius:2
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    processnotif.buttonColor = "#ff524f"
                    longnotifno.modeReverse = true
                    longnotifno.buttonColor = "#ff524f"
                    longnotifyes.modeReverse = false
                    longnotifyes.buttonColor = "#f6f6f6"
                    step_choose = "not"
                    processnotif.enabled = true
                }
            }
        }

        NotifButtonLong{
            id:longnotifyes
            x:423
            y:290
            buttonText:qsTr("Ya, sudah buat di aplikasi")
            r_radius:2
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    processnotif.buttonColor = "#ff524f"
                    longnotifyes.modeReverse = true
                    longnotifyes.buttonColor = "#ff524f"
                    longnotifno.modeReverse = false
                    longnotifno.buttonColor = "#f6f6f6"
                    step_choose = "done"
                    processnotif.enabled = true
                }
            }
        }

        NotifButton{
            id: processnotif
            x:744
            y:380
            buttonText:qsTr("LANJUT")
            buttonColor: "#c4c4c4"
            r_radius:2
            modeReverse:false
            enabled:false
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    if (step_choose == "not"){
                        hide_lupa_resi.start();
                        my_stack_view.push(input_phone, {status: "titip", title: qsTr("TITIP")})
                        //show_metode_bayar.start();
                        dimm_display.visible=false;
                        press=0;
                    } else if (step_choose == "done"){
                        dimm_display.visible=false
                        hide_lupa_resi.start();
                        slot_handler.start_courier_scan_barcode();
                        press=0;
                    }
                }
                onEntered:{
                    processnotif.modeReverse = true
                }
                onExited:{
                    processnotif.modeReverse = false
                }
            }
        }

        Image{
            x:939
            y:60
            height:33
            width:33
            source:"asset/global/button/close.png"
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    slot_handler.start_courier_scan_barcode();
                    hide_animation("lupa_resi");
                }
            }
        }
    }

    Component.onCompleted: {
        root.barcode_result.connect(handle_text)
        root.customer_store_express_result.connect(data_popsafe_user);
        root.start_get_locker_name_result.connect(show_locker_name);
    }

    Component.onDestruction: {
        root.barcode_result.disconnect(handle_text)
        root.customer_store_express_result.disconnect(data_popsafe_user);
        root.start_get_locker_name_result.disconnect(show_locker_name);
    }

    function handle_text(text){
        slot_handler.stop_courier_scan_barcode();
        popsafe_number.show_text = text;
        dimm_display.visible = true;
        // waiting.visible = true;
        // slot_handler.start_get_customer_store_express_info(text)
        check_pattern(text);
        touch_keyboard.count = text.length
        nextButton.buttonColor = "#ff524f"
        nextButton.enabled = true
    }

    function show_locker_name(result) {
        
        console.log("Locker_name_detail_stored: " + result)
        if (result == "") {
            lockername_local = "Locker PopBox" 
        } else {
            lockername_local = result
        }
        
    }

    function data_popsafe_user(data_parcel_popsafe) {
        console.log("data_popsafe_locker: " + data_parcel_popsafe)
        var result = JSON.parse(data_parcel_popsafe)
        
        console.log("result: " + result.isSuccess)
        
        if (result.isSuccess == "true") {
            dimm_display.visible = false;
            waiting.visible = false;
            popsafe_param = data_parcel_popsafe
            parse_data_popsafe(data_parcel_popsafe)
            checking_locker_name_first()
        }
        else{
            show_wrong_popsafe.start();
            dimm_display.visible=true
        }
        
    }

    function hide_animation(param){
        if(param=="keyboard") hide_key.start();
        if(param=="notif")hide_scan.start();
        if(param=="resi")hide_resi.start();
        if(param=="scan")hide_scan.start();
        if(param=="lupa_resi")hide_lupa_resi.start();
        if(param=="wrong_pop"){
            hide_wrong_popsafe.start();
            waiting.visible = false;
        }
        if(param=="wrong_loc")hide_wrong_location.start();
        dimm_display.visible=false
        slot_handler.start_courier_scan_barcode();
        press=0;
    }

    function validate_locker(x,y){
        var check1 = x.toLowerCase().replace(/\s+|a|e|i|u|o|-|[.\,\:\+\&\//\;\@\\!]/g, "")
        var check2 = y.toLowerCase().replace(/\s+|a|e|i|u|o|-|[.\,\:\+\&\//\;\@\\!]/g, "")
        if(check1==check2 ||
                check1.slice(-5)==check2.slice(-5) ||
                check1.indexOf(check2) > -1 ||
                check2.indexOf(check1) > -1){
            console.log("MATCH >> " + check1 + ' VS ' + check2)
            return true
        }else{
            console.log("NOT MATCH >> " + check1 + ' VS ' + check2)
            return false
        }
    }

    function checking_locker_name_first() {
        
        console.log("locker_local: " + lockername_local + " locker_popsafe : " + lockername_popsafe)
        
        if (validate_locker(lockername_local, lockername_popsafe)) {//order vs actual locker comparation
            if (chargeType == "BY_WEIGHT") {
                my_stack_view.push(customer_send_express_view)
            }
            if (chargeType == "NOT_CHARGE") {
                if (designationSize == "none") {
                    my_stack_view.push(detail_parcel, {show_img: show_img, data_popsafe: popsafe_param, isPopDeposit: isPopDeposit, parcel_number: orderno})
                }else{
                    slot_handler.start_choose_mouth_size(designationSize, "customer_store_express")
                }
            }
            if (chargeType == "FIXED_VALUE") {
                if (payOfAmount == 0) {
                    no_payOfAmount.open()
                }
                if (payOfAmount != 0) {
                    slot_handler.start_free_mouth_by_size(designationSize)
                }
            }
        }else{
            console.log(" wrong_location " ) 
            show_wrong_location.start()
            dimm_display.visible=true          
        }   
    }

    function parse_data_popsafe(data) {
        var result = JSON.parse(data)
        if (result.isSuccess == "true") {
            var addresses = result.endAddress
            var show_locker = ''
            var show_address = ''
            var split_address = ''

            //Parse From-To
            if (addresses.indexOf(delimiter) > -1){
                split_address = addresses.split(delimiter.toString())
                show_locker = split_address[0]
                show_address = split_address[1]
            }else{
                show_address = addresses
                show_locker = lockername_local
            }
        
            lockername = result.box_name
            taketime = result.expired_time
            orderno = result.customerStoreNumber
            if (result.parcelType == 'POPSAFE') {
                nameowner = result.recipientName
            } else {
                nameowner = result.sender

            }
            lockername_popsafe = show_locker
            chargeType = result.chargeType

            //Parse if classified as PopDeposit Parcel under "PDS" prefix
            if(orderno.substring(0,3)=="PDS"){
                isPopDeposit = true
            }else{
                isPopDeposit = false
            }

            if (chargeType == "NOT_CHARGE") {
                if ("designationSize" in result) {
                    if (result.designationSize=="") {
                        designationSize = "none"
                    }else if (result.designationSize=="DOC") {
                        designationSize = "S"
                    }else{
                        designationSize = result.designationSize
                    }
                }else{
                    designationSize = "none"
                }
            }
        }
    }

    function check_pattern(text){
        var pattern = /^[a-zA-Z0-9-_]{1,24}$/gm;
        var parcel_text = text;
        console.log("Parcel oke : "+ parcel_text);
        if(parcel_text.match(pattern)){
            console.log("BENAR : "+ parcel_text);
            slot_handler.start_get_customer_store_express_info(parcel_text);
            waiting.visible = true
        }else{
            console.log("SALAH : "+ parcel_text);
            show_wrongawb.start();
            dimm_display.visible=true
            popsafe_number.show_text = "";
            count=0;
            touch_keyboard.input_text("")
        }
    }

    function statusPage() {
        if(language=="MY"){
            my_stack_view.push(input_phone, {status: "titip", title: qsTr("TITIP"),  status_back: "home"})
        }else{
            if(data_send_parcel=="1"){
                slot_handler.start_get_customer_store_express_info(parcel_number)
                waiting.visible = true;
                dimm_display.visible = true;
                
                console.log("STATUS PARCELL : " + parcel_number)
                
                data_send_parcel = "0"
            }else{
                dimm_display.visible=true
                show_lupa_resi.start();
            }
        }
        
    }

}
