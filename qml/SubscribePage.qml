import QtQuick 2.4
import QtQuick.Controls 1.2

Background{
    id: courier_login_page
    property var press: '0'
    property var asset_global: 'asset/global/'
    property var asset_path: 'asset/subscribe/'
    property int character:26
    property var ch:1 
    property var count:0
    property var identity:""
    property var level_user
    property var level_name
    property var txt_time: ""
    property var language: 'ID'
    property variant check_number:[]
    property var phone_substring;

    property variant check_number_id: ['0814','0815', '0816', '0855', '0856', '0857', '0858',
        '0811', '0812', '0813', '0821', '0822', '0823', '0851', '0852', '0853',
        '0817', '0818', '0819', '0859', '0877', '0878', '0879',
        '0831', '0832', '0838', '9991', '9992', '9993', '9994', '9995', '9988',
        '0881', '0882', '0883', '0884', '0885', '0886', '0887', '0888', '0889',
        '0895', '0896', '0897', '0898', '0899']
    property variant check_number_my: ['010','011','012','013','014','015','016','017','018','019']

    Stack.onStatusChanged:{
        if(Stack.status == Stack.Activating){
            slot_handler.start_get_language();
            timer.secon = 90
            timer_note.restart()
            press = '0';
            show_key.start();
            input_username.bool = true
            input_phone.bool = false
            console.log(identity)
        }
        if(Stack.status==Stack.Deactivating){
            timer_note.stop()
        }
    }

    Component.onCompleted: {
        root.start_get_language_result.connect(locker_language);
    }

    Component.onDestruction: {
        root.start_get_language_result.disconnect(locker_language);
    }

    FontType{
        id:fontStyle
        //Type Font ### bold , extrabold , light , book, medium, black ###
    }

    Header{
        id:header
        title_text:qsTr("SUBSCRIBE")
        text_timer : txt_time
    }

    Item{
        id: timer
        property int secon  : 90
        Timer{
            id:timer_note
            interval:1000
            repeat:true
            running:true
            triggeredOnStart:true
            onTriggered:{
                timer.secon -= 1
                txt_time = timer.secon
                if(timer.secon < 1){
                    timer_note.stop()
                    my_stack_view.push(time_out)
                }
            }
        }
    }
    
    Text {
        id: username_txt
        x:50
        y:145
        text: qsTr("Nama")
        font.pixelSize:40
        color:"#414042"
        font.family: fontStyle.medium
    }

    Text {
        id: password_txt
        x:400
        y:145
        text: qsTr("Nomor Handphone")
        font.pixelSize:40
        color:"#414042"
        font.family: fontStyle.medium
    }

    Text {
        id: wrong_user
        visible:false
        x:51
        y:324
        text: qsTr("Username dan password salah! silakan masukkan kembali")
        font.pixelSize:22
        color:"#ff524f"
        font.family: fontStyle.book
    }

    InputText{
        id:input_username
        x:50
        y:230
        text_width:255
        r_width:331
        borderColor : "#8c8c8c"
        MouseArea {
            anchors.fill: parent
            onClicked: {
                ch=1
                press=1
                input_username.bool = true
                input_phone.bool = false
            }
        }
        Image{
            id: del_username
            x:283
            y:22
            width:36
            height:36
            visible:false
            source:asset_global + "button/delete.png"
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    input_username.show_text = "";
                    count=0;
                    ch=1
                    touch_keyboard.input_text("")
                    del_username.visible=false
                }
            }
        }
    }

    InputText{
        id:input_phone
        x:396
        y:230
        r_width:331
        text_width:255
        borderColor : "#8c8c8c"
        MouseArea {
            anchors.fill: parent
            onClicked: {
                ch=2;
                press=1;
                input_phone.bool = true
                input_username.bool = false
            }
        }
        Image{
            id: del_password
            x:283
            y:22
            width:36
            height:36
            visible:false
            source:asset_global + "button/delete.png"
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    input_phone.show_text = "";
                    count=0;
                    ch=2
                    touch_keyboard.input_text("")
                    del_password.visible=false
                }
            }
        }
    }

    NotifButton{
        id:btn_lanjut
        x:750
        y:230
        enabled: false
        buttonText:qsTr("LANJUT")
        buttonColor: "#8c8c8c"
        r_radius:0
        modeReverse:false
        MouseArea{
            anchors.fill: parent
            onClicked: {
                if(press != "1"){
                    return
                }
                press = "0";
                check_phonenumber(input_phone.show_text);
            }
            onEntered:{
                btn_lanjut.modeReverse = true
            }
            onExited:{
                btn_lanjut.modeReverse = false
            }
        }
    }

    KeyBoardAlphanumeric{
        id:touch_keyboard
        property var validate_c
        closeButton: false
        Component.onCompleted: {
            touch_keyboard.letter_button_clicked.connect(input_text)
            // touch_keyboard.function_button_clicked.connect(on_function_button_clicked)
        }

        function input_text(str){
            if(str == "OKE" || str == "OK"){
                str = "";
                if(press != "1"){
                    return
                }
                press = "0";
                check_phonenumber(input_phone.show_text);
            }
            if(ch==1){
                if (str == "" && input_username.show_text.length > 0){
                    input_username.show_text.length--
                    input_username.show_text=input_username.show_text.substring(0,input_username.show_text.length-1)
                    count--;                 
                }
                if (str != "" && input_username.show_text.length< character){
                    input_username.show_text.length++
                    count++;
                }
                if (input_username.show_text.length>=character){
                    str=""
                    input_username.show_text.length=character
                }else{
                    input_username.show_text += str
                }
                if(count>0){
                    if (input_phone.show_text!=""){
                        btn_lanjut.buttonColor = "#ff524f"
                        btn_lanjut.enabled = true
                    }
                    input_username.borderColor = "#ff7e7e"
                    del_username.visible=true
                } else{
                    btn_lanjut.buttonColor = "#c4c4c4"
                    btn_lanjut.enabled = false
                    input_username.borderColor = "#8c8c8c"
                    del_username.visible=false
                }
            }
            if(ch==2){
                if (str == "" && input_phone.show_text.length > 0){
                    input_phone.show_text.length--
                    input_phone.show_text=input_phone.show_text.substring(0,input_phone.show_text.length-1)
                    count--;                 
                }
                if (str != "" && input_phone.show_text.length< character){
                    input_phone.show_text.length++
                    count++;
                }
                if (input_phone.show_text.length>=character){
                    str=""
                    input_phone.show_text.length=character
                }else{
                    input_phone.show_text += str
                }
                if(count>0){
                    if (input_username.show_text!=""){
                        btn_lanjut.buttonColor = "#ff524f"
                        btn_lanjut.enabled = true
                    }
                    input_phone.borderColor = "#ff7e7e"
                    del_password.visible=true
                } else{
                    btn_lanjut.buttonColor = "#c4c4c4"
                    btn_lanjut.enabled = false
                    input_phone.borderColor = "#8c8c8c"
                    del_password.visible=false
                }

            }
        }
    }

    DimmDisplay{
        id:dimm_display
        visible:false
    }

    AnimatedImage{
        id: waiting
        visible:false
        width: 200
        height: 200
        anchors.verticalCenter: parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter
        source: 'asset/global/animation/loading.gif'
    }

NotifSwipe{
        id:notif_wrongnumber
        img_y:50
        source_img: asset_path + "wrong_number.png"
        title_text:qsTr("Periksa kembali No. Handphone <br>kamu ")
        body_text:qsTr("Mohon periksa kembali, format no. handphone yang <br>kamu masukkan salah.")
        NotifButton{
            id:ok_back
            x:50
            y:337
            buttonText:qsTr("KEMBALI")
            modeReverse:false
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    press = 1;
                    hide_wrong_number.start();
                    dimm_display.visible = false;
                }
                onEntered:{
                    ok_back.modeReverse = true;
                }
                onExited:{
                    ok_back.modeReverse = false;
                }
            }
        }
    }

    NumberAnimation {
        id:hide_key
        targets: touch_keyboard
        properties: "y"
        from:368
        to:768
        duration: 500
        easing.type: Easing.InOutBack
    }
    NumberAnimation {
        id:show_key
        targets: touch_keyboard
        properties: "y"
        from:768
        to:368
        duration: 500
        easing.type: Easing.InOutBack
    }

    NumberAnimation {
        id:hide_wrong_number
        targets: notif_wrongnumber
        properties: "y"
        from:287
        to:768
        duration: 500
        easing.type: Easing.InOutBack
    }
    NumberAnimation {
        id:show_wrong_number
        targets: notif_wrongnumber
        properties: "y"
        from:768
        to:287
        duration: 500
        easing.type: Easing.InOutBack
    }

    function check_phonenumber(phone_param) {
        if (language=="MY") {
            check_number = check_number_my;
            phone_substring = phone_param.substring(0,3);
        }else{
            check_number = check_number_id;
            phone_substring = phone_param.substring(0,4);
        }
        if(check_number.indexOf(phone_substring) > -1 && phone_param.length > 9){
            console.log("NOMER BENAR",phone_param);
            my_stack_view.push(input_email, {cust_name: input_username.show_text, cust_phone: phone_param});
            
        }else{
            console.log("NOMER SALAH",phone_param);
            show_wrong_number.start();
            dimm_display.visible = true;
        }
    }

    function locker_language(result) {
        console.log("Bahasa : ", result);
        language = result;
    }

}