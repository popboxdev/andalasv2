import QtQuick 2.4
import QtQuick.Controls 1.2

Background{
    id: success_return
    property var press: '0'
    property var asset_path: 'asset/send_page/'
    property var txt_time: ""
    show_img: ""
    img_vis: true

    Stack.onStatusChanged:{
        if(Stack.status == Stack.Activating){
            press = '0';
            timer.secon = 90;
            timer_note.restart();
        }
        if(Stack.status==Stack.Deactivating){
            timer_note.stop();
        }
    }

    FontType{
        id:fontStyle
        //Type Font ### bold , extrabold , light , book, medium, black ###
    }

    Header{
        id:header
        title_text:qsTr("Pengembalian Barang")
        img_vis:false
        text_timer : txt_time
    }

    Item{
        id: timer
        property int secon  : 90
        Timer{
            id:timer_note
            interval:1000
            repeat:true
            running:true
            triggeredOnStart:true
            onTriggered:{
                timer.secon -= 1
                txt_time = timer.secon
                if(timer.secon < 1){
                    timer_note.stop()
                    my_stack_view.push(time_out)
                }
            }
        }
    }

    Text {
        x:50
        y:215
        text: qsTr("Barang Telah Kami Terima")
        font.pixelSize:40
        color:"#414042"
        font.family: fontStyle.medium
    }

    Text {
        x:50
        y:298
        text: qsTr("Hai Poppers, pengembalian barang kamu telah kami <br>terima dan akan kami kirimkan ke warehouse toko <br>online tempat kamu belanja")
        font.pixelSize:24
        color:"#414042"
        font.family: fontStyle.book
    }

    Image{
        x:659
        y:198
        height:289
        width:325
        source:asset_path + "return/success.png"
    }
    NotifButton{
        id:btn_ok
        x:50
        y:416
        r_radius:0
        buttonText:qsTr("OKE")
        modeReverse:false
        MouseArea{
            anchors.fill: parent
            onClicked: {
                // show_selesai.start();
                my_stack_view.push(survey_page)
                // dimm_display.visible=true;
            }
            onEntered:{
                btn_ok.modeReverse = true
            }
            onExited:{
                btn_ok.modeReverse = false
            }
        }
    }

    DimmDisplay{
        id:dimm_display
        visible:false
    }

    NotifSwipe{
        id:notif_selesai
        source_img: asset_path + "return/isiform.png"
        img_y:50
        titlebody_x:50
        title_y:60
        body_y:180
        title_text:qsTr("Terima Kasih Kamu Telah <br>Menggunakan PopBox ")
        body_text:qsTr("Bantu kami untuk terus memberikan layanan <br>terbaik dengan mengisi form kepuasan <br>pelanggan.")
        NotifButton{
            id:btn_sls
            x:50
            y:304
            r_radius:0
            modeReverse:true
            buttonText:qsTr("SELESAI")
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
                }
                onEntered:{
                    btn_sls.modeReverse = false
                }
                onExited:{
                    btn_sls.modeReverse = true
                }
            }
        }
        NotifButton{
            id:btn_form
            x:300
            y:304
            r_radius:0
            buttonText:qsTr("ISI FORM")
            modeReverse:false
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    hide_selesai.start();
                    dimm_display.visible=false;
                    my_stack_view.push(survey_page)
                }
                onEntered:{
                    btn_form.modeReverse = true
                }
                onExited:{
                    btn_form.modeReverse = false
                }

            }
        }
    }

    NumberAnimation {
        id:hide_selesai
        targets: notif_selesai
        properties: "y"
        from:248
        to:768
        duration: 500
        easing.type: Easing.InOutBack
    }
    NumberAnimation {
        id:show_selesai
        targets: notif_selesai
        properties: "y"
        from:768
        to:248
        duration: 500
        easing.type: Easing.InOutBack
    }
}
