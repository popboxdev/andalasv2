import QtQuick 2.4
import QtQuick.Controls 1.2

Background{
    id: detail_parcel
    property var press: '0'
    property var asset_path: 'asset/survey/'
    property var survey_param:''
    property var color_button: "#c4c4c4"
    property var txt_time: ""

    show_img: ""
    img_vis: true

    Stack.onStatusChanged:{
        if(Stack.status == Stack.Activating){
            press = '0';
            timer.secon = 90;
            timer_note.restart();
            // header.timer_start=true
        }
        if(Stack.status==Stack.Deactivating){
            // header.timer_start=false
            timer_note.stop();
        }
    }

    FontType{
        id:fontStyle
        //Type Font ### bold , extrabold , light , book, medium, black ###
    }

    Header{
        id:header
        img_vis: false
        title_text:qsTr("Survey PopBox")
        text_timer : txt_time
    }

    Item{
        id: timer
        property int secon: 90
        property int time_sms: 30
        
        Timer{
            id:timer_note
            interval:1000
            repeat:true
            running:true
            triggeredOnStart:true
            onTriggered:{
                timer.secon -= 1
                if(timer.time_sms>0){
                    timer.time_sms -= 1
                }else{
                    timer.time_sms = 0
                }
                txt_time = timer.secon
                if(timer.secon < 1){
                    timer_note.stop()
                    my_stack_view.push(time_out)
                }
            }
        }
    }

    Text {
        id: title_text
        x:45
        y:139
        text: qsTr("Hai Popers Bagaimana menurut kamu tentang pengalaman <br>menggunakan Loker Popbox ?")
        font.pixelSize:32
        color:"#414042"
        font.family: fontStyle.book
    } 

    Rectangle{
        id:angry
        x:179
        y:297
        height:130
        width:130
        border.width: 0
        border.color: "#ff524f"
        color:"transparent"
        Image{
            height:123
            width:123
            anchors.verticalCenter: parent.verticalCenter
            anchors.horizontalCenter: parent.horizontalCenter
            source:asset_path + "angry.png"
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    survey_param="angry"
                    button_inactive(survey_param)
                }
            }
        }
    }

    Rectangle{
        id:confused
        x:450
        y:297
        height:130
        width:130
        border.width: 0
        border.color: "#ff524f"
        color:"transparent"
        Image{
            height:123
            width:123
            anchors.verticalCenter: parent.verticalCenter
            anchors.horizontalCenter: parent.horizontalCenter
            source:asset_path + "confused.png"
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    survey_param="confused"
                    button_inactive(survey_param)
                }
            }
        }
    }

    Rectangle{
        id:inlove
        x:721
        y:297
        height:130
        width:130
        border.width: 0
        border.color: "#ff524f"
        color:"transparent"
        Image{
            height:123
            width:123
            anchors.verticalCenter: parent.verticalCenter
            anchors.horizontalCenter: parent.horizontalCenter
            source:asset_path + "inlove.png"
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    survey_param="inlove"
                    button_inactive(survey_param)
                }
            }
        }
    }

    Rectangle{
        x:179
        y:435
        height:35
        width:130
        color:"transparent"
        Text {
            id:angry_text
            anchors.verticalCenter: parent.verticalCenter
            anchors.horizontalCenter: parent.horizontalCenter
            text: qsTr("Tidak Suka")
            font.bold: false
            font.pixelSize:24
            color:"#4a4a4a"
            font.family: fontStyle.book
        }
    }

    Rectangle{
        x:450
        y:435
        height:35
        width:130
        color:"transparent"
        Text {
            id:confused_text
            anchors.verticalCenter: parent.verticalCenter
            anchors.horizontalCenter: parent.horizontalCenter
            text: qsTr("Biasa Saja")
            font.bold: false
            font.pixelSize:24
            color:"#4a4a4a"
            font.family: fontStyle.book
        }
    }

    Rectangle{
        x:721
        y:435
        height:35
        width:130
        Text {
            id:inlove_text
            anchors.verticalCenter: parent.verticalCenter
            anchors.horizontalCenter: parent.horizontalCenter
            text: qsTr("Senang")
            font.bold: false
            font.pixelSize:24
            color:"#4a4a4a"
            font.family: fontStyle.book
        }
    }

    NotifButton{
        id:btn_lanjut
        x:759
        y:619
        r_width:230
        r_height:79
        buttonText:qsTr("LANJUT")
        buttonColor: color_button
        r_radius:2
        enabled: false
        MouseArea{
            anchors.fill: parent
            onClicked: {
                console.log(survey_param)
                my_stack_view.pop(my_stack_view.find(function(item){if(item.Stack.index === 0) return true }))
                // dimm_display.visible = true;
                // show_thanks.start();
                // header.timer_value = 5
                // header.timer_start=false
            }
        }
    }

    DimmDisplay{
        id:dimm_display
        visible:false
    }

    NotifSwipe{
        id:notif_thanks
        source_img: asset_path + "bg.png"
        img_y:50
        titlebody_x:40
        title_y:110
        body_y:227
        title_text:qsTr("Terima kasih Atas Partisipasi <br>Kamu")
        body_text:qsTr("Opini Popers bermanfaat untuk meningatkan <br>layanan PopBox .")
    }

    NumberAnimation {
        id:hide_thanks
        targets: notif_thanks
        properties: "y"
        from:248
        to:768
        duration: 500
        easing.type: Easing.InOutBack
    }
    NumberAnimation {
        id:show_thanks
        targets: notif_thanks
        properties: "y"
        from:768
        to:248
        duration: 500
        easing.type: Easing.InOutBack
    }


    function button_inactive(param){
        console.log(param)
        angry.border.width= 0
        confused.border.width= 0
        inlove.border.width= 0

        angry_text.font.bold= false
        confused_text.font.bold= false
        inlove_text.font.bold= false

        angry_text.color= "#4a4a4a"
        confused_text.color= "#4a4a4a"
        inlove_text.color= "#4a4a4a"
        color_button="#ff524f"
        btn_lanjut.enabled = true;

        if(param=="angry"){
            angry.border.width= 3
            angry_text.font.bold= true
            angry_text.color= "#ff524f"
        }
        if(param=="confused"){
            confused.border.width= 3
            confused_text.font.bold= true
            confused_text.color= "#ff524f"
        }
        if(param=="inlove"){
            inlove.border.width= 3
            inlove_text.font.bold= true
            inlove_text.color= "#ff524f"
        }
    }
}
