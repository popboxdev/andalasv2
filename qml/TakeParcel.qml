import QtQuick 2.4
import QtQuick.Controls 1.2

Background{
    id: take_parcel
    property var press: '0'
    property var pin_code: ''
    property var asset_path: 'asset/send_page/popsend/'
    property var title: qsTr("AMBIL")
    property var select_courier:""
    property var phone_number: ""
    property var parcel_number: ""
    property var no_parcel: ""
    property var font_reg: "SourceSansPro-Regular"
    property var font_bold: "SourceSansPro-SemiBold"
    property int count: 0
    property var parcel_overdue: ""
    property var txt_time: ""
    property int door_no: 0
    property var door_type: ""
    property var company_type: ""
    property var survey_expressNumber: ""
    property var survey_expressId: ""
    property var express_id: ""
    property var language: 'ID'
    property bool contactless: false
    property bool timer_popup: false
    property var is_success: "false"
    property var qr_content: ""
    // property int is_drop: 1
    

    Stack.onStatusChanged:{
        if(Stack.status == Stack.Activating){
            timer_startup.stop();
            check_input();
            timer.secon = 90
            timer_note.restart()
            press = '0';
            show_key.start();
    	    timer_payment.stop()
            // extend = undefined;
            console.log("COntacles :: ", contactless);
            
        }
        if(Stack.status==Stack.Deactivating){
            timer_startup.stop();
            timer_note.stop()
            slot_handler.stop_courier_scan_barcode()
        }
    }

    FontType{
        id:fontStyle
        //Type Font ### bold , extrabold , light , book, medium, black ###
    }

    Header{
        id:header
        title_text:qsTr(title)
        text_timer : txt_time
        funct : "home"
    }
    Item{
        id: timer
        property int secon  : 90
        Timer{
            id:timer_note
            interval:1000
            repeat:true
            running:true
            triggeredOnStart:true
            onTriggered:{
                timer.secon -= 1
                txt_time = timer.secon
                if(timer.secon < 1){
                    timer_note.stop()
                    if(contactless==true){
                        reset_pinbox();
                        contactless = false;
                        timer.secon = 90;
                        timer_note.restart();
                    }else{
                        my_stack_view.push(time_out);
                    }
                }
            }
        }
    }

    Timer {
        id: timer_startup
        interval: 1000
        repeat: false
        running: true
        onTriggered:{   
            load_input();
        }
    }
    
    Text {
        id: krm
        x:50
        y:145
        text: qsTr("Ketik / Scan Kode Ambil")
        font.pixelSize:40
        color:"#414042"
        font.family: fontStyle.medium
    }

    function reset_value(){
        first_box.show_text = "";
        second_box.show_text = "";
        third_box.show_text = "";
        fourth_box.show_text = "";
        fifth_box.show_text = "";
        sixth_box.show_text = "";
        pin_code = '';
        count = 0;
    }

    Row{
        x:50
        y: 230
        spacing: 15
        PinBox{
            id:first_box
        }
        PinBox{
            id:second_box
        }
        PinBox{
            id:third_box
        }
        PinBox{
            id:fourth_box
        }
        PinBox{
            id:fifth_box
        }
        PinBox{
            id:sixth_box
        }
    }

    NotifButton{
        id:btn_lanjut
        x:619
        y:230
        buttonText:qsTr("VERIFIKASI")
        buttonColor: "#8c8c8c"
        r_radius:0
        enabled: false
        modeReverse:false
        MouseArea{
            anchors.fill: parent
            onClicked: {
                if (press!='0') return;
                press = '1';
                pin_code = '';
                pin_code += first_box.show_text;
                pin_code += second_box.show_text;
                pin_code += third_box.show_text;
                pin_code += fourth_box.show_text;
                pin_code += fifth_box.show_text;
                pin_code += sixth_box.show_text;
                slot_handler.get_parcel_data_bypin(pin_code)
                waiting.visible = true;
                dimm_display.visible = true;
            }
            onEntered:{
                btn_lanjut.modeReverse = true
            }
            onExited:{
                btn_lanjut.modeReverse = false
            }
        }
    }

    Text {
        id: txt_pin
        x:50
        y:329
        text: qsTr("Cek WhatsApp dari POPBOX-ASIA atau lihat di Aplikasi PopBox")
        font.pixelSize:22
        color:"#3a3a3a"
        font.family: fontStyle.medium
    }

    KeyBoardAlphanumeric{
        id:touch_keyboard
        property var count:0
        property var validate_c
        closeButton: false
        Component.onCompleted: {
            touch_keyboard.letter_button_clicked.connect(show_validate_code)
        }
        function show_validate_code(str){
            if (str!=''&& str!='OKE'){
                if (count == 0) first_box.show_text = str;
                else if (count == 1) second_box.show_text = str;
                else if (count == 2) third_box.show_text = str;
                else if (count == 3) fourth_box.show_text = str;
                else if (count == 4) fifth_box.show_text = str;
                else if (count == 5) sixth_box.show_text = str;
                if (count <= 5) {
                    if(count==5)
                    {
                        button_active();
                        btn_lanjut.enabled = true
                    }
                    count++;
                }
            }else if(str==''){
                if (count == 1) first_box.show_text = str;
                else if (count == 2) second_box.show_text = str;
                else if (count == 3) third_box.show_text = str;
                else if (count == 4) fourth_box.show_text = str;
                else if (count == 5) fifth_box.show_text = str;
                else if (count == 6) {
                    sixth_box.show_text = str;
                    button_inactive();
                    btn_lanjut.enabled = false
                }
                if (count>0) {
                    count--;
                } else {
                    count=0;
                }
            }else if (str=='OKE'){
                if (count == 0) {
                    if (first_box.show_text =='')
                        first_box.show_text = '';
                    }
                else if (count == 1) {
                    if (second_box.show_text == '')
                        second_box.show_text ='';
                    }
                else if (count == 2) {
                    if (third_box.show_text == '')
                        third_box.show_text ='';
                    }
                else if (count == 3) {
                    if (fourth_box.show_text == '')
                        fourth_box.show_text = ''
                }
                else if (count == 4) {
                    if (fifth_box.show_text == '')
                        fifth_box.show_text =''
                    }
                else if (count == 5) {
                    if (sixth_box.show_text == '')
                        sixth_box.show_text = ''
                    }
            }
            press = '0';
        }
    }

    NumberAnimation {
        id:hide_popsafe
        targets: notif_popsafe
        properties: "y"
        from:248
        to:768
        duration: 500
        easing.type: Easing.InOutBack
    }
    NumberAnimation {
        id:show_popsafe
        targets: notif_popsafe
        properties: "y"
        from:768
        to:248
        duration: 500
        easing.type: Easing.InOutBack
    }
    NumberAnimation {
        id:hide_key
        targets: touch_keyboard
        properties: "y"
        from:368
        to:768
        duration: 500
        easing.type: Easing.InOutBack
    }
    NumberAnimation {
        id:show_key
        targets: touch_keyboard
        properties: "y"
        from:768
        to:368
        duration: 500
        easing.type: Easing.InOutBack
    }

    NumberAnimation {
        id:hide_ambil_parcel
        targets: notif_ambil_parcel
        properties: "y"
        from:248
        to:768
        duration: 500
        easing.type: Easing.InOutBack
    }
    NumberAnimation {
        id:show_ambil_parcel
        targets: notif_ambil_parcel
        properties: "y"
        from:768
        to:248
        duration: 500
        easing.type: Easing.InOutBack
    }

    NumberAnimation {
        id:hide_over_popsafe
        targets: notif_over_popsafe
        properties: "y"
        from:248
        to:768
        duration: 500
        easing.type: Easing.InOutBack
    }
    NumberAnimation {
        id:show_over_popsafe
        targets: notif_over_popsafe
        properties: "y"
        from:768
        to:248
        duration: 500
        easing.type: Easing.InOutBack
    }

    NumberAnimation {
        id:hide_over_parcel
        targets: notif_over_parcel
        properties: "y"
        from:248
        to:768
        duration: 500
        easing.type: Easing.InOutBack
    }
    NumberAnimation {
        id:show_over_parcel
        targets: notif_over_parcel
        properties: "y"
        from:768
        to:248
        duration: 500
        easing.type: Easing.InOutBack
    }

    NumberAnimation {
        id:hide_doorfail
        targets: door_fail
        properties: "y"
        from:248
        to:768
        duration: 500
        easing.type: Easing.InOutBack
    }
    
    NumberAnimation {
        id:show_doorfail
        targets: door_fail
        properties: "y"
        from:768
        to:248
        duration: 500
        easing.type: Easing.InOutBack
    }


    DimmDisplay{
        id:dimm_display
        visible:false
    }

    AnimatedImage{
        id: waiting
        visible:false
        width: 200
        height: 200
        anchors.verticalCenter: parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter
        source: 'asset/global/animation/loading.gif'
    }

    NotifSwipe{
        id: notif_over_parcel
        img_y:56
        source_img: 'asset/take_page/overdue_inlocker.png'
        img_height:440
        body_y: 130
        title_text:qsTr("Paket Melebihi Waktu")
        body_text:qsTr("Paket kamu telah melebihi durasi simpan, <br>hubungi. Customer Service PopBox untuk <br>melakukan perpanjangan.")

        NotifButton{
            id:btn_cs
            x:320
            y:348
            //r_width:283
            r_radius:2
            buttonText:qsTr("HUBUNGI CS")
            modeReverse:false
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    hide_over_parcel.start();
                    dimm_display.visible=false
                    my_stack_view.push(contact_us,{language: language})
                }
                onEntered:{
                    btn_cs.modeReverse = true
                }
                onExited:{
                    btn_cs.modeReverse = false
                }
            }
        }
        NotifButton{
            id:btn_back
            x:50
            y:348
            //r_width:283
            r_radius:2
            modeReverse: true
            buttonText:qsTr("KEMBALI KE HOME")
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    my_stack_view.pop(null)
                    dimm_display.visible=false
                }
                onEntered:{
                    btn_back.modeReverse = false
                }
                onExited:{
                    btn_back.modeReverse = true
                }
            }
        }

        Image{
            x:939
            y:60
            height:33
            width:33
            source:"asset/global/button/close.png"
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    touch_keyboard.count = pin_code.length
                    press = '0'
                    hide_over_parcel.start();
                    slot_handler.start_courier_scan_barcode();
                    dimm_display.visible=false
                }
            }
        }
    }


    NotifSwipe{
        id: notif_over_popsafe
        img_y:56
        //source_img: asset_path + "takedone.png"
        body_y: 130
        title_text:qsTr("Paket Melebihi Waktu")
        body_text:qsTr("Paket sudah melebihi batas waktu <br>pengambilan. Untuk mengambil paket <br>kamu. Silakan melakukan pembayaran <br>atas perpanjangan waktu paket kamu.")
        source_img: 'asset/take_page/overdue_inlocker.png'
        img_height:440

        NotifButton{
            id:ok_over
            x:320
            y:348
            //r_width:283
            r_radius:2
            buttonText:qsTr("LANJUTKAN")
            modeReverse:false
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    hide_over_popsafe.start()
                    my_stack_view.push(select_payment, {storeExtend: true, data_overdue: parcel_overdue})
                    dimm_display.visible=false
                }
                onEntered:{
                    ok_over.modeReverse = true
                }
                onExited:{
                    ok_over.modeReverse = false
                }
            }
        }
        NotifButton{
            id:ok_back
            x:50
            y:348
            //r_width:283
            r_radius:2
            modeReverse: true
            buttonText:qsTr("KEMBALI KE HOME")
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    my_stack_view.pop(null)
                    dimm_display.visible=false
                }
                onEntered:{
                    ok_back.modeReverse = false
                }
                onExited:{
                    ok_back.modeReverse = true
                }
            }
        }
        
        Image{
            x:939
            y:60
            height:33
            width:33
            source:"asset/global/button/close.png"
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    touch_keyboard.count = pin_code.length
                    press = '0'
                    hide_over_popsafe.start();
                    dimm_display.visible=false
                }

            }
        }
    }


    NotifSwipe{
        id: notif_ambil_parcel
        img_y:56
        source_img: 'asset/take_page/success.png'
        title_text:qsTr("Kode Ambil Berhasil Diverifikasi")
        title_y: 60
        body_text:qsTr("Kode ambil yang kamu masukkan berhasil diverifikasi, <br>mohon tunggu hingga pintu loker terbuka untuk <br>mengambil barang kamu.")
        body_y: 137
        Rectangle{
            id: timer_set
            x:50
            y:50
            QtObject{
                id:secon
                property int counter
                Component.onCompleted:{
                    secon.counter = 3
                }
            }

            Timer{
                id:timer_payment
                interval:1000
                repeat:true
                running:true
                triggeredOnStart:true
                onTriggered:{
                    secon.counter -= 1
                    if(secon.counter < 0){
                        timer_payment.stop()
                        console.log('id_express_ambil' + express_id)
                        my_stack_view.push(open_door, {nomorLoker: door_no, door_type: door_type, header_title: qsTr("AMBIL"), survey_expressNumber: survey_expressNumber, survey_expressId: survey_expressId, express_id: express_id, access_door: true, button_type:1, contactless: contactless})
                    }
                }
            }
        } 
    }

    NotifSwipe{
        id:notif_popsafe
        img_y:56
        //source_img: asset_path + "bgpin.png"
        source_img : 'asset/take_page/wrong.png'
        body_y: 130
        title_text:qsTr("PIN Yang Kamu Masukkan Salah")
        body_text:qsTr("PIN yang kamu masukkan salah, mohon <br>masukkan kembali PIN yang benar.")
        NotifButton{
            id:ok_retry
            x:320
            y:350
            //r_width:283
            r_radius:2
            buttonText:qsTr("MASUKKAN ULANG")
            modeReverse:false
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    reset_pinbox();
                    timer.secon = 90;
                }
                onEntered:{
                    ok_retry.modeReverse = true
                }
                onExited:{
                    ok_retry.modeReverse = false
                }
            }
        }
        NotifButton{
            id:oke_back
            x:50
            y:350
            //r_width:283
            r_radius:2
            modeReverse: true
            buttonText:qsTr("KEMBALI KE HOME")
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    my_stack_view.pop(null)
                    dimm_display.visible=false
                }
                onEntered:{
                    oke_back.modeReverse = false
                }
                onExited:{
                    oke_back.modeReverse = true
                }
            }
        }

        Image{
            x:939
            y:60
            height:33
            width:33
            source:"asset/global/button/close.png"
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    first_box.show_text = ''
                    second_box.show_text = ''
                    third_box.show_text= ''
                    fourth_box.show_text= ''
                    fifth_box.show_text= ''
                    sixth_box.show_text= ''
                    button_inactive();
                    btn_lanjut.enabled = false
                    press='0'
                    touch_keyboard.count = 0
                    hide_popsafe.start();
                    slot_handler.start_courier_scan_barcode();
                    dimm_display.visible=false
                }

            }
        }
    }

    NotifSwipe{
        id:door_fail
        img_y:50
        source_img: "asset/send_page/background/unknown_popsafeCopy.PNG"
        title_text:qsTr("Oops pintu gagal dibuka")
        body_text:qsTr("Mohon ulangi kembali proses")
        NotifButton{
            id:btn_door_fail
            x:50
            y:337
            r_radius:2
            buttonText:qsTr("BATAL")
            modeReverse:false
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    hide_doorfail.start();
                    dimm_display.visible=false
                    my_stack_view.pop(null)
                }
                onEntered:{
                    btn_door_fail.modeReverse = true
                }
                onExited:{
                    btn_door_fail.modeReverse = false
                }
            }
        }

        Image{
            x:939
            y:60
            height:33
            width:33
            source:"asset/global/button/close.png"
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    press=0;
                    hide_doorfail.start();
                    dimm_display.visible=false
                    // my_stack_view.pop()
                }
            }
        }
    }

    Component.onCompleted: {
        root.barcode_result.connect(handle_text)
        root.customer_take_express_result.connect(process_take_parcel)
        root.mouth_number_result.connect(get_locker_number)
        root.get_parcel_data_bypin_result.connect(result_check_parcel)

    }

    Component.onDestruction: {
        root.barcode_result.disconnect(handle_text)
        root.customer_take_express_result.disconnect(process_take_parcel)
        root.mouth_number_result.disconnect(get_locker_number)
        root.get_parcel_data_bypin_result.disconnect(result_check_parcel)

    }

    function handle_text(text){
        console.log(text)
        waiting.visible = true;
        dimm_display.visible = true;
        check_pattern(text);
    }

    function check_pattern(barcode_text){
        slot_handler.stop_courier_scan_barcode();
        var input = barcode_text;
        var result;
        var page;
        var code;
        var sc_separator = input.search("#");
        if(sc_separator > -1){
            result = input.split("#");
            page = result[0];
            code = result[1];
        }else{
            code = barcode_text;
        }
        
        first_box.show_text=code[0]
        second_box.show_text=code[1]
        third_box.show_text=code[2]
        fourth_box.show_text=code[3]
        fifth_box.show_text=code[4]
        sixth_box.show_text=code[5]
        pin_code = code
        slot_handler.get_parcel_data_bypin(pin_code)
        touch_keyboard.count = code.length
    }

    function button_active(){
        btn_lanjut.buttonColor = "#ff524f"
        first_box.borderColor = "#ff7e7e"
        second_box.borderColor = "#ff7e7e"
        third_box.borderColor = "#ff7e7e"
        fourth_box.borderColor = "#ff7e7e"
        fifth_box.borderColor = "#ff7e7e"
        sixth_box.borderColor = "#ff7e7e"
    }
    function button_inactive(){
        btn_lanjut.buttonColor = "#8c8c8c"
        first_box.borderColor = "#8c8c8c"
        second_box.borderColor = "#8c8c8c"
        third_box.borderColor = "#8c8c8c"
        fourth_box.borderColor = "#8c8c8c"
        fifth_box.borderColor = "#8c8c8c"
        sixth_box.borderColor = "#8c8c8c"
    }

    
    function get_locker_number(argument) {
        console.log("locker_number: " + argument)
        var result = JSON.parse(argument)
        if (result.isSuccess == "true") {
            console.log("locker_number_number: " + result.locker_number)
            door_no = result.locker_number
            door_type = result.locker_number_size
        }
    }

    function result_check_parcel(response) {
        var result = JSON.parse(response)
        console.log("result : ", JSON.stringify(response));
        
        if (result.isSuccess == 'true') {
            result = result.data
            survey_expressNumber = result.barcode
            survey_expressId = result.id_express
            express_id = result.id_express
            if (result.is_overdue == 'true') {
                if (result.groupname == 'POPDEPOSIT' || result.groupname == 'POPSAFE' || result.groupname == 'POPTITIP' || result.is_needed_pod != 1) {       
                    company_type = "NON-POPEXPRES"
                    slot_handler.customer_take_express(pin_code);
                } else {
                    slot_handler.start_video_capture('parcel_express_number'+result.expressNumber+"_"+pin_code)
                    dimm_display.visible = true;
                    show_over_parcel.start();
                } 
            } else {
                if (result.groupname == 'POPDEPOSIT' || result.groupname == 'POPSAFE' || result.groupname == 'POPTITIP' || result.is_needed_pod != 1) {
                    // console.log("pincode: " + result.pin_code)
                    company_type = "NON-POPEXPRES"
                    if(language=="MY"){
                        my_stack_view.push(input_recepient_name,{pin_code:pin_code, parcel_number:parcel_number, identity:qsTr("AMBIL"), survey_expressNumber: survey_expressNumber, survey_expressId: survey_expressId, express_id: express_id, language: language});
                    }else{
                        slot_handler.customer_take_express(pin_code);
                    }
                } else {
                    // company_type = "POPEXPRESS"
                    // slot_handler.customer_take_express(pin_code);
                    // dimatikan sementara sesui request bu linny karena dhl belum jalan, jadi input name setelah input pin di disable.
                    pin_code = result.pin_code
                    parcel_number = result.expressNumber
                    console.log('need_pod :' + result.is_needed_pod)
                    if ( result.is_needed_pod == 1 ) {
                        company_type = "POPEXPRESS"
                        my_stack_view.push(input_recepient_name,{pin_code:pin_code, parcel_number:parcel_number, identity:qsTr("AMBIL"), survey_expressNumber: survey_expressNumber, survey_expressId: survey_expressId, express_id: express_id, language: language})
                    } else {
                        company_type = "NON-POPEXPRES"
                        if(language=="MY"){
                            my_stack_view.push(input_recepient_name,{pin_code:pin_code, parcel_number:parcel_number, identity:qsTr("AMBIL"), survey_expressNumber: survey_expressNumber, survey_expressId: survey_expressId, express_id: express_id, language: language});
                        }else{
                            slot_handler.customer_take_express(pin_code);
                        }
                        
                    }
                }
            }
            
        } else if (result.isSuccess == 'false') {
            if(timer_popup==true){
                timer.secon = 5
                contactless = true;
                is_success = "false"
                save_contactless();
            }else{
                timer.secon = 90
                contactless = false;
            }
            dimm_display.visible=true
            show_popsafe.start()
        }
    }

    function process_take_parcel(response) {
        console.log("customer_take_express: " + response)
        waiting.visible = false;
        dimm_display.visible = false;
        
        var result = JSON.parse(response)
        if ( result.data != "" && company_type == "NON-POPEXPRES") {
            if (result.isSuccess == "true") {
                if (result.is_overdue == "true") {
                    if (result.parcelType == "POPSAFE") {
                        slot_handler.start_video_capture('take_popsafe_app_overdue'+pin_code)
                        dimm_display.visible = true;
                        show_over_parcel.start();
                    } else {
                        slot_handler.start_video_capture('take_popsafe_loker_overdue'+pin_code)
                        dimm_display.visible=true
                        show_over_popsafe.start()
                        parcel_overdue = result.data
                        console.log(" data_overdue : " + parcel_overdue)        
                    }
                } else {
                    slot_handler.start_video_capture('take_express_'+pin_code)
                    if(language=="MY"){
                        my_stack_view.push(input_recepient_name,{pin_code:pin_code, parcel_number:parcel_number, identity:qsTr("AMBIL"), survey_expressNumber: survey_expressNumber, survey_expressId: survey_expressId, express_id: express_id, language: language});
                    }else{
                        // slot_handler.start_video_capture('take_express_'+pin_code)
                        // slot_handler.get_express_mouth_number()

                        if ( express_id == "" || express_id == undefined ) {
                            var data_express = JSON.parse(result.data)
                            express_id = data_express.id
                        } 

                        slot_handler.start_get_express_mouth_number_by_express_id(express_id)
                        show_ambil_parcel.start();
                        timer_payment.start()
                        dimm_display.visible=true
                        if(contactless==true){
                            is_success = "true";
                            save_contactless();
                        }
                    }
                }
            } 
            else if (result.isSuccess == "false" && result.doorFlag == 'false'){
                dimm_display.visible=true
                show_doorfail.start()
            }
            else {
                dimm_display.visible=true
                show_popsafe.start()
            }
        }
        // else if (result.data == null && company_type == "POPEXPRESS"){
        //     if (result.isSuccess == "true"){
        //         slot_handler.start_video_capture('take_express_'+pin_code)
        //         slot_handler.get_express_mouth_number()
        //         show_ambil_parcel.start();
        //         timer_payment.start()
        //         dimm_display.visible=true
        //     } else{
        //         dimm_display.visible=true
        //         show_popsafe.start()
        //     }
        // }
    }

    function reset_pinbox() {
        pin_code = ''
        first_box.show_text = ''
        second_box.show_text = ''
        third_box.show_text= ''
        fourth_box.show_text= ''
        fifth_box.show_text= ''
        sixth_box.show_text= ''
        button_inactive();
        btn_lanjut.enabled = false
        press='0'
        touch_keyboard.count = 0
        hide_popsafe.start();
        slot_handler.start_courier_scan_barcode();
        dimm_display.visible= false
        waiting.visible = false
    }

    function load_input(){
        first_box.show_text=pin_code[0]
        second_box.show_text=pin_code[1]
        third_box.show_text=pin_code[2]
        fourth_box.show_text=pin_code[3]
        fifth_box.show_text=pin_code[4]
        sixth_box.show_text=pin_code[5]
        slot_handler.get_parcel_data_bypin(pin_code);
    }

    function check_input() {
        waiting.visible = true;
        dimm_display.visible = true;
        if(contactless==true){
            timer_popup = true;
            timer_startup.restart();
        }else{
            console.log("BUKAN CONTACTLESS");
            timer_popup = false;
            slot_handler.start_courier_scan_barcode();
            waiting.visible = false;
            dimm_display.visible = false;
        }
    }

    function save_contactless() {
        var data_contactless = JSON.stringify({
            "is_success": is_success,
            "qr_content": qr_content,
            "pincode": pin_code,
            "is_drop": 0,
            "is_taken" : 1
        })
        slot_handler.start_save_contactless(data_contactless);
    }
}
