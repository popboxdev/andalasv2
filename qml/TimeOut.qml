import QtQuick 2.4
import QtQuick.Controls 1.2

Background{
    id:times_up
    img_vis:false
    property var value_timer : 30
    property bool flagQr:false

    Stack.onStatusChanged:{
        if(Stack.status == Stack.Activating){
            show_time_out.start()
            dimm_display.visible= true
        }
        if(Stack.status==Stack.Deactivating){
        }
    }
    Rectangle{
        id: timer
        visible: false
        QtObject{
            id:secon
            property int count
            Component.onCompleted:{
                secon.count = value_timer
            }
        }

        Timer{
            id:timer_note
            interval:1000
            repeat:true
            running:true
            triggeredOnStart:true
            onTriggered:{
                secon.count -= 1
                if(secon.count < 1){
                    timer_note.stop()
                    hide_time_out.start()
                    if (flagQr == true ){
                        my_stack_view.pop()
                    }   
                    else{
                        my_stack_view.pop(null)
                    }
                }
            }
        }
    }

    NumberAnimation {
        id:show_time_out
        targets: time_out
        properties: "y"
        from:768
        to:248
        duration: 500
        easing.type: Easing.InOutBack
    }

    NumberAnimation {
        id:hide_time_out
        targets: time_out
        properties: "y"
        from:248
        to:768
        duration: 500
        easing.type: Easing.InOutBack
    }

    DimmDisplay{
        id:dimm_display
        visible:false
    }

    NotifSwipe{
        id:time_out
        title_text: qsTr("oppss.. Waktu Habis")
        body_text:qsTr("Apakah kamu ingin menambah waktu ?")
        titlebody_x: 50
        body_y: 130
        r_height: 535
        source_img:"asset/box/timeout.png"
        img_y:40
        img_height: 470

        NotifButton{
            id: no_time
            x: 50
            y: 350
            buttonText: qsTr("TIDAK")
            modeReverse: (flagQr==true)? false : true
            buttonColor:(flagQr==true)? "#8c8c8c" : '#ff524f'
            enabled:(flagQr== true)? false : true
            // modeReverse: true
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    hide_time_out.start()
                    my_stack_view.pop(null)
                }
                onEntered:{
                    no_time.modeReverse = false
                }
                onExited:{
                    no_time.modeReverse = true
                }
            }
        }

        NotifButton{
            id: yes_time
            x: 310
            y: 350
            buttonText: qsTr("YA")
            // modeReverse: true
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    hide_time_out.start()
                    dimm_display.visible= false
                    my_stack_view.pop()
                }
                onEntered:{
                    yes_time.modeReverse = true
                }
                onExited:{
                    yes_time.modeReverse = false
                }
            }
        }
    }
}