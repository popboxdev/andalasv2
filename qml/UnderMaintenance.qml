import QtQuick 2.4
import QtQuick.Controls 1.2

Rectangle{
    id:under
    property var count : 0
    property var language: 'ID'
    Image{
        width: 1024
        height: 768
        x:0
        y:0
        source: (language=='MY') ? "asset/desktop/maintenance_mode_15_-_malay.jpg" : "asset/desktop/maintenance15.jpg"
        // visible: true
    }
    Rectangle{
        x: 600
        y: 230
        width:50
        height:50
        color: "transparent"
        MouseArea {
            anchors.fill: parent
            onClicked: {
                count +=1;
                if (count==4){
                    my_stack_view.pop();
                    count = 0;
                }
                
            }
        }
    }
    Component.onCompleted: {
        root.maintenance_status_result.connect(maintenance_status);
    }

    Component.onDestruction: {
        root.maintenance_status_result.disconnect(maintenance_status);
    }
    function maintenance_status(rest_){
        if (rest_ == "False"){
            my_stack_view.pop(null)
        }
    }
}

