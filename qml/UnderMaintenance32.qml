import QtQuick 2.4
import QtQuick.Controls 1.2

Rectangle{
    id:under32
    property var count : 0
    property var language: 'ID'
    Image{
        width: 1080
        height: 1920
        x:0
        y:0
        source: (language=='MY') ? "asset/desktop/maintenance_mode_32_-malay.jpg" : "asset/desktop/maintenance32.jpg"
        // visible: true
    }
    Rectangle{
        x: 600
        y: 230
        width:50
        height:50
        // color: "transparent"
        color: "yellow"
        MouseArea {
            anchors.fill: parent
            onClicked: {
                count +=1;
                if (count==4){
                    my_stack_view.pop(null);
                    count = 0;
                }
                
            }
        }
    }
    Component.onCompleted: {
        root.maintenance_status_result.connect(maintenance_status);
    }

    Component.onDestruction: {
        root.maintenance_status_result.disconnect(maintenance_status);
    }
    function maintenance_status(rest_){
        if (rest_ == "False"){
            my_stack_view.pop(null)
        }
    }
}

