<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="id_ID">
<context>
    <name>SelectService</name>
    <message>
        <location filename="SelectService.qml" line="212"/>
        <source>Ketersediaan Loker</source>
        <translation>Locker Available</translation>
    </message>
    <message>
        <location filename="SelectService.qml" line="483"/>
        <source>Yahh ,, Lokernya Sudah Penuh</source>
        <translation>Oops , , Locker is full</translation>
    </message>
    <message>
        <location filename="SelectService.qml" line="484"/>
        <source>Silakan mencoba lagi beberapa saat lagi.</source>
        <translation>Please try within a few minutes.</translation>
    </message>
</context>

<context>
    <name>KeyBoardAlphanumeric</name>
    <message>
        <location filename="KeyBoardAlphanumeric.qml" line="46"/>
        <source>TUTUP</source>
        <translation>CLOSE</translation>
    </message>
    <message>
        <location filename="KeyBoardAlphanumeric.qml" line="46"/>
        <source>OKE</source>
        <translation>OK</translation>
    </message>
</context>

<context>
    <name>ReasonCancel</name>
    <message>
        <location filename="ReasonCancel.qml" line="46"/>
        <source>SPASI</source>
        <translation>SPACE</translation>
    </message>
    <message>
        <location filename="ReasonCancel.qml" line="46"/>
        <source>LANJUT</source>
        <translation>NEXT</translation>
    </message>
    <message>
        <location filename="ReasonCancel.qml" line="46"/>
        <source>KEMBALI</source>
        <translation>BACK</translation>
    </message>
    <message>
        <location filename="ReasonCancel.qml" line="46"/>
        <source>KEMBALI KE AWAL</source>
        <translation>BACK TO HOME</translation>
    </message>
    <message>
        <location filename="ReasonCancel.qml" line="46"/>
        <source>KIRIM LAGI</source>
        <translation>STORE AGAIN</translation>
    </message>

    <message>
        <location filename="ReasonCancel.qml" line="46"/>
        <source>Pembatalan Berhasil</source>
        <translation>Successful Cancellation</translation>
    </message>
    <message>
        <location filename="ReasonCancel.qml" line="46"/>
        <source>Proses pembatalan yang kamu lakukan berhasil</source>
        <translation>Your cancellation process was successful</translation>
    </message>
    <message>
        <location filename="ReasonCancel.qml" line="46"/>
        <source>Alasan Pembatalan</source>
        <translation>Reason for Cancellation</translation>
    </message>
    <message>
        <location filename="ReasonCancel.qml" line="46"/>
        <source>Barang tidak cukup masuk ke loker</source>
        <translation>The item will not fit in the locker</translation>
    </message>
    <message>
        <location filename="ReasonCancel.qml" line="46"/>
        <source>Pintu loker tidak terbuka</source>
        <translation>The locker door is not open</translation>
    </message>
    <message>
        <location filename="ReasonCancel.qml" line="46"/>
        <source>Masukkan alasan lainnya</source>
        <translation>Enter another reason</translation>
    </message>
</context>

<context>
    <name>TimeOut</name>
    <message>
        <location filename="TimeOut.qml" line="78"/>
        <source>oppss.. Waktu Habis</source>
        <translation>oppss.. Time Out</translation>
    </message>
    <message>
        <location filename="TimeOut.qml" line="80"/>
        <source>Apakah kamu ingin menambah waktu ?</source>
        <translation>Do you want to add some time?</translation>
    </message>
    <message>
        <location filename="TimeOut.qml" line="106"/>
        <source>YA</source>
        <translation>YES</translation>
    </message>
    <message>
        <location filename="TimeOut.qml" line="90"/>
        <source>TIDAK</source>
        <translation>NO</translation>
    </message>
</context>

<context>
    <name>InputCustomerName</name>
    <message>
        <location filename="InputCustomerName.qml" line="46"/>
        <source>Oops pintu gagal dibuka</source>
        <translation>Door is failed to open</translation>
    </message>
    <message>
        <location filename="InputCustomerName.qml" line="46"/>
        <source>Mohon ulangi kembali proses</source>
        <translation>Please repeat the process</translation>
    </message>
    <message>
        <location filename="InputCustomerName.qml" line="46"/>
        <source>Nama Kamu</source>
        <translation>Your Name</translation>
    </message>
    <message>
        <location filename="InputCustomerName.qml" line="46"/>
        <source>LANJUT</source>
        <translation>NEXT</translation>
    </message>
</context>

<context>
    <name>InputEmail</name>
    <message>
        <location filename="InputEmail.qml" line="46"/>
        <source>OKE</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="InputEmail.qml" line="46"/>
        <source>LANJUT</source>
        <translation>NEXT</translation>
    </message>
    <message>
        <location filename="InputEmail.qml" line="46"/>
        <source>BATAL</source>
        <translation>CANCEL</translation>
    </message>
    <message>
        <location filename="InputEmail.qml" line="46"/>
        <source>MASUKKAN ULANG</source>
        <translation>REINPUT</translation>
    </message>
    <message>
        <location filename="InputEmail.qml" line="46"/>
        <source>Terima Kasih</source>
        <translation>ThankYou</translation>
    </message>
    <message>
        <location filename="InputEmail.qml" line="46"/>
        <source>Telah berlangganan newsletter kami, Silahkan cek &lt;br&gt;kotak masuk di email kamu.</source>
        <translation>for subsribing our newsletter, please check &lt;br&gt;your inbox mail.</translation>
    </message>
    <message>
        <location filename="InputEmail.qml" line="46"/>
        <source>Alamat Email Salah</source>
        <translation>Error Email Address</translation>
    </message>
    <message>
        <location filename="InputEmail.qml" line="46"/>
        <source>Silakan masukkan alamat email yang benar, &lt;br&gt;misalnya nama@mail.com</source>
        <translation>Please enter a valid email address, for &lt;br&gt;example name@mail.com"</translation>
    </message>
    <message>
        <location filename="InputEmail.qml" line="46"/>
        <source>Email Sudah Digunakan</source>
        <translation>Email Already Used</translation>
    </message>
    <message>
        <location filename="InputEmail.qml" line="46"/>
        <source>Email yang kamu masukkan sudah terdaftar &lt;br&gt;untuk berlangganan newsletter, silahkan &lt;br&gt;masukkan email lain.</source>
        <translation>The email you entered is already registered &lt;br&gt;to subscribe to the newsletter, please &lt;br&gt;enter another email.</translation>
    </message>
    <message>
        <location filename="InputEmail.qml" line="46"/>
        <source>COBA LAGI</source>
        <translation>TRY AGAIN</translation>
    </message>
    <message>
        <location filename="InputEmail.qml" line="46"/>
        <source>Silakan masukkan alamat email yang benar, misalnya name@mail.com</source>
        <translation>Please enter a valid email address, for example name@mail.com</translation>
    </message>
</context>

<context>
    <name>SubscribePage</name>
    <message>
        <location filename="SubscribePage.qml" line="46"/>
        <source>Nama</source>
        <translation>Name</translation>
    </message>
    <message>
        <location filename="SubscribePage.qml" line="46"/>
        <source>Nomor Handphone</source>
        <translation>Mobile Number</translation>
    </message>
    <message>
        <location filename="SubscribePage.qml" line="46"/>
        <source>Periksa kembali No. Handphone &lt;br&gt;kamu </source>
        <translation>Please check your mobile &lt;br&gt;number </translation>
    </message>
    <message>
        <location filename="SubscribePage.qml" line="46"/>
        <source>Mohon periksa kembali, format no. handphone yang &lt;br&gt;kamu masukkan salah.</source>
        <translation>Please check again, your mobile number is wrong.</translation>
    </message>
    <message>
        <location filename="SubscribePage.qml" line="46"/>
        <source>KEMBALI</source>
        <translation>BACK</translation>
    </message>
    <message>
        <location filename="SubscribePage.qml" line="46"/>
        <source>LANJUT</source>
        <translation>NEXT</translation>
    </message>
</context>

<context>
    <name>ContactUs</name>
    <message>
        <location filename="ContactUs.qml" line="46"/>
        <source>08:00 - 21:00</source>
        <translation>09:00 - 18:00</translation>
    </message>
    <message>
        <location filename="ContactUs.qml" line="46"/>
        <source>08:00 - 19:00</source>
        <translation>Closed</translation>
    </message>
    <message>
        <location filename="ContactUs.qml" line="46"/>
        <source>Hubungi Kami</source>
        <translation>Contact Us</translation>
    </message>
    <message>
        <location filename="ContactUs.qml" line="46"/>
        <source>Punya Pertanyaan?</source>
        <translation>Have a Question?</translation>
    </message>
    <message>
        <location filename="ContactUs.qml" line="46"/>
        <source>Tim kami akan senang membantu kamu</source>
        <translation>Our team will be happy to help you</translation>
    </message>
    <message>
        <location filename="ContactUs.qml" line="46"/>
        <source>Jam Operasional</source>
        <translation>Operational hours</translation>
    </message>
    <message>
        <location filename="ContactUs.qml" line="46"/>
        <source>Senin - Jumat</source>
        <translation>Monday - Friday</translation>
    </message>
    <message>
        <location filename="ContactUs.qml" line="46"/>
        <source>Sabtu</source>
        <translation>Saturday</translation>
    </message>
    <message>
        <location filename="ContactUs.qml" line="46"/>
        <source>Minggu</source>
        <translation>Sunday</translation>
    </message>
    <message>
        <location filename="ContactUs.qml" line="46"/>
        <source>Live Chat : PopBoxApp dan Website PopBox</source>
        <translation>Live Chat : Website and PopBoxApp</translation>
    </message>
    <message>
        <location filename="ContactUs.qml" line="46"/>
        <source>Email : info@popbox.asia</source>
        <translation>Email : infomy@popbox.asia</translation>
    </message>
    <message>
        <location filename="ContactUs.qml" line="46"/>
        <source>Hotline : 021-22538719</source>
        <translation>Hotline : 03-2935 9024</translation>
    </message>
</context>

<context>
    <name>CourierHomePage</name>
    <message>
        <location filename="CourierHomePage.qml" line="46"/>
        <source>LOGISTIK</source>
        <translation>LOGISTIC</translation>
    </message>
    <message>
        <location filename="CourierHomePage.qml" line="46"/>
        <source>Yahh ,, Lokernya Sudah Penuh</source>
        <translation>Oops , , Locker is full</translation>
    </message>
    <message>
        <location filename="CourierHomePage.qml" line="46"/>
        <source>Silakan mencoba lagi beberapa saat lagi.</source>
        <translation>Please try within a few minutes.</translation>
    </message>
    <message>
        <location filename="CourierHomePage.qml" line="141"/>
        <source>AMBIL PAKET</source>
        <translation>Parcel &lt;br&gt;Collection</translation>
    </message>
    <message>
        <location filename="CourierHomePage.qml" line="165"/>
        <source>SIMPAN PAKET</source>
        <translation>Safekeep &lt;br&gt;Parcel</translation>
    </message>
    <message>
        <location filename="CourierHomePage.qml" line="46"/>
        <source>YA</source>
        <translation>YES</translation>
    </message>
    <message>
        <location filename="CourierHomePage.qml" line="46"/>
        <source>TIDAK</source>
        <translation>NO</translation>
    </message>
    <message>
        <location filename="CourierHomePage.qml" line="46"/>
        <source>Keluar</source>
        <translation>Logout</translation>
    </message>
    <message>
        <location filename="CourierHomePage.qml" line="46"/>
        <source>Layanan Logistik</source>
        <translation>Logistic Service</translation>
    </message>
    <message>
        <location filename="CourierHomePage.qml" line="46"/>
        <source>Pilih Layanan yang akan Anda gunakan</source>
        <translation>Please Choose Service Option</translation>
    </message>
    <message>
        <location filename="CourierHomePage.qml" line="46"/>
        <source>Keluar dari Menu Logistik ?</source>
        <translation>Exit from logistic menu?</translation>
    </message>
</context>


<context>
    <name>CourierServicePage</name>
    <message>
        <location filename="CourierServicePage.qml" line="46"/>
        <source>HALAMAN ADMIN</source>
        <translation>ADMINISTRATOR</translation>
    </message>

    <message>
        <location filename="CourierServicePage.qml" line="46"/>
        <source>Keluar</source>
        <translation>Logout</translation>
    </message>
    <message>
        <location filename="CourierServicePage.qml" line="46"/>
        <source>Pilih Opsi Layanan</source>
        <translation>Please Choose Service Option</translation>
    </message>
    <message>
        <location filename="CourierServicePage.qml" line="46"/>
        <source>Ambil Paket</source>
        <translation>Take Parcel</translation>
    </message>
    <message>
        <location filename="CourierServicePage.qml" line="46"/>
        <source>Akses Pintu</source>
        <translation>Door Access</translation>
    </message>
    <message>
        <location filename="CourierServicePage.qml" line="46"/>
        <source>Proses Init Database&lt;br&gt;Berhasil</source>
        <translation>Success Init Database</translation>
    </message>
    <message>
        <location filename="CourierServicePage.qml" line="46"/>
        <source>OKE</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="CourierServicePage.qml" line="46"/>
        <source>YA</source>
        <translation>YES</translation>
    </message>
    <message>
        <location filename="CourierServicePage.qml" line="46"/>
        <source>TIDAK</source>
        <translation>NO</translation>
    </message>
    <message>
        <location filename="CourierServicePage.qml" line="502"/>
        <source>Keluar dari Menu ?</source>
        <translation>Exit from Admin menu ?</translation>
    </message>
    <message>
        <location filename="CourierServicePage.qml" line="502"/>
        <source>Keluar dari Gui / Aplikasi ?</source>
        <translation>Exit from Gui / App ?</translation>
    </message>
</context>

<context>
    <name>OtherCourier</name>
    <message>
        <location filename="OtherCourier.qml" line="46"/>
        <source>Nama Perusahaan Kurir Logistik</source>
        <translation>Your Logistic Company</translation>
    </message>
    <message>
        <location filename="OtherCourier.qml" line="98"/>
        <source>Pastikan nama perusahaan logistik sudah benar</source>
        <translation>Please ensure the entered logistic company name is valid</translation>
    </message>
    <message>
        <location filename="OtherCourier.qml" line="108"/>
        <source>LANJUT</source>
        <translation>NEXT</translation>
    </message>
    <message>
        <location filename="OtherCourier.qml" line="122"/>
        <source>KURIR</source>
        <translation>COURIER</translation>
    </message>
    <message>
        <location filename="OtherCourier.qml" line="122"/>
        <source>Kamu (Kurir)</source>
        <translation> (Courier)</translation>
    </message>
</context>

<context>
    <name>SelectCourier</name>
    <message>
        <location filename="SelectCourier.qml" line="54"/>
        <source>KURIR</source>
        <translation>COURIER</translation>
    </message>
    <message>
        <location filename="SelectCourier.qml" line="54"/>
        <source>PILIH</source>
        <translation>CHOOSE</translation>
    </message>
    <message>
        <location filename="SelectCourier.qml" line="81"/>
        <source>Pilih Nama Kurir Logistik Kamu?</source>
        <translation>Please Choose Your Logistic Company?</translation>
    </message>
    <message>
        <location filename="SelectCourier.qml" line="168"/>
        <source>Kurir lainnya</source>
        <translation>Others</translation>
    </message>
    <message>
        <location filename="SelectCourier.qml" line="521"/>
        <source>Kamu (Kurir)</source>
        <translation> (Courier)</translation>
    </message>
</context>

<context>
    <name>DetailOnDemand</name>
    <message>
        <location filename="DetailOnDemand.qml" line="43"/>
        <source>Hai, </source>
        <translation>Hi, </translation>
    </message>
    <message>
        <location filename="DetailOnDemand.qml" line="43"/>
        <source>No. Order Kamu </source>
        <translation>Your Order Number </translation>
    </message>
    <message>
        <location filename="DetailOnDemand.qml" line="43"/>
        <source>Dari</source>
        <translation>From</translation>
    </message>
    <message>
        <location filename="DetailOnDemand.qml" line="43"/>
        <source>Tujuan</source>
        <translation>Destination</translation>
    </message>
    <message>
        <location filename="DetailOnDemand.qml" line="43"/>
        <source>BATAL</source>
        <translation>CANCEL</translation>
    </message>
    <message>
        <location filename="DetailOnDemand.qml" line="43"/>
        <source>LANJUT</source>
        <translation>NEXT</translation>
    </message>
</context>

<context>
    <name>DetailPopSend</name>
    <message>
        <location filename="DetailPopSend.qml" line="43"/>
        <source>Hai, </source>
        <translation>Hi, </translation>
    </message>
    <message>
        <location filename="DetailPopSend.qml" line="43"/>
        <source>No. Order Kamu </source>
        <translation>Your Order Number </translation>
    </message>
    <message>
        <location filename="DetailPopSend.qml" line="43"/>
        <source>Dari</source>
        <translation>From</translation>
    </message>
    <message>
        <location filename="DetailPopSend.qml" line="43"/>
        <source>Tujuan</source>
        <translation>Destination</translation>
    </message>
    <message>
        <location filename="DetailPopSend.qml" line="43"/>
        <source>LANJUT</source>
        <translation>NEXT</translation>
    </message>
    <message>
        <location filename="DetailPopSend.qml" line="43"/>
        <source>Pastikan paket kamu sudah dibungkus dan dicantumkan no. order</source>
        <translation>Make sure your package has been wrapped and included order number</translation>
    </message>
    <message>
        <location filename="DetailPopSend.qml" line="43"/>
        <source>Dengan mengklik tombol lanjut kamu telah setuju dengan &lt;b&gt;Syarat dan Ketentuan.&lt;/b&gt;</source>
        <translation>By clicking the next button you agree to the &lt;b&gt;Terms and Conditions.&lt;/b&gt;</translation>
    </message>
</context>

<context>
    <name>DetailStoreParcel</name>
    <message>
        <location filename="DetailStoreParcel.qml" line="43"/>
        <source>Hai, </source>
        <translation>Hi, </translation>
    </message>
    <message>
        <location filename="DetailStoreParcel.qml" line="43"/>
        <source>No. Order Kamu </source>
        <translation>Your Order Number </translation>
    </message>
    <message>
        <location filename="DetailStoreParcel.qml" line="43"/>
        <source>LANJUT</source>
        <translation>NEXT</translation>
    </message>
    <message>
        <location filename="DetailStoreParcel.qml" line="43"/>
        <source>TITIP</source>
        <translation>PopSafe</translation>
    </message>
    <message>
        <location filename="DetailStoreParcel.qml" line="43"/>
        <source>Locker Titip</source>
        <translation>Deposit Location</translation>
    </message>
    <message>
        <location filename="DetailStoreParcel.qml" line="43"/>
        <source>Batas Pengambilan Barang* </source>
        <translation>Time Limit to Collect* </translation>
    </message>
    <message>
        <location filename="DetailStoreParcel.qml" line="43"/>
        <source>Transaksi ini berlaku untuk satu (1) kali penyimpanan barang, kode buka yang akan &lt;br&gt;dikirimkan hanya dapat digunakan satu (1) kali.</source>
        <translation>This transaction is valid for one (1) item storage, the open code to be sent &lt;br&gt;can only be used once (1).</translation>
    </message>
    <message>
        <location filename="DetailStoreParcel.qml" line="43"/>
        <source>Dengan mengklik tombol lanjut kamu telah setuju dengan &lt;b&gt;Syarat dan Ketentuan.&lt;/b&gt;</source>
        <translation>By clicking the next button you agree to the &lt;b&gt;Terms and Conditions.&lt;/b&gt;</translation>
    </message>
</context>

<context>
    <name>DetailReturn</name>
    <message>
        <location filename="DetailReturn.qml" line="43"/>
        <source>YA</source>
        <translation>YES</translation>
    </message>
    <message>
        <location filename="DetailReturn.qml" line="43"/>
        <source>TIDAK</source>
        <translation>NO</translation>
    </message>
    <message>
        <location filename="DetailReturn.qml" line="43"/>
        <source>BATAL</source>
        <translation>CANCEL</translation>
    </message>
    <message>
        <location filename="DetailReturn.qml" line="43"/>
        <source>LANJUT</source>
        <translation>NEXT</translation>
    </message>
    <message>
        <location filename="DetailReturn.qml" line="43"/>
        <source>Pengembalian Barang</source>
        <translation>Returning Parcel</translation>
    </message>
    <message>
        <location filename="DetailReturn.qml" line="43"/>
        <source>Detail Pengembalian Barang</source>
        <translation>Parcel Return Details</translation>
    </message>
    <message>
        <location filename="DetailReturn.qml" line="43"/>
        <source>ID Paket</source>
        <translation>Packet ID</translation>
    </message>
    <message>
        <location filename="DetailReturn.qml" line="43"/>
        <source>No Handphone</source>
        <translation>Mobile Number</translation>
    </message>
    <message>
        <location filename="DetailReturn.qml" line="43"/>
        <source>&lt;u&gt;Ubah No Handphone&lt;/u&gt;</source>
        <translation>&lt;u&gt;Change Mobile Number&lt;/u&gt;</translation>
    </message>
    <message>
        <location filename="DetailReturn.qml" line="43"/>
        <source>Lokasi Loker      </source>
        <translation>Locker Location</translation>
    </message>
    <message>
        <location filename="DetailReturn.qml" line="43"/>
        <source>Pastikan nomor yang  dimasukkan aktif, notifikasi akan dikirimkan ke nomor tersebut.</source>
        <translation>Please check and ensure the mobile number is entered correctly.</translation>
    </message>
    <message>
        <location filename="DetailReturn.qml" line="43"/>
        <source>Apakah Kamu Ingin Membatalkan &lt;br&gt;Proses ?</source>
        <translation>Do you want to cancel the whole &lt;br&gt;process?</translation>
    </message>
    <message>
        <location filename="DetailReturn.qml" line="43"/>
        <source>Jika kamu membatalkan proses, maka akan diarahkan &lt;br&gt;ke halaman awal.</source>
        <translation>If you cancel this process, you will be returned &lt;br&gt;to home screen</translation>
    </message>
</context>

<context>
    <name>DetailCourierPopSend</name>
    <message>
        <location filename="DetailCourierPopSend.qml" line="43"/>
        <source>YA</source>
        <translation>YES</translation>
    </message>
    <message>
        <location filename="DetailCourierPopSend.qml" line="43"/>
        <source>TIDAK</source>
        <translation>NO</translation>
    </message>
    <message>
        <location filename="DetailCourierPopSend.qml" line="43"/>
        <source>BATAL</source>
        <translation>CANCEL</translation>
    </message>
    <message>
        <location filename="DetailCourierPopSend.qml" line="43"/>
        <source>LANJUT</source>
        <translation>NEXT</translation>
    </message>
    <message>
        <location filename="DetailCourierPopSend.qml" line="43"/>
        <source>Detail Pengiriman Barang</source>
        <translation>Parcel Delivery Details</translation>
    </message>
    <message>
        <location filename="DetailCourierPopSend.qml" line="43"/>
        <source>No. Resi</source>
        <translation>Tracking number</translation>
    </message>
    <message>
        <location filename="DetailCourierPopSend.qml" line="43"/>
        <source>No.Handphone Kurir</source>
        <translation>Courier Mobile Number</translation>
    </message>
    <message>
        <location filename="DetailCourierPopSend.qml" line="43"/>
        <source>No. Handphone Penerima</source>
        <translation>Recipients&apos;s Mobile Number</translation>
    </message>
    <message>
        <location filename="DetailCourierPopSend.qml" line="43"/>
        <source>KURIR</source>
        <translation>COURIER</translation>
    </message>
    <message>
        <location filename="DetailCourierPopSend.qml" line="43"/>
        <source>Kurir</source>
        <translation>Courier</translation>
    </message>
    <message>
        <location filename="DetailCourierPopSend.qml" line="43"/>
        <source>Pastikan nomor yang  dimasukkan aktif, notifikasi akan dikirimkan ke nomor tersebut.</source>
        <translation>Please check and ensure the mobile number is entered correctly.</translation>
    </message>
    <message>
        <location filename="DetailCourierPopSend.qml" line="43"/>
        <source>Apakah Kamu Ingin Membatalkan &lt;br&gt;Proses ?</source>
        <translation>Do you want to cancel the whole &lt;br&gt;process?</translation>
    </message>
    <message>
        <location filename="DetailCourierPopSend.qml" line="43"/>
        <source>Jika kamu membatalkan proses, maka akan diarahkan &lt;br&gt;ke halaman awal.</source>
        <translation>If you cancel this process, you will be returned &lt;br&gt;to home screen</translation>
    </message>
</context>

<context>
    <name>DetailCourierStore</name>
    <message>
        <location filename="DetailCourierStore.qml" line="43"/>
        <source>YA</source>
        <translation>YES</translation>
    </message>
    <message>
        <location filename="DetailCourierStore.qml" line="43"/>
        <source>TIDAK</source>
        <translation>NO</translation>
    </message>
    <message>
        <location filename="DetailCourierStore.qml" line="43"/>
        <source>BATAL</source>
        <translation>CANCEL</translation>
    </message>
    <message>
        <location filename="DetailCourierStore.qml" line="43"/>
        <source>LANJUT</source>
        <translation>NEXT</translation>
    </message>
    <message>
        <location filename="DetailCourierStore.qml" line="43"/>
        <source>SIMPAN PAKET</source>
        <translation>Safekeep Parcel</translation>
    </message>
    <message>
        <location filename="DetailCourierStore.qml" line="43"/>
        <source>Detail Pengiriman Barang</source>
        <translation>Parcel Delivery Details</translation>
    </message>
    <message>
        <location filename="DetailCourierStore.qml" line="43"/>
        <source>No. Resi                </source>
        <translation>Tracking number</translation>
    </message>
    <message>
        <location filename="DetailCourierStore.qml" line="43"/>
        <source>User Name Kurir</source>
        <translation>Courier Username</translation>
    </message>
    <message>
        <location filename="DetailCourierStore.qml" line="43"/>
        <source>No. Handphone Penerima</source>
        <translation>Recipients&apos;s Mobile Number</translation>
    </message>
    <message>
        <location filename="DetailCourierStore.qml" line="43"/>
        <source>Kurir     </source>
        <translation>Courier</translation>
    </message>
    <message>
        <location filename="DetailCourierStore.qml" line="43"/>
        <source>Pastikan nomor yang  dimasukkan aktif, notifikasi akan dikirimkan ke nomor tersebut.</source>
        <translation>Please check and ensure the mobile number is entered correctly.</translation>
    </message>
    <message>
        <location filename="DetailCourierStore.qml" line="43"/>
        <source>Apakah Kamu Ingin Membatalkan &lt;br&gt;Proses ?</source>
        <translation>Do you want to cancel the whole &lt;br&gt;process?</translation>
    </message>
    <message>
        <location filename="DetailCourierStore.qml" line="43"/>
        <source>Jika kamu membatalkan proses, maka akan diarahkan &lt;br&gt;ke halaman awal.</source>
        <translation>If you cancel this process, you will be returned &lt;br&gt;to home screen</translation>
    </message>
</context>

<context>
    <name>CourierTake</name>
    <message>
        <location filename="CourierTake.qml" line="46"/>
        <source>AMBIL PAKET</source>
        <translation>PARCEL COLLECTION</translation>
    </message>
    <message>
        <location filename="CourierTake.qml" line="46"/>
        <source>AMBIL</source>
        <translation>TAKE PARCEL</translation>
    </message>
    <message>
        <location filename="CourierTake.qml" line="46"/>
        <source>Terlambat</source>
        <translation>Overdue</translation>
    </message>
    <message>
        <location filename="CourierTake.qml" line="46"/>
        <source>Resi</source>
        <translation>Tracking Number</translation>
    </message>
    <message>
        <location filename="CourierTake.qml" line="46"/>
        <source>No Loker</source>
        <translation>Locker No</translation>
    </message>
    <message>
        <location filename="CourierTake.qml" line="46"/>
        <source>Durasi</source>
        <translation>Duration</translation>
    </message>
    <message>
        <location filename="CourierTake.qml" line="46"/>
        <source> Hari</source>
        <translation> Days</translation>
    </message>
    <message>
        <location filename="CourierTake.qml" line="46"/>
        <source>Detail</source>
        <translation>Details</translation>
    </message>
    <message>
        <location filename="CourierTake.qml" line="46"/>
        <source>No Handphone</source>
        <translation>Mobile Number</translation>
    </message>
    <message>
        <location filename="CourierTake.qml" line="46"/>
        <source>Waktu Simpan</source>
        <translation>Store Time</translation>
    </message>
    <message>
        <location filename="CourierTake.qml" line="46"/>
        <source>Waktu Terlambat</source>
        <translation>Overdue Time</translation>
    </message>
    <message>
        <location filename="CourierTake.qml" line="46"/>
        <source>Kembalikan</source>
        <translation>Return</translation>
    </message>
</context>

<context>
    <name>CourierStore</name>
    <message>
        <location filename="CourierStore.qml" line="154"/>
        <source>LANJUT</source>
        <translation>NEXT</translation>
    </message>
    <message>
        <location filename="CourierStore.qml" line="602"/>
        <source>MASUKKAN ULANG</source>
        <translation>REINPUT</translation>
    </message>
    <message>
        <location filename="CourierStore.qml" line="602"/>
        <source>MENGERTI</source>
        <translation>UNDERSTAND</translation>
    </message>
    <message>
        <location filename="CourierStore.qml" line="602"/>
        <source>SIMPAN PAKET</source>
        <translation>Safekeep Parcel</translation>
    </message>
    <message>
        <location filename="CourierStore.qml" line="150"/>
        <source>Lebih gampang scan no. resi kamu, &lt;b&gt;&lt;u&gt;lihat caranya di sini&lt;/u&gt;&lt;/b&gt;</source>
        <translation>Scan Barcode directly, &lt;b&gt;&lt;u&gt;click here for demo&lt;/u&gt;&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="CourierStore.qml" line="74"/>
        <source>Ketik / Scan No. Resi</source>
        <translation>Scan barcode or Type CN</translation>
    </message>
    <message>
        <location filename="CourierStore.qml" line="514"/>
        <source>Nomor Paket Tidak Dapat Diterima</source>
        <translation>Tracking number is wrong</translation>
    </message>
    <message>
        <location filename="CourierStore.qml" line="515"/>
        <source>Pastikan paket Anda adalah paket Collection &lt;br&gt;Point dan pastikan nomor tracking sudah &lt;br&gt;benar(nomor diawali dengan &apos;LXRP-&apos;, &apos;LXRT-&apos;, &lt;br&gt;&apos;ID&apos;, &apos;LXAP-&apos;, &apos;LXAT-&apos;, &apos;LXXB-&apos;, dan &apos;LXEE-&apos;).</source>
        <translation>Make sure your package is a Collection &lt;br&gt;Point package and make sure the tracking number is &lt;br&gt;correct (numbers start with &apos;LXRP-&apos;, &apos;LXRT-&apos;, &lt;br&gt;&apos;ID&apos;, &apos;LXAP-&apos;, &apos;LXAT-&apos;, &apos;LXBB-&apos; and &apos;LXEE-&apos;).</translation>
    </message>
    <message>
        <location filename="CourierStore.qml" line="585"/>
        <source>Pastikan paket Anda adalah paket Collection Point &lt;br&gt;dan pastikan nomor tracking sudah benar &lt;br&gt;dan tidak mengandung spasi atau sepecial karakter.</source>
        <translation>Make sure your package is a Collection Point package &lt;br&gt;and make sure the tracking number is correct &lt;br&gt;and doesn&apos;t contain spaces or special characters.</translation>
    </message>
    <message>
        <location filename="CourierStore.qml" line="665"/>
        <source>Format No. Resi Salah</source>
        <translation>Tracking number is wrong</translation>
    </message>
    <message>
        <location filename="CourierStore.qml" line="666"/>
        <source>Silakan periksa kembali no. resi yang &lt;br&gt;dimasukkan. Pastikan kamu scan ke barcode &lt;br&gt;yang benar.</source>
        <translation>Please check your tracking number again. &lt;br&gt;Make sure that you scan the correct barcode.</translation>
    </message>
    <message>
        <location filename="CourierStore.qml" line="987"/>
        <source>Lebih Gampang dengan Scan &lt;br&gt;No. Resi</source>
        <translation>To save time, just scan &lt;br&gt;your barcode.</translation>
    </message>
    <message>
        <location filename="CourierStore.qml" line="988"/>
        <source>Cukup arahkan barcode no. resi pada barcode &lt;br&gt;scanner yang berada di bawah layar bagian &lt;br&gt;tengah. Jaga jarak antara barcode dengan &lt;br&gt;barcode scanner agar dapat dibaca.</source>
        <translation>Present the barcode on the barcode scanner &lt;br&gt;below the screen. Please allow some 
        distance &lt;br&gt;between scanner and barcode for scanner &lt;br&gt;to detect.</translation>
    </message>
    <message>
        <location filename="CourierStore.qml" line="1026"/>
        <source>Apa itu No. Resi ?</source>
        <translation>What is CN Barcode ?</translation>
    </message>
    <message>
        <location filename="CourierStore.qml" line="1034"/>
        <source>No. Resi adalah nomor pengiriman yang &lt;br&gt;diberikan oleh logistik sebagai tanda bukti &lt;br&gt;pengiriman. &lt;br&gt; &lt;br&gt;Istilah untuk beberapa layanan: &lt;br&gt;Pengembalian Barang: return number/ nomor &lt;br&gt;Pengembalian &lt;br&gt;PopSend/PopSafe: order number (dapat dicek &lt;br&gt;di aplikasi PopBox)</source>
        <translation>CN Barcode is a tracking number given by &lt;br&gt;the logistic company as a proof of delivery. &lt;br&gt;Service Terms:&lt;br&gt;Parcel return: return number &lt;br&gt;PopSend/PopSafe: order number (Please check PopBox Apps)</translation>
    </message>
</context>

<context>
    <name>CourierLoginPage</name>
    <message>
        <location filename="CourierLoginPage.qml" line="123"/>
        <source>ULANGI</source>
        <translation>RETRY</translation>
    </message>
    <message>
        <location filename="CourierLoginPage.qml" line="123"/>
        <source>LANJUT</source>
        <translation>NEXT</translation>
    </message>
    <message>
        <location filename="CourierLoginPage.qml" line="123"/>
        <source>LOGISTIK</source>
        <translation>LOGISTICS</translation>
    </message>
    <message>
        <location filename="CourierLoginPage.qml" line="123"/>
        <source>Username dan password salah! silakan masukkan kembali</source>
        <translation>Authentication failed! Please check your username/password.</translation>
    </message>
    <message>
        <location filename="CourierLoginPage.qml" line="123"/>
        <source>BATAL</source>
        <translation>CANCEL</translation>
    </message>
    <message>
        <location filename="CourierLoginPage.qml" line="123"/>
        <source>Data Login Tidak Dikenal</source>
        <translation>Unrecognized login data</translation>
    </message>
    <message>
        <location filename="CourierLoginPage.qml" line="142"/>
        <source>Data Login yang kamu masukkan salah atau tidak &lt;br&gt;dikenal. Mohon masukkan kembali data login &lt;br&gt;anda</source>
        <translation>Your login data is not recognized. Please retry.</translation>
    </message>
</context>

<context>
    <name>BoxSizePage</name>
    <message>
        <location filename="BoxSizePage.qml" line="123"/>
        <source>Pilih Ukuran Loker</source>
        <translation>Select Compartment Size</translation>
    </message>
    <message>
        <location filename="BoxSizePage.qml" line="142"/>
        <source>Pastikan barang kamu sesuai dengan ukuran loker.</source>
        <translation>Please ensure the size of parcel is compatible to the chosen compartment.</translation>
    </message>
</context>

<context>
    <name>InputPin</name>
    <message>
        <location filename="InputPin.qml" line="130"/>
        <source>VERIFIKASI</source>
        <translation>CONFIRM</translation>
    </message>
    <message>
        <location filename="InputPin.qml" line="130"/>
        <source>Penerima</source>
        <translation> Receiver</translation>
    </message>
    <message>
        <location filename="InputPin.qml" line="130"/>
        <source>KURIR</source>
        <translation>COURIER</translation>
    </message>
    <message>
        <location filename="InputPin.qml" line="130"/>
        <source>MASUKKAN ULANG</source>
        <translation>REINPUT</translation>
    </message>
    <message>
        <location filename="InputPin.qml" line="130"/>
        <source>(Maksimal </source>
        <translation>(Maximum </translation>
    </message>
    <message>
        <location filename="InputPin.qml" line="324"/>
        <source>Kode Verifikasi</source>
        <translation>Verification Code</translation>
    </message>
    <message>
        <location filename="InputPin.qml" line="324"/>
        <source>&lt;u&gt;Kirim Ulang&lt;/u&gt;</source>
        <translation>&lt;u&gt;Resend OTP&lt;/u&gt;</translation>
    </message>
    <message>
        <location filename="InputPin.qml" line="324"/>
        <source>Masukkan kode verifikasi yang dikirimkan lewat &lt;br&gt;WhatsApp ke nomor </source>
        <translation>Insert verification code sent to </translation>
    </message>
    <message>
        <location filename="InputPin.qml" line="130"/>
        <source>Pastikan nomor Customer yang kamu masukan sudah benar</source>
        <translation>Please check and ensure the mobile number is entered correctly.</translation>
    </message>
    <message>
        <location filename="InputPin.qml" line="324"/>
        <source>Kode Verifikasi Salah</source>
        <translation>Wrong verification code</translation>
    </message>
    <message>
        <location filename="InputPin.qml" line="325"/>
        <source>Kode verifikasi yang kamu masukkan salah. &lt;br&gt;Periksa kembali dan masukkan ulang untuk &lt;br&gt;melanjutkan proses</source>
        <translation>Your inputted code is not matched with &lt;br&gt;our record. Please check again and reinput.</translation>
    </message>
</context>

<context>
    <name>InputPhoneNumber</name>
    <message>
        <location filename="InputPhoneNumber.qml" line="102"/>
        <source>No. Handphone </source>
        <translation>Mobile Number</translation>
    </message>
    <message>
        <location filename="InputPhoneNumber.qml" line="102"/>
        <source>Masukkan Ulang No. Handphone </source>
        <translation>Re-input Mobile Number</translation>
    </message>
    <message>
        <location filename="InputPhoneNumber.qml" line="123"/>
        <source>No.Handphone Yang Kamu &lt;br&gt;Masukkan Salah</source>
        <translation>Your Mobile Number is &lt;br&gt;Wrong</translation>
    </message>
    <message>
        <location filename="InputPhoneNumber.qml" line="123"/>
        <source>Periksa kembali, Pastikan kamu memasukkan &lt;br&gt;No. Customer secara benar</source>
        <translation>Please check again, your mobile number is wrong.</translation>
    </message>
    <message>
        <location filename="InputPhoneNumber.qml" line="123"/>
        <source>ULANGI</source>
        <translation>RETRY</translation>
    </message>
    <message>
        <location filename="InputPhoneNumber.qml" line="102"/>
        <source>LANJUT</source>
        <translation>NEXT</translation>
    </message>
    <message>
        <location filename="InputPhoneNumber.qml" line="102"/>
        <source>LANJUTKAN</source>
        <translation>NEXT</translation>
    </message>
    <message>
        <location filename="InputPhoneNumber.qml" line="102"/>
        <source>GANTI NOMOR</source>
        <translation>CHANGE NUMBER</translation>
    </message>
    <message>
        <location filename="InputPhoneNumber.qml" line="102"/>
        <source>KEMBALI</source>
        <translation>BACK</translation>
    </message>
    <message>
        <location filename="InputPhoneNumber.qml" line="102"/>
        <source>BATAL</source>
        <translation>CANCEL</translation>
    </message>
    <message>
        <location filename="InputPhoneNumber.qml" line="102"/>
        <source>UBAH NO. KURIR</source>
        <translation>CHANGE COURIER NUMBER</translation>
    </message>
    <message>
        <location filename="InputPhoneNumber.qml" line="102"/>
        <source>UBAH NO. PELANGGAN</source>
        <translation>CHANGE CUST NUMBER</translation>
    </message>
    <message>
        <location filename="InputPhoneNumber.qml" line="122"/>
        <source>Kamu (Kurir)</source>
        <translation> (Courier)</translation>
    </message>
    <message>
        <location filename="InputPhoneNumber.qml" line="102"/>
        <source>No. Handphone Kurir dan Pelanggan &lt;br&gt;Sama</source>
        <translation>Courier and Customer Phone Cannot &lt;br&gt;Be Same</translation>
    </message>
    <message>
        <location filename="InputPhoneNumber.qml" line="102"/>
        <source>No. Handphone Kurir</source>
        <translation>Courier Mobile Number</translation>
    </message>
    <message>
        <location filename="InputPhoneNumber.qml" line="102"/>
        <source>No. Handphone Pelanggan</source>
        <translation>Customer Mobile Number</translation>
    </message>
    <message>
        <location filename="InputPhoneNumber.qml" line="102"/>
        <source>Nomor handphone kurir dan nomor yang kamu masukkan &lt;br&gt;tidak boleh sama, silakan perbaiki sebelum melanjutkan &lt;br&gt;proses.</source>
        <translation>Courier&apos;s Mobile Number and Customer&apos;s cannot &lt;br&gt;be same. Please fix it before continuing.</translation>
    </message>
    <message>
        <location filename="InputPhoneNumber.qml" line="16"/>
        <source>Pastikan nomor yang dimasukkan aktif, notifikasi akan dikirimkan ke nomor tersebut</source>
        <translation>Please ensure the entered mobile number is active, &lt;br&gt;a verification code will be sent to this number.</translation>
    </message>
    <message>
        <location filename="InputPhoneNumber.qml" line="526"/>
        <source>Periksa kembali No. Handphone &lt;br&gt;kamu </source>
        <translation>Please check your mobile &lt;br&gt;number </translation>
    </message>
    <message>
        <location filename="InputPhoneNumber.qml" line="526"/>
        <source>Mohon periksa kembali, format no. handphone yang &lt;br&gt;kamu masukkan salah.</source>
        <translation>Please check again, your mobile number is wrong.</translation>
    </message>
    <message>
        <location filename="InputPhoneNumber.qml" line="527"/>
        <source>Pastikan no. handphone kamu aktif dan dapat &lt;br&gt;terima WhatsApp. Kode verifikasi akan dikirimkan &lt;br&gt;ke nomor kamu.</source>
        <translation>Please ensure your mobile number is active and &lt;br&gt;able to receive SMS. A verification code will be &lt;br&gt;sent to this number.</translation>
    </message>

</context>

<context>
    <name>SendParcel</name>
    <message>
        <location filename="SendParcel.qml" line="47"/>
        <source>KIRIM</source>
        <translation>PopSend</translation>
    </message>
    <message>
        <location filename="SendParcel.qml" line="771"/>
        <source>COBA KEMBALI</source>
        <translation>TRY AGAIN</translation>
    </message>
    <message>
        <location filename="SendParcel.qml" line="1095"/>
        <source>Kamu</source>
        <translation> You</translation>
    </message>
    <message>
        <location filename="SendParcel.qml" line="771"/>
        <source>BATAL</source>
        <translation>CANCEL</translation>
    </message>
    <message>
        <location filename="SendParcel.qml" line="771"/>
        <source>TUTUP</source>
        <translation>CLOSE</translation>
    </message>
    <message>
        <location filename="SendParcel.qml" line="771"/>
        <source>KURIR</source>
        <translation>COURIER</translation>
    </message>
    <message>
        <location filename="SendParcel.qml" line="154"/>
        <source>LANJUT</source>
        <translation>NEXT</translation>
    </message>
    <message>
        <location filename="SendParcel.qml" line="602"/>
        <source>MASUKKAN ULANG</source>
        <translation>REINPUT</translation>
    </message>
    <message>
        <location filename="SendParcel.qml" line="602"/>
        <source>MENGERTI</source>
        <translation>UNDERSTAND</translation>
    </message>
    <message>
        <location filename="SendParcel.qml" line="624"/>
        <source>HUBUNGI CS</source>
        <translation>CONTACT CS</translation>
    </message>
    <message>
        <location filename="SendParcel.qml" line="1375"/>
        <source>KEMBALI KE HOME</source>
        <translation>BACK TO HOME</translation>
    </message>
    <message>
        <location filename="SendParcel.qml" line="1060"/>
        <source>Pilih Layanan</source>
        <translation>Choose Service</translation>
    </message>

    <message>
        <location filename="SendParcel.qml" line="1169"/>
        <source>Kurir Kirim Barang Dengan No. Resi</source>
        <translation>Courier Send Parcel With CN Barcode</translation>
    </message>
    <message>
        <location filename="SendParcel.qml" line="1197"/>
        <source>Kurir Kirim Barang Tanpa No. Resi</source>
        <translation>Courier Send Parcel Without CN Barcode</translation>
    </message>
    <message>
        <location filename="SendParcel.qml" line="1252"/>
        <source>PopSend (kirim barang via aplikasi)</source>
        <translation>PopSend (Send Parcel Through App)</translation>
    </message>
    <message>
        <location filename="SendParcel.qml" line="1115"/>
        <source>Pengembalian Barang</source>
        <translation>Returning Parcel</translation>
    </message>
    <message>
        <location filename="SendParcel.qml" line="1135"/>
        <source>Kurir Kirim Barang</source>
        <translation>Courier Send Parcel</translation>
    </message>

    <message>
        <location filename="SendParcel.qml" line="151"/>
        <source>Pilih Layanan Lain ?</source>
        <translation>Choose Other Service ?</translation>
    </message>
    <message>
        <location filename="SendParcel.qml" line="151"/>
        <source>Nomor Tidak Dikenal</source>
        <translation>Number is not recognized</translation>
    </message>
    <message>
        <location filename="SendParcel.qml" line="151"/>
        <source>Periksa kembali no. pengembalian barang dan &lt;br&gt;pastikan kamu telah memasukkan nomor yang &lt;br&gt;benar</source>
        <translation>Please check your return number and &lt;br&gt;make sure that you input the correct number</translation>
    </message>

    <message>
        <location filename="SendParcel.qml" line="763"/>
        <source>Yah, Internet tidak stabil</source>
        <translation>Internet connection is unstable</translation>
    </message>
    <message>
        <location filename="SendParcel.qml" line="764"/>
        <source>Maaf jaringan internet sedang tidak stabil, &lt;br&gt;silakan coba kembali</source>
        <translation>Sorry, internet connection is unstable. &lt;br&gt;Please try again.</translation>
    </message>

    <message>
        <location filename="SendParcel.qml" line="665"/>
        <source>Format No. Resi Salah</source>
        <translation>Tracking number is wrong</translation>
    </message>
    <message>
        <location filename="SendParcel.qml" line="666"/>
        <source>Silakan periksa kembali no. resi yang &lt;br&gt;dimasukkan. Pastikan kamu scan ke barcode &lt;br&gt;yang benar.</source>
        <translation>Please check your tracking number again. &lt;br&gt;Make sure that you scan the correct barcode.</translation>
    </message>

    <message>
        <location filename="SendParcel.qml" line="1061"/>
        <source>Hai Popers, layanan apa &lt;br&gt;yang kamu ingin gunakan?</source>
        <translation>Hi Popers, what service &lt;br&gt;that you would like to choose?</translation>
    </message>
    <message>
        <location filename="SendParcel.qml" line="594"/>
        <source>Order Kamu Tidak Dikenali</source>
        <translation>Your order number is not recognized</translation>
    </message>
    <message>
        <location filename="SendParcel.qml" line="595"/>
        <source>Hai Popers, order kamu tidak dikenali. Periksa kembali nomer &lt;br&gt;order kamu dan pastikan kamu telah memasukkan &lt;br&gt;dengan benar</source>
        <translation>Hi Popers, your order number is not recognized. Please check again &lt;br&gt;your order number and make sure it is correct</translation>
    </message>
    <message>
        <location filename="SendParcel.qml" line="1163"/>
        <source>Hai Popers, layanan apa yang kamu &lt;br&gt;ingin gunakan?</source>
        <translation>Hi Popers, what service that you &lt;br&gt;would like to choose?</translation>
    </message>
    <message>
        <location filename="SendParcel.qml" line="74"/>
        <source>Ketik / Scan No. Resi</source>
        <translation>Scan barcode or Type CN</translation>
    </message>
    <message>
        <location filename="SendParcel.qml" line="1471"/>
        <source>Ketik No. Pengembalian barang</source>
        <translation>Type CN Return Parcel</translation>
    </message>
    <message>
        <location filename="SendParcel.qml" line="194"/>
        <source>TEKAN DI SINI</source>
        <translation>Press here!</translation>
    </message>
    <message>
        <location filename="SendParcel.qml" line="131"/>
        <source>Lebih gampang scan no. resi kamu, &lt;b&gt;&lt;u&gt;lihat caranya di sini&lt;/u&gt;&lt;/b&gt;</source>
        <translation>Scan Barcode directly, &lt;b&gt;&lt;u&gt;click here for demo&lt;/u&gt;&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="SendParcel.qml" line="987"/>
        <source>Lebih Gampang dengan Scan &lt;br&gt;No. Resi</source>
        <translation>To save time, just scan &lt;br&gt;your barcode.</translation>
    </message>
    <message>
        <location filename="SendParcel.qml" line="988"/>
        <source>Cukup arahkan barcode no. resi pada barcode &lt;br&gt;scanner yang berada di bawah layar bagian &lt;br&gt;tengah. Jaga jarak antara barcode dengan &lt;br&gt;barcode scanner agar dapat dibaca.</source>
        <translation>Present the barcode on the barcode scanner &lt;br&gt;below the screen. Please allow some 
        distance &lt;br&gt;between scanner and barcode for scanner &lt;br&gt;to detect.</translation>
    </message>
    <message>
        <location filename="SendParcel.qml" line="1026"/>
        <source>Apa itu No. Resi ?</source>
        <translation>What is CN Barcode ?</translation>
    </message>
    <message>
        <location filename="SendParcel.qml" line="1034"/>
        <source>No. Resi adalah nomor pengiriman yang &lt;br&gt;diberikan oleh logistik sebagai tanda bukti &lt;br&gt;pengiriman. &lt;br&gt; &lt;br&gt;Istilah untuk beberapa layanan: &lt;br&gt;Pengembalian Barang: return number/ nomor &lt;br&gt;Pengembalian &lt;br&gt;PopSend/PopSafe: order number (dapat dicek &lt;br&gt;di aplikasi PopBox)</source>
        <translation>CN Barcode is a tracking number given by &lt;br&gt;the logistic company as a proof of delivery. &lt;br&gt;Service Terms:&lt;br&gt;Parcel return: return number &lt;br&gt;PopSend/PopSafe: order number (Please check PopBox Apps)</translation>
    </message>
    <message>
        <location filename="SendParcel.qml" line="853"/>
        <source>No. Resi Yang Kamu Masukkan &lt;br&gt;Terindikasi Sebagai Order PopSafe</source>
        <translation>Your tracking number is &lt;br&gt;indicated as PopSafe order</translation>
    </message>
    <message>
        <location filename="SendParcel.qml" line="854"/>
        <source>Popers, jika kamu kurir dan ingin kirim barang pilih &lt;br&gt;KURIR. Jika kamu sudah membuat order dari aplikasi &lt;br&gt;PopBox pilih LANJUT.</source>
        <translation>Popers, if you are courier and wants to send parcel please &lt;br&gt;choose COURIER. If you have created order from PopBox &lt;br&gt;app please choose CONTINUE.</translation>
    </message>
</context>

<context>
    <name>StoreParcel</name>
    <message>
        <location filename="StoreParcel.qml" line="103"/>
        <source>TITIP</source>
        <translation>PopSafe</translation>
    </message>
    <message>
        <location filename="StoreParcel.qml" line="112"/>
        <source>Ketik / Scan No. Resi</source>
        <translation>Scan barcode or Type CN</translation>
    </message>
    <message>
        <location filename="StoreParcel.qml" line="180"/>
        <source>LANJUT</source>
        <translation>NEXT</translation>
    </message>
    <message>
        <location filename="StoreParcel.qml" line="697"/>
        <source>MENGERTI</source>
        <translation>UNDERSTAND</translation>
    </message>
    <message>
        <location filename="StoreParcel.qml" line="492"/>
        <source>MASUKKAN ULANG</source>
        <translation>REINPUT</translation>
    </message>
    <message>
        <location filename="StoreParcel.qml" line="492"/>
        <source>HUBUNGI CS</source>
        <translation>CONTACT CS</translation>
    </message>
    <message>
        <location filename="StoreParcel.qml" line="492"/>
        <source>KEMBALI KE HOME</source>
        <translation>BACK TO HOME</translation>
    </message>
    <message>
        <location filename="StoreParcel.qml" line="214"/>
        <source>TEKAN DI SINI</source>
        <translation>Press here!</translation>
    </message>
    <message>
        <location filename="StoreParcel.qml" line="726"/>
        <source>Order Titip</source>
        <translation>Order PopSafe</translation>
    </message>
    <message>
        <location filename="StoreParcel.qml" line="727"/>
        <source>Kamu sudah punya resi TITIP/ PopSafe?</source>
        <translation>Do you have tracking number for &lt;br&gt;DEPOSIT/ PopSafe?</translation>
    </message>
    <message>
        <location filename="StoreParcel.qml" line="738"/>
        <source>Belum, buat sekarang</source>
        <translation>No, For Now</translation>
    </message>
    <message>
        <location filename="StoreParcel.qml" line="758"/>
        <source>Ya, sudah buat di aplikasi</source>
        <translation>Yes, On App</translation>
    </message>
    <message>
        <location filename="StoreParcel.qml" line="214"/>
        <source>Kamu Tidak Punya / Lupa No. Resi ?</source>
        <translation>Forgot /No CN barcode?</translation>
    </message>
    <message>
        <location filename="StoreParcel.qml" line="150"/>
        <source>Lebih gampang scan no. resi kamu, &lt;b&gt;&lt;u&gt;lihat caranya di sini&lt;/u&gt;&lt;/b&gt;</source>
        <translation>Scan Barcode directly, &lt;b&gt;&lt;u&gt;click here for demo&lt;/u&gt;&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="StoreParcel.qml" line="691"/>
        <source>Lebih Gampang dengan Scan &lt;br&gt;No. Resi</source>
        <translation>To save time, just scan &lt;br&gt;your barcode.</translation>
    </message>
    <message>
        <location filename="StoreParcel.qml" line="692"/>
        <source>Cukup arahkan barcode no. resi pada barcode &lt;br&gt;scanner yang berada di bawah layar bagian &lt;br&gt;tengah. Jaga jarak antara barcode dengan &lt;br&gt;barcode scanner agar dapat dibaca.</source>
        <translation>Present the barcode on the barcode scanner &lt;br&gt;below the screen. Please allow some 
        distance &lt;br&gt;between scanner and barcode for scanner &lt;br&gt;to detect.</translation>
    </message>
    <message>
        <location filename="StoreParcel.qml" line="668"/>
        <source>Apa itu No. Resi ?</source>
        <translation>What is CN Barcode ?</translation>
    </message>
    <message>
        <location filename="StoreParcel.qml" line="669"/>
        <source>No. Resi adalah nomor pengiriman yang &lt;br&gt;diberikan oleh logistik sebagai tanda bukti &lt;br&gt;pengiriman. &lt;br&gt; &lt;br&gt;Istilah untuk beberapa layanan: &lt;br&gt;Pengembalian Barang: return number/ nomor &lt;br&gt;Pengembalian &lt;br&gt;PopSend/PopSafe: order number (dapat dicek &lt;br&gt;di aplikasi PopBox)</source>
        <translation>CN Barcode is a tracking number given by &lt;br&gt;the logistic company as a proof of delivery. &lt;br&gt;Service Terms:&lt;br&gt;Parcel return: return number/ number &lt;br&gt;PopSend/PopSafe: order number (Please check PopBox Apps)</translation>
    </message>

    <message>
        <location filename="StoreParcel.qml" line="533"/>
        <source>Order Kamu Salah Lokasi Loker</source>
        <translation>Your order is not matched with location</translation>
    </message>
    <message>
        <location filename="StoreParcel.qml" line="534"/>
        <source>Hai Popers, order kamu tidak dapat diproses, karena &lt;br&gt;salah lokasi loker. Pastikan kamu telah berada di loker &lt;br&gt;yang sesuai.</source>
        <translation>Hi Popers, we are unable to process your order. &lt;br&gt;Please make sure that you are on correct locker location.</translation>
    </message>

    <message>
        <location filename="StoreParcel.qml" line="599"/>
        <source>Order Kamu Tidak Dikenali</source>
        <translation>Your order number is not recognized</translation>
    </message>
    <message>
        <location filename="StoreParcel.qml" line="600"/>
        <source>Hai Popers, order kamu tidak dikenali. Periksa kembali nomer &lt;br&gt;order kamu dan pastikan kamu telah memasukkan &lt;br&gt;dengan benar</source>
        <translation>Hi Popers, your order number is not recognized. Please check again &lt;br&gt;your order number and make sure it is correct</translation>
    </message>

</context>


<context>
    <name>TakeParcel</name>
    <message>
        <location filename="TakeParcel.qml" line="9"/>
        <source>AMBIL</source>
        <translation>Parcel Collection</translation>
    </message>
    <message>
        <location filename="TakeParcel.qml" line="130"/>
        <source>VERIFIKASI</source>
        <translation>CONFIRM</translation>
    </message>
    <message>
        <location filename="TakeParcel.qml" line="130"/>
        <source>BATAL</source>
        <translation>CANCEL</translation>
    </message>
    <message>
        <location filename="TakeParcel.qml" line="76"/>
        <source>Ketik / Scan Kode Ambil</source>
        <translation>Enter / Scan Pincode</translation>
    </message>
    <message>
        <location filename="TakeParcel.qml" line="602"/>
        <source>MASUKKAN ULANG</source>
        <translation>REINPUT</translation>
    </message>
    <message>
        <location filename="TakeParcel.qml" line="602"/>
        <source>LANJUTKAN</source>
        <translation>NEXT</translation>
    </message>
    <message>
        <location filename="TakeParcel.qml" line="624"/>
        <source>HUBUNGI CS</source>
        <translation>CONTACT CS</translation>
    </message>
    <message>
        <location filename="TakeParcel.qml" line="362"/>
        <source>KEMBALI KE HOME</source>
        <translation>BACK TO HOME</translation>
    </message>
    <message>
        <location filename="TakeParcel.qml" line="46"/>
        <source>Oops pintu gagal dibuka</source>
        <translation>Door is failed to open</translation>
    </message>
    <message>
        <location filename="TakeParcel.qml" line="46"/>
        <source>Mohon ulangi kembali proses</source>
        <translation>Please repeat the process</translation>
    </message>
    <message>
        <location filename="TakeParcel.qml" line="76"/>
        <source>PIN Yang Kamu Masukkan Salah</source>
        <translation>Wrong PIN</translation>
    </message>
    <message>
        <location filename="TakeParcel.qml" line="76"/>
        <source>PIN yang kamu masukkan salah, mohon &lt;br&gt;masukkan kembali PIN yang benar.</source>
        <translation>We are unable to recognize your PIN code, &lt;br&gt;please reinput your PIN and make sure it is correct.</translation>
    </message>

    <message>
        <location filename="TakeParcel.qml" line="76"/>
        <source>Paket Melebihi Waktu</source>
        <translation>Your Parcel is Overdue</translation>
    </message>
    <message>
        <location filename="TakeParcel.qml" line="76"/>
        <source>Paket sudah melebihi batas waktu &lt;br&gt;pengambilan. Untuk mengambil paket &lt;br&gt;kamu. Silakan melakukan pembayaran &lt;br&gt;atas perpanjangan waktu paket kamu.</source>
        <translation>Your parcel has exceeded time limit to &lt;br&gt;collect. To take your parcel &lt;br&gt;please make payment for extending &lt;br&gt;your parcel time limit.</translation>
    </message>
    <message>
        <location filename="TakeParcel.qml" line="76"/>
        <source>Paket kamu telah melebihi durasi simpan, &lt;br&gt;hubungi. Customer Service PopBox untuk &lt;br&gt;melakukan perpanjangan.</source>
        <translation>Your parcel has exceeded store duration in &lt;br&gt;our lockers. Please contact our Customer Service &lt;br&gt;to extend.</translation>
    </message>

    <message>
        <location filename="TakeParcel.qml" line="76"/>
        <source>Kode Ambil Berhasil Diverifikasi</source>
        <translation>PIN Code Is Succesfully Verified</translation>
    </message>
    <message>
        <location filename="TakeParcel.qml" line="76"/>
        <source>Kode ambil yang kamu masukkan berhasil diverifikasi, &lt;br&gt;mohon tunggu hingga pintu loker terbuka untuk &lt;br&gt;mengambil barang kamu.</source>
        <translation>You have succesfully inputted the PIN. &lt;br&gt;Please wait until the locker door is opened &lt;br&gt;to collect your parcel.</translation>
    </message>

    <message>
        <location filename="TakeParcel.qml" line="154"/>
        <source>Cek WhatsApp dari POPBOX-ASIA atau lihat di Aplikasi PopBox</source>
        <translation>Check your SMS or the PopBox App for the Pincode</translation>
    </message>
</context>

<context>
    <name>SelectPaymentPage</name>
    <message>
        <location filename="SelectPaymentPage.qml" line="43"/>
        <source>TITIP</source>
        <translation>PopSafe</translation>
    </message>
    <message>
        <location filename="SelectPaymentPage.qml" line="43"/>
        <source>AMBIL</source>
        <translation>Parcel Collection</translation>
    </message>
    <message>
        <location filename="SelectPaymentPage.qml" line="10"/>
        <source>YA</source>
        <translation>YES</translation>
    </message>
    <message>
        <location filename="SelectPaymentPage.qml" line="10"/>
        <source>Rp </source>
        <translation>RM </translation>
    </message>
    <message>
        <location filename="SelectPaymentPage.qml" line="10"/>
        <source>TIDAK</source>
        <translation>NO</translation>
    </message>
    <message>
        <location filename="SelectPaymentPage.qml" line="1123"/>
        <source>SUDAH BAYAR</source>
        <translation>ALREADY PAID</translation>
    </message>
    <message>
        <location filename="SelectPaymentPage.qml" line="808"/>
        <source>ULANGI PEMBAYARAN</source>
        <translation>REPAYMENT</translation>
    </message>
    <message>
        <location filename="SelectPaymentPage.qml" line="618"/>
        <source>SCAN QR ULANG</source>
        <translation>RESCANING QR</translation>
    </message>

    <message>
        <location filename="SelectPaymentPage.qml" line="808"/>
        <source>CEK PEMBAYARAN</source>
        <translation>PAYMENT CHECK</translation>
    </message>

    <message>
        <location filename="SelectPaymentPage.qml" line="799"/>
        <source>Pengecekan Pembayaran</source>
        <translation>Payment Checking</translation>
    </message>

    <message>
        <location filename="SelectPaymentPage.qml" line="799"/>
        <source>Mohon menunggu, saat ini loker sedang &lt;br&gt;melakukan pengecekan pembayaran kamu.</source>
        <translation>Please wait, the locker is currently &lt;br&gt;checking your payment.</translation>
    </message>

    <message>
        <location filename="SelectPaymentPage.qml" line="808"/>
        <source>Pembayaran Tidak Terverifikasi</source>
        <translation>Unverified Payment</translation>
    </message>

    <message>
        <location filename="SelectPaymentPage.qml" line="808"/>
        <source>Silakan tekan tombol CEK PEMBAYARAN untuk &lt;br&gt;mengecek kembali status pembayaran kamu. &lt;br&gt;Tekan tombol GANTI PEMBAYARAN jika ingin &lt;br&gt;melakukan pergantian metode pembayaran.</source>
        <translation>Please press the CHECK PAYMENT button to &lt;br&gt;check your payment status again. Press &lt;br&gt;the CHANGE PAYMENT button if you want &lt;br&gt;to change the payment method.</translation>
    </message>

    <message>
        <location filename="SelectPaymentPage.qml" line="808"/>
        <source>GANTI PEMBAYARAN</source>
        <translation>CHANGE PAYMENT</translation>
    </message>
    <message>
        <location filename="SelectPaymentPage.qml" line="180"/>
        <source>Silakan Cek Pembayaran</source>
        <translation>Please Check Payment</translation>
    </message>
    <message>
        <location filename="SelectPaymentPage.qml" line="180"/>
        <source>Mohon maaf terjadi kendala dalam transaksi &lt;br&gt;anda silahkan klik button check pembayaran &lt;br&gt;untuk mengetahui status pembayaran.</source>
        <translation>Sorry, there is a problem in your transaction, &lt;br&gt;please click the payment check button &lt;br&gt;to find out the payment status.</translation>
    </message>
    <message>
        <location filename="SelectPaymentPage.qml" line="180"/>
        <source>LANJUT</source>
        <translation>NEXT</translation>
    </message>
    <message>
        <location filename="SelectPaymentPage.qml" line="328"/>
        <source>BATAL</source>
        <translation>CANCEL</translation>
    </message>
    <message>
        <location filename="SelectPaymentPage.qml" line="866"/>
        <source>Pilih Metode Pembayaran</source>
        <translation>Choose Payment Method</translation>
    </message>
    <message>
        <location filename="SelectPaymentPage.qml" line="866"/>
        <source>Tempelkan Kartu Emoney Di Reader</source>
        <translation>Put your card on reader</translation>
    </message>
    <message>
        <location filename="SelectPaymentPage.qml" line="866"/>
        <source>Mohon maaf, untuk saat ini pintunya tidak dapat dibuka. &lt;br&gt;Terimakasih</source>
        <translation>Sorry, for now the door cannot be opened. &lt;br&gt;Thank you.</translation>
    </message>
    <message>
        <location filename="SelectPaymentPage.qml" line="866"/>
        <source>PILIH METODE PEMBAYARAN</source>
        <translation>CHOOSE PAYMENT METHOD</translation>
    </message>
    <message>
        <location filename="SelectPaymentPage.qml" line="866"/>
        <source>&lt;u&gt;Ubah No Handphone&lt;u&gt;</source>
        <translation>&lt;u&gt;Change Mobile Number&lt;u&gt;</translation>
    </message>
    <message>
        <location filename="SelectPaymentPage.qml" line="177"/>
        <source>No. Handphone</source>
        <translation>Mobile Number</translation>
    </message>
    <message>
        <location filename="SelectPaymentPage.qml" line="204"/>
        <source>Durasi   </source>
        <translation>Duration</translation>
    </message>
    <message>
        <location filename="SelectPaymentPage.qml" line="239"/>
        <source>Harga</source>
        <translation>Price</translation>
    </message>
    <message>
        <location filename="SelectPaymentPage.qml" line="239"/>
        <source> Jam</source>
        <translation> Hours</translation>
    </message>
    <message>
        <location filename="SelectPaymentPage.qml" line="249"/>
        <source>Metode Pembayaran</source>
        <translation>Payment Method</translation>
    </message>

    <message>
        <location filename="SelectPaymentPage.qml" line="1134"/>
        <source>No Order</source>
        <translation>Order No</translation>
    </message>
    <message>
        <location filename="SelectPaymentPage.qml" line="1143"/>
        <source>Jumlah</source>
        <translation>Total     </translation>
    </message>
    <message>
        <location filename="SelectPaymentPage.qml" line="1152"/>
        <source>Bayar     </source>
        <translation>Payment     </translation>
    </message>

    <message>
        <location filename="SelectPaymentPage.qml" line="1123"/>
        <source>Bayar Dengan Scan QR</source>
        <translation>Scan QR code to Pay</translation>
    </message>
    <message>
        <location filename="SelectPaymentPage.qml" line="1123"/>
        <source>Ayo Segera Scan QR-nya!</source>
        <translation>Please scan the QR Code</translation>
    </message>
    <message>
        <location filename="SelectPaymentPage.qml" line="397"/>
        <source>Pastikan nomor yang dimasukkan aktif, notifikasi akan dikirimkan ke nomor tersebut.</source>
        <translation>Please ensure the entered mobile number is active, &lt;br&gt;a verification code will be sent to this number.</translation>
    </message>
    <message>
        <location filename="SelectPaymentPage.qml" line="706"/>
        <source>Selamat Pembayaran Kamu Berhasil</source>
        <translation>Congratulations, your payment is successful</translation>
    </message>
    <message>
        <location filename="SelectPaymentPage.qml" line="707"/>
        <source>Mohon maaf, transaksi kamu tidak dapat &lt;br&gt;dibatalkan, karena status pembayaran sudah &lt;br&gt;berhasil.</source>
        <translation>Sorry, your transaction cannot be &lt;br&gt;cancelled because transaction has been &lt;br&gt;successful</translation>
    </message>

    <message>
        <location filename="SelectPaymentPage.qml" line="799"/>
        <source>Pembayaran Kamu Gagal</source>
        <translation>Unsuccessful payment attempt</translation>
    </message>
    <message>
        <location filename="SelectPaymentPage.qml" line="800"/>
        <source>Mohon maaf terjadi kendala dalam transaksi anda &lt;br&gt;silakan ulangi pembayaran kamu, jika kamu sudah &lt;br&gt;membayar harap hubungi Customer Service kami</source>
        <translation>Sorry, your payment attempt might have failed due to &lt;br&gt;technical difficulty or connection error. Kindly reattempt &lt;br&gt;payment to proceed. If your payment has been successful, &lt;br&gt;please contact us for assistance. Thank you.</translation>
    </message>
    <message>
        <location filename="SelectPaymentPage.qml" line="1073"/>
        <source>Apakah Kamu Ingin &lt;br&gt;Membatalkan Proses?</source>
        <translation>Do you want to &lt;br&gt;cancel the process?</translation>
    </message>
    <message>
        <location filename="SelectPaymentPage.qml" line="1074"/>
        <source>Jika kamu membatalkan proses, maka akan &lt;br&gt;diarahkan ke halaman awal.</source>
        <translation>If you cancel the process, you will be &lt;br&gt;redirected to home page</translation>
    </message>

    <message>
        <location filename="SelectPaymentPage.qml" line="581"/>
        <source>Cek Notifikasi di HP Kamu</source>
        <translation>Check notifications on your phone</translation>
    </message>
    <message>
        <location filename="SelectPaymentPage.qml" line="582"/>
        <source>Silakan cek aplikasi OVO di HP kamu dan &lt;br&gt;konfirmasi pembayaran untuk melanjutkan &lt;br&gt;proses</source>
        <translation>Please check on your OVO app to &lt;br&gt;continue the process</translation>
    </message>

</context>

<context>
    <name>SurveyPage</name>
    <message>
        <location filename="SurveyPage.qml" line="162"/>
        <source>Tidak Suka</source>
        <translation>Dislike</translation>
    </message>
    <message>
        <location filename="SurveyPage.qml" line="180"/>
        <source>Biasa Saja</source>
        <translation>Neutral</translation>
    </message>
    <message>
        <location filename="SurveyPage.qml" line="197"/>
        <source>Senang</source>
        <translation>Happy</translation>
    </message>
    <message>
        <location filename="SurveyPage.qml" line="211"/>
        <source>LANJUT</source>
        <translation>NEXT</translation>
    </message>
</context>

<context>
    <name>OpenDoorPage</name>
    <message>
        <location filename="OpenDoorPage.qml" line="16"/>
        <source>BUKA PINTU</source>
        <translation>OPEN DOOR</translation>
    </message>
    <message>
        <location filename="OpenDoorPage.qml" line="16"/>
        <source>KIRIM LAGI</source>
        <translation>SEND AGAIN</translation>
    </message>
    <message>
        <location filename="OpenDoorPage.qml" line="139"/>
        <source>Pintu Loker Telah Terbuka</source>
        <translation>Locker Door Has Opened</translation>
    </message>
    <message>
        <location filename="OpenDoorPage.qml" line="16"/>
        <source>Pastikan kamu menutup pintu loker setelah selesai</source>
        <translation>Make sure you close the locker doors when you&apos;re done</translation>
    </message>
    <message>
        <location filename="OpenDoorPage.qml" line="16"/>
        <source>&lt;b&gt;BUKA KEMBALI&lt;/b&gt;</source>
        <translation>&lt;b&gt;RE-OPEN LOCKER&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="OpenDoorPage.qml" line="16"/>
        <source> Kali</source>
        <translation> x</translation>
    </message>
    <message>
        <location filename="OpenDoorPage.qml" line="16"/>
        <source>Tekan &lt;b&gt;AMBIL LAGI&lt;/b&gt; untuk kembali ke halaman ambil/simpan paket.</source>
        <translation>Press &lt;b&gt;TAKE AGAIN&lt;/b&gt; to parcel collection page</translation>
    </message>
    <message>
        <location filename="OpenDoorPage.qml" line="16"/>
        <source>Tekan &lt;b&gt;BUKA KEMBALI&lt;/b&gt; untuk membuka kembali loker.</source>
        <translation>Press &lt;b&gt;RE-OPEN LOCKER&lt;/b&gt; to reopen chosen compartment</translation>
    </message>
    <message>
        <location filename="OpenDoorPage.qml" line="16"/>
        <source>Tekan &lt;b&gt;SELESAI&lt;/b&gt; untuk menyelesaikan proses.</source>
        <translation>Press &lt;b&gt;COMPLETE&lt;/b&gt; after successful depositing of parcel</translation>
    </message>
    <message>
        <location filename="OpenDoorPage.qml" line="16"/>
        <source>Tekan &lt;b&gt;GANTI LOKER&lt;/b&gt; untuk mengubah ukuran loker.</source>
        <translation>Press &lt;b&gt;CHANGE LOCKER&lt;/b&gt; to change compartment </translation>
    </message>
    <message>
        <location filename="OpenDoorPage.qml" line="16"/>
        <source>BATAL</source>
        <translation>CANCEL</translation>
    </message>
    <message>
        <location filename="OpenDoorPage.qml" line="16"/>
        <source>GANTI LOKER</source>
        <translation>CHANGE LOCKER</translation>
    </message>
    <message>
        <location filename="OpenDoorPage.qml" line="16"/>
        <source>BUKA KEMBALI</source>
        <translation>RE-OPEN LOCKER</translation>
    </message>
    <message>
        <location filename="OpenDoorPage.qml" line="16"/>
        <source>SELESAI</source>
        <translation>DONE</translation>
    </message>
    <message>
        <location filename="OpenDoorPage.qml" line="16"/>
        <source>AMBIL LAGI</source>
        <translation>TAKE AGAIN</translation>
    </message>
    <message>
        <location filename="OpenDoorPage.qml" line="16"/>
        <source>Terima Kasih Telah Menggunakan PopBox</source>
        <translation>Thank you for using PopBox service</translation>
    </message>
    <message>
        <location filename="OpenDoorPage.qml" line="16"/>
        <source>Bantu kami untuk terus memberikan layanan terbaik, pilih tingkat &lt;br&gt;kepuasan kamu</source>
        <translation>Help us to improve our service quality by filling in this &lt;br&gt;survey form</translation>
    </message>
    <message>
        <location filename="OpenDoorPage.qml" line="422"/>
        <source>Tidak Suka</source>
        <translation>Dislike</translation>
    </message>
    <message>
        <location filename="OpenDoorPage.qml" line="432"/>
        <source>Biasa Saja</source>
        <translation>Neutral</translation>
    </message>
    <message>
        <location filename="OpenDoorPage.qml" line="442"/>
        <source>Senang</source>
        <translation>Happy</translation>
    </message>
    <message>
        <location filename="OpenDoorPage.qml" line="16"/>
        <source>Terima Kasih Telah Menggunakan &lt;br&gt;Layanan PopBox</source>
        <translation>Thank you for using PopBox service</translation>
    </message>
    <message>
        <location filename="OpenDoorPage.qml" line="16"/>
        <source>WhatsApp notifikasi telah dikirimkan &lt;br&gt;ke no. pelanggan.</source>
        <translation>SMS/WhatsApp has been sent to customer&apos;s &lt;br&gt;mobile number</translation>
    </message>
    <message>
        <location filename="OpenDoorPage.qml" line="442"/>
        <source>SIMPAN PAKET LAGI</source>
        <translation>SAFEKEEP AGAIN</translation>
    </message>
    <message>
        <location filename="OpenDoorPage.qml" line="442"/>
        <source>Barang Telah Kami Terima</source>
        <translation>We have received your item</translation>
    </message>
    <message>
        <location filename="OpenDoorPage.qml" line="442"/>
        <source>Hai Poppers, pengembalian barang kamu telah &lt;br&gt;kami terima dan akan kami kirimkan ke &lt;br&gt;warehouse toko online tempat kamu belanja"</source>
        <translation>Hi Popers, we have received your item(s) and we will send it to the online store that you shopped frm</translation>
    </message>
    <message>
        <location filename="OpenDoorPage.qml" line="442"/>
        <source>Oops pintu gagal dibuka</source>
        <translation>Door is failed to open</translation>
    </message>
    <message>
        <location filename="OpenDoorPage.qml" line="442"/>
        <source>Mohon ulangi kembali proses</source>
        <translation>Please repeat the process</translation>
    </message>
</context>

<context>
    <name>CabinetPage</name>
    <message>
        <location filename="CabinetPage.qml" line="46"/>
        <source>Terlambat</source>
        <translation>Overdue</translation>
    </message>
    <message>
        <location filename="CabinetPage.qml" line="46"/>
        <source>Gagal</source>
        <translation>Failed</translation>
    </message>
    <message>
        <location filename="CabinetPage.qml" line="46"/>
        <source>Disable</source>
        <translation>Disable</translation>
    </message>
    <message>
        <location filename="CabinetPage.qml" line="46"/>
        <source>Kosong</source>
        <translation>Empty</translation>
    </message>
    <message>
        <location filename="CabinetPage.qml" line="46"/>
        <source>Penuh</source>
        <translation>Full</translation>
    </message>

    <message>
        <location filename="CabinetPage.qml" line="46"/>
        <source>GAGAL SYNC</source>
        <translation>SYNC FAILED</translation>
    </message>
    <message>
        <location filename="CabinetPage.qml" line="46"/>
        <source>HUBUNGI TEAM IT SEBELUM SYNC LOKER</source>
        <translation>PLEASE CONTACT IT TEAM BEFORE SYNC LOCKER</translation>
    </message>
    <message>
        <location filename="CabinetPage.qml" line="46"/>
        <source>CATATAN</source>
        <translation>NOTE    </translation>
    </message>
    <message>
        <location filename="CabinetPage.qml" line="46"/>
        <source>STATUS</source>
        <translation>STATUS</translation>
    </message>
    <message>
        <location filename="CabinetPage.qml" line="46"/>
        <source>SYNC LOKER</source>
        <translation>SYNC LOCKER</translation>
    </message>
    <message>
        <location filename="CabinetPage.qml" line="46"/>
        <source> Jam</source>
        <translation> Hours</translation>
    </message>
    <message>
        <location filename="CabinetPage.qml" line="46"/>
        <source> Menit</source>
        <translation> Minutes</translation>
    </message>
    <message>
        <location filename="CabinetPage.qml" line="46"/>
        <source> Detik</source>
        <translation> Seconds</translation>
    </message>
    <message>
        <location filename="CabinetPage.qml" line="46"/>
        <source>Buka Semua</source>
        <translation>Open All</translation>
    </message>
    <message>
        <location filename="CabinetPage.qml" line="46"/>
        <source>Buka Kosong</source>
        <translation>Open Empty</translation>
    </message>
    <message>
        <location filename="CabinetPage.qml" line="46"/>
        <source>BUKA LOKER</source>
        <translation>OPEN LOCKER</translation>
    </message>
    <message>
        <location filename="CabinetPage.qml" line="46"/>
        <source>SIMPAN</source>
        <translation>SAVE</translation>
    </message>
    <message>
        <location filename="CabinetPage.qml" line="46"/>
        <source>Nomor Resi                       </source>
        <translation>Tracking Number</translation>
    </message>
    <message>
        <location filename="CabinetPage.qml" line="46"/>
        <source>Nomor Handphone         </source>
        <translation>Mobile Number</translation>
    </message>
    <message>
        <location filename="CabinetPage.qml" line="46"/>
        <source>Durasi Simpan </source>
        <translation>Duration</translation>
    </message>
    <message>
        <location filename="CabinetPage.qml" line="46"/>
        <source>Waktu Simpan  </source>
        <translation>Store Time</translation>
    </message>
    <message>
        <location filename="CabinetPage.qml" line="46"/>
        <source>Waktu Terlambat</source>
        <translation>Overdue Time</translation>
    </message>
    <message>
        <location filename="CabinetPage.qml" line="46"/>
        <source>AMBIL BARANG</source>
        <translation>TAKE PARCEL</translation>
    </message>
    <message>
        <location filename="CabinetPage.qml" line="46"/>
        <source>LANJUT</source>
        <translation>NEXT</translation>
    </message>
    <message>
        <location filename="CabinetPage.qml" line="46"/>
        <source>BATAL</source>
        <translation>CANCEL</translation>
    </message>
    <message>
        <location filename="CabinetPage.qml" line="46"/>
        <source>Proses Ini akan Menyelesaikan &lt;br&gt;Order Kamu</source>
        <translation>This process will finish &lt;br&gt;your order</translation>
    </message>
    <message>
        <location filename="CabinetPage.qml" line="46"/>
        <source>Pastikan barang tidak tertinggal didalam loker</source>
        <translation>Please ensure you don&apos;t leave anything inside the locker</translation>
    </message>
    <message>
        <location filename="CabinetPage.qml" line="46"/>
        <source>Apakah Kamu Ingin Membuka &lt;br&gt;Semua Loker yang Kosong ?</source>
        <translation>Do you want to open all &lt;br&gt;available lockers ?</translation>
    </message>
    <message>
        <location filename="CabinetPage.qml" line="46"/>
        <source>Apakah Kamu Ingin Membuka &lt;br&gt;Semua Loker ?</source>
        <translation>Do you want to open &lt;br&gt;all lockers ?</translation>
    </message>
    <message>
        <location filename="CabinetPage.qml" line="46"/>
        <source>Pilihan ini untuk membuka semua loker yang kosong &lt;br&gt;Data login akan tercatat dalam sistem</source>
        <translation>This option is for opening all available lockers and &lt;br&gt;login data will be written on our system</translation>
    </message>
    <message>
        <location filename="CabinetPage.qml" line="46"/>
        <source>Pilihan ini untuk membuka semua loker &lt;br&gt;Data login akan tercatat dalam sistem</source>
        <translation>This option is for opening all lockers &lt;br&gt;and login data will be written on our system</translation>
    </message>
    <message>
        <location filename="CabinetPage.qml" line="46"/>
        <source>Cek Isi Loker Atau Tambahkan &lt;br&gt;Barang di Loker</source>
        <translation>Check locker compartment or &lt;br&gt;add items on locker</translation>
    </message>
    <message>
        <location filename="CabinetPage.qml" line="46"/>
        <source>Pilihan ini hanya untuk mengecek isi loker atau &lt;br&gt;menambah barang di dalam loker</source>
        <translation>This option is only for checking compartment availability &lt;br&gt;or drop items inside locker</translation>
    </message>
    <message>
        <location filename="CabinetPage.qml" line="46"/>
        <source>Proses Gagal</source>
        <translation>Failed</translation>
    </message>
    <message>
        <location filename="CabinetPage.qml" line="46"/>
        <source>Proses Gagal , Silakan coba kembali.</source>
        <translation>Process was failed, please retry.</translation>
    </message>
    <message>
        <location filename="CabinetPage.qml" line="46"/>
        <source>Proses Berhasil</source>
        <translation>Success</translation>
    </message>
    <message>
        <location filename="CabinetPage.qml" line="46"/>
        <source>Selamat Proses Berhasil</source>
        <translation>Congratulations, process was succesful</translation>
    </message>
    <message>
        <location filename="CabinetPage.qml" line="46"/>
        <source>TUTUP</source>
        <translation>CLOSE</translation>
    </message>
</context>

</TS>