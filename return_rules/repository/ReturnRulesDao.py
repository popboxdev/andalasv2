from database import ClientDatabase
__author__ = 'popbox@popbox.asia'


def get_data_rules(merchant_name):
    sql = 'SELECT * FROM ReturnRules WHERE groupName = :merchant_name'
    return ClientDatabase.get_result_set(sql, merchant_name)[0]

def delete_record_return_rules():
    sql = 'DELETE FROM ReturnRules'
    return ClientDatabase.delete_record(sql)

def insert_return_rule(params):
    sql = 'INSERT INTO ReturnRules (id,regularContent,groupName,operator_id,logistic_id, ecommerce_id, deleteFlag)VALUES (:id,:regularContent,:groupName,:operator_id,:logistic_id,:ecommerce_id,:deleteFlag)'
    ClientDatabase.insert_or_update_database(sql, params)

